// Lean compiler output
// Module: MatchingLogic.AppContext
// Imports: Init MatchingLogic.Pattern MatchingLogic.Substitution MatchingLogic.Positivity
#include <lean/lean.h>
#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wunused-label"
#elif defined(__GNUC__) && !defined(__CLANG__)
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-label"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#endif
#ifdef __cplusplus
extern "C" {
#endif
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__12;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext_substEvar(lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__substEvar__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__17;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__1;
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__1;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__4;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__1;
lean_object* lean_mk_empty_array_with_capacity(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__9;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__28;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__3;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__3;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__4;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__3;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__1;
LEAN_EXPORT lean_object* l_ML_AppContext_isFreeEvar(lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext_term___u2b1d_x3e__;
static lean_object* l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__4;
static lean_object* l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__8;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__15;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__6;
LEAN_EXPORT lean_object* l_ML_AppContext_evars___rarg(lean_object*);
static lean_object* l_ML_AppContext_term___x3c_u2b1d_____closed__3;
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__8;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__right__1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT uint8_t l_ML_AppContext_isFreeEvar___rarg(lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__8;
static lean_object* l_ML_AppContext_term_u25a1___closed__3;
LEAN_EXPORT lean_object* l_ML_AppContext_toList(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__19;
LEAN_EXPORT uint8_t l_ML_AppContext_isEvar___rarg(lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__1;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__substEvar__1___lambda__1___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Syntax_getArgs(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__4;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__6;
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__12;
LEAN_EXPORT lean_object* l_ML_AppContext_map(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__33;
lean_object* l_Lean_replaceRef(lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__2;
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__9;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__1(lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Pattern_substEvar___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext_term___x5b___x5d;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__7;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__22;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__left__1___lambda__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__2;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__5;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__13;
uint8_t l_Lean_Syntax_isOfKind(lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__9;
LEAN_EXPORT lean_object* l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c;
LEAN_EXPORT lean_object* l_ML_AppContext_isEvar(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__3;
static lean_object* l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__10;
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__11;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__4;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__7;
LEAN_EXPORT lean_object* l_ML_AppContext_isEvar___rarg___boxed(lean_object*, lean_object*);
lean_object* l_Lean_Syntax_getNumArgs(lean_object*);
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__2;
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0____private_MatchingLogic_AppContext_0__ML_reprAppContext_match__1_splitter____x40_MatchingLogic_AppContext___hyg_542_(lean_object*, lean_object*);
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__1;
static lean_object* l_ML_AppContext_term_u25a1___closed__4;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__20;
lean_object* l_Lean_Name_mkStr3(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext_isFreeEvar___rarg___boxed(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_instDecidableEqAppContext(lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext_insert___rarg___boxed(lean_object*, lean_object*);
lean_object* l_List_appendTR___rarg(lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__1;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__27;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__5;
static lean_object* l_ML_AppContext_term_u25a1___closed__5;
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0____private_MatchingLogic_AppContext_0__ML_reprAppContext_match__1_splitter____x40_MatchingLogic_AppContext___hyg_542____rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__6;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__11;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__7;
LEAN_EXPORT lean_object* l_ML_AppContext_insert(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__24;
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__3;
lean_object* l_Lean_SourceInfo_fromRef(lean_object*, uint8_t);
static lean_object* l_ML_AppContext_term___x3c_u2b1d_____closed__2;
lean_object* lean_nat_to_int(lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext_toList___rarg___boxed(lean_object*);
lean_object* l_Lean_Syntax_node6(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__18;
LEAN_EXPORT lean_object* l_ML_AppContext_term___x3c_u2b1d__;
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__4;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1(lean_object*, lean_object*, lean_object*);
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__13;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__5;
LEAN_EXPORT lean_object* l_ML_instDecidableEqAppContext___rarg___boxed(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext_term_u25a1;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__7;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__right__1___lambda__1___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
uint8_t l_ML_Pattern_isEvar___rarg(lean_object*, lean_object*);
static lean_object* l_ML_AppContext_term___u2b1d_x3e_____closed__5;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__12;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__14;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__4;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__substEvar__1___lambda__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__30;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__6;
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___boxed(lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Syntax_node3(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__5;
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542_(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__6;
static lean_object* l_ML_AppContext_term___x3c_u2b1d_____closed__6;
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__14;
LEAN_EXPORT lean_object* l_ML_AppContext_insert___rarg(lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__5;
static lean_object* l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__1;
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0____private_MatchingLogic_AppContext_0__ML_reprAppContext_match__1_splitter____x40_MatchingLogic_AppContext___hyg_542____rarg(lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_addMacroScope(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__13;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__14;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__right__1___lambda__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext_toList___rarg(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__14;
lean_object* l_Lean_Name_str___override(lean_object*, lean_object*);
static lean_object* l_ML_AppContext_term_u25a1___closed__2;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__8;
lean_object* l_ML_Pattern_evars___rarg(lean_object*);
lean_object* l_Lean_Syntax_node2(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__11;
lean_object* l_Lean_Syntax_getArg(lean_object*, lean_object*);
uint8_t l_Lean_Syntax_matchesNull(lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__7;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__6;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__3;
LEAN_EXPORT lean_object* l_ML_AppContext_substEvar___rarg___boxed(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__9;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__3;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__empty__1___boxed(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__left__1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext_insertLeftAtBox(lean_object*);
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__16;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__3;
lean_object* l_Array_append___rarg(lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__4;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5;
LEAN_EXPORT uint8_t l_ML_instDecidableEqAppContext___rarg(lean_object*, lean_object*, lean_object*);
lean_object* l_Array_extract___rarg(lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Syntax_node4(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__25;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__1___boxed(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext_evars(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__23;
static lean_object* l_ML_AppContext_term___x3c_u2b1d_____closed__1;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__2;
static lean_object* l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__2;
lean_object* l___private_MatchingLogic_Pattern_0__ML_reprPattern____x40_MatchingLogic_Pattern___hyg_2464____rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__31;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__9;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__11;
static lean_object* l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__9;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__8;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext_term___u2b1d_x3e_____closed__6;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__15;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__10;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__12;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__13;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__left__1___lambda__1___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__2;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__6;
lean_object* l_Lean_Name_mkStr2(lean_object*, lean_object*);
static lean_object* l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__3;
static lean_object* l_ML_AppContext_term___u2b1d_x3e_____closed__3;
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__6;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__7;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_instReprAppContext___rarg(lean_object*);
static lean_object* l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__7;
lean_object* l_Lean_Syntax_node1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__8;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__7;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__21;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__2;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__1;
lean_object* l_Repr_addAppParen(lean_object*, lean_object*);
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__5;
static lean_object* l_ML_AppContext_term___u2b1d_x3e_____closed__4;
lean_object* l___private_MatchingLogic_Pattern_0__ML_decEqPattern____x40_MatchingLogic_Pattern___hyg_144____rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__2;
lean_object* lean_nat_sub(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext_evars___rarg___boxed(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2___closed__1;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__2;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__3;
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__10;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__9;
uint8_t l_ML_Pattern_isFreeEvar___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__empty__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext_term___u2b1d_x3e_____closed__2;
static lean_object* l_ML_AppContext_term___x3c_u2b1d_____closed__5;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__6;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__10;
static lean_object* l_ML_AppContext_term_u25a1___closed__1;
LEAN_EXPORT lean_object* l_ML_AppContext_substEvar___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__8;
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_AppContext_map___rarg(lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__26;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__29;
lean_object* l_Lean_Name_mkStr4(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4;
LEAN_EXPORT lean_object* l_ML_instReprAppContext(lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0__ML_decEqAppContext____x40_MatchingLogic_AppContext___hyg_71____rarg___boxed(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__32;
uint8_t lean_nat_dec_le(lean_object*, lean_object*);
static lean_object* l_ML_AppContext_term___x3c_u2b1d_____closed__4;
static lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__7;
LEAN_EXPORT lean_object* l_ML_instInhabitedAppContext(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__8;
lean_object* l_String_toSubstring_x27(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__5;
LEAN_EXPORT lean_object* l_ML_AppContext_insertLeftAtBox___rarg(lean_object*, lean_object*);
LEAN_EXPORT uint8_t l___private_MatchingLogic_AppContext_0__ML_decEqAppContext____x40_MatchingLogic_AppContext___hyg_71____rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0__ML_decEqAppContext____x40_MatchingLogic_AppContext___hyg_71_(lean_object*);
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__10;
static lean_object* l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__5;
static lean_object* l_ML_AppContext_term___x5b___x5d___closed__8;
static lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__16;
static lean_object* l_ML_AppContext_term___u2b1d_x3e_____closed__1;
LEAN_EXPORT lean_object* l_ML_instInhabitedAppContext(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_box(0);
return x_2;
}
}
LEAN_EXPORT uint8_t l___private_MatchingLogic_AppContext_0__ML_decEqAppContext____x40_MatchingLogic_AppContext___hyg_71____rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
switch (lean_obj_tag(x_2)) {
case 0:
{
lean_dec(x_1);
if (lean_obj_tag(x_3) == 0)
{
uint8_t x_4; 
x_4 = 1;
return x_4;
}
else
{
uint8_t x_5; 
lean_dec(x_3);
x_5 = 0;
return x_5;
}
}
case 1:
{
if (lean_obj_tag(x_3) == 1)
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_6 = lean_ctor_get(x_2, 0);
lean_inc(x_6);
x_7 = lean_ctor_get(x_2, 1);
lean_inc(x_7);
lean_dec(x_2);
x_8 = lean_ctor_get(x_3, 0);
lean_inc(x_8);
x_9 = lean_ctor_get(x_3, 1);
lean_inc(x_9);
lean_dec(x_3);
lean_inc(x_1);
x_10 = l___private_MatchingLogic_Pattern_0__ML_decEqPattern____x40_MatchingLogic_Pattern___hyg_144____rarg(x_1, x_6, x_8);
x_11 = lean_unbox(x_10);
lean_dec(x_10);
if (x_11 == 0)
{
uint8_t x_12; 
lean_dec(x_9);
lean_dec(x_7);
lean_dec(x_1);
x_12 = 0;
return x_12;
}
else
{
x_2 = x_7;
x_3 = x_9;
goto _start;
}
}
else
{
uint8_t x_14; 
lean_dec(x_3);
lean_dec(x_2);
lean_dec(x_1);
x_14 = 0;
return x_14;
}
}
default: 
{
if (lean_obj_tag(x_3) == 2)
{
lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; uint8_t x_20; 
x_15 = lean_ctor_get(x_2, 0);
lean_inc(x_15);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
lean_dec(x_2);
x_17 = lean_ctor_get(x_3, 0);
lean_inc(x_17);
x_18 = lean_ctor_get(x_3, 1);
lean_inc(x_18);
lean_dec(x_3);
lean_inc(x_1);
x_19 = l___private_MatchingLogic_Pattern_0__ML_decEqPattern____x40_MatchingLogic_Pattern___hyg_144____rarg(x_1, x_15, x_17);
x_20 = lean_unbox(x_19);
lean_dec(x_19);
if (x_20 == 0)
{
uint8_t x_21; 
lean_dec(x_18);
lean_dec(x_16);
lean_dec(x_1);
x_21 = 0;
return x_21;
}
else
{
x_2 = x_16;
x_3 = x_18;
goto _start;
}
}
else
{
uint8_t x_23; 
lean_dec(x_3);
lean_dec(x_2);
lean_dec(x_1);
x_23 = 0;
return x_23;
}
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0__ML_decEqAppContext____x40_MatchingLogic_AppContext___hyg_71_(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l___private_MatchingLogic_AppContext_0__ML_decEqAppContext____x40_MatchingLogic_AppContext___hyg_71____rarg___boxed), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0__ML_decEqAppContext____x40_MatchingLogic_AppContext___hyg_71____rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
uint8_t x_4; lean_object* x_5; 
x_4 = l___private_MatchingLogic_AppContext_0__ML_decEqAppContext____x40_MatchingLogic_AppContext___hyg_71____rarg(x_1, x_2, x_3);
x_5 = lean_box(x_4);
return x_5;
}
}
LEAN_EXPORT uint8_t l_ML_instDecidableEqAppContext___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
uint8_t x_4; 
x_4 = l___private_MatchingLogic_AppContext_0__ML_decEqAppContext____x40_MatchingLogic_AppContext___hyg_71____rarg(x_1, x_2, x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_instDecidableEqAppContext(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_instDecidableEqAppContext___rarg___boxed), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_instDecidableEqAppContext___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
uint8_t x_4; lean_object* x_5; 
x_4 = l_ML_instDecidableEqAppContext___rarg(x_1, x_2, x_3);
x_5 = lean_box(x_4);
return x_5;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML.AppContext.empty", 19);
return x_1;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__1;
x_2 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_unsigned_to_nat(2u);
x_2 = lean_nat_to_int(x_1);
return x_2;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__3;
x_2 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__2;
x_3 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__5() {
_start:
{
lean_object* x_1; uint8_t x_2; lean_object* x_3; 
x_1 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__4;
x_2 = 0;
x_3 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set_uint8(x_3, sizeof(void*)*1, x_2);
return x_3;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_unsigned_to_nat(1u);
x_2 = lean_nat_to_int(x_1);
return x_2;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__6;
x_2 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__2;
x_3 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__8() {
_start:
{
lean_object* x_1; uint8_t x_2; lean_object* x_3; 
x_1 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__7;
x_2 = 0;
x_3 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set_uint8(x_3, sizeof(void*)*1, x_2);
return x_3;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__9() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML.AppContext.left", 18);
return x_1;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__9;
x_2 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__10;
x_2 = lean_box(1);
x_3 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__12() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML.AppContext.right", 19);
return x_1;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__12;
x_2 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__14() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__13;
x_2 = lean_box(1);
x_3 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
switch (lean_obj_tag(x_2)) {
case 0:
{
lean_object* x_4; uint8_t x_5; 
lean_dec(x_1);
x_4 = lean_unsigned_to_nat(1024u);
x_5 = lean_nat_dec_le(x_4, x_3);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
x_6 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__5;
x_7 = l_Repr_addAppParen(x_6, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; 
x_8 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__8;
x_9 = l_Repr_addAppParen(x_8, x_3);
return x_9;
}
}
case 1:
{
lean_object* x_10; lean_object* x_11; lean_object* x_12; uint8_t x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; 
x_10 = lean_ctor_get(x_2, 0);
lean_inc(x_10);
x_11 = lean_ctor_get(x_2, 1);
lean_inc(x_11);
lean_dec(x_2);
x_12 = lean_unsigned_to_nat(1024u);
x_13 = lean_nat_dec_le(x_12, x_3);
lean_inc(x_1);
x_14 = l___private_MatchingLogic_Pattern_0__ML_reprPattern____x40_MatchingLogic_Pattern___hyg_2464____rarg(x_1, x_10, x_12);
x_15 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__11;
x_16 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_16, 0, x_15);
lean_ctor_set(x_16, 1, x_14);
x_17 = lean_box(1);
x_18 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_18, 0, x_16);
lean_ctor_set(x_18, 1, x_17);
x_19 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg(x_1, x_11, x_12);
x_20 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_20, 0, x_18);
lean_ctor_set(x_20, 1, x_19);
if (x_13 == 0)
{
lean_object* x_21; lean_object* x_22; uint8_t x_23; lean_object* x_24; lean_object* x_25; 
x_21 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__3;
x_22 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_22, 0, x_21);
lean_ctor_set(x_22, 1, x_20);
x_23 = 0;
x_24 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_24, 0, x_22);
lean_ctor_set_uint8(x_24, sizeof(void*)*1, x_23);
x_25 = l_Repr_addAppParen(x_24, x_3);
return x_25;
}
else
{
lean_object* x_26; lean_object* x_27; uint8_t x_28; lean_object* x_29; lean_object* x_30; 
x_26 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__6;
x_27 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_27, 0, x_26);
lean_ctor_set(x_27, 1, x_20);
x_28 = 0;
x_29 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_29, 0, x_27);
lean_ctor_set_uint8(x_29, sizeof(void*)*1, x_28);
x_30 = l_Repr_addAppParen(x_29, x_3);
return x_30;
}
}
default: 
{
lean_object* x_31; lean_object* x_32; lean_object* x_33; uint8_t x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; 
x_31 = lean_ctor_get(x_2, 0);
lean_inc(x_31);
x_32 = lean_ctor_get(x_2, 1);
lean_inc(x_32);
lean_dec(x_2);
x_33 = lean_unsigned_to_nat(1024u);
x_34 = lean_nat_dec_le(x_33, x_3);
lean_inc(x_1);
x_35 = l___private_MatchingLogic_Pattern_0__ML_reprPattern____x40_MatchingLogic_Pattern___hyg_2464____rarg(x_1, x_31, x_33);
x_36 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__14;
x_37 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_37, 0, x_36);
lean_ctor_set(x_37, 1, x_35);
x_38 = lean_box(1);
x_39 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_39, 0, x_37);
lean_ctor_set(x_39, 1, x_38);
x_40 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg(x_1, x_32, x_33);
x_41 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_41, 0, x_39);
lean_ctor_set(x_41, 1, x_40);
if (x_34 == 0)
{
lean_object* x_42; lean_object* x_43; uint8_t x_44; lean_object* x_45; lean_object* x_46; 
x_42 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__3;
x_43 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_43, 0, x_42);
lean_ctor_set(x_43, 1, x_41);
x_44 = 0;
x_45 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_45, 0, x_43);
lean_ctor_set_uint8(x_45, sizeof(void*)*1, x_44);
x_46 = l_Repr_addAppParen(x_45, x_3);
return x_46;
}
else
{
lean_object* x_47; lean_object* x_48; uint8_t x_49; lean_object* x_50; lean_object* x_51; 
x_47 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__6;
x_48 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_48, 0, x_47);
lean_ctor_set(x_48, 1, x_41);
x_49 = 0;
x_50 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_50, 0, x_48);
lean_ctor_set_uint8(x_50, sizeof(void*)*1, x_49);
x_51 = l_Repr_addAppParen(x_50, x_3);
return x_51;
}
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542_(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___boxed), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg(x_1, x_2, x_3);
lean_dec(x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_instReprAppContext___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___boxed), 3, 1);
lean_closure_set(x_2, 0, x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_instReprAppContext(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_instReprAppContext___rarg), 1, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_insert___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
switch (lean_obj_tag(x_2)) {
case 0:
{
lean_inc(x_1);
return x_1;
}
case 1:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; 
x_3 = lean_ctor_get(x_2, 0);
x_4 = lean_ctor_get(x_2, 1);
x_5 = l_ML_AppContext_insert___rarg(x_1, x_4);
lean_inc(x_3);
x_6 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_6, 0, x_3);
lean_ctor_set(x_6, 1, x_5);
return x_6;
}
default: 
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; 
x_7 = lean_ctor_get(x_2, 0);
x_8 = lean_ctor_get(x_2, 1);
x_9 = l_ML_AppContext_insert___rarg(x_1, x_8);
lean_inc(x_7);
x_10 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_10, 0, x_9);
lean_ctor_set(x_10, 1, x_7);
return x_10;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_insert(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_AppContext_insert___rarg___boxed), 2, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_insert___rarg___boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = l_ML_AppContext_insert___rarg(x_1, x_2);
lean_dec(x_2);
lean_dec(x_1);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0____private_MatchingLogic_AppContext_0__ML_reprAppContext_match__1_splitter____x40_MatchingLogic_AppContext___hyg_542____rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 0:
{
lean_dec(x_4);
lean_dec(x_3);
lean_inc(x_2);
return x_2;
}
case 1:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; 
lean_dec(x_4);
x_5 = lean_ctor_get(x_1, 0);
lean_inc(x_5);
x_6 = lean_ctor_get(x_1, 1);
lean_inc(x_6);
lean_dec(x_1);
x_7 = lean_apply_2(x_3, x_5, x_6);
return x_7;
}
default: 
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; 
lean_dec(x_3);
x_8 = lean_ctor_get(x_1, 0);
lean_inc(x_8);
x_9 = lean_ctor_get(x_1, 1);
lean_inc(x_9);
lean_dec(x_1);
x_10 = lean_apply_2(x_4, x_8, x_9);
return x_10;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0____private_MatchingLogic_AppContext_0__ML_reprAppContext_match__1_splitter____x40_MatchingLogic_AppContext___hyg_542_(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_AppContext_0____private_MatchingLogic_AppContext_0__ML_reprAppContext_match__1_splitter____x40_MatchingLogic_AppContext___hyg_542____rarg___boxed), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_AppContext_0____private_MatchingLogic_AppContext_0__ML_reprAppContext_match__1_splitter____x40_MatchingLogic_AppContext___hyg_542____rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = l___private_MatchingLogic_AppContext_0____private_MatchingLogic_AppContext_0__ML_reprAppContext_match__1_splitter____x40_MatchingLogic_AppContext___hyg_542____rarg(x_1, x_2, x_3, x_4);
lean_dec(x_2);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML", 2);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("AppContext", 10);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_[_]", 8);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__1;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_3 = l_ML_AppContext_term___x5b___x5d___closed__3;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("andthen", 7);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext_term___x5b___x5d___closed__5;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__7() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("[", 1);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__7;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__9() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term", 4);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext_term___x5b___x5d___closed__9;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__10;
x_2 = lean_unsigned_to_nat(0u);
x_3 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__12() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__6;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__8;
x_3 = l_ML_AppContext_term___x5b___x5d___closed__11;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__13() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("]", 1);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__14() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__13;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__6;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__12;
x_3 = l_ML_AppContext_term___x5b___x5d___closed__14;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d___closed__16() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__4;
x_2 = lean_unsigned_to_nat(1022u);
x_3 = lean_unsigned_to_nat(0u);
x_4 = l_ML_AppContext_term___x5b___x5d___closed__15;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___x5d() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__16;
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Lean", 4);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Parser", 6);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Term", 4);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("app", 3);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__3;
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__4;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("insert", 6);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__6;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__6;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__1;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__6;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__9;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__9;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__12() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__11;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__10;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__12;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__14() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("null", 4);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__14;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_AppContext_term___x5b___x5d___closed__4;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; uint8_t x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
lean_dec(x_1);
x_12 = lean_ctor_get(x_2, 5);
lean_inc(x_12);
x_13 = 0;
x_14 = l_Lean_SourceInfo_fromRef(x_12, x_13);
x_15 = lean_ctor_get(x_2, 2);
lean_inc(x_15);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
lean_dec(x_2);
x_17 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__8;
x_18 = l_Lean_addMacroScope(x_16, x_17, x_15);
x_19 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__7;
x_20 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__13;
lean_inc(x_14);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_14);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
lean_inc(x_14);
x_23 = l_Lean_Syntax_node2(x_14, x_22, x_11, x_9);
x_24 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5;
x_25 = l_Lean_Syntax_node2(x_14, x_24, x_21, x_23);
x_26 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_26, 0, x_25);
lean_ctor_set(x_26, 1, x_3);
return x_26;
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; 
x_4 = lean_box(0);
x_5 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_5, 0, x_4);
lean_ctor_set(x_5, 1, x_3);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_unsigned_to_nat(0u);
x_2 = lean_mk_empty_array_with_capacity(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; 
x_8 = l_Lean_replaceRef(x_1, x_6);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = l_ML_AppContext_term___x5b___x5d___closed__7;
lean_inc(x_10);
x_12 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_11);
x_13 = l_ML_AppContext_term___x5b___x5d___closed__13;
lean_inc(x_10);
x_14 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_13);
x_15 = l_ML_AppContext_term___x5b___x5d___closed__4;
lean_inc(x_10);
x_16 = l_Lean_Syntax_node4(x_10, x_15, x_4, x_12, x_3, x_14);
x_17 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2___closed__1;
x_18 = l_Array_append___rarg(x_17, x_5);
x_19 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
lean_inc(x_10);
x_20 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_20, 0, x_10);
lean_ctor_set(x_20, 1, x_19);
lean_ctor_set(x_20, 2, x_18);
x_21 = l_Lean_Syntax_node2(x_10, x_2, x_16, x_20);
x_22 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_22, 0, x_21);
lean_ctor_set(x_22, 1, x_7);
return x_22;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ident", 5);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_alloc_closure((void*)(l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__1___boxed), 3, 0);
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; uint8_t x_18; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__3;
x_17 = lean_unsigned_to_nat(2u);
lean_inc(x_15);
x_18 = l_Lean_Syntax_matchesNull(x_15, x_17);
if (x_18 == 0)
{
lean_object* x_19; uint8_t x_20; 
x_19 = l_Lean_Syntax_getNumArgs(x_15);
x_20 = lean_nat_dec_le(x_17, x_19);
if (x_20 == 0)
{
lean_object* x_21; lean_object* x_22; 
lean_dec(x_19);
lean_dec(x_15);
lean_dec(x_9);
x_21 = lean_box(0);
x_22 = lean_apply_3(x_16, x_21, x_2, x_3);
return x_22;
}
else
{
lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; 
x_23 = l_Lean_Syntax_getArg(x_15, x_8);
x_24 = l_Lean_Syntax_getArg(x_15, x_14);
x_25 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_26 = lean_nat_sub(x_19, x_8);
lean_dec(x_19);
x_27 = l_Array_extract___rarg(x_25, x_17, x_26);
lean_dec(x_26);
lean_dec(x_25);
x_28 = lean_box(2);
x_29 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
x_30 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_30, 0, x_28);
lean_ctor_set(x_30, 1, x_29);
lean_ctor_set(x_30, 2, x_27);
x_31 = l_Lean_Syntax_getArgs(x_30);
lean_dec(x_30);
x_32 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2(x_9, x_4, x_23, x_24, x_31, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_32;
}
}
else
{
lean_object* x_33; lean_object* x_34; lean_object* x_35; uint8_t x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; 
x_33 = l_Lean_Syntax_getArg(x_15, x_8);
x_34 = l_Lean_Syntax_getArg(x_15, x_14);
lean_dec(x_15);
x_35 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_36 = 0;
x_37 = l_Lean_SourceInfo_fromRef(x_35, x_36);
x_38 = l_ML_AppContext_term___x5b___x5d___closed__7;
lean_inc(x_37);
x_39 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_39, 0, x_37);
lean_ctor_set(x_39, 1, x_38);
x_40 = l_ML_AppContext_term___x5b___x5d___closed__13;
lean_inc(x_37);
x_41 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_41, 0, x_37);
lean_ctor_set(x_41, 1, x_40);
x_42 = l_ML_AppContext_term___x5b___x5d___closed__4;
x_43 = l_Lean_Syntax_node4(x_37, x_42, x_34, x_39, x_33, x_41);
x_44 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_44, 0, x_43);
lean_ctor_set(x_44, 1, x_3);
return x_44;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__1(x_1, x_2, x_3);
lean_dec(x_2);
lean_dec(x_1);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; 
x_8 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2(x_1, x_2, x_3, x_4, x_5, x_6, x_7);
lean_dec(x_6);
lean_dec(x_1);
return x_8;
}
}
static lean_object* _init_l_ML_AppContext_term_u25a1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term□", 7);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term_u25a1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__1;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_3 = l_ML_AppContext_term_u25a1___closed__1;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term_u25a1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("□", 3);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term_u25a1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext_term_u25a1___closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext_term_u25a1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term_u25a1___closed__2;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = l_ML_AppContext_term_u25a1___closed__4;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term_u25a1() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_AppContext_term_u25a1___closed__5;
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("empty", 5);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__1;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__1;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__4;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__4;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__6;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__5;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_AppContext_term_u25a1___closed__2;
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; 
x_8 = lean_ctor_get(x_2, 5);
lean_inc(x_8);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = lean_ctor_get(x_2, 2);
lean_inc(x_11);
x_12 = lean_ctor_get(x_2, 1);
lean_inc(x_12);
lean_dec(x_2);
x_13 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__3;
x_14 = l_Lean_addMacroScope(x_12, x_13, x_11);
x_15 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__2;
x_16 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__8;
x_17 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_17, 0, x_10);
lean_ctor_set(x_17, 1, x_15);
lean_ctor_set(x_17, 2, x_14);
lean_ctor_set(x_17, 3, x_16);
x_18 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_18, 0, x_17);
lean_ctor_set(x_18, 1, x_3);
return x_18;
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__empty__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; uint8_t x_7; 
x_6 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5;
lean_inc(x_1);
x_7 = l_Lean_Syntax_isOfKind(x_1, x_6);
if (x_7 == 0)
{
lean_object* x_8; lean_object* x_9; 
lean_dec(x_1);
x_8 = lean_box(0);
x_9 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_9, 0, x_8);
lean_ctor_set(x_9, 1, x_3);
return x_9;
}
else
{
lean_object* x_10; lean_object* x_11; uint8_t x_12; 
x_10 = lean_unsigned_to_nat(0u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
lean_inc(x_11);
x_12 = l_Lean_Syntax_isOfKind(x_11, x_4);
if (x_12 == 0)
{
lean_object* x_13; lean_object* x_14; 
lean_dec(x_11);
lean_dec(x_1);
x_13 = lean_box(0);
x_14 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_14, 0, x_13);
lean_ctor_set(x_14, 1, x_3);
return x_14;
}
else
{
lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; uint8_t x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; 
x_15 = lean_unsigned_to_nat(1u);
x_16 = l_Lean_Syntax_getArg(x_1, x_15);
lean_dec(x_1);
x_17 = l_Lean_Syntax_getArgs(x_16);
lean_dec(x_16);
x_18 = l_Lean_replaceRef(x_11, x_2);
lean_dec(x_11);
x_19 = 0;
x_20 = l_Lean_SourceInfo_fromRef(x_18, x_19);
x_21 = l_ML_AppContext_term_u25a1___closed__3;
lean_inc(x_20);
x_22 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_22, 0, x_20);
lean_ctor_set(x_22, 1, x_21);
x_23 = l_ML_AppContext_term_u25a1___closed__2;
lean_inc(x_20);
x_24 = l_Lean_Syntax_node1(x_20, x_23, x_22);
x_25 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2___closed__1;
x_26 = l_Array_append___rarg(x_25, x_17);
x_27 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
lean_inc(x_20);
x_28 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_28, 0, x_20);
lean_ctor_set(x_28, 1, x_27);
lean_ctor_set(x_28, 2, x_26);
x_29 = l_Lean_Syntax_node2(x_20, x_6, x_24, x_28);
x_30 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_30, 0, x_29);
lean_ctor_set(x_30, 1, x_3);
return x_30;
}
}
}
else
{
lean_object* x_31; uint8_t x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; 
x_31 = l_Lean_replaceRef(x_1, x_2);
lean_dec(x_1);
x_32 = 0;
x_33 = l_Lean_SourceInfo_fromRef(x_31, x_32);
x_34 = l_ML_AppContext_term_u25a1___closed__3;
lean_inc(x_33);
x_35 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_35, 0, x_33);
lean_ctor_set(x_35, 1, x_34);
x_36 = l_ML_AppContext_term_u25a1___closed__2;
x_37 = l_Lean_Syntax_node1(x_33, x_36, x_35);
x_38 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_38, 0, x_37);
lean_ctor_set(x_38, 1, x_3);
return x_38;
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__empty__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__empty__1(x_1, x_2, x_3);
lean_dec(x_2);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___x3c_u2b1d_____closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_<⬝_", 10);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___x3c_u2b1d_____closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__1;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_3 = l_ML_AppContext_term___x3c_u2b1d_____closed__1;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___x3c_u2b1d_____closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("<⬝", 4);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___x3c_u2b1d_____closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext_term___x3c_u2b1d_____closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext_term___x3c_u2b1d_____closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__6;
x_2 = l_ML_AppContext_term___x3c_u2b1d_____closed__4;
x_3 = l_ML_AppContext_term___x5b___x5d___closed__11;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___x3c_u2b1d_____closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext_term___x3c_u2b1d_____closed__2;
x_2 = lean_unsigned_to_nat(1022u);
x_3 = lean_unsigned_to_nat(0u);
x_4 = l_ML_AppContext_term___x3c_u2b1d_____closed__5;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext_term___x3c_u2b1d__() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_AppContext_term___x3c_u2b1d_____closed__6;
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("AppContext.right", 16);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("right", 5);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__3;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__1;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__3;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__5;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__5;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__6;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__8;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_AppContext_term___x3c_u2b1d_____closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; uint8_t x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
lean_dec(x_1);
x_12 = lean_ctor_get(x_2, 5);
lean_inc(x_12);
x_13 = 0;
x_14 = l_Lean_SourceInfo_fromRef(x_12, x_13);
x_15 = lean_ctor_get(x_2, 2);
lean_inc(x_15);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
lean_dec(x_2);
x_17 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__4;
x_18 = l_Lean_addMacroScope(x_16, x_17, x_15);
x_19 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__2;
x_20 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__9;
lean_inc(x_14);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_14);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
lean_inc(x_14);
x_23 = l_Lean_Syntax_node2(x_14, x_22, x_11, x_9);
x_24 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5;
x_25 = l_Lean_Syntax_node2(x_14, x_24, x_21, x_23);
x_26 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_26, 0, x_25);
lean_ctor_set(x_26, 1, x_3);
return x_26;
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__right__1___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; 
x_8 = l_Lean_replaceRef(x_1, x_6);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = l_ML_AppContext_term___x3c_u2b1d_____closed__3;
lean_inc(x_10);
x_12 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_11);
x_13 = l_ML_AppContext_term___x3c_u2b1d_____closed__2;
lean_inc(x_10);
x_14 = l_Lean_Syntax_node3(x_10, x_13, x_4, x_12, x_3);
x_15 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2___closed__1;
x_16 = l_Array_append___rarg(x_15, x_5);
x_17 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
lean_inc(x_10);
x_18 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_18, 0, x_10);
lean_ctor_set(x_18, 1, x_17);
lean_ctor_set(x_18, 2, x_16);
x_19 = l_Lean_Syntax_node2(x_10, x_2, x_14, x_18);
x_20 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_20, 0, x_19);
lean_ctor_set(x_20, 1, x_7);
return x_20;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__right__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = lean_unsigned_to_nat(2u);
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; uint8_t x_19; 
x_18 = l_Lean_Syntax_getNumArgs(x_15);
x_19 = lean_nat_dec_le(x_16, x_18);
if (x_19 == 0)
{
lean_object* x_20; lean_object* x_21; 
lean_dec(x_18);
lean_dec(x_15);
lean_dec(x_9);
lean_dec(x_2);
x_20 = lean_box(0);
x_21 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_21, 0, x_20);
lean_ctor_set(x_21, 1, x_3);
return x_21;
}
else
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; 
x_22 = l_Lean_Syntax_getArg(x_15, x_8);
x_23 = l_Lean_Syntax_getArg(x_15, x_14);
x_24 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_25 = lean_nat_sub(x_18, x_8);
lean_dec(x_18);
x_26 = l_Array_extract___rarg(x_24, x_16, x_25);
lean_dec(x_25);
lean_dec(x_24);
x_27 = lean_box(2);
x_28 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
x_29 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_29, 0, x_27);
lean_ctor_set(x_29, 1, x_28);
lean_ctor_set(x_29, 2, x_26);
x_30 = l_Lean_Syntax_getArgs(x_29);
lean_dec(x_29);
x_31 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__right__1___lambda__1(x_9, x_4, x_22, x_23, x_30, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_31;
}
}
else
{
lean_object* x_32; lean_object* x_33; lean_object* x_34; uint8_t x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; 
x_32 = l_Lean_Syntax_getArg(x_15, x_8);
x_33 = l_Lean_Syntax_getArg(x_15, x_14);
lean_dec(x_15);
x_34 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_35 = 0;
x_36 = l_Lean_SourceInfo_fromRef(x_34, x_35);
x_37 = l_ML_AppContext_term___x3c_u2b1d_____closed__3;
lean_inc(x_36);
x_38 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_38, 0, x_36);
lean_ctor_set(x_38, 1, x_37);
x_39 = l_ML_AppContext_term___x3c_u2b1d_____closed__2;
x_40 = l_Lean_Syntax_node3(x_36, x_39, x_33, x_38, x_32);
x_41 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_41, 0, x_40);
lean_ctor_set(x_41, 1, x_3);
return x_41;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__right__1___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; 
x_8 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__right__1___lambda__1(x_1, x_2, x_3, x_4, x_5, x_6, x_7);
lean_dec(x_6);
lean_dec(x_1);
return x_8;
}
}
static lean_object* _init_l_ML_AppContext_term___u2b1d_x3e_____closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_⬝>_", 10);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___u2b1d_x3e_____closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__1;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_3 = l_ML_AppContext_term___u2b1d_x3e_____closed__1;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___u2b1d_x3e_____closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("⬝>", 4);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___u2b1d_x3e_____closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext_term___u2b1d_x3e_____closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext_term___u2b1d_x3e_____closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__6;
x_2 = l_ML_AppContext_term___u2b1d_x3e_____closed__4;
x_3 = l_ML_AppContext_term___x5b___x5d___closed__11;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___u2b1d_x3e_____closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext_term___u2b1d_x3e_____closed__2;
x_2 = lean_unsigned_to_nat(1022u);
x_3 = lean_unsigned_to_nat(0u);
x_4 = l_ML_AppContext_term___u2b1d_x3e_____closed__5;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext_term___u2b1d_x3e__() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_AppContext_term___u2b1d_x3e_____closed__6;
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("AppContext.left", 15);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("left", 4);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__3;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__1;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__3;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__5;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__5;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__6;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__8;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_AppContext_term___u2b1d_x3e_____closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; uint8_t x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
lean_dec(x_1);
x_12 = lean_ctor_get(x_2, 5);
lean_inc(x_12);
x_13 = 0;
x_14 = l_Lean_SourceInfo_fromRef(x_12, x_13);
x_15 = lean_ctor_get(x_2, 2);
lean_inc(x_15);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
lean_dec(x_2);
x_17 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__4;
x_18 = l_Lean_addMacroScope(x_16, x_17, x_15);
x_19 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__2;
x_20 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__9;
lean_inc(x_14);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_14);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
lean_inc(x_14);
x_23 = l_Lean_Syntax_node2(x_14, x_22, x_9, x_11);
x_24 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5;
x_25 = l_Lean_Syntax_node2(x_14, x_24, x_21, x_23);
x_26 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_26, 0, x_25);
lean_ctor_set(x_26, 1, x_3);
return x_26;
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__left__1___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; 
x_8 = l_Lean_replaceRef(x_1, x_6);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = l_ML_AppContext_term___u2b1d_x3e_____closed__3;
lean_inc(x_10);
x_12 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_11);
x_13 = l_ML_AppContext_term___u2b1d_x3e_____closed__2;
lean_inc(x_10);
x_14 = l_Lean_Syntax_node3(x_10, x_13, x_3, x_12, x_4);
x_15 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2___closed__1;
x_16 = l_Array_append___rarg(x_15, x_5);
x_17 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
lean_inc(x_10);
x_18 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_18, 0, x_10);
lean_ctor_set(x_18, 1, x_17);
lean_ctor_set(x_18, 2, x_16);
x_19 = l_Lean_Syntax_node2(x_10, x_2, x_14, x_18);
x_20 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_20, 0, x_19);
lean_ctor_set(x_20, 1, x_7);
return x_20;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__left__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = lean_unsigned_to_nat(2u);
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; uint8_t x_19; 
x_18 = l_Lean_Syntax_getNumArgs(x_15);
x_19 = lean_nat_dec_le(x_16, x_18);
if (x_19 == 0)
{
lean_object* x_20; lean_object* x_21; 
lean_dec(x_18);
lean_dec(x_15);
lean_dec(x_9);
lean_dec(x_2);
x_20 = lean_box(0);
x_21 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_21, 0, x_20);
lean_ctor_set(x_21, 1, x_3);
return x_21;
}
else
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; 
x_22 = l_Lean_Syntax_getArg(x_15, x_8);
x_23 = l_Lean_Syntax_getArg(x_15, x_14);
x_24 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_25 = lean_nat_sub(x_18, x_8);
lean_dec(x_18);
x_26 = l_Array_extract___rarg(x_24, x_16, x_25);
lean_dec(x_25);
lean_dec(x_24);
x_27 = lean_box(2);
x_28 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
x_29 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_29, 0, x_27);
lean_ctor_set(x_29, 1, x_28);
lean_ctor_set(x_29, 2, x_26);
x_30 = l_Lean_Syntax_getArgs(x_29);
lean_dec(x_29);
x_31 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__left__1___lambda__1(x_9, x_4, x_22, x_23, x_30, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_31;
}
}
else
{
lean_object* x_32; lean_object* x_33; lean_object* x_34; uint8_t x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; 
x_32 = l_Lean_Syntax_getArg(x_15, x_8);
x_33 = l_Lean_Syntax_getArg(x_15, x_14);
lean_dec(x_15);
x_34 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_35 = 0;
x_36 = l_Lean_SourceInfo_fromRef(x_34, x_35);
x_37 = l_ML_AppContext_term___u2b1d_x3e_____closed__3;
lean_inc(x_36);
x_38 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_38, 0, x_36);
lean_ctor_set(x_38, 1, x_37);
x_39 = l_ML_AppContext_term___u2b1d_x3e_____closed__2;
x_40 = l_Lean_Syntax_node3(x_36, x_39, x_32, x_38, x_33);
x_41 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_41, 0, x_40);
lean_ctor_set(x_41, 1, x_3);
return x_41;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__left__1___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; 
x_8 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__left__1___lambda__1(x_1, x_2, x_3, x_4, x_5, x_6, x_7);
lean_dec(x_6);
lean_dec(x_1);
return x_8;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_insertLeftAtBox___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
switch (lean_obj_tag(x_2)) {
case 0:
{
lean_object* x_3; lean_object* x_4; 
x_3 = lean_box(0);
x_4 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_3);
return x_4;
}
case 1:
{
uint8_t x_5; 
x_5 = !lean_is_exclusive(x_2);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
x_6 = lean_ctor_get(x_2, 1);
x_7 = l_ML_AppContext_insertLeftAtBox___rarg(x_1, x_6);
lean_ctor_set(x_2, 1, x_7);
return x_2;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; 
x_8 = lean_ctor_get(x_2, 0);
x_9 = lean_ctor_get(x_2, 1);
lean_inc(x_9);
lean_inc(x_8);
lean_dec(x_2);
x_10 = l_ML_AppContext_insertLeftAtBox___rarg(x_1, x_9);
x_11 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_11, 0, x_8);
lean_ctor_set(x_11, 1, x_10);
return x_11;
}
}
default: 
{
uint8_t x_12; 
x_12 = !lean_is_exclusive(x_2);
if (x_12 == 0)
{
lean_object* x_13; lean_object* x_14; 
x_13 = lean_ctor_get(x_2, 1);
x_14 = l_ML_AppContext_insertLeftAtBox___rarg(x_1, x_13);
lean_ctor_set(x_2, 1, x_14);
return x_2;
}
else
{
lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; 
x_15 = lean_ctor_get(x_2, 0);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
lean_inc(x_15);
lean_dec(x_2);
x_17 = l_ML_AppContext_insertLeftAtBox___rarg(x_1, x_16);
x_18 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_18, 0, x_15);
lean_ctor_set(x_18, 1, x_17);
return x_18;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_insertLeftAtBox(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_AppContext_insertLeftAtBox___rarg), 2, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_map___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
switch (lean_obj_tag(x_2)) {
case 0:
{
lean_object* x_3; 
lean_dec(x_1);
x_3 = lean_box(0);
return x_3;
}
case 1:
{
uint8_t x_4; 
x_4 = !lean_is_exclusive(x_2);
if (x_4 == 0)
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; 
x_5 = lean_ctor_get(x_2, 0);
x_6 = lean_ctor_get(x_2, 1);
lean_inc(x_1);
x_7 = lean_apply_1(x_1, x_5);
x_8 = l_ML_AppContext_map___rarg(x_1, x_6);
lean_ctor_set(x_2, 1, x_8);
lean_ctor_set(x_2, 0, x_7);
return x_2;
}
else
{
lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; 
x_9 = lean_ctor_get(x_2, 0);
x_10 = lean_ctor_get(x_2, 1);
lean_inc(x_10);
lean_inc(x_9);
lean_dec(x_2);
lean_inc(x_1);
x_11 = lean_apply_1(x_1, x_9);
x_12 = l_ML_AppContext_map___rarg(x_1, x_10);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_11);
lean_ctor_set(x_13, 1, x_12);
return x_13;
}
}
default: 
{
uint8_t x_14; 
x_14 = !lean_is_exclusive(x_2);
if (x_14 == 0)
{
lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; 
x_15 = lean_ctor_get(x_2, 0);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_1);
x_17 = lean_apply_1(x_1, x_15);
x_18 = l_ML_AppContext_map___rarg(x_1, x_16);
lean_ctor_set(x_2, 1, x_18);
lean_ctor_set(x_2, 0, x_17);
return x_2;
}
else
{
lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; 
x_19 = lean_ctor_get(x_2, 0);
x_20 = lean_ctor_get(x_2, 1);
lean_inc(x_20);
lean_inc(x_19);
lean_dec(x_2);
lean_inc(x_1);
x_21 = lean_apply_1(x_1, x_19);
x_22 = l_ML_AppContext_map___rarg(x_1, x_20);
x_23 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_23, 0, x_21);
lean_ctor_set(x_23, 1, x_22);
return x_23;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_map(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_AppContext_map___rarg), 2, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_toList___rarg(lean_object* x_1) {
_start:
{
if (lean_obj_tag(x_1) == 0)
{
lean_object* x_2; 
x_2 = lean_box(0);
return x_2;
}
else
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; 
x_3 = lean_ctor_get(x_1, 0);
x_4 = lean_ctor_get(x_1, 1);
x_5 = l_ML_AppContext_toList___rarg(x_4);
lean_inc(x_3);
x_6 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_6, 0, x_3);
lean_ctor_set(x_6, 1, x_5);
return x_6;
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_toList(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_AppContext_toList___rarg___boxed), 1, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_toList___rarg___boxed(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = l_ML_AppContext_toList___rarg(x_1);
lean_dec(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_evars___rarg(lean_object* x_1) {
_start:
{
if (lean_obj_tag(x_1) == 0)
{
lean_object* x_2; 
x_2 = lean_box(0);
return x_2;
}
else
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; 
x_3 = lean_ctor_get(x_1, 0);
x_4 = lean_ctor_get(x_1, 1);
x_5 = l_ML_Pattern_evars___rarg(x_3);
x_6 = l_ML_AppContext_evars___rarg(x_4);
x_7 = l_List_appendTR___rarg(x_5, x_6);
return x_7;
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_evars(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_AppContext_evars___rarg___boxed), 1, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_evars___rarg___boxed(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = l_ML_AppContext_evars___rarg(x_1);
lean_dec(x_1);
return x_2;
}
}
LEAN_EXPORT uint8_t l_ML_AppContext_isEvar___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
if (lean_obj_tag(x_1) == 0)
{
uint8_t x_3; 
x_3 = 0;
return x_3;
}
else
{
lean_object* x_4; lean_object* x_5; uint8_t x_6; 
x_4 = lean_ctor_get(x_1, 0);
x_5 = lean_ctor_get(x_1, 1);
x_6 = l_ML_Pattern_isEvar___rarg(x_4, x_2);
if (x_6 == 0)
{
x_1 = x_5;
goto _start;
}
else
{
uint8_t x_8; 
x_8 = 1;
return x_8;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_isEvar(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_AppContext_isEvar___rarg___boxed), 2, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_isEvar___rarg___boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; lean_object* x_4; 
x_3 = l_ML_AppContext_isEvar___rarg(x_1, x_2);
lean_dec(x_2);
lean_dec(x_1);
x_4 = lean_box(x_3);
return x_4;
}
}
LEAN_EXPORT uint8_t l_ML_AppContext_isFreeEvar___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
if (lean_obj_tag(x_1) == 0)
{
uint8_t x_3; 
x_3 = 0;
return x_3;
}
else
{
lean_object* x_4; lean_object* x_5; uint8_t x_6; 
x_4 = lean_ctor_get(x_1, 0);
x_5 = lean_ctor_get(x_1, 1);
x_6 = l_ML_Pattern_isFreeEvar___rarg(x_4, x_2);
if (x_6 == 0)
{
x_1 = x_5;
goto _start;
}
else
{
uint8_t x_8; 
x_8 = 1;
return x_8;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_isFreeEvar(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_AppContext_isFreeEvar___rarg___boxed), 2, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_isFreeEvar___rarg___boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; lean_object* x_4; 
x_3 = l_ML_AppContext_isFreeEvar___rarg(x_1, x_2);
lean_dec(x_2);
lean_dec(x_1);
x_4 = lean_box(x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_substEvar___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 0:
{
lean_object* x_4; 
x_4 = lean_box(0);
return x_4;
}
case 1:
{
uint8_t x_5; 
x_5 = !lean_is_exclusive(x_1);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; 
x_6 = lean_ctor_get(x_1, 0);
x_7 = lean_ctor_get(x_1, 1);
x_8 = l_ML_Pattern_substEvar___rarg(x_6, x_2, x_3);
x_9 = l_ML_AppContext_substEvar___rarg(x_7, x_2, x_3);
lean_ctor_set(x_1, 1, x_9);
lean_ctor_set(x_1, 0, x_8);
return x_1;
}
else
{
lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; 
x_10 = lean_ctor_get(x_1, 0);
x_11 = lean_ctor_get(x_1, 1);
lean_inc(x_11);
lean_inc(x_10);
lean_dec(x_1);
x_12 = l_ML_Pattern_substEvar___rarg(x_10, x_2, x_3);
x_13 = l_ML_AppContext_substEvar___rarg(x_11, x_2, x_3);
x_14 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_14, 0, x_12);
lean_ctor_set(x_14, 1, x_13);
return x_14;
}
}
default: 
{
uint8_t x_15; 
x_15 = !lean_is_exclusive(x_1);
if (x_15 == 0)
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; 
x_16 = lean_ctor_get(x_1, 0);
x_17 = lean_ctor_get(x_1, 1);
x_18 = l_ML_Pattern_substEvar___rarg(x_16, x_2, x_3);
x_19 = l_ML_AppContext_substEvar___rarg(x_17, x_2, x_3);
lean_ctor_set(x_1, 1, x_19);
lean_ctor_set(x_1, 0, x_18);
return x_1;
}
else
{
lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; 
x_20 = lean_ctor_get(x_1, 0);
x_21 = lean_ctor_get(x_1, 1);
lean_inc(x_21);
lean_inc(x_20);
lean_dec(x_1);
x_22 = l_ML_Pattern_substEvar___rarg(x_20, x_2, x_3);
x_23 = l_ML_AppContext_substEvar___rarg(x_21, x_2, x_3);
x_24 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_24, 0, x_22);
lean_ctor_set(x_24, 1, x_23);
return x_24;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_substEvar(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_AppContext_substEvar___rarg___boxed), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_substEvar___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML_AppContext_substEvar___rarg(x_1, x_2, x_3);
lean_dec(x_3);
lean_dec(x_2);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_[_⇐ᵉ_]ᶜ", 18);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__1;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_3 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__1;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("⇐ᵉ", 6);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__6;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__12;
x_3 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__4;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__6;
x_2 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__5;
x_3 = l_ML_AppContext_term___x5b___x5d___closed__11;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__7() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("]ᶜ", 4);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__7;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__6;
x_2 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__6;
x_3 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__8;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__2;
x_2 = lean_unsigned_to_nat(1022u);
x_3 = lean_unsigned_to_nat(0u);
x_4 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__9;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__10;
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("substEvar", 9);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__1;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__1;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__4;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__4;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__6;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__5;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; uint8_t x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
x_12 = lean_unsigned_to_nat(4u);
x_13 = l_Lean_Syntax_getArg(x_1, x_12);
lean_dec(x_1);
x_14 = lean_ctor_get(x_2, 5);
lean_inc(x_14);
x_15 = 0;
x_16 = l_Lean_SourceInfo_fromRef(x_14, x_15);
x_17 = lean_ctor_get(x_2, 2);
lean_inc(x_17);
x_18 = lean_ctor_get(x_2, 1);
lean_inc(x_18);
lean_dec(x_2);
x_19 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__3;
x_20 = l_Lean_addMacroScope(x_18, x_19, x_17);
x_21 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__2;
x_22 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__8;
lean_inc(x_16);
x_23 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_23, 0, x_16);
lean_ctor_set(x_23, 1, x_21);
lean_ctor_set(x_23, 2, x_20);
lean_ctor_set(x_23, 3, x_22);
x_24 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
lean_inc(x_16);
x_25 = l_Lean_Syntax_node3(x_16, x_24, x_9, x_11, x_13);
x_26 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5;
x_27 = l_Lean_Syntax_node2(x_16, x_26, x_23, x_25);
x_28 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_28, 0, x_27);
lean_ctor_set(x_28, 1, x_3);
return x_28;
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__substEvar__1___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8) {
_start:
{
lean_object* x_9; uint8_t x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; 
x_9 = l_Lean_replaceRef(x_1, x_7);
x_10 = 0;
x_11 = l_Lean_SourceInfo_fromRef(x_9, x_10);
x_12 = l_ML_AppContext_term___x5b___x5d___closed__7;
lean_inc(x_11);
x_13 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_13, 0, x_11);
lean_ctor_set(x_13, 1, x_12);
x_14 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__3;
lean_inc(x_11);
x_15 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_15, 0, x_11);
lean_ctor_set(x_15, 1, x_14);
x_16 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__7;
lean_inc(x_11);
x_17 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_17, 0, x_11);
lean_ctor_set(x_17, 1, x_16);
x_18 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__2;
lean_inc(x_11);
x_19 = l_Lean_Syntax_node6(x_11, x_18, x_3, x_13, x_4, x_15, x_5, x_17);
x_20 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2___closed__1;
x_21 = l_Array_append___rarg(x_20, x_6);
x_22 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
lean_inc(x_11);
x_23 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_23, 0, x_11);
lean_ctor_set(x_23, 1, x_22);
lean_ctor_set(x_23, 2, x_21);
x_24 = l_Lean_Syntax_node2(x_11, x_2, x_19, x_23);
x_25 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_25, 0, x_24);
lean_ctor_set(x_25, 1, x_8);
return x_25;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__substEvar__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = lean_unsigned_to_nat(3u);
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; uint8_t x_19; 
x_18 = l_Lean_Syntax_getNumArgs(x_15);
x_19 = lean_nat_dec_le(x_16, x_18);
if (x_19 == 0)
{
lean_object* x_20; lean_object* x_21; 
lean_dec(x_18);
lean_dec(x_15);
lean_dec(x_9);
lean_dec(x_2);
x_20 = lean_box(0);
x_21 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_21, 0, x_20);
lean_ctor_set(x_21, 1, x_3);
return x_21;
}
else
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; 
x_22 = l_Lean_Syntax_getArg(x_15, x_8);
x_23 = l_Lean_Syntax_getArg(x_15, x_14);
x_24 = lean_unsigned_to_nat(2u);
x_25 = l_Lean_Syntax_getArg(x_15, x_24);
x_26 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_27 = lean_nat_sub(x_18, x_8);
lean_dec(x_18);
x_28 = l_Array_extract___rarg(x_26, x_16, x_27);
lean_dec(x_27);
lean_dec(x_26);
x_29 = lean_box(2);
x_30 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
x_31 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_31, 0, x_29);
lean_ctor_set(x_31, 1, x_30);
lean_ctor_set(x_31, 2, x_28);
x_32 = l_Lean_Syntax_getArgs(x_31);
lean_dec(x_31);
x_33 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__substEvar__1___lambda__1(x_9, x_4, x_22, x_23, x_25, x_32, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_33;
}
}
else
{
lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; uint8_t x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; 
x_34 = l_Lean_Syntax_getArg(x_15, x_8);
x_35 = l_Lean_Syntax_getArg(x_15, x_14);
x_36 = lean_unsigned_to_nat(2u);
x_37 = l_Lean_Syntax_getArg(x_15, x_36);
lean_dec(x_15);
x_38 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_39 = 0;
x_40 = l_Lean_SourceInfo_fromRef(x_38, x_39);
x_41 = l_ML_AppContext_term___x5b___x5d___closed__7;
lean_inc(x_40);
x_42 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_42, 0, x_40);
lean_ctor_set(x_42, 1, x_41);
x_43 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__3;
lean_inc(x_40);
x_44 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_44, 0, x_40);
lean_ctor_set(x_44, 1, x_43);
x_45 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__7;
lean_inc(x_40);
x_46 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_46, 0, x_40);
lean_ctor_set(x_46, 1, x_45);
x_47 = l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__2;
x_48 = l_Lean_Syntax_node6(x_40, x_47, x_34, x_42, x_35, x_44, x_37, x_46);
x_49 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_49, 0, x_48);
lean_ctor_set(x_49, 1, x_3);
return x_49;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__substEvar__1___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8) {
_start:
{
lean_object* x_9; 
x_9 = l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__substEvar__1___lambda__1(x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8);
lean_dec(x_7);
lean_dec(x_1);
return x_9;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Pattern", 7);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticAutopos", 13);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__1;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__1;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__2;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Tactic", 6);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("seq1", 4);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4;
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__5;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__7() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("paren", 5);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4;
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__7;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__9() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("(", 1);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__10() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticSeq", 9);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4;
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__10;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__12() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticSeq1Indented", 18);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4;
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__12;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__14() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticTry_", 10);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4;
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__14;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__16() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("try", 3);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__17() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tactic_<;>_", 11);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__18() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4;
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__17;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__19() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("apply", 5);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__20() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4;
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__19;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__21() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("AppContext.wf_insert", 20);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__22() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__21;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__23() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("wf_insert", 9);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__24() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__23;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__25() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_AppContext_term___x5b___x5d___closed__1;
x_2 = l_ML_AppContext_term___x5b___x5d___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__23;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__26() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__25;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__27() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__26;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__28() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("<;>", 3);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__29() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("autopos", 7);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__30() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(")", 1);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__31() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(";", 1);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__32() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("done", 4);
return x_1;
}
}
static lean_object* _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__33() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1;
x_2 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2;
x_3 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4;
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__32;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__3;
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; lean_object* x_50; lean_object* x_51; lean_object* x_52; lean_object* x_53; lean_object* x_54; lean_object* x_55; lean_object* x_56; lean_object* x_57; 
x_8 = lean_ctor_get(x_2, 5);
lean_inc(x_8);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = lean_ctor_get(x_2, 2);
lean_inc(x_11);
x_12 = lean_ctor_get(x_2, 1);
lean_inc(x_12);
lean_dec(x_2);
x_13 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__9;
lean_inc(x_10);
x_14 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_13);
x_15 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__16;
lean_inc(x_10);
x_16 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_16, 0, x_10);
lean_ctor_set(x_16, 1, x_15);
x_17 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__19;
lean_inc(x_10);
x_18 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_18, 0, x_10);
lean_ctor_set(x_18, 1, x_17);
x_19 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__24;
x_20 = l_Lean_addMacroScope(x_12, x_19, x_11);
x_21 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__22;
x_22 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__27;
lean_inc(x_10);
x_23 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_23, 0, x_10);
lean_ctor_set(x_23, 1, x_21);
lean_ctor_set(x_23, 2, x_20);
lean_ctor_set(x_23, 3, x_22);
x_24 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__20;
lean_inc(x_10);
x_25 = l_Lean_Syntax_node2(x_10, x_24, x_18, x_23);
x_26 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__28;
lean_inc(x_10);
x_27 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_27, 0, x_10);
lean_ctor_set(x_27, 1, x_26);
x_28 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__29;
lean_inc(x_10);
x_29 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_29, 0, x_10);
lean_ctor_set(x_29, 1, x_28);
lean_inc(x_10);
x_30 = l_Lean_Syntax_node1(x_10, x_4, x_29);
x_31 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__18;
lean_inc(x_10);
x_32 = l_Lean_Syntax_node3(x_10, x_31, x_25, x_27, x_30);
x_33 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15;
lean_inc(x_10);
x_34 = l_Lean_Syntax_node1(x_10, x_33, x_32);
x_35 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__13;
lean_inc(x_10);
x_36 = l_Lean_Syntax_node1(x_10, x_35, x_34);
x_37 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__11;
lean_inc(x_10);
x_38 = l_Lean_Syntax_node1(x_10, x_37, x_36);
x_39 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__15;
lean_inc(x_10);
x_40 = l_Lean_Syntax_node2(x_10, x_39, x_16, x_38);
lean_inc(x_10);
x_41 = l_Lean_Syntax_node1(x_10, x_33, x_40);
lean_inc(x_10);
x_42 = l_Lean_Syntax_node1(x_10, x_35, x_41);
lean_inc(x_10);
x_43 = l_Lean_Syntax_node1(x_10, x_37, x_42);
x_44 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__30;
lean_inc(x_10);
x_45 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_45, 0, x_10);
lean_ctor_set(x_45, 1, x_44);
x_46 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__8;
lean_inc(x_10);
x_47 = l_Lean_Syntax_node3(x_10, x_46, x_14, x_43, x_45);
x_48 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__31;
lean_inc(x_10);
x_49 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_49, 0, x_10);
lean_ctor_set(x_49, 1, x_48);
x_50 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__32;
lean_inc(x_10);
x_51 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_51, 0, x_10);
lean_ctor_set(x_51, 1, x_50);
x_52 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__33;
lean_inc(x_10);
x_53 = l_Lean_Syntax_node1(x_10, x_52, x_51);
lean_inc(x_10);
x_54 = l_Lean_Syntax_node3(x_10, x_33, x_47, x_49, x_53);
x_55 = l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__6;
x_56 = l_Lean_Syntax_node1(x_10, x_55, x_54);
x_57 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_57, 0, x_56);
lean_ctor_set(x_57, 1, x_3);
return x_57;
}
}
}
lean_object* initialize_Init(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Pattern(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Substitution(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Positivity(uint8_t builtin, lean_object*);
static bool _G_initialized = false;
LEAN_EXPORT lean_object* initialize_MatchingLogic_AppContext(uint8_t builtin, lean_object* w) {
lean_object * res;
if (_G_initialized) return lean_io_result_mk_ok(lean_box(0));
_G_initialized = true;
res = initialize_Init(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Pattern(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Substitution(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Positivity(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__1 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__1();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__1);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__2 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__2();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__2);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__3 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__3();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__3);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__4 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__4();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__4);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__5 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__5();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__5);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__6 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__6();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__6);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__7 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__7();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__7);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__8 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__8();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__8);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__9 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__9();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__9);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__10 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__10();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__10);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__11 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__11();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__11);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__12 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__12();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__12);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__13 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__13();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__13);
l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__14 = _init_l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__14();
lean_mark_persistent(l___private_MatchingLogic_AppContext_0__ML_reprAppContext____x40_MatchingLogic_AppContext___hyg_542____rarg___closed__14);
l_ML_AppContext_term___x5b___x5d___closed__1 = _init_l_ML_AppContext_term___x5b___x5d___closed__1();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__1);
l_ML_AppContext_term___x5b___x5d___closed__2 = _init_l_ML_AppContext_term___x5b___x5d___closed__2();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__2);
l_ML_AppContext_term___x5b___x5d___closed__3 = _init_l_ML_AppContext_term___x5b___x5d___closed__3();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__3);
l_ML_AppContext_term___x5b___x5d___closed__4 = _init_l_ML_AppContext_term___x5b___x5d___closed__4();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__4);
l_ML_AppContext_term___x5b___x5d___closed__5 = _init_l_ML_AppContext_term___x5b___x5d___closed__5();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__5);
l_ML_AppContext_term___x5b___x5d___closed__6 = _init_l_ML_AppContext_term___x5b___x5d___closed__6();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__6);
l_ML_AppContext_term___x5b___x5d___closed__7 = _init_l_ML_AppContext_term___x5b___x5d___closed__7();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__7);
l_ML_AppContext_term___x5b___x5d___closed__8 = _init_l_ML_AppContext_term___x5b___x5d___closed__8();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__8);
l_ML_AppContext_term___x5b___x5d___closed__9 = _init_l_ML_AppContext_term___x5b___x5d___closed__9();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__9);
l_ML_AppContext_term___x5b___x5d___closed__10 = _init_l_ML_AppContext_term___x5b___x5d___closed__10();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__10);
l_ML_AppContext_term___x5b___x5d___closed__11 = _init_l_ML_AppContext_term___x5b___x5d___closed__11();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__11);
l_ML_AppContext_term___x5b___x5d___closed__12 = _init_l_ML_AppContext_term___x5b___x5d___closed__12();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__12);
l_ML_AppContext_term___x5b___x5d___closed__13 = _init_l_ML_AppContext_term___x5b___x5d___closed__13();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__13);
l_ML_AppContext_term___x5b___x5d___closed__14 = _init_l_ML_AppContext_term___x5b___x5d___closed__14();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__14);
l_ML_AppContext_term___x5b___x5d___closed__15 = _init_l_ML_AppContext_term___x5b___x5d___closed__15();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__15);
l_ML_AppContext_term___x5b___x5d___closed__16 = _init_l_ML_AppContext_term___x5b___x5d___closed__16();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d___closed__16);
l_ML_AppContext_term___x5b___x5d = _init_l_ML_AppContext_term___x5b___x5d();
lean_mark_persistent(l_ML_AppContext_term___x5b___x5d);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__1);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__2);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__3 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__3();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__3);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__4 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__4();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__4);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__5);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__6 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__6();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__6);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__7 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__7();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__7);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__8 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__8();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__8);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__9 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__9();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__9);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__10 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__10();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__10);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__11 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__11();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__11);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__12 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__12();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__12);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__13 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__13();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__13);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__14 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__14();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__14);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___x5d__1___closed__15);
l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2___closed__1 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2___closed__1();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___lambda__2___closed__1);
l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__1 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__1();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__1);
l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__2 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__2();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__2);
l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__3 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__3();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______unexpand__ML__AppContext__insert__1___closed__3);
l_ML_AppContext_term_u25a1___closed__1 = _init_l_ML_AppContext_term_u25a1___closed__1();
lean_mark_persistent(l_ML_AppContext_term_u25a1___closed__1);
l_ML_AppContext_term_u25a1___closed__2 = _init_l_ML_AppContext_term_u25a1___closed__2();
lean_mark_persistent(l_ML_AppContext_term_u25a1___closed__2);
l_ML_AppContext_term_u25a1___closed__3 = _init_l_ML_AppContext_term_u25a1___closed__3();
lean_mark_persistent(l_ML_AppContext_term_u25a1___closed__3);
l_ML_AppContext_term_u25a1___closed__4 = _init_l_ML_AppContext_term_u25a1___closed__4();
lean_mark_persistent(l_ML_AppContext_term_u25a1___closed__4);
l_ML_AppContext_term_u25a1___closed__5 = _init_l_ML_AppContext_term_u25a1___closed__5();
lean_mark_persistent(l_ML_AppContext_term_u25a1___closed__5);
l_ML_AppContext_term_u25a1 = _init_l_ML_AppContext_term_u25a1();
lean_mark_persistent(l_ML_AppContext_term_u25a1);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__1 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__1();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__1);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__2 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__2();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__2);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__3 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__3();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__3);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__4 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__4();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__4);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__5 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__5();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__5);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__6 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__6();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__6);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__7 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__7();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__7);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__8 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__8();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term_u25a1__1___closed__8);
l_ML_AppContext_term___x3c_u2b1d_____closed__1 = _init_l_ML_AppContext_term___x3c_u2b1d_____closed__1();
lean_mark_persistent(l_ML_AppContext_term___x3c_u2b1d_____closed__1);
l_ML_AppContext_term___x3c_u2b1d_____closed__2 = _init_l_ML_AppContext_term___x3c_u2b1d_____closed__2();
lean_mark_persistent(l_ML_AppContext_term___x3c_u2b1d_____closed__2);
l_ML_AppContext_term___x3c_u2b1d_____closed__3 = _init_l_ML_AppContext_term___x3c_u2b1d_____closed__3();
lean_mark_persistent(l_ML_AppContext_term___x3c_u2b1d_____closed__3);
l_ML_AppContext_term___x3c_u2b1d_____closed__4 = _init_l_ML_AppContext_term___x3c_u2b1d_____closed__4();
lean_mark_persistent(l_ML_AppContext_term___x3c_u2b1d_____closed__4);
l_ML_AppContext_term___x3c_u2b1d_____closed__5 = _init_l_ML_AppContext_term___x3c_u2b1d_____closed__5();
lean_mark_persistent(l_ML_AppContext_term___x3c_u2b1d_____closed__5);
l_ML_AppContext_term___x3c_u2b1d_____closed__6 = _init_l_ML_AppContext_term___x3c_u2b1d_____closed__6();
lean_mark_persistent(l_ML_AppContext_term___x3c_u2b1d_____closed__6);
l_ML_AppContext_term___x3c_u2b1d__ = _init_l_ML_AppContext_term___x3c_u2b1d__();
lean_mark_persistent(l_ML_AppContext_term___x3c_u2b1d__);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__1 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__1();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__1);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__2 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__2();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__2);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__3 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__3();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__3);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__4 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__4();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__4);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__5 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__5();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__5);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__6 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__6();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__6);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__7 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__7();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__7);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__8 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__8();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__8);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__9 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__9();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x3c_u2b1d____1___closed__9);
l_ML_AppContext_term___u2b1d_x3e_____closed__1 = _init_l_ML_AppContext_term___u2b1d_x3e_____closed__1();
lean_mark_persistent(l_ML_AppContext_term___u2b1d_x3e_____closed__1);
l_ML_AppContext_term___u2b1d_x3e_____closed__2 = _init_l_ML_AppContext_term___u2b1d_x3e_____closed__2();
lean_mark_persistent(l_ML_AppContext_term___u2b1d_x3e_____closed__2);
l_ML_AppContext_term___u2b1d_x3e_____closed__3 = _init_l_ML_AppContext_term___u2b1d_x3e_____closed__3();
lean_mark_persistent(l_ML_AppContext_term___u2b1d_x3e_____closed__3);
l_ML_AppContext_term___u2b1d_x3e_____closed__4 = _init_l_ML_AppContext_term___u2b1d_x3e_____closed__4();
lean_mark_persistent(l_ML_AppContext_term___u2b1d_x3e_____closed__4);
l_ML_AppContext_term___u2b1d_x3e_____closed__5 = _init_l_ML_AppContext_term___u2b1d_x3e_____closed__5();
lean_mark_persistent(l_ML_AppContext_term___u2b1d_x3e_____closed__5);
l_ML_AppContext_term___u2b1d_x3e_____closed__6 = _init_l_ML_AppContext_term___u2b1d_x3e_____closed__6();
lean_mark_persistent(l_ML_AppContext_term___u2b1d_x3e_____closed__6);
l_ML_AppContext_term___u2b1d_x3e__ = _init_l_ML_AppContext_term___u2b1d_x3e__();
lean_mark_persistent(l_ML_AppContext_term___u2b1d_x3e__);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__1 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__1();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__1);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__2 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__2();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__2);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__3 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__3();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__3);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__4 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__4();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__4);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__5 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__5();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__5);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__6 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__6();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__6);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__7 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__7();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__7);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__8 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__8();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__8);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__9 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__9();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___u2b1d_x3e____1___closed__9);
l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__1 = _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__1();
lean_mark_persistent(l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__1);
l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__2 = _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__2();
lean_mark_persistent(l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__2);
l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__3 = _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__3();
lean_mark_persistent(l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__3);
l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__4 = _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__4();
lean_mark_persistent(l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__4);
l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__5 = _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__5();
lean_mark_persistent(l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__5);
l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__6 = _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__6();
lean_mark_persistent(l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__6);
l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__7 = _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__7();
lean_mark_persistent(l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__7);
l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__8 = _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__8();
lean_mark_persistent(l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__8);
l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__9 = _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__9();
lean_mark_persistent(l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__9);
l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__10 = _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__10();
lean_mark_persistent(l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c___closed__10);
l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c = _init_l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c();
lean_mark_persistent(l_ML_AppContext_term___x5b___u21d0_u1d49___x5d_u1d9c);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__1 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__1();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__1);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__2 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__2();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__2);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__3 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__3();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__3);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__4 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__4();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__4);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__5 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__5();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__5);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__6 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__6();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__6);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__7 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__7();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__7);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__8 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__8();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__AppContext__term___x5b___u21d0_u1d49___x5d_u1d9c__1___closed__8);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__1 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__1();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__1);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__2 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__2();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__2);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__3 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__3();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__3);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__4);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__5 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__5();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__5);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__6 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__6();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__6);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__7 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__7();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__7);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__8 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__8();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__8);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__9 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__9();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__9);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__10 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__10();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__10);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__11 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__11();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__11);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__12 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__12();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__12);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__13 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__13();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__13);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__14 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__14();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__14);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__15 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__15();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__15);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__16 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__16();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__16);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__17 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__17();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__17);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__18 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__18();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__18);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__19 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__19();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__19);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__20 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__20();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__20);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__21 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__21();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__21);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__22 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__22();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__22);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__23 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__23();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__23);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__24 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__24();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__24);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__25 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__25();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__25);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__26 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__26();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__26);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__27 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__27();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__27);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__28 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__28();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__28);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__29 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__29();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__29);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__30 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__30();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__30);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__31 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__31();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__31);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__32 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__32();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__32);
l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__33 = _init_l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__33();
lean_mark_persistent(l_ML_AppContext___aux__MatchingLogic__AppContext______macroRules__ML__Pattern__tacticAutopos__1___closed__33);
return lean_io_result_mk_ok(lean_box(0));
}
#ifdef __cplusplus
}
#endif
