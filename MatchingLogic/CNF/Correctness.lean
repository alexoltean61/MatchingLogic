import MatchingLogic.CNF.Lemma

namespace ML
namespace Pattern

-- In this file, we prove that our definition of CNF is correct:
--    the inputs on which CNF returns *something* are exactly those that satisfy
--    the specification.

-- We claim that a pattern φ is in CNF iff:
--   1. It is conjunctive (that is, !(hasArrows φ)),
--   2. Negations are only applied to atoms (that is, !(hasNegations φ))
--   3. Has no disjunction not distributed over conjunctions (that is, !(isUndistributed φ))
--
-- These conjunction of these conditions is cnfConditions, defined in Specs.lean.

lemma conjCNF : cnfConditions φ → cnfConditions ψ → cnfConditions (φ ⋀ ψ) := by
  rw [←and_imp]
  contrapose
  intro h
  simp only [cnfConditions, Bool.and_eq_true, Bool.not_eq_true', not_and_or, Bool.not_eq_false] at h
  simp only [not_and_or, Bool.not_eq_true]
  apply Or.elim h
  . clear h
    intro h
    apply Or.elim h
    . clear h
      intro h
      simp only [cnfConditions, Bool.and_eq_false_eq_eq_false_or_eq_false, Bool.not_eq_false']
      conv =>
        rw [or_assoc, or_comm, or_assoc]
        rhs; rhs; rw [or_comm]
      conv =>
        rhs; rw [or_comm, or_assoc]
        rhs; rw [or_assoc, ←or_assoc]
      simp [arrowsConj h]
    . clear h
      intro h
      simp only [cnfConditions, Bool.and_eq_false_eq_eq_false_or_eq_false, Bool.not_eq_false']
      conv =>
        rw [or_assoc, or_comm, or_assoc]
        rhs; lhs; lhs; rw [or_comm]
      conv =>
        rhs; rw [or_comm, or_assoc]
        rhs; rw [or_assoc, ←or_assoc]
      simp [negationsConj h]
  . clear h
    intro h
    simp only [cnfConditions, Bool.and_eq_false_eq_eq_false_or_eq_false, Bool.not_eq_false']
    conv =>
      rw [or_assoc, or_comm, or_assoc]
      rhs; lhs; rw [or_comm]
    rw [←or_assoc]
    apply Or.inl
    rw [←or_assoc]
    simp [undistributedConj h]

lemma isConjNotCNFR : isConjunction φ → !cnfConditions (φ ⋁ ψ) := by
  intro h
  simp only [cnfConditions, Bool.not_and, Bool.not_not, Bool.or_eq_true]
  apply Or.inr
  exact conjunctionUndistributedR h

lemma isConjNotCNFL : isConjunction ψ → !cnfConditions (φ ⋁ ψ) := by
  intro h
  simp only [cnfConditions, Bool.not_and, Bool.not_not, Bool.or_eq_true]
  apply Or.inr
  exact conjunctionUndistributedL h

lemma disjNotCNFR : !cnfConditions φ → !cnfConditions (φ ⋁ ψ) := by
  intro h
  simp only [cnfConditions, Bool.not_and, Bool.not_not, Bool.or_eq_true] at h ⊢
  apply Or.elim h
  . intro h
    apply Or.elim h
    . intro h
      apply Or.inl; apply Or.inl
      exact arrowsDisjR h
    . intro h
      apply Or.inl; apply Or.inr
      exact negationsDisjR h
  . intro h
    apply Or.inr
    exact undistributedDisjR h

lemma disjNotCNFL : !cnfConditions ψ → !cnfConditions (φ ⋁ ψ) := by
  intro h
  simp only [cnfConditions, Bool.not_and, Bool.not_not, Bool.or_eq_true] at h ⊢
  apply Or.elim h
  . intro h
    apply Or.elim h
    . intro h
      apply Or.inl; apply Or.inl
      exact arrowsDisjL h
    . intro h
      apply Or.inl; apply Or.inr
      exact negationsDisjL h
  . intro h
    apply Or.inr
    exact undistributedDisjL h

lemma conjNotCNFR : !cnfConditions φ → !cnfConditions (φ ⋀ ψ) := by
  intro h
  simp only [cnfConditions, Bool.not_and, Bool.not_not, Bool.or_eq_true] at h ⊢
  apply Or.elim h
  . intro h
    apply Or.elim h
    . intro h
      apply Or.inl; apply Or.inl
      exact arrowsConjR h
    . intro h
      apply Or.inl; apply Or.inr
      exact negationsConjR h
  . intro h
    apply Or.inr
    exact undistributedConjR h

lemma conjNotCNFL : !cnfConditions ψ → !cnfConditions (φ ⋀ ψ) := by
  intro h
  simp only [cnfConditions, Bool.not_and, Bool.not_not, Bool.or_eq_true] at h ⊢
  apply Or.elim h
  . intro h
    apply Or.elim h
    . intro h
      apply Or.inl; apply Or.inl
      exact arrowsConjL h
    . intro h
      apply Or.inl; apply Or.inr
      exact negationsConjL h
  . intro h
    apply Or.inr
    exact undistributedConjL h

lemma literal_none : (Literal φ = none) → (isDisjunction φ ∨ isConjunction φ ∨ hasArrows φ ∨ hasNegations φ) := by
  -- in practice, this statement is just a boolean validity
  -- for the moment, don't bother proving it, try to automate
  --   as much as possible.
  --
  --   it's ugly and hard to maintain, but hey,
  --     it worked.
  rw [←Option.not_isSome_iff_eq_none, ←isLiteral', isNotLiteralIff]
  unfold isNotLiteral isAtom isDisjunction isConjunction hasArrows hasNegations
  split
  . simp [*] at *
    split
    . simp [*] at *
      split
      . simp [*] at *
      . simp [*] at *
        split
        . simp [*] at *
        . simp [*] at *
          split
          . next h =>
            simp [*] at *
            simp [h.1, h.2] at *
          . next h =>
            simp [*] at *
            simp [←h] at *
            rw [←Bool.not_eq_true]
            simp [test1, isAtom]
          . next h =>
            simp [*] at *
            simp [h.1, h.2] at *
          . simp [*] at *
    . simp [*] at *
  . simp [*] at *
    split
    . simp [*] at *
    . simp [*] at *
      split
      . simp [*] at *
      . simp [*] at *
      . simp [*] at *
      . simp [*] at *
      . simp [*] at *
  . simp [*] at *

lemma disj_none : (Disjunction φ = none) → (isConjunction φ ∨ !cnfConditions φ) := by
  unfold Disjunction
  simp only [Option.map_eq_map, Option.orElse_eq_none, Option.map_eq_none', Bool.not_and,
    Bool.not_not, Bool.or_eq_true, and_imp]
  intro h1 h2
  split at h2
  . next φ =>
    clear h1 h2
    simp only [cnfConditions, Bool.not_not, Bool.not_or, Bool.not_and, Bool.or_eq_true,
      Bool.not_eq_true']
    apply Or.inr; apply Or.inl; apply Or.inr
    exact not_not_negations
  . next φ ψ _ =>
    clear h1
    rw [←negation, ←disjunction]
    simp only [Seq.seq, Option.map_eq_map, Option.bind_eq_none, Option.map_eq_some',
      Option.map_eq_none', forall_exists_index, and_imp, forall_apply_eq_imp_iff₂] at h2
    apply Or.inr
    by_cases h : (Disjunction φ).isSome
    . rw [Option.isSome_iff_exists] at h
      match h with
      | ⟨a, isDisj⟩ =>
          specialize h2 a isDisj
          clear h isDisj
          apply Or.elim (disj_none h2)
          . exact isConjNotCNFL
          . exact disjNotCNFL
    . rw [Bool.not_eq_true, Option.not_isSome, Option.isNone] at h
      -- annoying that i have to:
      split at h
      . contradiction
      . next is_none =>
        have := disj_none is_none
        apply Or.elim (disj_none is_none)
        . exact isConjNotCNFR
        . exact disjNotCNFR
  . next not_nn not_disj =>
    simp
    rw [←or_assoc, ←or_assoc]
    apply Or.inl
    rw [or_assoc]
    conv at not_nn =>
      intro a; rw [←negation, ←negation]
    conv at not_disj =>
      intro a b; rw [←negation, ←disjunction]
    clear h2
    have h_lit := literal_none h1
    unfold isDisjunction at h_lit
    split at h_lit
    . next φ ψ =>
      specialize not_disj φ ψ
      contradiction
    . simp only [false_or] at h_lit
      exact h_lit

lemma disj_some : Disjunction φ = some c → cnfConditions φ := by
  simp only [cnfConditions, Bool.and_eq_true]
  intro h
  simp only [disjCNF1 h, disjCNF2 h, and_self, disjCNF3 h]

theorem cnf_if' {φ : Pattern 𝕊} : ¬isCNF φ → ¬cnfConditions φ := by
  intro h
  unfold isCNF at h
  rw [Option.not_isSome_iff_eq_none] at h
  unfold CNF at h
  simp only [Option.map_eq_map, Option.orElse_eq_none, Option.map_eq_none'] at h
  have ⟨not_disj, not_clauses⟩ := h
  clear h
  split at not_clauses
  . next ψ χ =>
    rw [←negation, ←negation, ←negation, ←negation, ←disjunction, ←conjunction]
    simp only [Seq.seq, Option.map_eq_map, Option.bind_eq_none, Option.map_eq_some',
      Option.map_eq_none', forall_exists_index, and_imp, forall_apply_eq_imp_iff₂,
        Bool.not_eq_true, ←Bool.not_eq_true'] at not_clauses ⊢
    by_cases h : (CNF ψ).isSome
    . rw [Option.isSome_iff_exists] at h
      match h with
      | ⟨a, is_cnf⟩ =>
          specialize not_clauses a is_cnf
          apply conjNotCNFL
          simp only [Bool.not_eq_true', ←Bool.not_eq_true]
          apply cnf_if'
          simp only [isCNF, not_clauses, Option.isSome_none, not_false_eq_true]
    . simp only [Bool.not_eq_true, Option.not_isSome] at h
      apply conjNotCNFR
      simp only [Bool.not_eq_true', ←Bool.not_eq_true]
      apply cnf_if'
      simp only [isCNF, Bool.not_eq_true, Option.not_isSome, h]
  . next not_conj =>
    clear not_clauses
    conv at not_conj =>
      intro ψ χ; rw [←negation, ←negation, ←negation, ←negation, ←disjunction, ←conjunction]
    -- not_disj means: φ is neither a literal (atom or negated atom), nor a disjunction of DISJUNCTIONS
    have h_disj := disj_none not_disj
    -- not_conj means: φ is not a conjunction of any arbitrary two formulas
    unfold isConjunction at h_disj
    split at h_disj
    . next ψ χ =>
      specialize not_conj ψ χ
      contradiction
    . simp at h_disj
      simp only [cnfConditions, Bool.and_eq_true, Bool.not_eq_true', not_and_or,
        Bool.not_eq_false]
      exact h_disj

theorem cnf_if {φ : Pattern 𝕊} : cnfConditions φ → isCNF φ := by
  contrapose
  exact cnf_if'

theorem if_cnf {φ : Pattern 𝕊} : isCNF φ → cnfConditions φ := by
  unfold isCNF CNF
  simp only [Option.map_eq_map, Option.isSome_iff_exists, Option.orElse_eq_some,
    Option.map_eq_some', toList, Option.map_eq_none', forall_exists_index]
  intro L h
  apply Or.elim h
  . clear h
    intro ⟨c, ⟨h, _⟩⟩
    revert h
    exact disj_some
  . clear h
    intro ⟨hl, hr⟩
    split at hr
    . rw [←negation, ←negation, ←negation, ←negation, ←disjunction, ←conjunction] at hl ⊢
      simp only [Seq.seq, Option.map_eq_map, Option.bind_eq_some, Option.map_eq_some',
        exists_exists_and_eq_and] at hr
      have ⟨c1, ⟨hc1, ⟨c2, ⟨hc2, _⟩⟩⟩⟩ := hr
      apply conjCNF
      . apply if_cnf
        simp only [isCNF, Option.isSome_iff_exists]
        exact ⟨c1, hc1⟩
      . apply if_cnf
        simp only [isCNF, Option.isSome_iff_exists]
        exact ⟨c2, hc2⟩
    . contradiction

theorem cnf_iff {φ : Pattern 𝕊} : isCNF φ ↔ cnfConditions φ := by
  apply Iff.intro
  . exact if_cnf
  . exact cnf_if

#print axioms cnf_iff
