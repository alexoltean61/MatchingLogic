import MatchingLogic.CNF.Basic

namespace ML
namespace Pattern


-- In this file, we aim to express the boolean conditions that a formula must satisfy
--   in order to be in CNF.
-- In other words, we want to specify the inputs on which the CNF Option from Basic.lean
--   outputs something other than `none`.
--
-- The fact that this *is* the correct specification is proved in Correctness.lean.

/--
  Checks if a formula is atomic. See also `Pattern.Atom`.
-/
def isAtom : Pattern 𝕊 → Bool
| _ ⇒ _ => false
| _      => true

def isAtom' : Pattern 𝕊 → Bool := λ φ => (Atom φ).isSome

/--
  Checks if a formula is a negated atom. See also `Pattern.negAtom`.
-/
def isNegAtom : Pattern 𝕊 → Bool
| ∼φ    => isAtom φ
| _     => false

def isNegAtom' : Pattern 𝕊 → Bool := λ φ => (negAtom φ).isSome

/--
  Checks if a formula is a literal. See also `Pattern.Literal`.
-/
def isLiteral : Pattern 𝕊 → Bool :=
  λ φ => isAtom φ || isNegAtom φ

def isLiteral' : Pattern 𝕊 → Bool := λ φ => (Literal φ).isSome

def isNotLiteral : Pattern 𝕊 → Bool
| ∼φ    => !isAtom φ
| _ ⇒ _ => true
| _      => false

/--
  Checks if a formula is a disjunction.

  This is not equivalent to `(Disjunction φ).isSome`! That function returns
  `true` if `φ` is a literal. This function does not, since it requires at least two
  formulas to be disjointed.
-/
def isDisjunction : Pattern 𝕊 → Bool
| _ ⋁ _ => true
| _     => false

/--
  Checks if a formula is a conjunction.
-/
def isConjunction : Pattern 𝕊 → Bool
| _ ⋀ _ => true
| _     => false

/--
  Checks if a pattern is in CNF. See also `Pattern.CNF`.
-/
def isCNF : Pattern 𝕊 → Bool := λ φ => (CNF φ).isSome

/--
  Checks if a pattern is implicative.

  That is, returns "false" if a pattern is made only of negation, conjunction and disjunction
      (has no arrows).
-/
@[simp]
def hasArrows : Pattern 𝕊 → Bool
| ∼∼φ   => hasArrows (∼φ)
| φ ⋁ ψ => hasArrows φ || hasArrows ψ
| ∼φ    => hasArrows φ
| _ ⇒ _ => true
| _      => false

-- Bear in mind that this evaluates as *not* having arrows:
#eval hasArrows ((∼ML.Pattern.symbol 0) ⇒ ML.Pattern.symbol 1)
-- Because this term matches with a disjunction
--     i.e., with (ML.Pattern.symbol 0 ⋁ ML.Pattern.symbol 1)
-- There is no way to have:
--     hasArrows ((∼ML.Pattern.symbol 0) ⇒ ML.Pattern.symbol 1) = true, and
--     hasArrows (ML.Pattern.symbol 0 ⋁ ML.Pattern.symbol 1)     = false.

def countArrows : Pattern 𝕊 → ℕ
| ∼φ => countArrows φ
| φ ⋁ ψ => countArrows φ + countArrows ψ
| φ ⇒ ψ => 1 + countArrows φ + countArrows ψ
| _     => 0

/--
  Returns "true" if there is some negated subpattern which is not a negative atom.

  Returns "false" if no negations in a pattern occur outside of its negative atoms.
-/
@[simp]
def hasNegations : Pattern 𝕊 → Bool
| φ ⋀ ψ  => hasNegations φ || hasNegations ψ
| ∼φ     => !isAtom (φ)
| φ ⇒ ψ  => hasNegations ψ ||
    match φ with
    | ∼φ => hasNegations φ
    | φ  => hasNegations φ
-- provisional.
-- explanation: (φ ⋁ ψ) is defined as (∼φ ⇒ ψ)
--   if i add (φ ⋁ ψ) as a pattern to match on, Lean's isDefEq gets stuck.
--   but if i don't, disjunction gets matched as an implication,
--     with the first formula negated!!
--     and so this function wrongly returns that φ ⋁ ψ is negated.
--
-- one way to go around this is to do another match inside the φ ⇒ ψ case
--   this is the current way.
--
-- another way would be to assume the pattern is already non-implicative
--   (as a hypothesis),
--   and only check for (φ ⋁ ψ), not (φ ⇒ ψ)
| _       => false


/--
  Debugging function. Checks if a formula has negations, and also returns one such negation.
-/
def Neg : Pattern 𝕊 → Option (Pattern 𝕊)
| φ ⋀ ψ  => Neg φ <|> Neg ψ
| ∼φ     =>
    if (!isAtom (φ))
    then some (∼φ)
    else none
| φ ⇒ ψ  => Neg ψ <|>
    match φ with
    | ∼φ => Neg φ
    | φ  => Neg φ
| _       => none

-- Double negation matches as disjunction:
-- ∼∼φ = (∼φ ⇒ ⊥) = φ ⋁ ⊥
--
-- So what if you have a doubly negated CONJUNCTION?
--   i.e., ∼∼(φ ⋀ ψ).
-- This is the exact same term as:
--          (φ ⋀ ψ) ⋁ ⊥
--
-- But, on paper, you would say ∼∼(φ ⋀ ψ) contains NO undistributed disjunctions,
--           while (φ ⋀ ψ) ⋁ ⊥ DOES contain a disjunction undistributed over a conjunction.
--
-- However, since the two terms are EQUAL, you will NEVER EVER obtain different values for
--    isUndistributed applied to them! Completely regardless of how you define isUndistributed.
--
-- You must make a choice:
-- either both evaluate to false (which means that some undistributed expressions go undetected: weak criterion),
--   or both evaluate to true (which means that some distributed expressions are detected as undistributed: strong criterion).
--
-- We implement the STRONG version of isUndistributed. This definition evaluates both
--         ∼∼(φ ⋀ ψ)       and       (φ ⋀ ψ) ⋁ ⊥
-- as having undistributed conjunctions.

/--
  Checks if a pattern has any disjunctions applied to a conjunction.

  Returns "false" if all disjunctions are distributed over conjunctions.

  If it returns "true", it does not necessarily mean that there *was* an undistributed disjunction.
  See comment above.
-/
@[simp]
def isUndistributed : Pattern 𝕊 → Bool
| φ ⋁ ψ => (isConjunction φ || isConjunction ψ) || (isUndistributed φ || isUndistributed ψ)
-- conjunction matches as implication, treat it separately:
| φ ⋀ ψ => isUndistributed φ || isUndistributed ψ
| φ ⇒ ψ => isUndistributed φ || isUndistributed ψ
| _     => false


#eval (∼∼ML.Pattern.symbol 0) = (ML.Pattern.symbol 0 ⋁ ML.Pattern.bottom)
#eval (∼∼(ML.Pattern.symbol 0 ⋀ ML.Pattern.symbol 1)) = ((ML.Pattern.symbol 0 ⋀ ML.Pattern.symbol 1) ⋁ ML.Pattern.bottom)
#eval isUndistributed (∼∼(ML.Pattern.symbol 0 ⋀ ML.Pattern.symbol 1))   -- expecting false
                                                                          -- as discussed, returns true
#eval isUndistributed ((ML.Pattern.symbol 0 ⋀ ML.Pattern.symbol 1) ⋁ ML.Pattern.bottom)
                                                                          -- expecting true, returns true
#eval isUndistributed (ML.Pattern.bottom ⋁ (ML.Pattern.symbol 0 ⋀ ML.Pattern.symbol 1))
                                                                          -- expecting true, returns true

/--
  Debugging function. Checks if a formula is undistributed, and also returns one disjunction not distributed.
-/
def Undis : Pattern 𝕊 → Option (Pattern 𝕊)
| ∼∼φ   => Undis φ
| φ ⋁ ψ =>
  if (isConjunction φ || isConjunction ψ)
  then some (φ ⋁ ψ)
  else Undis φ <|> Undis ψ
| φ ⇒ ψ => Undis φ <|> Undis ψ
| _     => none

@[simp]
def cnfConditions (φ : Pattern 𝕊) := !(hasArrows φ) && !(hasNegations φ) && !(isUndistributed φ)
