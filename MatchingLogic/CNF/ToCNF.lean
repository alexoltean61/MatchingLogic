import MatchingLogic.CNF.Correctness

namespace ML
namespace Pattern

/-
  Here, we define the translation of arbitrary formulas to CNF.

  Our aim is to ensure we can easily prove, axiomatically,
    that this translation renders equivalent formulas.

  So we define three helper functions:
    toConjunctive; removeNegations; distribute
  Each of these rewrites formulas according to propositional equivalences.

  toCNF will be the composition of these three functions.
-/

/--
  Helper function for induction / recursion on conjunctive patterns.
-/
def conjunctiveSizeOf : Pattern 𝕊 → ℕ
| φ ⋀ ψ => conjunctiveSizeOf φ + conjunctiveSizeOf ψ + 1
| φ ⋁ ψ => conjunctiveSizeOf φ + conjunctiveSizeOf ψ + 1
| ∼φ    => conjunctiveSizeOf φ + 1
| φ ⇒ ψ => conjunctiveSizeOf φ + conjunctiveSizeOf ψ + 1
| _      => 1

lemma sizeOf1 : conjunctiveSizeOf φ < conjunctiveSizeOf (φ ⋀ ψ) := by
  simp [disjunction, conjunction, negation, conjunctiveSizeOf]
  linarith
lemma sizeOf2 : conjunctiveSizeOf ψ < conjunctiveSizeOf (φ ⋀ ψ) := by
  simp [disjunction, conjunction, negation, conjunctiveSizeOf]
  linarith
lemma sizeOf3 : conjunctiveSizeOf φ < conjunctiveSizeOf (∼∼φ) := by
  simp [disjunction, conjunction, negation, conjunctiveSizeOf]
  linarith
lemma sizeOf4 : conjunctiveSizeOf (φ ⋁ χ) < conjunctiveSizeOf ((ψ ⋀ χ) ⋁ φ) := by
  simp [disjunction, conjunction, negation, conjunctiveSizeOf]
  linarith
lemma sizeOf5 : conjunctiveSizeOf φ < conjunctiveSizeOf (φ ⋁ ψ) := by
  simp [disjunction, conjunction, negation, conjunctiveSizeOf]
  linarith
lemma sizeOf6 : conjunctiveSizeOf ψ < conjunctiveSizeOf (φ ⋁ ψ) := by
  simp [disjunction, conjunction, negation, conjunctiveSizeOf]
  linarith
lemma sizeOf7 : conjunctiveSizeOf (∼φ) < conjunctiveSizeOf (∼(φ ⋁ ψ)) := by
  admit
lemma sizeOf8 : conjunctiveSizeOf (∼ψ) < conjunctiveSizeOf (∼(φ ⋁ ψ)) := by
  admit
lemma sizeOf9 : conjunctiveSizeOf φ < conjunctiveSizeOf (∼(φ ⇒ ψ)) := by
  admit
lemma sizeOf10 : conjunctiveSizeOf (∼ψ) < conjunctiveSizeOf (∼(φ ⇒ ψ)) := by
  admit
lemma sizeOf11 : conjunctiveSizeOf φ < conjunctiveSizeOf (φ ⇒ ψ) := by
  admit
lemma sizeOf12 : conjunctiveSizeOf ψ < conjunctiveSizeOf (φ ⇒ ψ) := by
  admit

/--
  Rewrites a pattern to conjunctive form.
-/
def toConjunctive : Pattern 𝕊 → Pattern 𝕊
| ∼φ    => ∼(toConjunctive φ)
| φ ⋁ ψ => (toConjunctive φ) ⋁ (toConjunctive ψ)
| φ ⇒ ψ => ∼(toConjunctive φ) ⋁ (toConjunctive ψ)
| φ      => φ

/--
  Removes negations in front of non-atomic patterns.

  removeNegations, applied to a formula in conjunctive form, renders
    a formula in negation normal form (NNF).
-/

def removeNegations  : Pattern 𝕊 → Pattern 𝕊
| φ ⋀ ψ    => removeNegations φ ⋀ removeNegations ψ
| ∼∼φ      => removeNegations φ
| φ ⋁ ψ    => removeNegations φ ⋁ removeNegations ψ
| ∼(φ ⋁ ψ) => removeNegations (∼φ) ⋀ removeNegations (∼ψ)
-- ∼(φ ⋀ ψ) is definitionally equal to ∼∼(∼φ ⋁ ∼ψ) and gets matched
--    as double negation
| ∼(φ ⇒ ψ) => removeNegations φ ⋀ removeNegations (∼ψ)
| φ ⇒ ψ    => removeNegations φ ⇒ removeNegations ψ
| φ        => φ
termination_by φ => conjunctiveSizeOf φ
decreasing_by
  repeat admit
/-
  all_goals simp_wf
  . apply sizeOf1
  . apply sizeOf2
  . apply sizeOf3
  . apply sizeOf5
  . apply sizeOf6
  . apply sizeOf7
  . apply sizeOf8
  . apply sizeOf9
  . apply sizeOf10
  . apply sizeOf11
  . apply sizeOf12
-/

/-
  `distDisjoin φ ψ` creates the disjunction between φ and ψ, distributing the
    disjunction over any conjuction
-/
def distDisjoin    : Pattern 𝕊 → Pattern 𝕊 → Pattern 𝕊
| φ, (ψ ⋀ χ) => distDisjoin φ ψ ⋀ distDisjoin φ χ
| (ψ ⋀ χ), φ => distDisjoin φ ψ ⋀ distDisjoin φ χ
| φ, ψ       => φ ⋁ ψ
termination_by φ ψ => conjunctiveSizeOf φ + conjunctiveSizeOf ψ
decreasing_by
  all_goals simp_wf
  . apply sizeOf1
  . apply sizeOf2
  . rw [Nat.add_comm, add_lt_add_iff_right]; apply sizeOf1
  . rw [Nat.add_comm, add_lt_add_iff_right]; apply sizeOf2

/--
  Distributes all disjunctions over conjunctions in a pattern.
-/
def distribute    : Pattern 𝕊 → Pattern 𝕊
| φ ⋀ ψ =>  distribute φ ⋀ distribute ψ
| ∼φ    => ∼distribute φ
| φ ⋁ ψ => distDisjoin (distribute φ) (distribute ψ)
| φ ⇒ ψ => distribute φ ⇒ distribute ψ
| φ     => φ

/--
  Rewrites a pattern to conjunctive normal form (CNF).
-/
def toCNF : Pattern 𝕊 → Pattern 𝕊 :=
  distribute ∘ removeNegations ∘ toConjunctive

#eval CNF (toCNF (ML.Pattern.symbol 0 ⋁ ∼∼ML.Pattern.application (∼∼ML.Pattern.symbol 1 ⇒ ML.Pattern.symbol 2) (ML.Pattern.symbol 0 ⇒ ML.Pattern.symbol 2)))
#eval CNF (toCNF ((ML.Pattern.symbol 0 ⇒ ML.Pattern.symbol 1) ⇒ ∼∼∼∼((ML.Pattern.symbol 1 ⇒ ML.Pattern.symbol 2) ⇒ (ML.Pattern.symbol 0 ⇒ ML.Pattern.symbol 2))))

-- Now, we aim to show that this toCNF translation matches our definition of CNF from the previous section.
--    That is, we seek: isCNF (toCNF φ)
--
-- Here we use the equivalence proved in theorem cnf_iff. We show that the composition of our three helper functions
--    enforces the properties required for a formula to be a CNF.
theorem toConjunctiveIsConjunctive   : ∀ {φ : Pattern 𝕊}, !(hasArrows (toConjunctive φ)) := by
  intro φ
  admit
theorem removeNegationsIsConjunctive : ∀ {φ : Pattern 𝕊}, !(hasArrows φ)    → !(hasArrows (removeNegations φ)) := by
  admit
theorem removeNegationsIsNotNegated  : ∀ {φ : Pattern 𝕊}, !(hasNegations (removeNegations φ)) := by
  admit
theorem distributeIsConjunctive      : ∀ {φ : Pattern 𝕊}, !(hasArrows φ)    → !(hasArrows (distribute φ)) := by
  admit
theorem distributeIsNotNegated       : ∀ {φ : Pattern 𝕊}, !(hasNegations φ) → !(hasNegations (distribute φ)) := by
  admit
theorem distributeDistributes        : ∀ {φ : Pattern 𝕊}, !(isUndistributed (distribute φ)) := by
  admit

theorem toCNFisCNF : ∀ {φ : Pattern 𝕊}, isCNF (toCNF φ) := by
  intro φ
  simp only [toCNF, Function.comp_apply, cnf_iff, cnfConditions, Bool.and_eq_true]
  apply And.intro
  . apply And.intro
    . apply distributeIsConjunctive
      apply removeNegationsIsConjunctive
      apply toConjunctiveIsConjunctive
    . apply distributeIsNotNegated
      apply removeNegationsIsNotNegated
  . apply distributeDistributes

/-
  Next, we want to turn a List of Lists (a CNF) into a pattern.
  The following two definitions aren't exactly good, because they add
    ⋁ ⊥ to all disjunctions, and
    ⋀ ⊤ to all conjunctions.

  So the result of CNFto is no longer in CNF...
-/
def toDisjunction : List (Pattern 𝕊) → Pattern 𝕊
| []      =>  ⊥
| h :: t  =>  h ⋁ toDisjunction t

def toConjunction : List (Pattern 𝕊) → Pattern 𝕊
| []      =>  ⊤
| h :: t  =>  h ⋀ toConjunction t

def CNFto : List (List (Pattern 𝕊)) → Pattern 𝕊 := λ cnf =>
  toConjunction (cnf.map toDisjunction)

-- doesn't work yet:
#eval CNF (CNFto [[ML.Pattern.symbol 0, ML.Pattern.symbol 0],
[ML.Pattern.implication (ML.Pattern.symbol 0) (ML.Pattern.bottom),
ML.Pattern.symbol 1,
ML.Pattern.implication (ML.Pattern.symbol 3) (ML.Pattern.bottom)]])
