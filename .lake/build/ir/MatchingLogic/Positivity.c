// Lean compiler output
// Module: MatchingLogic.Positivity
// Imports: Init MatchingLogic.Pattern MatchingLogic.Substitution Lean.Elab.Tactic
#include <lean/lean.h>
#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wunused-label"
#elif defined(__GNUC__) && !defined(__CLANG__)
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-label"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#endif
#ifdef __cplusplus
extern "C" {
#endif
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__8;
LEAN_EXPORT lean_object* l_ML_Pattern_tacticRepeat__bounded____;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__19;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__5;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_hasPositiveOcc_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__8;
static lean_object* l_ML_Pattern_Hidden__u03c6_u2081___closed__2;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__10;
static lean_object* l_ML_Pattern_tacticAutoposc___closed__1;
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__2(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_hasPositiveOcc_match__1_splitter(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_hasNegativeOcc(lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__7;
lean_object* lean_mk_empty_array_with_capacity(lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__24;
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__6;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__8;
static lean_object* l_ML_Pattern_Hidden__u03c6_u2080___closed__1;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__18;
static lean_object* l_ML_Pattern_Hidden__u03c6_u2081___closed__1;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__11;
LEAN_EXPORT uint8_t l_ML_Pattern_hasBoundPositiveOcc___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__25;
LEAN_EXPORT lean_object* l_ML_Pattern_hasBoundPositiveOcc___rarg___boxed(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_tacticAutopos___closed__5;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__2;
static lean_object* l_ML_Pattern_hasBoundPositiveOcc___rarg___closed__1;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__3;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__5;
static lean_object* l_ML_Pattern_tacticAutoposc___closed__3;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__4;
static lean_object* l_ML_Pattern_Hidden__u03c6_u2083___closed__2;
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__22;
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__17;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__3;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__6;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__14;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__9;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__4;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__7;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__12;
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__10;
LEAN_EXPORT uint8_t l_ML_Pattern_hasPositiveOcc___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_hasNegativeOcc_match__1_splitter(lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__10;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__13;
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__15;
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__4;
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__8;
uint8_t l_Lean_Syntax_isOfKind(lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__17;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__4;
LEAN_EXPORT lean_object* l_ML_Pattern_hasBoundPositiveOcc(lean_object*);
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__12;
LEAN_EXPORT lean_object* l_ML_Pattern_hasFreePositiveOcc(lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__2;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__14;
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__11;
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4(lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Name_mkStr3(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__13;
uint8_t l_instDecidableNot___rarg(uint8_t);
static lean_object* l_ML_Pattern_Hidden__u03c6_u2082___closed__1;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__17;
static lean_object* l_ML_Pattern_tacticAutoposc___closed__5;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_size_match__1_splitter___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_Hidden_X;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__15;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__3;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__1;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__16;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_substEvar_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static uint8_t l_ML_Pattern_isPositive___rarg___closed__2;
lean_object* l_Lean_SourceInfo_fromRef(lean_object*, uint8_t);
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__20;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__7;
lean_object* l_Lean_Syntax_node6(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT uint8_t l_ML_Pattern_hasFreePositiveOcc___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_hasFreePositiveOcc___rarg___boxed(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_hasFreeNegativeOcc___rarg___boxed(lean_object*, lean_object*);
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__7;
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__14;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__2;
static lean_object* l_ML_Pattern_Hidden__u03c6_u2080___closed__2;
static lean_object* l_ML_Pattern_tacticAutopos___closed__1;
LEAN_EXPORT lean_object* l_ML_Pattern_isPositive(lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__19;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__9;
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__11;
static lean_object* l_ML_Pattern_tacticAutopos___closed__3;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_substEvar_match__1_splitter(lean_object*, lean_object*);
lean_object* l_Lean_Syntax_node3(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__16;
LEAN_EXPORT uint8_t l_ML_Pattern_hasFreeNegativeOcc___rarg(lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__23;
static lean_object* l_ML_Pattern_hasBoundPositiveOcc___rarg___closed__2;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_isPositive_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__12;
static lean_object* l_ML_Pattern_tacticAutopos___closed__2;
lean_object* l_Lean_addMacroScope(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_Hidden_Y;
lean_object* l_Lean_Name_str___override(lean_object*, lean_object*);
lean_object* l_Lean_Syntax_node2(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__9;
LEAN_EXPORT lean_object* l_ML_Pattern_hasBoundNegativeOcc___rarg___boxed(lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Syntax_getArg(lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__3;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__13;
static lean_object* l_ML_Pattern_tacticAutoposc___closed__4;
uint8_t l_List_elem___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_Hidden__u03c6_u2082;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__1;
LEAN_EXPORT uint8_t l_ML_Pattern_hasBoundNegativeOcc___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__6;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_isPositive_match__1_splitter(lean_object*, lean_object*);
static lean_object* l_ML_Pattern_tacticAutoposc___closed__2;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__9;
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__13;
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__1;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__8;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_size_match__1_splitter(lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__4;
lean_object* l_Lean_Syntax_node4(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__9;
LEAN_EXPORT lean_object* l_ML_Pattern_hasNegativeOcc___rarg___boxed(lean_object*, lean_object*);
LEAN_EXPORT uint8_t l_ML_Pattern_hasNegativeOcc___rarg(lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__6;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__18;
LEAN_EXPORT lean_object* l_ML_Pattern_Hidden__u03c6_u2081;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__6;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_size_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__5;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_substEvar_match__1_splitter___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__15;
uint8_t lean_nat_dec_eq(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_Hidden__u03c6_u2083;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__10;
static lean_object* l_ML_Pattern_tacticAutopos___closed__4;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__1;
LEAN_EXPORT lean_object* l_ML_Pattern_tacticAutopos;
LEAN_EXPORT lean_object* l_ML_Pattern_hasPositiveOcc___rarg___boxed(lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__11;
lean_object* l_Lean_Syntax_node1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__2;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__1;
static lean_object* l_ML_Pattern_tacticRepeat__bounded_______closed__16;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__15;
static lean_object* l_ML_Pattern_Hidden__u03c6_u2083___closed__4;
LEAN_EXPORT lean_object* l_ML_Pattern_hasBoundNegativeOcc(lean_object*);
lean_object* lean_nat_sub(lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__21;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__27;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__12;
static lean_object* l_ML_Pattern_Hidden__u03c6_u2083___closed__1;
LEAN_EXPORT uint8_t l_ML_Pattern_isPositive___rarg(lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__2;
lean_object* l_Nat_decEq___boxed(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_tacticAutoposc;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_hasNegativeOcc_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__6;
LEAN_EXPORT lean_object* l_ML_Pattern_hasFreeNegativeOcc(lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__3;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__7;
LEAN_EXPORT lean_object* l_ML_Pattern_isPositive___rarg___boxed(lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__4;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__5;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__5;
lean_object* l_Lean_Name_mkStr4(lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_instBEq___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__14;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__7;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__5;
static uint8_t l_ML_Pattern_isPositive___rarg___closed__1;
static lean_object* l_ML_Pattern_Hidden__u03c6_u2083___closed__3;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__20;
LEAN_EXPORT lean_object* l_ML_Pattern_hasPositiveOcc(lean_object*);
lean_object* l_String_toSubstring_x27(lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__26;
LEAN_EXPORT lean_object* l_ML_Pattern_Hidden__u03c6_u2080;
lean_object* l_Nat_repr(lean_object*);
lean_object* l_Lean_Syntax_toNat(lean_object*);
lean_object* l_Lean_Syntax_mkNumLit(lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_size_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8, lean_object* x_9) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 0:
{
lean_object* x_10; lean_object* x_11; 
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
x_10 = lean_ctor_get(x_1, 0);
lean_inc(x_10);
lean_dec(x_1);
x_11 = lean_apply_1(x_2, x_10);
return x_11;
}
case 1:
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_2);
x_12 = lean_ctor_get(x_1, 0);
lean_inc(x_12);
lean_dec(x_1);
x_13 = lean_apply_1(x_3, x_12);
return x_13;
}
case 2:
{
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
lean_inc(x_4);
return x_4;
}
case 3:
{
lean_object* x_14; lean_object* x_15; 
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_3);
lean_dec(x_2);
x_14 = lean_ctor_get(x_1, 0);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_1(x_5, x_14);
return x_15;
}
case 4:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_7, x_16, x_17);
return x_18;
}
case 5:
{
lean_object* x_19; lean_object* x_20; lean_object* x_21; 
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_ctor_get(x_1, 0);
lean_inc(x_19);
x_20 = lean_ctor_get(x_1, 1);
lean_inc(x_20);
lean_dec(x_1);
x_21 = lean_apply_2(x_6, x_19, x_20);
return x_21;
}
case 6:
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_22 = lean_ctor_get(x_1, 0);
lean_inc(x_22);
x_23 = lean_ctor_get(x_1, 1);
lean_inc(x_23);
lean_dec(x_1);
x_24 = lean_apply_2(x_9, x_22, x_23);
return x_24;
}
default: 
{
lean_object* x_25; lean_object* x_26; lean_object* x_27; 
lean_dec(x_9);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_25 = lean_ctor_get(x_1, 0);
lean_inc(x_25);
x_26 = lean_ctor_get(x_1, 1);
lean_inc(x_26);
lean_dec(x_1);
x_27 = lean_apply_2(x_8, x_25, x_26);
return x_27;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_size_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Positivity_0__ML_Pattern_size_match__1_splitter___rarg___boxed), 9, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_size_match__1_splitter___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8, lean_object* x_9) {
_start:
{
lean_object* x_10; 
x_10 = l___private_MatchingLogic_Positivity_0__ML_Pattern_size_match__1_splitter___rarg(x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8, x_9);
lean_dec(x_4);
return x_10;
}
}
LEAN_EXPORT uint8_t l_ML_Pattern_hasPositiveOcc___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 1:
{
lean_object* x_3; uint8_t x_4; 
x_3 = lean_ctor_get(x_1, 0);
x_4 = lean_nat_dec_eq(x_2, x_3);
return x_4;
}
case 4:
{
lean_object* x_5; lean_object* x_6; uint8_t x_7; 
x_5 = lean_ctor_get(x_1, 0);
x_6 = lean_ctor_get(x_1, 1);
x_7 = l_ML_Pattern_hasPositiveOcc___rarg(x_5, x_2);
if (x_7 == 0)
{
x_1 = x_6;
goto _start;
}
else
{
uint8_t x_9; 
x_9 = 1;
return x_9;
}
}
case 5:
{
lean_object* x_10; lean_object* x_11; uint8_t x_12; 
x_10 = lean_ctor_get(x_1, 0);
x_11 = lean_ctor_get(x_1, 1);
x_12 = l_ML_Pattern_hasNegativeOcc___rarg(x_10, x_2);
if (x_12 == 0)
{
x_1 = x_11;
goto _start;
}
else
{
uint8_t x_14; 
x_14 = 1;
return x_14;
}
}
case 6:
{
lean_object* x_15; 
x_15 = lean_ctor_get(x_1, 1);
x_1 = x_15;
goto _start;
}
case 7:
{
lean_object* x_17; 
x_17 = lean_ctor_get(x_1, 1);
x_1 = x_17;
goto _start;
}
default: 
{
uint8_t x_19; 
x_19 = 0;
return x_19;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_hasPositiveOcc(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Pattern_hasPositiveOcc___rarg___boxed), 2, 0);
return x_2;
}
}
LEAN_EXPORT uint8_t l_ML_Pattern_hasNegativeOcc___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 4:
{
lean_object* x_3; lean_object* x_4; uint8_t x_5; 
x_3 = lean_ctor_get(x_1, 0);
x_4 = lean_ctor_get(x_1, 1);
x_5 = l_ML_Pattern_hasNegativeOcc___rarg(x_3, x_2);
if (x_5 == 0)
{
x_1 = x_4;
goto _start;
}
else
{
uint8_t x_7; 
x_7 = 1;
return x_7;
}
}
case 5:
{
lean_object* x_8; lean_object* x_9; uint8_t x_10; 
x_8 = lean_ctor_get(x_1, 0);
x_9 = lean_ctor_get(x_1, 1);
x_10 = l_ML_Pattern_hasPositiveOcc___rarg(x_8, x_2);
if (x_10 == 0)
{
x_1 = x_9;
goto _start;
}
else
{
uint8_t x_12; 
x_12 = 1;
return x_12;
}
}
case 6:
{
lean_object* x_13; 
x_13 = lean_ctor_get(x_1, 1);
x_1 = x_13;
goto _start;
}
case 7:
{
lean_object* x_15; 
x_15 = lean_ctor_get(x_1, 1);
x_1 = x_15;
goto _start;
}
default: 
{
uint8_t x_17; 
x_17 = 0;
return x_17;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_hasNegativeOcc(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Pattern_hasNegativeOcc___rarg___boxed), 2, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_hasPositiveOcc___rarg___boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; lean_object* x_4; 
x_3 = l_ML_Pattern_hasPositiveOcc___rarg(x_1, x_2);
lean_dec(x_2);
lean_dec(x_1);
x_4 = lean_box(x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_hasNegativeOcc___rarg___boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; lean_object* x_4; 
x_3 = l_ML_Pattern_hasNegativeOcc___rarg(x_1, x_2);
lean_dec(x_2);
lean_dec(x_1);
x_4 = lean_box(x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_hasPositiveOcc_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 1:
{
lean_object* x_8; lean_object* x_9; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_8 = lean_ctor_get(x_1, 0);
lean_inc(x_8);
lean_dec(x_1);
x_9 = lean_apply_1(x_3, x_8);
return x_9;
}
case 4:
{
lean_object* x_10; lean_object* x_11; lean_object* x_12; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_10 = lean_ctor_get(x_1, 0);
lean_inc(x_10);
x_11 = lean_ctor_get(x_1, 1);
lean_inc(x_11);
lean_dec(x_1);
x_12 = lean_apply_2(x_4, x_10, x_11);
return x_12;
}
case 5:
{
lean_object* x_13; lean_object* x_14; lean_object* x_15; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_13 = lean_ctor_get(x_1, 0);
lean_inc(x_13);
x_14 = lean_ctor_get(x_1, 1);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_2(x_2, x_13, x_14);
return x_15;
}
case 6:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_7);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_6, x_16, x_17);
return x_18;
}
case 7:
{
lean_object* x_19; lean_object* x_20; lean_object* x_21; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_ctor_get(x_1, 0);
lean_inc(x_19);
x_20 = lean_ctor_get(x_1, 1);
lean_inc(x_20);
lean_dec(x_1);
x_21 = lean_apply_2(x_5, x_19, x_20);
return x_21;
}
default: 
{
lean_object* x_22; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_22 = lean_apply_6(x_7, x_1, lean_box(0), lean_box(0), lean_box(0), lean_box(0), lean_box(0));
return x_22;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_hasPositiveOcc_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Positivity_0__ML_Pattern_hasPositiveOcc_match__1_splitter___rarg), 7, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_hasNegativeOcc_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 4:
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_7 = lean_ctor_get(x_1, 0);
lean_inc(x_7);
x_8 = lean_ctor_get(x_1, 1);
lean_inc(x_8);
lean_dec(x_1);
x_9 = lean_apply_2(x_3, x_7, x_8);
return x_9;
}
case 5:
{
lean_object* x_10; lean_object* x_11; lean_object* x_12; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_10 = lean_ctor_get(x_1, 0);
lean_inc(x_10);
x_11 = lean_ctor_get(x_1, 1);
lean_inc(x_11);
lean_dec(x_1);
x_12 = lean_apply_2(x_2, x_10, x_11);
return x_12;
}
case 6:
{
lean_object* x_13; lean_object* x_14; lean_object* x_15; 
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_13 = lean_ctor_get(x_1, 0);
lean_inc(x_13);
x_14 = lean_ctor_get(x_1, 1);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_2(x_5, x_13, x_14);
return x_15;
}
case 7:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_4, x_16, x_17);
return x_18;
}
default: 
{
lean_object* x_19; 
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_apply_5(x_6, x_1, lean_box(0), lean_box(0), lean_box(0), lean_box(0));
return x_19;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_hasNegativeOcc_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Positivity_0__ML_Pattern_hasNegativeOcc_match__1_splitter___rarg), 6, 0);
return x_3;
}
}
LEAN_EXPORT uint8_t l_ML_Pattern_hasFreePositiveOcc___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 1:
{
lean_object* x_3; uint8_t x_4; 
x_3 = lean_ctor_get(x_1, 0);
x_4 = lean_nat_dec_eq(x_2, x_3);
return x_4;
}
case 4:
{
lean_object* x_5; lean_object* x_6; uint8_t x_7; 
x_5 = lean_ctor_get(x_1, 0);
x_6 = lean_ctor_get(x_1, 1);
x_7 = l_ML_Pattern_hasFreePositiveOcc___rarg(x_5, x_2);
if (x_7 == 0)
{
x_1 = x_6;
goto _start;
}
else
{
uint8_t x_9; 
x_9 = 1;
return x_9;
}
}
case 5:
{
lean_object* x_10; lean_object* x_11; uint8_t x_12; 
x_10 = lean_ctor_get(x_1, 0);
x_11 = lean_ctor_get(x_1, 1);
x_12 = l_ML_Pattern_hasFreeNegativeOcc___rarg(x_10, x_2);
if (x_12 == 0)
{
x_1 = x_11;
goto _start;
}
else
{
uint8_t x_14; 
x_14 = 1;
return x_14;
}
}
case 6:
{
lean_object* x_15; 
x_15 = lean_ctor_get(x_1, 1);
x_1 = x_15;
goto _start;
}
case 7:
{
lean_object* x_17; lean_object* x_18; uint8_t x_19; 
x_17 = lean_ctor_get(x_1, 0);
x_18 = lean_ctor_get(x_1, 1);
x_19 = lean_nat_dec_eq(x_2, x_17);
if (x_19 == 0)
{
x_1 = x_18;
goto _start;
}
else
{
uint8_t x_21; 
x_21 = 0;
return x_21;
}
}
default: 
{
uint8_t x_22; 
x_22 = 0;
return x_22;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_hasFreePositiveOcc(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Pattern_hasFreePositiveOcc___rarg___boxed), 2, 0);
return x_2;
}
}
LEAN_EXPORT uint8_t l_ML_Pattern_hasFreeNegativeOcc___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 4:
{
lean_object* x_3; lean_object* x_4; uint8_t x_5; 
x_3 = lean_ctor_get(x_1, 0);
x_4 = lean_ctor_get(x_1, 1);
x_5 = l_ML_Pattern_hasFreeNegativeOcc___rarg(x_3, x_2);
if (x_5 == 0)
{
x_1 = x_4;
goto _start;
}
else
{
uint8_t x_7; 
x_7 = 1;
return x_7;
}
}
case 5:
{
lean_object* x_8; lean_object* x_9; uint8_t x_10; 
x_8 = lean_ctor_get(x_1, 0);
x_9 = lean_ctor_get(x_1, 1);
x_10 = l_ML_Pattern_hasFreePositiveOcc___rarg(x_8, x_2);
if (x_10 == 0)
{
x_1 = x_9;
goto _start;
}
else
{
uint8_t x_12; 
x_12 = 1;
return x_12;
}
}
case 6:
{
lean_object* x_13; 
x_13 = lean_ctor_get(x_1, 1);
x_1 = x_13;
goto _start;
}
case 7:
{
lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_15 = lean_ctor_get(x_1, 0);
x_16 = lean_ctor_get(x_1, 1);
x_17 = lean_nat_dec_eq(x_2, x_15);
if (x_17 == 0)
{
x_1 = x_16;
goto _start;
}
else
{
uint8_t x_19; 
x_19 = 0;
return x_19;
}
}
default: 
{
uint8_t x_20; 
x_20 = 0;
return x_20;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_hasFreeNegativeOcc(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Pattern_hasFreeNegativeOcc___rarg___boxed), 2, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_hasFreePositiveOcc___rarg___boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; lean_object* x_4; 
x_3 = l_ML_Pattern_hasFreePositiveOcc___rarg(x_1, x_2);
lean_dec(x_2);
lean_dec(x_1);
x_4 = lean_box(x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_hasFreeNegativeOcc___rarg___boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; lean_object* x_4; 
x_3 = l_ML_Pattern_hasFreeNegativeOcc___rarg(x_1, x_2);
lean_dec(x_2);
lean_dec(x_1);
x_4 = lean_box(x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_hasBoundPositiveOcc___rarg___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_alloc_closure((void*)(l_Nat_decEq___boxed), 2, 0);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_hasBoundPositiveOcc___rarg___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern_hasBoundPositiveOcc___rarg___closed__1;
x_2 = lean_alloc_closure((void*)(l_instBEq___rarg), 3, 1);
lean_closure_set(x_2, 0, x_1);
return x_2;
}
}
LEAN_EXPORT uint8_t l_ML_Pattern_hasBoundPositiveOcc___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 1:
{
lean_object* x_4; uint8_t x_5; 
x_4 = lean_ctor_get(x_1, 0);
x_5 = lean_nat_dec_eq(x_2, x_4);
if (x_5 == 0)
{
uint8_t x_6; 
lean_dec(x_3);
lean_dec(x_2);
x_6 = 0;
return x_6;
}
else
{
lean_object* x_7; uint8_t x_8; 
x_7 = l_ML_Pattern_hasBoundPositiveOcc___rarg___closed__2;
x_8 = l_List_elem___rarg(x_7, x_2, x_3);
return x_8;
}
}
case 4:
{
lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_9 = lean_ctor_get(x_1, 0);
x_10 = lean_ctor_get(x_1, 1);
lean_inc(x_3);
lean_inc(x_2);
x_11 = l_ML_Pattern_hasBoundPositiveOcc___rarg(x_9, x_2, x_3);
if (x_11 == 0)
{
x_1 = x_10;
goto _start;
}
else
{
uint8_t x_13; 
lean_dec(x_3);
lean_dec(x_2);
x_13 = 1;
return x_13;
}
}
case 5:
{
lean_object* x_14; lean_object* x_15; uint8_t x_16; 
x_14 = lean_ctor_get(x_1, 0);
x_15 = lean_ctor_get(x_1, 1);
lean_inc(x_3);
lean_inc(x_2);
x_16 = l_ML_Pattern_hasBoundNegativeOcc___rarg(x_14, x_2, x_3);
if (x_16 == 0)
{
x_1 = x_15;
goto _start;
}
else
{
uint8_t x_18; 
lean_dec(x_3);
lean_dec(x_2);
x_18 = 1;
return x_18;
}
}
case 6:
{
lean_object* x_19; 
x_19 = lean_ctor_get(x_1, 1);
x_1 = x_19;
goto _start;
}
case 7:
{
lean_object* x_21; lean_object* x_22; lean_object* x_23; 
x_21 = lean_ctor_get(x_1, 0);
x_22 = lean_ctor_get(x_1, 1);
lean_inc(x_21);
x_23 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_23, 0, x_21);
lean_ctor_set(x_23, 1, x_3);
x_1 = x_22;
x_3 = x_23;
goto _start;
}
default: 
{
uint8_t x_25; 
lean_dec(x_3);
lean_dec(x_2);
x_25 = 0;
return x_25;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_hasBoundPositiveOcc(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Pattern_hasBoundPositiveOcc___rarg___boxed), 3, 0);
return x_2;
}
}
LEAN_EXPORT uint8_t l_ML_Pattern_hasBoundNegativeOcc___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 4:
{
lean_object* x_4; lean_object* x_5; uint8_t x_6; 
x_4 = lean_ctor_get(x_1, 0);
x_5 = lean_ctor_get(x_1, 1);
lean_inc(x_3);
lean_inc(x_2);
x_6 = l_ML_Pattern_hasBoundNegativeOcc___rarg(x_4, x_2, x_3);
if (x_6 == 0)
{
x_1 = x_5;
goto _start;
}
else
{
uint8_t x_8; 
lean_dec(x_3);
lean_dec(x_2);
x_8 = 1;
return x_8;
}
}
case 5:
{
lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_9 = lean_ctor_get(x_1, 0);
x_10 = lean_ctor_get(x_1, 1);
lean_inc(x_3);
lean_inc(x_2);
x_11 = l_ML_Pattern_hasBoundPositiveOcc___rarg(x_9, x_2, x_3);
if (x_11 == 0)
{
x_1 = x_10;
goto _start;
}
else
{
uint8_t x_13; 
lean_dec(x_3);
lean_dec(x_2);
x_13 = 1;
return x_13;
}
}
case 6:
{
lean_object* x_14; lean_object* x_15; 
lean_dec(x_3);
x_14 = lean_ctor_get(x_1, 1);
x_15 = lean_box(0);
x_1 = x_14;
x_3 = x_15;
goto _start;
}
case 7:
{
lean_object* x_17; lean_object* x_18; lean_object* x_19; 
x_17 = lean_ctor_get(x_1, 0);
x_18 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
x_19 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_19, 0, x_17);
lean_ctor_set(x_19, 1, x_3);
x_1 = x_18;
x_3 = x_19;
goto _start;
}
default: 
{
uint8_t x_21; 
lean_dec(x_3);
lean_dec(x_2);
x_21 = 0;
return x_21;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_hasBoundNegativeOcc(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Pattern_hasBoundNegativeOcc___rarg___boxed), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_hasBoundPositiveOcc___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
uint8_t x_4; lean_object* x_5; 
x_4 = l_ML_Pattern_hasBoundPositiveOcc___rarg(x_1, x_2, x_3);
lean_dec(x_1);
x_5 = lean_box(x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_hasBoundNegativeOcc___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
uint8_t x_4; lean_object* x_5; 
x_4 = l_ML_Pattern_hasBoundNegativeOcc___rarg(x_1, x_2, x_3);
lean_dec(x_1);
x_5 = lean_box(x_4);
return x_5;
}
}
static uint8_t _init_l_ML_Pattern_isPositive___rarg___closed__1() {
_start:
{
uint8_t x_1; uint8_t x_2; 
x_1 = 0;
x_2 = l_instDecidableNot___rarg(x_1);
return x_2;
}
}
static uint8_t _init_l_ML_Pattern_isPositive___rarg___closed__2() {
_start:
{
uint8_t x_1; uint8_t x_2; 
x_1 = 1;
x_2 = l_instDecidableNot___rarg(x_1);
return x_2;
}
}
LEAN_EXPORT uint8_t l_ML_Pattern_isPositive___rarg(lean_object* x_1) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 4:
{
lean_object* x_2; lean_object* x_3; uint8_t x_4; 
x_2 = lean_ctor_get(x_1, 0);
x_3 = lean_ctor_get(x_1, 1);
x_4 = l_ML_Pattern_isPositive___rarg(x_2);
if (x_4 == 0)
{
uint8_t x_5; 
x_5 = 0;
return x_5;
}
else
{
x_1 = x_3;
goto _start;
}
}
case 5:
{
lean_object* x_7; lean_object* x_8; uint8_t x_9; 
x_7 = lean_ctor_get(x_1, 0);
x_8 = lean_ctor_get(x_1, 1);
x_9 = l_ML_Pattern_isPositive___rarg(x_7);
if (x_9 == 0)
{
uint8_t x_10; 
x_10 = 0;
return x_10;
}
else
{
x_1 = x_8;
goto _start;
}
}
case 6:
{
lean_object* x_12; 
x_12 = lean_ctor_get(x_1, 1);
x_1 = x_12;
goto _start;
}
case 7:
{
lean_object* x_14; lean_object* x_15; uint8_t x_16; 
x_14 = lean_ctor_get(x_1, 0);
x_15 = lean_ctor_get(x_1, 1);
x_16 = l_ML_Pattern_isPositive___rarg(x_15);
if (x_16 == 0)
{
uint8_t x_17; 
x_17 = 0;
return x_17;
}
else
{
uint8_t x_18; 
x_18 = l_ML_Pattern_hasFreeNegativeOcc___rarg(x_15, x_14);
if (x_18 == 0)
{
uint8_t x_19; 
x_19 = l_ML_Pattern_isPositive___rarg___closed__1;
return x_19;
}
else
{
uint8_t x_20; 
x_20 = l_ML_Pattern_isPositive___rarg___closed__2;
return x_20;
}
}
}
default: 
{
uint8_t x_21; 
x_21 = 1;
return x_21;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_isPositive(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Pattern_isPositive___rarg___boxed), 1, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_isPositive___rarg___boxed(lean_object* x_1) {
_start:
{
uint8_t x_2; lean_object* x_3; 
x_2 = l_ML_Pattern_isPositive___rarg(x_1);
lean_dec(x_1);
x_3 = lean_box(x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_isPositive_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 4:
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_7 = lean_ctor_get(x_1, 0);
lean_inc(x_7);
x_8 = lean_ctor_get(x_1, 1);
lean_inc(x_8);
lean_dec(x_1);
x_9 = lean_apply_2(x_3, x_7, x_8);
return x_9;
}
case 5:
{
lean_object* x_10; lean_object* x_11; lean_object* x_12; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_10 = lean_ctor_get(x_1, 0);
lean_inc(x_10);
x_11 = lean_ctor_get(x_1, 1);
lean_inc(x_11);
lean_dec(x_1);
x_12 = lean_apply_2(x_4, x_10, x_11);
return x_12;
}
case 6:
{
lean_object* x_13; lean_object* x_14; lean_object* x_15; 
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_13 = lean_ctor_get(x_1, 0);
lean_inc(x_13);
x_14 = lean_ctor_get(x_1, 1);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_2(x_5, x_13, x_14);
return x_15;
}
case 7:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_2, x_16, x_17);
return x_18;
}
default: 
{
lean_object* x_19; 
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_apply_5(x_6, x_1, lean_box(0), lean_box(0), lean_box(0), lean_box(0));
return x_19;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_isPositive_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Positivity_0__ML_Pattern_isPositive_match__1_splitter___rarg), 6, 0);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML", 2);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Pattern", 7);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticRepeat_bounded__", 22);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__1;
x_2 = l_ML_Pattern_tacticRepeat__bounded_______closed__2;
x_3 = l_ML_Pattern_tacticRepeat__bounded_______closed__3;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("andthen", 7);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern_tacticRepeat__bounded_______closed__5;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__7() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("repeat_bounded", 14);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__8() {
_start:
{
lean_object* x_1; uint8_t x_2; lean_object* x_3; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__7;
x_2 = 0;
x_3 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set_uint8(x_3, sizeof(void*)*1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__9() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("num", 3);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern_tacticRepeat__bounded_______closed__9;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__10;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__12() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__6;
x_2 = l_ML_Pattern_tacticRepeat__bounded_______closed__8;
x_3 = l_ML_Pattern_tacticRepeat__bounded_______closed__11;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__13() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticSeq", 9);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__14() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern_tacticRepeat__bounded_______closed__13;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__14;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__16() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__6;
x_2 = l_ML_Pattern_tacticRepeat__bounded_______closed__12;
x_3 = l_ML_Pattern_tacticRepeat__bounded_______closed__15;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded_______closed__17() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__4;
x_2 = lean_unsigned_to_nat(1022u);
x_3 = l_ML_Pattern_tacticRepeat__bounded_______closed__16;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_tacticRepeat__bounded____() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__17;
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Lean", 4);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Parser", 6);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Tactic", 6);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern_tacticRepeat__bounded_______closed__13;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("first", 5);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__5;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__7() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("null", 4);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__7;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__9() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("group", 5);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__9;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__11() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("|", 1);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__12() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticSeq1Indented", 18);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__12;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__14() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("paren", 5);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__14;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__16() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("(", 1);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__17() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(")", 1);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__18() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(";", 1);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__19() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("skip", 4);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__20() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__19;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern_tacticRepeat__bounded_______closed__4;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(1u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML_Pattern_tacticRepeat__bounded_______closed__10;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(1);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(2u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__4;
lean_inc(x_15);
x_17 = l_Lean_Syntax_isOfKind(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; lean_object* x_19; 
lean_dec(x_15);
lean_dec(x_9);
lean_dec(x_2);
x_18 = lean_box(1);
x_19 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_19, 0, x_18);
lean_ctor_set(x_19, 1, x_3);
return x_19;
}
else
{
lean_object* x_20; lean_object* x_21; uint8_t x_22; 
x_20 = l_Lean_Syntax_toNat(x_9);
lean_dec(x_9);
x_21 = lean_unsigned_to_nat(0u);
x_22 = lean_nat_dec_eq(x_20, x_21);
if (x_22 == 0)
{
lean_object* x_23; uint8_t x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; lean_object* x_50; lean_object* x_51; lean_object* x_52; lean_object* x_53; lean_object* x_54; lean_object* x_55; lean_object* x_56; lean_object* x_57; lean_object* x_58; lean_object* x_59; lean_object* x_60; lean_object* x_61; lean_object* x_62; lean_object* x_63; 
x_23 = lean_ctor_get(x_2, 5);
lean_inc(x_23);
lean_dec(x_2);
x_24 = 0;
x_25 = l_Lean_SourceInfo_fromRef(x_23, x_24);
x_26 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__5;
lean_inc(x_25);
x_27 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_27, 0, x_25);
lean_ctor_set(x_27, 1, x_26);
x_28 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__11;
lean_inc(x_25);
x_29 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_29, 0, x_25);
lean_ctor_set(x_29, 1, x_28);
x_30 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__16;
lean_inc(x_25);
x_31 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_31, 0, x_25);
lean_ctor_set(x_31, 1, x_30);
x_32 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__17;
lean_inc(x_25);
x_33 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_33, 0, x_25);
lean_ctor_set(x_33, 1, x_32);
x_34 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__15;
lean_inc(x_15);
lean_inc(x_25);
x_35 = l_Lean_Syntax_node3(x_25, x_34, x_31, x_15, x_33);
x_36 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__18;
lean_inc(x_25);
x_37 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_37, 0, x_25);
lean_ctor_set(x_37, 1, x_36);
x_38 = l_ML_Pattern_tacticRepeat__bounded_______closed__7;
lean_inc(x_25);
x_39 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_39, 0, x_25);
lean_ctor_set(x_39, 1, x_38);
x_40 = lean_nat_sub(x_20, x_8);
lean_dec(x_20);
x_41 = l_Nat_repr(x_40);
x_42 = lean_box(2);
x_43 = l_Lean_Syntax_mkNumLit(x_41, x_42);
lean_inc(x_25);
x_44 = l_Lean_Syntax_node3(x_25, x_4, x_39, x_43, x_15);
x_45 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__8;
lean_inc(x_25);
x_46 = l_Lean_Syntax_node3(x_25, x_45, x_35, x_37, x_44);
x_47 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__13;
lean_inc(x_25);
x_48 = l_Lean_Syntax_node1(x_25, x_47, x_46);
lean_inc(x_25);
x_49 = l_Lean_Syntax_node1(x_25, x_16, x_48);
x_50 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__10;
lean_inc(x_29);
lean_inc(x_25);
x_51 = l_Lean_Syntax_node2(x_25, x_50, x_29, x_49);
x_52 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__19;
lean_inc(x_25);
x_53 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_53, 0, x_25);
lean_ctor_set(x_53, 1, x_52);
x_54 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__20;
lean_inc(x_25);
x_55 = l_Lean_Syntax_node1(x_25, x_54, x_53);
lean_inc(x_25);
x_56 = l_Lean_Syntax_node1(x_25, x_45, x_55);
lean_inc(x_25);
x_57 = l_Lean_Syntax_node1(x_25, x_47, x_56);
lean_inc(x_25);
x_58 = l_Lean_Syntax_node1(x_25, x_16, x_57);
lean_inc(x_25);
x_59 = l_Lean_Syntax_node2(x_25, x_50, x_29, x_58);
lean_inc(x_25);
x_60 = l_Lean_Syntax_node2(x_25, x_45, x_51, x_59);
x_61 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__6;
x_62 = l_Lean_Syntax_node2(x_25, x_61, x_27, x_60);
x_63 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_63, 0, x_62);
lean_ctor_set(x_63, 1, x_3);
return x_63;
}
else
{
lean_object* x_64; uint8_t x_65; lean_object* x_66; lean_object* x_67; lean_object* x_68; lean_object* x_69; lean_object* x_70; lean_object* x_71; 
lean_dec(x_20);
lean_dec(x_15);
x_64 = lean_ctor_get(x_2, 5);
lean_inc(x_64);
lean_dec(x_2);
x_65 = 0;
x_66 = l_Lean_SourceInfo_fromRef(x_64, x_65);
x_67 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__19;
lean_inc(x_66);
x_68 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_68, 0, x_66);
lean_ctor_set(x_68, 1, x_67);
x_69 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__20;
x_70 = l_Lean_Syntax_node1(x_66, x_69, x_68);
x_71 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_71, 0, x_70);
lean_ctor_set(x_71, 1, x_3);
return x_71;
}
}
}
}
}
}
static lean_object* _init_l_ML_Pattern_tacticAutopos___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticAutopos", 13);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_tacticAutopos___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__1;
x_2 = l_ML_Pattern_tacticRepeat__bounded_______closed__2;
x_3 = l_ML_Pattern_tacticAutopos___closed__1;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_tacticAutopos___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("autopos", 7);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_tacticAutopos___closed__4() {
_start:
{
lean_object* x_1; uint8_t x_2; lean_object* x_3; 
x_1 = l_ML_Pattern_tacticAutopos___closed__3;
x_2 = 0;
x_3 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set_uint8(x_3, sizeof(void*)*1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_tacticAutopos___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_tacticAutopos___closed__2;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = l_ML_Pattern_tacticAutopos___closed__4;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_tacticAutopos() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_Pattern_tacticAutopos___closed__5;
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("seq1", 4);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__1;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("300", 3);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tactic_<;>_", 11);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__4;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("constructor", 11);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__6;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__8() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("<;>", 3);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__9() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticTry_", 10);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__9;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__11() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("try", 3);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__12() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("assumption", 10);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__12;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__14() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("done", 4);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__14;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern_tacticAutopos___closed__2;
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; lean_object* x_50; lean_object* x_51; lean_object* x_52; lean_object* x_53; lean_object* x_54; lean_object* x_55; lean_object* x_56; lean_object* x_57; lean_object* x_58; lean_object* x_59; lean_object* x_60; lean_object* x_61; lean_object* x_62; lean_object* x_63; lean_object* x_64; lean_object* x_65; lean_object* x_66; 
x_8 = lean_ctor_get(x_2, 5);
lean_inc(x_8);
lean_dec(x_2);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__16;
lean_inc(x_10);
x_12 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_11);
x_13 = l_ML_Pattern_tacticRepeat__bounded_______closed__7;
lean_inc(x_10);
x_14 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_13);
x_15 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__3;
lean_inc(x_10);
x_16 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_16, 0, x_10);
lean_ctor_set(x_16, 1, x_15);
x_17 = l_ML_Pattern_tacticRepeat__bounded_______closed__10;
lean_inc(x_10);
x_18 = l_Lean_Syntax_node1(x_10, x_17, x_16);
x_19 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__6;
lean_inc(x_10);
x_20 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_20, 0, x_10);
lean_ctor_set(x_20, 1, x_19);
x_21 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__7;
lean_inc(x_10);
x_22 = l_Lean_Syntax_node1(x_10, x_21, x_20);
x_23 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__8;
lean_inc(x_10);
x_24 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_24, 0, x_10);
lean_ctor_set(x_24, 1, x_23);
x_25 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__11;
lean_inc(x_10);
x_26 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_26, 0, x_10);
lean_ctor_set(x_26, 1, x_25);
x_27 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__12;
lean_inc(x_10);
x_28 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_28, 0, x_10);
lean_ctor_set(x_28, 1, x_27);
x_29 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__13;
lean_inc(x_10);
x_30 = l_Lean_Syntax_node1(x_10, x_29, x_28);
x_31 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__8;
lean_inc(x_10);
x_32 = l_Lean_Syntax_node1(x_10, x_31, x_30);
x_33 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__13;
lean_inc(x_10);
x_34 = l_Lean_Syntax_node1(x_10, x_33, x_32);
x_35 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__4;
lean_inc(x_10);
x_36 = l_Lean_Syntax_node1(x_10, x_35, x_34);
x_37 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__10;
lean_inc(x_10);
x_38 = l_Lean_Syntax_node2(x_10, x_37, x_26, x_36);
x_39 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__5;
lean_inc(x_10);
x_40 = l_Lean_Syntax_node3(x_10, x_39, x_22, x_24, x_38);
lean_inc(x_10);
x_41 = l_Lean_Syntax_node1(x_10, x_31, x_40);
lean_inc(x_10);
x_42 = l_Lean_Syntax_node1(x_10, x_33, x_41);
lean_inc(x_10);
x_43 = l_Lean_Syntax_node1(x_10, x_35, x_42);
x_44 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__17;
lean_inc(x_10);
x_45 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_45, 0, x_10);
lean_ctor_set(x_45, 1, x_44);
x_46 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__15;
lean_inc(x_45);
lean_inc(x_12);
lean_inc(x_10);
x_47 = l_Lean_Syntax_node3(x_10, x_46, x_12, x_43, x_45);
lean_inc(x_10);
x_48 = l_Lean_Syntax_node1(x_10, x_31, x_47);
lean_inc(x_10);
x_49 = l_Lean_Syntax_node1(x_10, x_33, x_48);
lean_inc(x_10);
x_50 = l_Lean_Syntax_node1(x_10, x_35, x_49);
x_51 = l_ML_Pattern_tacticRepeat__bounded_______closed__4;
lean_inc(x_10);
x_52 = l_Lean_Syntax_node3(x_10, x_51, x_14, x_18, x_50);
lean_inc(x_10);
x_53 = l_Lean_Syntax_node1(x_10, x_31, x_52);
lean_inc(x_10);
x_54 = l_Lean_Syntax_node1(x_10, x_33, x_53);
lean_inc(x_10);
x_55 = l_Lean_Syntax_node1(x_10, x_35, x_54);
lean_inc(x_10);
x_56 = l_Lean_Syntax_node3(x_10, x_46, x_12, x_55, x_45);
x_57 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__18;
lean_inc(x_10);
x_58 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_58, 0, x_10);
lean_ctor_set(x_58, 1, x_57);
x_59 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__14;
lean_inc(x_10);
x_60 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_60, 0, x_10);
lean_ctor_set(x_60, 1, x_59);
x_61 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__15;
lean_inc(x_10);
x_62 = l_Lean_Syntax_node1(x_10, x_61, x_60);
lean_inc(x_10);
x_63 = l_Lean_Syntax_node3(x_10, x_31, x_56, x_58, x_62);
x_64 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__2;
x_65 = l_Lean_Syntax_node1(x_10, x_64, x_63);
x_66 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_66, 0, x_65);
lean_ctor_set(x_66, 1, x_3);
return x_66;
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__2(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern_tacticAutopos___closed__2;
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; 
x_8 = lean_ctor_get(x_2, 5);
lean_inc(x_8);
lean_dec(x_2);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__12;
lean_inc(x_10);
x_12 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_11);
x_13 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__13;
x_14 = l_Lean_Syntax_node1(x_10, x_13, x_12);
x_15 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_15, 0, x_14);
lean_ctor_set(x_15, 1, x_3);
return x_15;
}
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Mathlib", 7);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticSimp_rw___", 16);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__2;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("simp_rw", 7);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_unsigned_to_nat(0u);
x_2 = lean_mk_empty_array_with_capacity(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("rwRuleSeq", 9);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__6;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__8() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("[", 1);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__9() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("rwRule", 6);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__9;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__11() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("positive_equivalent", 19);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__12() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__11;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__11;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__14() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__1;
x_2 = l_ML_Pattern_tacticRepeat__bounded_______closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__11;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__14;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__16() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__15;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__17() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("]", 1);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__18() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("location", 8);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__19() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__18;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__20() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("at", 2);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__21() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("locationWildcard", 16);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__22() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__21;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__23() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("*", 1);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__24() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("simp", 4);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__25() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__24;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__26() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("simpStar", 8);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__27() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__26;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern_tacticAutopos___closed__2;
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; lean_object* x_50; lean_object* x_51; lean_object* x_52; lean_object* x_53; lean_object* x_54; lean_object* x_55; lean_object* x_56; lean_object* x_57; lean_object* x_58; lean_object* x_59; lean_object* x_60; lean_object* x_61; lean_object* x_62; lean_object* x_63; lean_object* x_64; lean_object* x_65; lean_object* x_66; lean_object* x_67; lean_object* x_68; lean_object* x_69; lean_object* x_70; lean_object* x_71; 
x_8 = lean_ctor_get(x_2, 5);
lean_inc(x_8);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = lean_ctor_get(x_2, 2);
lean_inc(x_11);
x_12 = lean_ctor_get(x_2, 1);
lean_inc(x_12);
lean_dec(x_2);
x_13 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__16;
lean_inc(x_10);
x_14 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_13);
x_15 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__4;
lean_inc(x_10);
x_16 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_16, 0, x_10);
lean_ctor_set(x_16, 1, x_15);
x_17 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__8;
x_18 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__5;
lean_inc(x_10);
x_19 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_19, 0, x_10);
lean_ctor_set(x_19, 1, x_17);
lean_ctor_set(x_19, 2, x_18);
x_20 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__8;
lean_inc(x_10);
x_21 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_21, 0, x_10);
lean_ctor_set(x_21, 1, x_20);
x_22 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__13;
x_23 = l_Lean_addMacroScope(x_12, x_22, x_11);
x_24 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__12;
x_25 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__16;
lean_inc(x_10);
x_26 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_26, 0, x_10);
lean_ctor_set(x_26, 1, x_24);
lean_ctor_set(x_26, 2, x_23);
lean_ctor_set(x_26, 3, x_25);
x_27 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__10;
lean_inc(x_19);
lean_inc(x_10);
x_28 = l_Lean_Syntax_node2(x_10, x_27, x_19, x_26);
lean_inc(x_10);
x_29 = l_Lean_Syntax_node1(x_10, x_17, x_28);
x_30 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__17;
lean_inc(x_10);
x_31 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_31, 0, x_10);
lean_ctor_set(x_31, 1, x_30);
x_32 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__7;
lean_inc(x_31);
lean_inc(x_21);
lean_inc(x_10);
x_33 = l_Lean_Syntax_node3(x_10, x_32, x_21, x_29, x_31);
x_34 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__20;
lean_inc(x_10);
x_35 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_35, 0, x_10);
lean_ctor_set(x_35, 1, x_34);
x_36 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__23;
lean_inc(x_10);
x_37 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_37, 0, x_10);
lean_ctor_set(x_37, 1, x_36);
x_38 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__22;
lean_inc(x_37);
lean_inc(x_10);
x_39 = l_Lean_Syntax_node1(x_10, x_38, x_37);
x_40 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__19;
lean_inc(x_10);
x_41 = l_Lean_Syntax_node2(x_10, x_40, x_35, x_39);
lean_inc(x_10);
x_42 = l_Lean_Syntax_node1(x_10, x_17, x_41);
x_43 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__3;
lean_inc(x_42);
lean_inc(x_19);
lean_inc(x_10);
x_44 = l_Lean_Syntax_node4(x_10, x_43, x_16, x_19, x_33, x_42);
x_45 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__18;
lean_inc(x_10);
x_46 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_46, 0, x_10);
lean_ctor_set(x_46, 1, x_45);
x_47 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__24;
lean_inc(x_10);
x_48 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_48, 0, x_10);
lean_ctor_set(x_48, 1, x_47);
x_49 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__27;
lean_inc(x_10);
x_50 = l_Lean_Syntax_node1(x_10, x_49, x_37);
lean_inc(x_10);
x_51 = l_Lean_Syntax_node1(x_10, x_17, x_50);
lean_inc(x_10);
x_52 = l_Lean_Syntax_node3(x_10, x_17, x_21, x_51, x_31);
x_53 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__25;
lean_inc_n(x_19, 2);
lean_inc(x_10);
x_54 = l_Lean_Syntax_node6(x_10, x_53, x_48, x_19, x_19, x_19, x_52, x_42);
lean_inc(x_46);
lean_inc(x_10);
x_55 = l_Lean_Syntax_node3(x_10, x_17, x_44, x_46, x_54);
x_56 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__13;
lean_inc(x_10);
x_57 = l_Lean_Syntax_node1(x_10, x_56, x_55);
x_58 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__4;
lean_inc(x_10);
x_59 = l_Lean_Syntax_node1(x_10, x_58, x_57);
x_60 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__17;
lean_inc(x_10);
x_61 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_61, 0, x_10);
lean_ctor_set(x_61, 1, x_60);
x_62 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__15;
lean_inc(x_10);
x_63 = l_Lean_Syntax_node3(x_10, x_62, x_14, x_59, x_61);
x_64 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__14;
lean_inc(x_10);
x_65 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_65, 0, x_10);
lean_ctor_set(x_65, 1, x_64);
x_66 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__15;
lean_inc(x_10);
x_67 = l_Lean_Syntax_node1(x_10, x_66, x_65);
lean_inc(x_10);
x_68 = l_Lean_Syntax_node3(x_10, x_17, x_63, x_46, x_67);
x_69 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__2;
x_70 = l_Lean_Syntax_node1(x_10, x_69, x_68);
x_71 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_71, 0, x_70);
lean_ctor_set(x_71, 1, x_3);
return x_71;
}
}
}
static lean_object* _init_l_ML_Pattern_tacticAutoposc___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticAutoposc", 14);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_tacticAutoposc___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__1;
x_2 = l_ML_Pattern_tacticRepeat__bounded_______closed__2;
x_3 = l_ML_Pattern_tacticAutoposc___closed__1;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_tacticAutoposc___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("autoposc", 8);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_tacticAutoposc___closed__4() {
_start:
{
lean_object* x_1; uint8_t x_2; lean_object* x_3; 
x_1 = l_ML_Pattern_tacticAutoposc___closed__3;
x_2 = 0;
x_3 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set_uint8(x_3, sizeof(void*)*1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_tacticAutoposc___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_tacticAutoposc___closed__2;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = l_ML_Pattern_tacticAutoposc___closed__4;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_tacticAutoposc() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_Pattern_tacticAutoposc___closed__5;
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("apply", 5);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__1;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML.AppContext.wf_insert", 23);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__3;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("AppContext", 10);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("wf_insert", 9);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__5;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__6;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern_tacticAutoposc___closed__2;
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; lean_object* x_50; lean_object* x_51; 
x_8 = lean_ctor_get(x_2, 5);
lean_inc(x_8);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = lean_ctor_get(x_2, 2);
lean_inc(x_11);
x_12 = lean_ctor_get(x_2, 1);
lean_inc(x_12);
lean_dec(x_2);
x_13 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__16;
lean_inc(x_10);
x_14 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_13);
x_15 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__1;
lean_inc(x_10);
x_16 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_16, 0, x_10);
lean_ctor_set(x_16, 1, x_15);
x_17 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__7;
x_18 = l_Lean_addMacroScope(x_12, x_17, x_11);
x_19 = lean_box(0);
x_20 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__4;
lean_inc(x_10);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_10);
lean_ctor_set(x_21, 1, x_20);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_19);
x_22 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__2;
lean_inc(x_10);
x_23 = l_Lean_Syntax_node2(x_10, x_22, x_16, x_21);
x_24 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__8;
lean_inc(x_10);
x_25 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_25, 0, x_10);
lean_ctor_set(x_25, 1, x_24);
x_26 = l_ML_Pattern_tacticAutopos___closed__3;
lean_inc(x_10);
x_27 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_27, 0, x_10);
lean_ctor_set(x_27, 1, x_26);
x_28 = l_ML_Pattern_tacticAutopos___closed__2;
lean_inc(x_10);
x_29 = l_Lean_Syntax_node1(x_10, x_28, x_27);
x_30 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__5;
lean_inc(x_10);
x_31 = l_Lean_Syntax_node3(x_10, x_30, x_23, x_25, x_29);
x_32 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__8;
lean_inc(x_10);
x_33 = l_Lean_Syntax_node1(x_10, x_32, x_31);
x_34 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__13;
lean_inc(x_10);
x_35 = l_Lean_Syntax_node1(x_10, x_34, x_33);
x_36 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__4;
lean_inc(x_10);
x_37 = l_Lean_Syntax_node1(x_10, x_36, x_35);
x_38 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__17;
lean_inc(x_10);
x_39 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_39, 0, x_10);
lean_ctor_set(x_39, 1, x_38);
x_40 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__15;
lean_inc(x_10);
x_41 = l_Lean_Syntax_node3(x_10, x_40, x_14, x_37, x_39);
x_42 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__18;
lean_inc(x_10);
x_43 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_43, 0, x_10);
lean_ctor_set(x_43, 1, x_42);
x_44 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__14;
lean_inc(x_10);
x_45 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_45, 0, x_10);
lean_ctor_set(x_45, 1, x_44);
x_46 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__15;
lean_inc(x_10);
x_47 = l_Lean_Syntax_node1(x_10, x_46, x_45);
lean_inc(x_10);
x_48 = l_Lean_Syntax_node3(x_10, x_32, x_41, x_43, x_47);
x_49 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__2;
x_50 = l_Lean_Syntax_node1(x_10, x_49, x_48);
x_51 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_51, 0, x_50);
lean_ctor_set(x_51, 1, x_3);
return x_51;
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_substEvar_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8, lean_object* x_9) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 0:
{
lean_object* x_10; lean_object* x_11; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_10 = lean_ctor_get(x_1, 0);
lean_inc(x_10);
lean_dec(x_1);
x_11 = lean_apply_1(x_2, x_10);
return x_11;
}
case 1:
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_12 = lean_ctor_get(x_1, 0);
lean_inc(x_12);
lean_dec(x_1);
x_13 = lean_apply_1(x_3, x_12);
return x_13;
}
case 2:
{
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
lean_inc(x_9);
return x_9;
}
case 3:
{
lean_object* x_14; lean_object* x_15; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_14 = lean_ctor_get(x_1, 0);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_1(x_8, x_14);
return x_15;
}
case 4:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_6, x_16, x_17);
return x_18;
}
case 5:
{
lean_object* x_19; lean_object* x_20; lean_object* x_21; 
lean_dec(x_8);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_ctor_get(x_1, 0);
lean_inc(x_19);
x_20 = lean_ctor_get(x_1, 1);
lean_inc(x_20);
lean_dec(x_1);
x_21 = lean_apply_2(x_7, x_19, x_20);
return x_21;
}
case 6:
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_22 = lean_ctor_get(x_1, 0);
lean_inc(x_22);
x_23 = lean_ctor_get(x_1, 1);
lean_inc(x_23);
lean_dec(x_1);
x_24 = lean_apply_2(x_4, x_22, x_23);
return x_24;
}
default: 
{
lean_object* x_25; lean_object* x_26; lean_object* x_27; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_25 = lean_ctor_get(x_1, 0);
lean_inc(x_25);
x_26 = lean_ctor_get(x_1, 1);
lean_inc(x_26);
lean_dec(x_1);
x_27 = lean_apply_2(x_5, x_25, x_26);
return x_27;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_substEvar_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Positivity_0__ML_Pattern_substEvar_match__1_splitter___rarg___boxed), 9, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Positivity_0__ML_Pattern_substEvar_match__1_splitter___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8, lean_object* x_9) {
_start:
{
lean_object* x_10; 
x_10 = l___private_MatchingLogic_Positivity_0__ML_Pattern_substEvar_match__1_splitter___rarg(x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8, x_9);
lean_dec(x_9);
return x_10;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(",", 1);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("simpLemma", 9);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__2;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("positive_subst_evar", 19);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__4;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__4;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_tacticRepeat__bounded_______closed__1;
x_2 = l_ML_Pattern_tacticRepeat__bounded_______closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__4;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__8;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern_tacticAutopos___closed__2;
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; lean_object* x_50; lean_object* x_51; lean_object* x_52; lean_object* x_53; lean_object* x_54; lean_object* x_55; 
x_8 = lean_ctor_get(x_2, 5);
lean_inc(x_8);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = lean_ctor_get(x_2, 2);
lean_inc(x_11);
x_12 = lean_ctor_get(x_2, 1);
lean_inc(x_12);
lean_dec(x_2);
x_13 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__24;
lean_inc(x_10);
x_14 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_13);
x_15 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__8;
x_16 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__5;
lean_inc(x_10);
x_17 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_17, 0, x_10);
lean_ctor_set(x_17, 1, x_15);
lean_ctor_set(x_17, 2, x_16);
x_18 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__8;
lean_inc(x_10);
x_19 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_19, 0, x_10);
lean_ctor_set(x_19, 1, x_18);
x_20 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__23;
lean_inc(x_10);
x_21 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_21, 0, x_10);
lean_ctor_set(x_21, 1, x_20);
x_22 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__27;
lean_inc(x_21);
lean_inc(x_10);
x_23 = l_Lean_Syntax_node1(x_10, x_22, x_21);
x_24 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__1;
lean_inc(x_10);
x_25 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_25, 0, x_10);
lean_ctor_set(x_25, 1, x_24);
x_26 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__6;
x_27 = l_Lean_addMacroScope(x_12, x_26, x_11);
x_28 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__5;
x_29 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__9;
lean_inc(x_10);
x_30 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_30, 0, x_10);
lean_ctor_set(x_30, 1, x_28);
lean_ctor_set(x_30, 2, x_27);
lean_ctor_set(x_30, 3, x_29);
x_31 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__3;
lean_inc_n(x_17, 2);
lean_inc(x_10);
x_32 = l_Lean_Syntax_node3(x_10, x_31, x_17, x_17, x_30);
lean_inc(x_10);
x_33 = l_Lean_Syntax_node3(x_10, x_15, x_23, x_25, x_32);
x_34 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__17;
lean_inc(x_10);
x_35 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_35, 0, x_10);
lean_ctor_set(x_35, 1, x_34);
lean_inc(x_10);
x_36 = l_Lean_Syntax_node3(x_10, x_15, x_19, x_33, x_35);
x_37 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__20;
lean_inc(x_10);
x_38 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_38, 0, x_10);
lean_ctor_set(x_38, 1, x_37);
x_39 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__22;
lean_inc(x_10);
x_40 = l_Lean_Syntax_node1(x_10, x_39, x_21);
x_41 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__19;
lean_inc(x_10);
x_42 = l_Lean_Syntax_node2(x_10, x_41, x_38, x_40);
lean_inc(x_10);
x_43 = l_Lean_Syntax_node1(x_10, x_15, x_42);
x_44 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__25;
lean_inc_n(x_17, 2);
lean_inc(x_10);
x_45 = l_Lean_Syntax_node6(x_10, x_44, x_14, x_17, x_17, x_17, x_36, x_43);
x_46 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__18;
lean_inc(x_10);
x_47 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_47, 0, x_10);
lean_ctor_set(x_47, 1, x_46);
x_48 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__14;
lean_inc(x_10);
x_49 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_49, 0, x_10);
lean_ctor_set(x_49, 1, x_48);
x_50 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__15;
lean_inc(x_10);
x_51 = l_Lean_Syntax_node1(x_10, x_50, x_49);
lean_inc(x_10);
x_52 = l_Lean_Syntax_node3(x_10, x_15, x_45, x_47, x_51);
x_53 = l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__2;
x_54 = l_Lean_Syntax_node1(x_10, x_53, x_52);
x_55 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_55, 0, x_54);
lean_ctor_set(x_55, 1, x_3);
return x_55;
}
}
}
static lean_object* _init_l_ML_Pattern_Hidden_X() {
_start:
{
lean_object* x_1; 
x_1 = lean_unsigned_to_nat(0u);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_Hidden_Y() {
_start:
{
lean_object* x_1; 
x_1 = lean_unsigned_to_nat(1u);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2080___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern_Hidden_X;
x_2 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2080___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_Pattern_Hidden_X;
x_2 = l_ML_Pattern_Hidden__u03c6_u2080___closed__1;
x_3 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2080() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_Pattern_Hidden__u03c6_u2080___closed__2;
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2081___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(2);
x_2 = l_ML_Pattern_Hidden__u03c6_u2080___closed__1;
x_3 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2081___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_Pattern_Hidden_X;
x_2 = l_ML_Pattern_Hidden__u03c6_u2081___closed__1;
x_3 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2081() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_Pattern_Hidden__u03c6_u2081___closed__2;
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2082___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(2);
x_2 = l_ML_Pattern_Hidden__u03c6_u2081___closed__2;
x_3 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2082() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_Pattern_Hidden__u03c6_u2082___closed__1;
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2083___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern_Hidden_Y;
x_2 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2083___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_Pattern_Hidden__u03c6_u2083___closed__1;
x_2 = l_ML_Pattern_Hidden__u03c6_u2080___closed__1;
x_3 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2083___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_Pattern_Hidden_Y;
x_2 = l_ML_Pattern_Hidden__u03c6_u2083___closed__2;
x_3 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2083___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_Pattern_Hidden_X;
x_2 = l_ML_Pattern_Hidden__u03c6_u2083___closed__3;
x_3 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_Hidden__u03c6_u2083() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_Pattern_Hidden__u03c6_u2083___closed__4;
return x_1;
}
}
lean_object* initialize_Init(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Pattern(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Substitution(uint8_t builtin, lean_object*);
lean_object* initialize_Lean_Elab_Tactic(uint8_t builtin, lean_object*);
static bool _G_initialized = false;
LEAN_EXPORT lean_object* initialize_MatchingLogic_Positivity(uint8_t builtin, lean_object* w) {
lean_object * res;
if (_G_initialized) return lean_io_result_mk_ok(lean_box(0));
_G_initialized = true;
res = initialize_Init(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Pattern(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Substitution(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_Lean_Elab_Tactic(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
l_ML_Pattern_hasBoundPositiveOcc___rarg___closed__1 = _init_l_ML_Pattern_hasBoundPositiveOcc___rarg___closed__1();
lean_mark_persistent(l_ML_Pattern_hasBoundPositiveOcc___rarg___closed__1);
l_ML_Pattern_hasBoundPositiveOcc___rarg___closed__2 = _init_l_ML_Pattern_hasBoundPositiveOcc___rarg___closed__2();
lean_mark_persistent(l_ML_Pattern_hasBoundPositiveOcc___rarg___closed__2);
l_ML_Pattern_isPositive___rarg___closed__1 = _init_l_ML_Pattern_isPositive___rarg___closed__1();
l_ML_Pattern_isPositive___rarg___closed__2 = _init_l_ML_Pattern_isPositive___rarg___closed__2();
l_ML_Pattern_tacticRepeat__bounded_______closed__1 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__1();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__1);
l_ML_Pattern_tacticRepeat__bounded_______closed__2 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__2();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__2);
l_ML_Pattern_tacticRepeat__bounded_______closed__3 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__3();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__3);
l_ML_Pattern_tacticRepeat__bounded_______closed__4 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__4();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__4);
l_ML_Pattern_tacticRepeat__bounded_______closed__5 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__5();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__5);
l_ML_Pattern_tacticRepeat__bounded_______closed__6 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__6();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__6);
l_ML_Pattern_tacticRepeat__bounded_______closed__7 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__7();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__7);
l_ML_Pattern_tacticRepeat__bounded_______closed__8 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__8();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__8);
l_ML_Pattern_tacticRepeat__bounded_______closed__9 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__9();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__9);
l_ML_Pattern_tacticRepeat__bounded_______closed__10 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__10();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__10);
l_ML_Pattern_tacticRepeat__bounded_______closed__11 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__11();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__11);
l_ML_Pattern_tacticRepeat__bounded_______closed__12 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__12();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__12);
l_ML_Pattern_tacticRepeat__bounded_______closed__13 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__13();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__13);
l_ML_Pattern_tacticRepeat__bounded_______closed__14 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__14();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__14);
l_ML_Pattern_tacticRepeat__bounded_______closed__15 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__15();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__15);
l_ML_Pattern_tacticRepeat__bounded_______closed__16 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__16();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__16);
l_ML_Pattern_tacticRepeat__bounded_______closed__17 = _init_l_ML_Pattern_tacticRepeat__bounded_______closed__17();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded_______closed__17);
l_ML_Pattern_tacticRepeat__bounded____ = _init_l_ML_Pattern_tacticRepeat__bounded____();
lean_mark_persistent(l_ML_Pattern_tacticRepeat__bounded____);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__1);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__2);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__3);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__4 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__4();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__4);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__5 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__5();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__5);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__6 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__6();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__6);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__7 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__7();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__7);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__8 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__8();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__8);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__9 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__9();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__9);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__10 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__10();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__10);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__11 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__11();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__11);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__12 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__12();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__12);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__13 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__13();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__13);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__14 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__14();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__14);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__15 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__15();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__15);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__16 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__16();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__16);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__17 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__17();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__17);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__18 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__18();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__18);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__19 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__19();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__19);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__20 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__20();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticRepeat__bounded______1___closed__20);
l_ML_Pattern_tacticAutopos___closed__1 = _init_l_ML_Pattern_tacticAutopos___closed__1();
lean_mark_persistent(l_ML_Pattern_tacticAutopos___closed__1);
l_ML_Pattern_tacticAutopos___closed__2 = _init_l_ML_Pattern_tacticAutopos___closed__2();
lean_mark_persistent(l_ML_Pattern_tacticAutopos___closed__2);
l_ML_Pattern_tacticAutopos___closed__3 = _init_l_ML_Pattern_tacticAutopos___closed__3();
lean_mark_persistent(l_ML_Pattern_tacticAutopos___closed__3);
l_ML_Pattern_tacticAutopos___closed__4 = _init_l_ML_Pattern_tacticAutopos___closed__4();
lean_mark_persistent(l_ML_Pattern_tacticAutopos___closed__4);
l_ML_Pattern_tacticAutopos___closed__5 = _init_l_ML_Pattern_tacticAutopos___closed__5();
lean_mark_persistent(l_ML_Pattern_tacticAutopos___closed__5);
l_ML_Pattern_tacticAutopos = _init_l_ML_Pattern_tacticAutopos();
lean_mark_persistent(l_ML_Pattern_tacticAutopos);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__1 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__1();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__1);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__2 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__2();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__2);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__3 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__3();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__3);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__4 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__4();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__4);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__5 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__5();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__5);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__6 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__6();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__6);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__7 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__7();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__7);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__8 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__8();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__8);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__9 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__9();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__9);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__10 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__10();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__10);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__11 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__11();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__11);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__12 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__12();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__12);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__13 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__13();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__13);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__14 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__14();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__14);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__15 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__15();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__1___closed__15);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__1 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__1();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__1);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__2 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__2();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__2);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__3 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__3();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__3);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__4 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__4();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__4);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__5 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__5();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__5);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__6 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__6();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__6);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__7 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__7();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__7);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__8 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__8();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__8);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__9 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__9();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__9);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__10 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__10();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__10);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__11 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__11();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__11);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__12 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__12();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__12);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__13 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__13();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__13);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__14 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__14();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__14);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__15 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__15();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__15);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__16 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__16();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__16);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__17 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__17();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__17);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__18 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__18();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__18);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__19 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__19();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__19);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__20 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__20();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__20);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__21 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__21();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__21);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__22 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__22();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__22);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__23 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__23();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__23);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__24 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__24();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__24);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__25 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__25();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__25);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__26 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__26();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__26);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__27 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__27();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__3___closed__27);
l_ML_Pattern_tacticAutoposc___closed__1 = _init_l_ML_Pattern_tacticAutoposc___closed__1();
lean_mark_persistent(l_ML_Pattern_tacticAutoposc___closed__1);
l_ML_Pattern_tacticAutoposc___closed__2 = _init_l_ML_Pattern_tacticAutoposc___closed__2();
lean_mark_persistent(l_ML_Pattern_tacticAutoposc___closed__2);
l_ML_Pattern_tacticAutoposc___closed__3 = _init_l_ML_Pattern_tacticAutoposc___closed__3();
lean_mark_persistent(l_ML_Pattern_tacticAutoposc___closed__3);
l_ML_Pattern_tacticAutoposc___closed__4 = _init_l_ML_Pattern_tacticAutoposc___closed__4();
lean_mark_persistent(l_ML_Pattern_tacticAutoposc___closed__4);
l_ML_Pattern_tacticAutoposc___closed__5 = _init_l_ML_Pattern_tacticAutoposc___closed__5();
lean_mark_persistent(l_ML_Pattern_tacticAutoposc___closed__5);
l_ML_Pattern_tacticAutoposc = _init_l_ML_Pattern_tacticAutoposc();
lean_mark_persistent(l_ML_Pattern_tacticAutoposc);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__1 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__1();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__1);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__2 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__2();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__2);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__3 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__3();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__3);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__4 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__4();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__4);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__5 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__5();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__5);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__6 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__6();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__6);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__7 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__7();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutoposc__1___closed__7);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__1 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__1();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__1);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__2 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__2();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__2);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__3 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__3();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__3);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__4 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__4();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__4);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__5 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__5();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__5);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__6 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__6();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__6);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__7 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__7();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__7);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__8 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__8();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__8);
l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__9 = _init_l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__9();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Positivity______macroRules__ML__Pattern__tacticAutopos__4___closed__9);
l_ML_Pattern_Hidden_X = _init_l_ML_Pattern_Hidden_X();
lean_mark_persistent(l_ML_Pattern_Hidden_X);
l_ML_Pattern_Hidden_Y = _init_l_ML_Pattern_Hidden_Y();
lean_mark_persistent(l_ML_Pattern_Hidden_Y);
l_ML_Pattern_Hidden__u03c6_u2080___closed__1 = _init_l_ML_Pattern_Hidden__u03c6_u2080___closed__1();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2080___closed__1);
l_ML_Pattern_Hidden__u03c6_u2080___closed__2 = _init_l_ML_Pattern_Hidden__u03c6_u2080___closed__2();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2080___closed__2);
l_ML_Pattern_Hidden__u03c6_u2080 = _init_l_ML_Pattern_Hidden__u03c6_u2080();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2080);
l_ML_Pattern_Hidden__u03c6_u2081___closed__1 = _init_l_ML_Pattern_Hidden__u03c6_u2081___closed__1();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2081___closed__1);
l_ML_Pattern_Hidden__u03c6_u2081___closed__2 = _init_l_ML_Pattern_Hidden__u03c6_u2081___closed__2();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2081___closed__2);
l_ML_Pattern_Hidden__u03c6_u2081 = _init_l_ML_Pattern_Hidden__u03c6_u2081();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2081);
l_ML_Pattern_Hidden__u03c6_u2082___closed__1 = _init_l_ML_Pattern_Hidden__u03c6_u2082___closed__1();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2082___closed__1);
l_ML_Pattern_Hidden__u03c6_u2082 = _init_l_ML_Pattern_Hidden__u03c6_u2082();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2082);
l_ML_Pattern_Hidden__u03c6_u2083___closed__1 = _init_l_ML_Pattern_Hidden__u03c6_u2083___closed__1();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2083___closed__1);
l_ML_Pattern_Hidden__u03c6_u2083___closed__2 = _init_l_ML_Pattern_Hidden__u03c6_u2083___closed__2();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2083___closed__2);
l_ML_Pattern_Hidden__u03c6_u2083___closed__3 = _init_l_ML_Pattern_Hidden__u03c6_u2083___closed__3();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2083___closed__3);
l_ML_Pattern_Hidden__u03c6_u2083___closed__4 = _init_l_ML_Pattern_Hidden__u03c6_u2083___closed__4();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2083___closed__4);
l_ML_Pattern_Hidden__u03c6_u2083 = _init_l_ML_Pattern_Hidden__u03c6_u2083();
lean_mark_persistent(l_ML_Pattern_Hidden__u03c6_u2083);
return lean_io_result_mk_ok(lean_box(0));
}
#ifdef __cplusplus
}
#endif
