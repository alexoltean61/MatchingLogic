import MatchingLogic.Pattern

namespace ML
namespace Pattern

/--
  Wraps an element (a : α) into the singleton list [a].

  todo: this must be in the standard library somewhere, check where.
-/
@[simp]
def toList {α : Type} : α → List α := [].cons

-- In the following section, we define the basic constructions needed to compute the
--   propositional conjunctive normal forms of patterns.
-- These constructions are "stateful", because the actual list of lists associated with the
--   CNF is also computed, using Option.

/--
  Returns "some φ" if φ is an atomic pattern, "none" otherwise.

  A pattern is atomic if it was NOT obtained using the propositional constructor ⇒.
-/
def Atom : Pattern 𝕊 → Option (Pattern 𝕊)
| _ ⇒ _    => none
| φ         => φ

/--
  Returns "some φ" if φ is a negated atom, "none" otherwise.

  A negated atom is the negation of an atom.
-/
def negAtom : Pattern 𝕊 → Option (Pattern 𝕊)
| ∼ψ  => Option.map negation (Atom ψ)
| _   => none

/--
  Returns "some φ" if φ is a literal, "none" otherwise.

  A literal is an atom or a negated atom.
-/
def Literal : Pattern 𝕊 → Option (Pattern 𝕊) :=
  λ φ => Atom φ <|> negAtom φ

/--
  If φ is a disjunction of literals, returns "some L", where L is a list of all literals in φ.

  Otherwise, returns "none".
-/
def Disjunction : Pattern 𝕊 → Option (List (Pattern 𝕊)) := λ φ =>
  Option.map toList (Literal φ) <|> (
    match φ with
    -- with current syntax definitions, double negation
    --  matches as disjunction. but it should not,
    --  so we begin by checking explicitly for double negations:
    | ∼∼_   => none
    -- then:
    | ψ ⋁ χ => (. ++ .) <$> (Disjunction ψ) <*> (Disjunction χ)
    | _     => none
  )

#eval Disjunction (ML.Pattern.symbol 1 ⋁ (ML.Pattern.symbol 0 ⋀ ML.Pattern.symbol 3))

/--
  If φ is a conjunction of literals, returns "some L", where L is a list of all literals in φ.

  Otherwise, returns "none".
-/
def Conjunction : Pattern 𝕊 → Option (List (Pattern 𝕊)) := λ φ =>
  Option.map toList (Literal φ) <|> (
    match φ with
    | ψ ⋀ χ => (. ++ .) <$> (Conjunction ψ) <*> (Conjunction χ)
    | _     => none
  )

/--
  If φ is in conjunctive normal form, returns "some L", where L is a list of all clauses in φ.

  Otherwise, returns "none".

  A pattern is in conjunctive normal form iff it is a conjunction of disjunctions. These disjunctions
  are called "clauses".
-/
def CNF : Pattern 𝕊 → Option (List (List (Pattern 𝕊))) := λ φ =>
  Option.map toList (Disjunction φ) <|> (
    match φ with
    | ψ ⋀ χ => (. ++ .) <$> (CNF ψ) <*> (CNF χ)
    | _     => none
  )

#eval CNF (((@Pattern.symbol ℕ 0) ⋁ (@Pattern.symbol ℕ 0)) ⋀ (∼(@Pattern.symbol ℕ 0) ⋁ (@Pattern.symbol ℕ 1) ⋁ ∼(@Pattern.symbol ℕ 3)))
