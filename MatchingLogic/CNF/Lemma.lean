import MatchingLogic.CNF.Basic
import MatchingLogic.CNF.Specs

namespace ML
namespace Pattern

-- In this file, we prove lemmas required for the correctness of CNF specification.

  lemma not_not_negations : hasNegations (∼∼φ) := by
    unfold hasNegations
    split
    . next h =>
      simp [negation] at h
    . next h =>
      simp only [negation, implication.injEq, and_true] at h
      simp [←h, isAtom, Atom]
    . next h1 h2 =>
      exfalso
      apply h1
      apply Eq.symm
      simp [negation] at h2
      exact h2.2
    . next h =>
      simp [negation] at h

  lemma negAtNotAt : (isNegAtom φ) → ¬(isAtom φ) := by
    cases φ <;> simp [isNegAtom, isAtom]

  lemma test1 : isAtom' φ ↔ isAtom φ := by
    apply Iff.intro
    <;> {
      unfold isAtom' isAtom Atom
      split
      . simp
      . simp
    }

  lemma negNotAtom : !isAtom' (∼φ) := by
    simp only [Bool.not_eq_true', ←Bool.not_eq_true, test1, isAtom, not_false_eq_true]

  lemma test2 : isNegAtom' φ ↔ isNegAtom φ := by
    apply Iff.intro
    . unfold isNegAtom' negAtom
      intro h
      split at h
      . simp [Option.map] at h
        split at h
        . next h_some =>
          simp [isNegAtom, ←test1, isAtom', h_some]
        . simp at h
      . simp at h
    . unfold isNegAtom
      split
      . simp only [← test1, isAtom', Option.isSome_iff_exists, isNegAtom', negAtom,
        Option.map_eq_some', forall_exists_index]
        intro ψ h
        exists ∼ψ
        exists ψ
      . simp

  lemma isSomeAtom : (Atom φ = some a) → (isAtom φ) := by
    intro hat
    have := Option.isSome_iff_exists.mpr ⟨a, hat⟩
    rw [←isAtom', test1] at this
    assumption

  lemma isSomeNegAtom : (negAtom φ = some a) → (isNegAtom φ) := by
    intro hat
    have := Option.isSome_iff_exists.mpr ⟨a, hat⟩
    rw [←isNegAtom', test2] at this
    assumption

  lemma test3 : isLiteral' φ ↔ isLiteral φ := by
    apply Iff.intro
    . simp only [isLiteral', Option.isSome_iff_exists, Literal, Option.orElse_eq_some]
      intro ⟨ψ, h⟩
      apply Or.elim h
      . clear h
        intro hat
        simp [isLiteral, isSomeAtom hat]
      . intro ⟨_, hnat⟩
        simp [isLiteral, isSomeNegAtom hnat]
    . simp [isLiteral, ←test1, ←test2]
      intro h
      apply Or.elim h
      . simp only [isAtom', isLiteral', Literal, Option.isSome_iff_exists, Option.orElse_eq_some]
        intro ⟨a, hat⟩
        exists a
        apply Or.inl; exact hat
      . simp only [isNegAtom', isLiteral', Literal, Option.isSome_iff_exists, Option.orElse_eq_some]
        intro ⟨a, hnat⟩
        exists a
        apply Or.inr
        apply And.intro
        . rw [←Option.not_isSome_iff_eq_none, ←isAtom', test1]
          apply negAtNotAt
          exact isSomeNegAtom hnat
        . exact hnat

  lemma isNotLiteralIff : ¬isLiteral' φ ↔ isNotLiteral φ := by
    cases φ with
    | implication φ ψ =>
        simp [test3, isLiteral, isAtom]
        simp [isNegAtom, isNotLiteral]
        split
        . next φ h =>
          simp [h]
        . next h =>
          split
          . next χ _ =>
            specialize h χ
            contradiction
          . simp only
          . next habs =>
            specialize habs φ ψ
            contradiction
    | _ => simp [test3, isLiteral, isNotLiteral, isAtom]

  lemma conjNotLiteral : isConjunction φ → isNotLiteral φ := by
    unfold isConjunction isNotLiteral
    intro h
    split
    . split at h
      . next hyp =>
        simp only [implication.injEq, and_true] at hyp
        simp [hyp, isAtom]
      . contradiction
    . rfl
    . next hyp _ =>
      split at h
      . simp at hyp
      . contradiction

  lemma disjNotConj : Disjunction φ = some c → !isConjunction φ := by
    contrapose
    intro h
    simp only [Bool.not_eq_true', Bool.not_eq_false] at h
    revert c
    apply forall_not_of_not_exists
    unfold Disjunction
    simp only [Option.map_eq_map, ← Option.isSome_iff_exists, Bool.not_eq_true, Option.not_isSome,
      Option.isNone_iff_eq_none, Option.orElse_eq_none, Option.map_eq_none']
    apply And.intro
    . simp only [←Option.isNone_iff_eq_none, ←Option.not_isSome, ←Bool.not_eq_true]
      rw [←isLiteral', isNotLiteralIff]
      exact conjNotLiteral h
    . split
      . rfl
      . simp only [Seq.seq, Option.map_eq_map, Option.bind_eq_none, Option.map_eq_some',
        Option.map_eq_none', forall_exists_index, and_imp, forall_apply_eq_imp_iff₂]
        unfold isConjunction at h
        split at h
        . next hyp =>
          simp at hyp
        . contradiction
      . rfl

  lemma conjunctionUndistributedL : isConjunction ψ → isUndistributed (φ ⋁ ψ) := by
    intro h_conj
    unfold isUndistributed
    split
    . -- disjunction
      next h =>
      simp only [disjunction, negation, implication.injEq, and_true] at h
      rw [h.2] at h_conj
      simp [h_conj]
    . -- conjunction
      next h =>
      simp [disjunction, negation] at h
    . -- implication
      next h1 _ h2 =>
      simp only [disjunction, negation, implication.injEq] at h2
      specialize h1 φ (Eq.symm h2.left)
      contradiction
    . -- everything else
      next h =>
      simp [disjunction, negation] at h

  lemma conjunctionUndistributedR : isConjunction φ → isUndistributed (φ ⋁ ψ) := by
    intro h_conj
    unfold isUndistributed
    split
    . -- disjunction
      next h =>
      simp only [disjunction, negation, implication.injEq, and_true] at h
      rw [h.1] at h_conj
      simp [h_conj]
    . -- conjunction
      next h =>
      simp [disjunction, negation] at h
    . -- implication
      next h1 _ h2 =>
      simp only [disjunction, negation, implication.injEq] at h2
      specialize h1 φ (Eq.symm h2.left)
      contradiction
    . -- everything else
      next h =>
      simp [disjunction, negation] at h

  lemma arrowsNegated : hasArrows (∼φ) → hasArrows φ := by
    intro h_arr
    unfold hasArrows at h_arr
    split at h_arr
    . -- double negation
      next h =>
      simp only [negation, implication.injEq, and_true] at h
      simp only [h]
      exact h_arr
    . -- disjunction
      next h1 h2 =>
      exfalso
      simp only [negation, implication.injEq] at h2
      apply h1
      apply Eq.symm
      exact h2.2
    . -- negation
      next h =>
      simp only [negation, implication.injEq, and_true] at h
      simp [h, h_arr]
    . -- implication
      next h1 h2 =>
      exfalso
      apply h1
      simp only [negation, implication.injEq] at h2
      apply Eq.symm
      exact h2.2
    . contradiction

  lemma arrowsImplR : hasArrows φ → hasArrows (φ ⇒ ψ) := by
    intro h_arr
    unfold hasArrows
    split
    . -- double negation
      next h =>
      simp only [implication.injEq] at h
      simp only [h.1] at h_arr
      exact h_arr
    . -- disjunction
      next h =>
      simp at h
      rw [h.1, ←negation] at h_arr
      simp [arrowsNegated h_arr]
    . -- negation
      next h =>
      simp at h
      rw [←h.1]
      assumption
    . -- implication
      rfl
    . next h =>
      specialize h φ ψ rfl
      contradiction

  lemma arrowsImplL : hasArrows φ → hasArrows (ψ ⇒ φ) := by
    intro h_arr
    unfold hasArrows
    split
    . -- double negation
      next h =>
      simp only [implication.injEq] at h
      simp [h.2] at h_arr
    . -- disjunction
      next h =>
      simp at h
      rw [h.2] at h_arr
      simp [h_arr]
    . -- negation
      next h =>
      simp at h
      simp [h.2, hasArrows] at h_arr
    . -- implication
      rfl
    . next h =>
      specialize h ψ φ rfl
      contradiction

  lemma arrowsDisjR : hasArrows φ → hasArrows (φ ⋁ ψ) := by
    intro h
    unfold disjunction negation
    repeat apply arrowsImplR
    assumption

  lemma arrowsDisjL : hasArrows φ → hasArrows (ψ ⋁ φ) := by
    intro h
    unfold disjunction negation
    apply arrowsImplL
    assumption

  lemma negArrows : hasArrows (∼φ) → hasArrows φ := by
    intro h
    unfold hasArrows at h
    split at h
    . next heq =>
      simp only [negation._eq_1, implication.injEq, and_true] at heq
      rw [heq]
      exact h
    . next heq =>
      simp only [negation._eq_1, implication.injEq, Bool.or_eq_true] at heq h
      simp only [← heq.2, hasArrows._eq_5, or_false, heq.1] at h ⊢
      apply arrowsImplR
      exact h
    . next heq =>
      simp only [negation._eq_1, implication.injEq, and_true] at heq
      rw [heq]
      exact h
    . next h1 h2 =>
      exfalso
      apply h1
      apply Eq.symm
      simp only [negation._eq_1, implication.injEq] at h2
      exact h2.2
    . contradiction

  lemma disjArrows : !hasArrows φ → !hasArrows ψ → !hasArrows (φ ⋁ ψ) := by
    contrapose
    simp only [Bool.not_eq_true', not_forall, Bool.not_eq_false, exists_prop, and_imp]
    intro h1 h2
    unfold hasArrows at h2
    split at h2
    . next h =>
      simp [disjunction, negation] at h
      rw [←h.1] at h2
      apply negArrows
      exact h2
    . next h =>
      simp only [disjunction, negation._eq_1, implication.injEq, and_true] at h
      simp only [← h.1, ← h.2, h1, Bool.or_false] at h2
      assumption
    . next h h' =>
      exfalso
      apply h
      apply Eq.symm
      simp [disjunction] at h'
      exact h'.1
    . next h _ h' =>
      exfalso
      apply h
      apply Eq.symm
      simp [disjunction] at h'
      exact h'.1
    . contradiction

  -- ∼∼φ = (∼φ ⇒ ⊥) = φ ⋁ ⊥
  #eval hasNegations (@Pattern.symbol ℕ 0)
  #eval hasNegations (@Pattern.bottom ℕ)
  #eval hasNegations (@Pattern.symbol ℕ 0 ⋁ @Pattern.bottom ℕ)
  -- due to the way pattern matching works, in order for the following lemma to hold,
  --   we need to impose the condition that ψ ≠ ⊥:

  lemma disjNegations : !hasNegations φ → !hasNegations ψ → ψ ≠ bottom → !hasNegations (φ ⋁ ψ) := by
    contrapose
    simp only [Bool.not_eq_true', not_forall, Bool.not_eq_false, exists_prop, and_imp]
    intro h1 hne h2
    unfold hasNegations at h2
    split at h2
    . -- conjunction
      next h =>
      simp [disjunction, negation] at h
    . -- negation
      next h =>
      clear h1 h2
      exfalso
      apply hne
      simp only [disjunction, negation._eq_1, implication.injEq, and_true] at h
      exact h.2
    . -- implication
      next h h' =>
      simp [disjunction] at h'
      rw [h'.2] at h1
      simp only [h1, ← h'.1, Bool.false_or] at h2
      exact h2
    . -- others
      contradiction

  lemma disjUndistributed : !isUndistributed φ → !isUndistributed ψ → !isConjunction φ → !isConjunction ψ → !isUndistributed (φ ⋁ ψ) := by
    intro h1 h2 h3 h4
    unfold isUndistributed
    simp only [Bool.not_eq_true']
    split
    . next h =>
      simp only [disjunction, negation._eq_1, implication.injEq, and_true] at h
      simp only [Bool.not_eq_true'] at h1 h2 h3 h4
      simp [←h.1, ←h.2, h1, h2, h3, h4]
    . next h =>
      simp [disjunction, negation] at h
    . next h1 _ h2 =>
      simp [disjunction, negation] at h2
      specialize h1 φ (Eq.symm h2.1)
      contradiction
    . next h _ _ =>
      specialize h φ ψ rfl
      contradiction

  lemma arrowsConjR : hasArrows φ → hasArrows (φ ⋀ ψ) := by
    intro h
    unfold conjunction negation disjunction
    repeat apply arrowsImplR
    assumption

  lemma arrowsConjL : hasArrows ψ → hasArrows (φ ⋀ ψ) := by
    intro h
    unfold conjunction negation disjunction
    apply arrowsImplR
    apply arrowsImplL
    apply arrowsImplR
    assumption

  lemma negationsDisjR : hasNegations φ → hasNegations (φ ⋁ ψ) := by
    intro h_arr
    unfold hasNegations
    split
    . -- conjunction
      next h =>
      simp [disjunction, negation] at h
    . -- negation
      next h =>
      simp [disjunction, negation] at h
      rw [←h.1, Bool.not_iff_not, ←test1, ←Bool.not_iff_not]
      exact negNotAtom
    . -- implication
      next h1 h2 h3 =>
      simp [disjunction, negation] at h3
      simp [←h3.2, ←h3.1, h_arr]
    . next h =>
      specialize h (∼φ) ψ rfl
      contradiction

  lemma negationsDisjL : hasNegations ψ → hasNegations (φ ⋁ ψ) := by
    intro h_arr
    unfold hasNegations
    split
    . -- conjunction
      next h =>
      simp [disjunction, negation] at h
    . -- negation
      next h =>
      simp [disjunction, negation] at h
      rw [←h.1, Bool.not_iff_not, ←test1, ←Bool.not_iff_not]
      exact negNotAtom
    . -- implication
      next h1 h2 h3 =>
      simp [disjunction, negation] at h3
      simp [←h3.2, ←h3.1, h_arr]
    . next h =>
      specialize h (∼φ) ψ rfl
      contradiction

  lemma negationsConjR : hasNegations φ → hasNegations (φ ⋀ ψ) := by
    simp only [hasNegations, Bool.or_eq_true]
    intro
    apply Or.inl
    assumption

  lemma negationsConjL : hasNegations ψ → hasNegations (φ ⋀ ψ) := by
    simp only [hasNegations, Bool.or_eq_true]
    intro
    apply Or.inr
    assumption

  lemma undistributedDisjR : isUndistributed φ → isUndistributed (φ ⋁ ψ) := by
    intro h_arr
    unfold isUndistributed
    split
    . -- disjunction
      next h =>
      simp only [disjunction, negation, implication.injEq, and_true] at h
      simp [←h, h_arr]
    . -- conjunction
      next h =>
      simp [disjunction, negation] at h
    . -- implication
      next h1 _ h2 =>
      simp [disjunction, negation] at h2
      specialize h1 φ (Eq.symm h2.1)
      contradiction
    . next h =>
      specialize h (∼φ) ψ rfl
      contradiction

  lemma undistributedDisjL : isUndistributed ψ → isUndistributed (φ ⋁ ψ) := by
    intro h_arr
    unfold isUndistributed
    split
    . -- disjunction
      next h =>
      simp only [disjunction, negation, implication.injEq, and_true] at h
      simp [←h, h_arr]
    . -- conjunction
      next h =>
      simp [disjunction, negation] at h
    . next h1 _ h2 =>
      simp [disjunction, negation] at h2
      specialize h1 φ (Eq.symm h2.1)
      contradiction
    . next h =>
      specialize h (∼φ) ψ rfl
      contradiction

  lemma undistributedConjR : isUndistributed φ → isUndistributed (φ ⋀ ψ) := by
    simp only [isUndistributed, Bool.or_eq_true]
    intro
    apply Or.inl
    assumption

  lemma undistributedConjL : isUndistributed ψ → isUndistributed (φ ⋀ ψ) := by
    simp only [isUndistributed, Bool.or_eq_true]
    intro
    apply Or.inr
    assumption

  lemma helper {α : Type} {p : α → Prop} {q : Prop} : (∀ x, p x → q) ↔ (∃ x, p x) → q := by
    exact Iff.symm forall_exists_index

  lemma atomNoArrows    : isAtom φ → !hasArrows φ := by
    unfold isAtom
    intro h
    split at h
    . contradiction
    . next h2 =>
      clear h
      unfold hasArrows
      simp only [Bool.not_eq_true']
      split
      . next φ =>
        exfalso
        apply h2 (φ ⇒ ⊥) ⊥
        rfl
      . next φ ψ _ =>
        exfalso
        apply h2 (φ ⇒ ⊥) ψ
        rfl
      . next φ _ _ =>
        exfalso
        apply h2 φ ⊥
        rfl
      . next φ ψ _ _ _ =>
        exfalso
        apply h2 φ ψ
        rfl
      . rfl

  lemma negAtomNoArrows    : isNegAtom φ → !hasArrows φ := by
    unfold isNegAtom
    intro h
    split at h
    . have := atomNoArrows h
      clear h
      revert this
      contrapose
      simp only [Bool.not_eq_true', Bool.not_eq_false, Bool.not_eq_true]
      apply negArrows
    . contradiction

  lemma literalNoArrows    : isLiteral φ → !hasArrows φ := by
    simp only [isLiteral, Bool.or_eq_true]
    intro h
    apply Or.elim h
    . exact atomNoArrows
    . exact negAtomNoArrows

  lemma atomNoNegations : isAtom φ → !hasNegations φ := by
    unfold isAtom
    intro h
    split at h
    . contradiction
    . next hyp =>
      clear h
      simp only [Bool.not_eq_true']
      unfold hasNegations
      split
      . next φ ψ =>
        exfalso
        apply hyp (((φ ⇒ ⊥) ⇒ ⊥) ⇒ ψ ⇒ ⊥) ⊥
        rfl
      . next φ _ =>
        exfalso
        apply hyp φ ⊥
        rfl
      . next φ ψ _ _ =>
        exfalso
        apply hyp φ ψ
        rfl
      . rfl

  lemma negAtomNoNegations    : isNegAtom φ → !hasNegations φ := by
    unfold isNegAtom
    intro h
    split at h
    . simp only [Bool.not_eq_true']
      unfold hasNegations
      split
      . next hyp =>
        simp only [implication.injEq, and_true] at hyp
        simp [hyp, isAtom] at h
      . next hyp =>
        simp only [implication.injEq, and_true] at hyp
        simp [←hyp, test1, h]
      . next hyp1 hyp2 =>
        exfalso
        apply hyp1
        apply Eq.symm
        simp only [implication.injEq] at hyp2
        exact hyp2.2
      . rfl
    . contradiction

  lemma literalNoNegations : isLiteral φ → !hasNegations φ := by
    simp only [isLiteral, Bool.or_eq_true]
    intro h
    apply Or.elim h
    . exact atomNoNegations
    . exact negAtomNoNegations

  lemma atomDistributed : isAtom φ → !isUndistributed φ := by
    unfold isAtom
    intro h
    split at h
    . contradiction
    . next hyp =>
      clear h
      unfold isUndistributed
      simp only [Bool.not_eq_true']
      split
      . next φ ψ =>
        exfalso
        apply hyp (φ ⇒ ⊥) ψ
        rfl
      . next φ ψ =>
        exfalso
        apply hyp (((φ ⇒ ⊥) ⇒ ⊥) ⇒ ψ ⇒ ⊥) ⊥
        rfl
      . next φ ψ _ _ =>
        exfalso
        apply hyp φ ψ
        rfl
      . rfl

  lemma negAtomDistributed : isNegAtom φ → !isUndistributed φ := by
    unfold isNegAtom
    intro h
    split at h
    . unfold isUndistributed
      simp only [Bool.not_eq_true']
      split
      . next heq =>
        simp only [implication.injEq] at heq
        simp [heq.1, isAtom] at h
      . next heq =>
        simp only [implication.injEq, and_true] at heq
        simp [heq, isAtom] at h
      . next heq =>
        simp only [implication.injEq] at heq
        simp only [← heq.1, ← heq.2, isUndistributed._eq_4, Bool.or_false]
        rw [←Bool.not_eq_true']
        exact atomDistributed h
      . rfl
    . contradiction

  lemma literalDistributed : isLiteral φ → !isUndistributed φ := by
    simp only [isLiteral, Bool.or_eq_true]
    intro h
    apply Or.elim h
    . exact atomDistributed
    . exact negAtomDistributed

  lemma disjCNF1 : Disjunction φ = some c → !(hasArrows φ) := by
    unfold Disjunction
    simp? [Option.orElse_eq_some, toList, -Bool.not_eq_true']
    intro h
    apply Or.elim h
    . clear h
      intro ⟨c, ⟨hc, foo⟩⟩
      clear foo
      revert c
      rw [helper, ←Option.isSome_iff_exists, ←isLiteral', test3]
      exact literalNoArrows
    . clear h
      intro ⟨h1, h2⟩
      split at h2
      . contradiction
      . clear h1
        simp [Seq.seq] at h2
        rw [←negation, ←disjunction]
        apply disjArrows
        repeat {
          have ⟨_, ⟨_, ⟨_, ⟨_, _⟩⟩⟩⟩ := h2
          apply disjCNF1
          assumption
        }
      . contradiction

  lemma disjCNF2 : Disjunction φ = some c → !(hasNegations φ) := by
    unfold Disjunction
    simp? [Option.orElse_eq_some, toList, -Bool.not_eq_true']
    intro h
    apply Or.elim h
    . clear h
      intro ⟨c, ⟨hc, foo⟩⟩
      clear foo
      revert c
      rw [helper, ←Option.isSome_iff_exists, ←isLiteral', test3]
      exact literalNoNegations
    . clear h
      intro ⟨h1, h2⟩
      split at h2
      . contradiction
      . next h =>
        clear h1
        simp [Seq.seq] at h2
        rw [←negation, ←disjunction]
        apply disjNegations
        repeat {
          have ⟨_, ⟨_, ⟨_, ⟨_, _⟩⟩⟩⟩ := h2
          apply disjCNF2
          assumption
        }
        assumption
      . contradiction

  lemma disjCNF3 : Disjunction φ = some c → !(isUndistributed φ) := by
    unfold Disjunction
    simp? [Option.orElse_eq_some, toList, -Bool.not_eq_true']
    intro h
    apply Or.elim h
    . clear h
      intro ⟨c, ⟨hc, foo⟩⟩
      clear foo
      revert c
      rw [helper, ←Option.isSome_iff_exists, ←isLiteral', test3]
      exact literalDistributed
    . clear h
      intro ⟨h1, h2⟩
      split at h2
      . contradiction
      . clear h1
        simp [Seq.seq] at h2
        rw [←negation, ←disjunction]
        have ⟨_, ⟨_, ⟨_, ⟨_, _⟩⟩⟩⟩ := h2
        apply disjUndistributed
        repeat {
          apply disjCNF3
          assumption
        }
        repeat {
          apply disjNotConj
          assumption
        }
      . contradiction

  lemma arrowsConj : hasArrows (φ ⋀ ψ) → (hasArrows φ ∨ hasArrows ψ) := by
    simp only [hasArrows, Bool.or_eq_true]
    rw [←negation]
    intro h
    apply Or.elim h
    . intro h
      apply Or.inl
      exact negArrows h
    . intro h
      apply Or.inr
      exact negArrows h

  lemma negationsConj : hasNegations (φ ⋀ ψ) → (hasNegations φ ∨ hasNegations ψ) := by
    simp only [hasNegations, Bool.or_eq_true, imp_self]

  lemma undistributedConj : isUndistributed (φ ⋀ ψ) → (isUndistributed φ ∨ isUndistributed ψ) := by
    simp only [isUndistributed, Bool.or_eq_true, imp_self]
