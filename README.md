## Lean formalization of Applicative Matching Logic 

The technical report describes [this release](https://gitlab.com/ilds/aml-lean/MatchingLogic/-/tags/stage-1)

### Team
[Horațiu Cheval](https://cs.unibuc.ro/~hcheval/)  

#### Former members
[Andrei Burdușa](https://gitlab.com/andreiburdusa) \
[Bogdan Macovei](https://cs.unibuc.ro/~bmacovei/)


### For developers
Download project and get precompiled mathlib oleans with 
``` 
git clone https://gitlab.com/ilds/aml-lean/MatchingLogic.git 
lake update 
lake exe cache get
```
