// Lean compiler output
// Module: MatchingLogic.Proof
// Imports: Init MatchingLogic.Pattern MatchingLogic.Substitution MatchingLogic.Positivity MatchingLogic.AppContext MatchingLogic.Premises MatchingLogic.Tautology
#include <lean/lean.h>
#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wunused-label"
#elif defined(__GNUC__) && !defined(__CLANG__)
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-label"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#endif
#ifdef __cplusplus
extern "C" {
#endif
LEAN_EXPORT lean_object* l_ML_Proof_propagationExist___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_toRule2(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottom_x27___rarg(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_framing(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__13;
static lean_object* l_ML_term_u1d39_____closed__4;
static lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__3;
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__11;
static lean_object* l_ML_Proof_propagationBottom_x27___rarg___closed__3;
lean_object* lean_mk_empty_array_with_capacity(lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Proof_0__ML_Proof_size_match__1_splitter___rarg___boxed(lean_object**);
static lean_object* l_ML_term_u1d39_____closed__7;
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__1;
LEAN_EXPORT lean_object* l_ML_Proof_weaken(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_expansion(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_permutationConj___rarg(lean_object*, lean_object*);
lean_object* l_ML_Pattern_top(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_modusPonensExtraHyp(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__7;
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__14;
static lean_object* l_ML_Proof_propagationBottom_x27___rarg___closed__1;
LEAN_EXPORT lean_object* l_ML_Proof_contractionConj___rarg(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__5;
LEAN_EXPORT lean_object* l_ML_Proof_conjIntroRule___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__3;
LEAN_EXPORT lean_object* l_ML_Proof_univQuan(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroLeft___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Proof_0__ML_Proof_size_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_negConjAsImpl___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_selfImplTopImplSelf___rarg(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_conjIntroRule(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_weakeningDisj(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__2;
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__5;
LEAN_EXPORT lean_object* l_ML_Proof_weakeningDisj___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_doubleNegCtx___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_size(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_modusPonensExtraHyp___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Proof_topImplSelfImplSelf___rarg___closed__1;
LEAN_EXPORT lean_object* l_ML_Proof_contractionDisj(lean_object*, lean_object*);
static lean_object* l_ML_term___u22a2_____closed__5;
static lean_object* l_ML_term_u1d39_____closed__6;
LEAN_EXPORT lean_object* l_ML_Proof_doubleNegCtx(lean_object*, lean_object*);
static lean_object* l_ML_term___u22a2_____closed__8;
lean_object* l_Lean_Syntax_getArgs(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_implDistribLeft___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__modusPonens__1___lambda__1___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_replaceRef(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroLeft(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_exportation(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_implProjLeft___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_univGen___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_disjImpl2(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_implProjRight(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_implDistribRight(lean_object*, lean_object*);
lean_object* l_ML_Pattern_substEvar___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_extraPremise___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_univQuan___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT uint8_t l_ML_Proof_usesKnasterTarski___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_usesKnasterTarski___rarg___boxed(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_exFalso(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_propagationExistR___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_implDistribLeft(lean_object*, lean_object*);
uint8_t l_Lean_Syntax_isOfKind(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__12;
LEAN_EXPORT lean_object* l_ML_Proof_pushExistInConj___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__8;
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__9;
LEAN_EXPORT lean_object* l_ML_Proof_equivSelf___rarg(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottom___rarg(lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_disjImpl(lean_object*, lean_object*);
lean_object* l_ML_Pattern_negation___rarg(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_toRule(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_weaken___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_syllogismExtraHyp(lean_object*, lean_object*);
lean_object* l_Lean_Syntax_getNumArgs(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_permutationDisj(lean_object*, lean_object*);
static lean_object* l_ML_term_u1d39_____closed__2;
LEAN_EXPORT lean_object* l_ML_Proof_implProjRight___rarg(lean_object*, lean_object*);
static lean_object* l_ML_term___u22a2_____closed__12;
lean_object* l_Lean_Name_mkStr3(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Proof_propagationBottom_x27___rarg___closed__6;
LEAN_EXPORT lean_object* l_ML_Proof_existSelfImpl(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_implSelf___rarg(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_syllogism___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Pattern_universal___rarg(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__6;
LEAN_EXPORT lean_object* l_ML_Proof_importation___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroAtHyp(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_propagtionDisjR(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_exportation___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_size___rarg(lean_object*, lean_object*);
lean_object* l_Lean_SourceInfo_fromRef(lean_object*, uint8_t);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__1___boxed(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_disjImpl___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_propagationExist___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_conjIntroAtConclThm___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottomR___rarg___boxed(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_disjImpl2___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Proof_0__ML_Proof_usesKnasterTarski_match__1_splitter___rarg___boxed(lean_object**);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__10;
static lean_object* l_ML_term_u1d39_____closed__5;
LEAN_EXPORT lean_object* l_ML_Proof_conjIntroHypConcLeft___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_pushExistInConj(lean_object*, lean_object*);
lean_object* l_ML_Pattern_equivalence___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_conjElimRight___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_expansion___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__15;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__modusPonens__1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_topImplSelfImplSelf(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Proof_0__ML_Proof_usesKnasterTarski_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_framing___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_univGeneralization(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__7;
lean_object* l_Lean_Syntax_node3(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_doubleNegElim(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_contractionDisj___rarg(lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__modusPonens__1___lambda__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_ML_AppContext_insert___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottom_x27(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottom(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_exFalso___rarg(lean_object*);
static lean_object* l_ML_term___u22a2_____closed__10;
lean_object* l_Lean_addMacroScope(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u22a2_____closed__6;
LEAN_EXPORT lean_object* l_ML_Proof_permuteHyps___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Name_str___override(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Proof_0__ML_Proof_usesKnasterTarski_match__1_splitter(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_implExistSelf___rarg(lean_object*, lean_object*);
lean_object* l_Lean_Syntax_node2(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_conjElimLeft(lean_object*, lean_object*);
lean_object* l_Lean_Syntax_getArg(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__4;
uint8_t l_Lean_Syntax_matchesNull(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_syllogismExtraHyp___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_term___u22a2__;
LEAN_EXPORT lean_object* l_ML_Proof_doubleNegElim___rarg(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_implSelf(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_implDistribRight___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__9;
LEAN_EXPORT lean_object* l_ML_term_u1d39__;
lean_object* l_Array_append___rarg(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__3;
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottomR___rarg(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_existSelfImpl___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_propagationExistR(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_pushConjInExist(lean_object*, lean_object*);
lean_object* l_Array_extract___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_conjIntro(lean_object*, lean_object*);
lean_object* l_ML_Pattern_substSvar___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__2;
LEAN_EXPORT lean_object* l_ML_Proof_univGen(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroAtHypThm(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroAtHyp___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_modusPonensThm(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_permuteHyps(lean_object*, lean_object*);
static lean_object* l_ML_term_u1d39_____closed__1;
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__8;
LEAN_EXPORT lean_object* l_ML_Proof_univGeneralization___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_toRule___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__6;
LEAN_EXPORT lean_object* l_ML_Proof_weakeningConj(lean_object*, lean_object*);
lean_object* l_Lean_Name_mkStr2(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_conjIntroHypConcLeft(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_usesKnasterTarski(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_weakeningConj___rarg(lean_object*, lean_object*);
lean_object* l_Lean_Syntax_node1(lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Pattern_conjunction___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_topImplSelfImplSelf___rarg(lean_object*);
static lean_object* l_ML_term___u22a2_____closed__11;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_extraPremise(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroAtHypThm___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_propagtionDisjR___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u22a2_____closed__7;
lean_object* lean_nat_sub(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_importation(lean_object*, lean_object*);
static lean_object* l_ML_Proof_propagationBottom___rarg___closed__1;
static lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2___closed__1;
LEAN_EXPORT lean_object* l_ML_Proof_propagationDisj(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__4;
static lean_object* l_ML_Proof_propagationBottom_x27___rarg___closed__4;
LEAN_EXPORT lean_object* l_ML_Proof_conjIntro___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_propagationExist(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_permutationDisj___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_excluddedMiddle___rarg(lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_implProjLeft(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_syllogism(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_conjIntroAtConclThm(lean_object*, lean_object*);
static lean_object* l_ML_Proof_propagationBottom_x27___rarg___closed__2;
LEAN_EXPORT lean_object* l_ML_Proof_negImplIntro___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_pushConjInExist___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Proof_propagationBottom___rarg___closed__2;
static lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__1;
static lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__1;
lean_object* l_ML_Pattern_disjunction___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_selfImplTopImplSelf(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_negImplIntro(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_conjElimLeft___rarg(lean_object*, lean_object*);
lean_object* l_Lean_Name_mkStr4(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Proof_propagationBottom_x27___rarg___closed__5;
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottomR(lean_object*, lean_object*);
static lean_object* l_ML_term___u22a2_____closed__9;
LEAN_EXPORT lean_object* l_ML_Proof_excluddedMiddle(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroRight___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_conjElimRight(lean_object*, lean_object*);
uint8_t lean_nat_dec_le(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Proof_0__ML_Proof_size_match__1_splitter(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u1d39_____closed__3;
lean_object* lean_nat_add(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_toRule2___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_implExistSelf(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroRight(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__2;
LEAN_EXPORT lean_object* l_ML_Proof_equivSelf(lean_object*, lean_object*);
static lean_object* l_ML_term___u22a2_____closed__3;
LEAN_EXPORT lean_object* l_ML_Proof_doubleNegIntro(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_negConjAsImpl(lean_object*, lean_object*);
lean_object* l_String_toSubstring_x27(lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_propagationDisj___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_framing___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_permutationConj(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_contractionConj(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Proof_modusPonensThm___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u22a2_____closed__2;
static lean_object* l_ML_term___u22a2_____closed__1;
LEAN_EXPORT lean_object* l_ML_Proof_doubleNegIntro___rarg(lean_object*);
static lean_object* l_ML_term___u22a2_____closed__4;
static lean_object* _init_l_ML_term___u22a2_____closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML", 2);
return x_1;
}
}
static lean_object* _init_l_ML_term___u22a2_____closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_⊢_", 9);
return x_1;
}
}
static lean_object* _init_l_ML_term___u22a2_____closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term___u22a2_____closed__1;
x_2 = l_ML_term___u22a2_____closed__2;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term___u22a2_____closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("andthen", 7);
return x_1;
}
}
static lean_object* _init_l_ML_term___u22a2_____closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_term___u22a2_____closed__4;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term___u22a2_____closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(" ⊢ ", 5);
return x_1;
}
}
static lean_object* _init_l_ML_term___u22a2_____closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term___u22a2_____closed__6;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term___u22a2_____closed__8() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term", 4);
return x_1;
}
}
static lean_object* _init_l_ML_term___u22a2_____closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_term___u22a2_____closed__8;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term___u22a2_____closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term___u22a2_____closed__9;
x_2 = lean_unsigned_to_nat(26u);
x_3 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term___u22a2_____closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term___u22a2_____closed__5;
x_2 = l_ML_term___u22a2_____closed__7;
x_3 = l_ML_term___u22a2_____closed__10;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term___u22a2_____closed__12() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_term___u22a2_____closed__3;
x_2 = lean_unsigned_to_nat(25u);
x_3 = lean_unsigned_to_nat(26u);
x_4 = l_ML_term___u22a2_____closed__11;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_term___u22a2__() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_term___u22a2_____closed__12;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Lean", 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Parser", 6);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Term", 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("app", 3);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__1;
x_2 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__2;
x_3 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__3;
x_4 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__4;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Proof", 5);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__6;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__6;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term___u22a2_____closed__1;
x_2 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__6;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__9;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__9;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__12() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__11;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__10;
x_2 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__12;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__14() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("null", 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__14;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_term___u22a2_____closed__3;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; uint8_t x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
lean_dec(x_1);
x_12 = lean_ctor_get(x_2, 5);
lean_inc(x_12);
x_13 = 0;
x_14 = l_Lean_SourceInfo_fromRef(x_12, x_13);
x_15 = lean_ctor_get(x_2, 2);
lean_inc(x_15);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
lean_dec(x_2);
x_17 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__8;
x_18 = l_Lean_addMacroScope(x_16, x_17, x_15);
x_19 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__7;
x_20 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__13;
lean_inc(x_14);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_14);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__15;
lean_inc(x_14);
x_23 = l_Lean_Syntax_node2(x_14, x_22, x_9, x_11);
x_24 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__5;
x_25 = l_Lean_Syntax_node2(x_14, x_24, x_21, x_23);
x_26 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_26, 0, x_25);
lean_ctor_set(x_26, 1, x_3);
return x_26;
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; 
x_4 = lean_box(0);
x_5 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_5, 0, x_4);
lean_ctor_set(x_5, 1, x_3);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_unsigned_to_nat(0u);
x_2 = lean_mk_empty_array_with_capacity(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; 
x_8 = l_Lean_replaceRef(x_1, x_6);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = l_ML_term___u22a2_____closed__6;
lean_inc(x_10);
x_12 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_11);
x_13 = l_ML_term___u22a2_____closed__3;
lean_inc(x_10);
x_14 = l_Lean_Syntax_node3(x_10, x_13, x_3, x_12, x_4);
x_15 = l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2___closed__1;
x_16 = l_Array_append___rarg(x_15, x_5);
x_17 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__15;
lean_inc(x_10);
x_18 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_18, 0, x_10);
lean_ctor_set(x_18, 1, x_17);
lean_ctor_set(x_18, 2, x_16);
x_19 = l_Lean_Syntax_node2(x_10, x_2, x_14, x_18);
x_20 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_20, 0, x_19);
lean_ctor_set(x_20, 1, x_7);
return x_20;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ident", 5);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_alloc_closure((void*)(l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__1___boxed), 3, 0);
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__5;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; uint8_t x_18; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__3;
x_17 = lean_unsigned_to_nat(2u);
lean_inc(x_15);
x_18 = l_Lean_Syntax_matchesNull(x_15, x_17);
if (x_18 == 0)
{
lean_object* x_19; uint8_t x_20; 
x_19 = l_Lean_Syntax_getNumArgs(x_15);
x_20 = lean_nat_dec_le(x_17, x_19);
if (x_20 == 0)
{
lean_object* x_21; lean_object* x_22; 
lean_dec(x_19);
lean_dec(x_15);
lean_dec(x_9);
x_21 = lean_box(0);
x_22 = lean_apply_3(x_16, x_21, x_2, x_3);
return x_22;
}
else
{
lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; 
x_23 = l_Lean_Syntax_getArg(x_15, x_8);
x_24 = l_Lean_Syntax_getArg(x_15, x_14);
x_25 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_26 = lean_nat_sub(x_19, x_8);
lean_dec(x_19);
x_27 = l_Array_extract___rarg(x_25, x_17, x_26);
lean_dec(x_26);
lean_dec(x_25);
x_28 = lean_box(2);
x_29 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__15;
x_30 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_30, 0, x_28);
lean_ctor_set(x_30, 1, x_29);
lean_ctor_set(x_30, 2, x_27);
x_31 = l_Lean_Syntax_getArgs(x_30);
lean_dec(x_30);
x_32 = l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2(x_9, x_4, x_23, x_24, x_31, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_32;
}
}
else
{
lean_object* x_33; lean_object* x_34; lean_object* x_35; uint8_t x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; 
x_33 = l_Lean_Syntax_getArg(x_15, x_8);
x_34 = l_Lean_Syntax_getArg(x_15, x_14);
lean_dec(x_15);
x_35 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_36 = 0;
x_37 = l_Lean_SourceInfo_fromRef(x_35, x_36);
x_38 = l_ML_term___u22a2_____closed__6;
lean_inc(x_37);
x_39 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_39, 0, x_37);
lean_ctor_set(x_39, 1, x_38);
x_40 = l_ML_term___u22a2_____closed__3;
x_41 = l_Lean_Syntax_node3(x_37, x_40, x_33, x_39, x_34);
x_42 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_42, 0, x_41);
lean_ctor_set(x_42, 1, x_3);
return x_42;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__1(x_1, x_2, x_3);
lean_dec(x_2);
lean_dec(x_1);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; 
x_8 = l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2(x_1, x_2, x_3, x_4, x_5, x_6, x_7);
lean_dec(x_6);
lean_dec(x_1);
return x_8;
}
}
static lean_object* _init_l_ML_term_u1d39_____closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("termᴹ_", 8);
return x_1;
}
}
static lean_object* _init_l_ML_term_u1d39_____closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term___u22a2_____closed__1;
x_2 = l_ML_term_u1d39_____closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term_u1d39_____closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ᴹ", 3);
return x_1;
}
}
static lean_object* _init_l_ML_term_u1d39_____closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term_u1d39_____closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term_u1d39_____closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term___u22a2_____closed__9;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term_u1d39_____closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term___u22a2_____closed__5;
x_2 = l_ML_term_u1d39_____closed__4;
x_3 = l_ML_term_u1d39_____closed__5;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u1d39_____closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u1d39_____closed__2;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = l_ML_term_u1d39_____closed__6;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u1d39__() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_term_u1d39_____closed__7;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Proof.modusPonens", 17);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("modusPonens", 11);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__6;
x_2 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__3;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term___u22a2_____closed__1;
x_2 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__6;
x_3 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__3;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__5;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__5;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__6;
x_2 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__8;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_term_u1d39_____closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; 
x_8 = lean_unsigned_to_nat(1u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
lean_dec(x_1);
x_10 = lean_ctor_get(x_2, 5);
lean_inc(x_10);
x_11 = 0;
x_12 = l_Lean_SourceInfo_fromRef(x_10, x_11);
x_13 = lean_ctor_get(x_2, 2);
lean_inc(x_13);
x_14 = lean_ctor_get(x_2, 1);
lean_inc(x_14);
lean_dec(x_2);
x_15 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__4;
x_16 = l_Lean_addMacroScope(x_14, x_15, x_13);
x_17 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__2;
x_18 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__9;
lean_inc(x_12);
x_19 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_19, 0, x_12);
lean_ctor_set(x_19, 1, x_17);
lean_ctor_set(x_19, 2, x_16);
lean_ctor_set(x_19, 3, x_18);
x_20 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__15;
lean_inc(x_12);
x_21 = l_Lean_Syntax_node1(x_12, x_20, x_9);
x_22 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__5;
x_23 = l_Lean_Syntax_node2(x_12, x_22, x_19, x_21);
x_24 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_24, 0, x_23);
lean_ctor_set(x_24, 1, x_3);
return x_24;
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__modusPonens__1___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; uint8_t x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; 
x_7 = l_Lean_replaceRef(x_1, x_5);
x_8 = 0;
x_9 = l_Lean_SourceInfo_fromRef(x_7, x_8);
x_10 = l_ML_term_u1d39_____closed__3;
lean_inc(x_9);
x_11 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_11, 0, x_9);
lean_ctor_set(x_11, 1, x_10);
x_12 = l_ML_term_u1d39_____closed__2;
lean_inc(x_9);
x_13 = l_Lean_Syntax_node2(x_9, x_12, x_11, x_3);
x_14 = l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2___closed__1;
x_15 = l_Array_append___rarg(x_14, x_4);
x_16 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__15;
lean_inc(x_9);
x_17 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_17, 0, x_9);
lean_ctor_set(x_17, 1, x_16);
lean_ctor_set(x_17, 2, x_15);
x_18 = l_Lean_Syntax_node2(x_9, x_2, x_13, x_17);
x_19 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_19, 0, x_18);
lean_ctor_set(x_19, 1, x_6);
return x_19;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__modusPonens__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__5;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; uint8_t x_16; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
lean_inc(x_15);
x_16 = l_Lean_Syntax_matchesNull(x_15, x_14);
if (x_16 == 0)
{
lean_object* x_17; uint8_t x_18; 
x_17 = l_Lean_Syntax_getNumArgs(x_15);
x_18 = lean_nat_dec_le(x_14, x_17);
if (x_18 == 0)
{
lean_object* x_19; lean_object* x_20; 
lean_dec(x_17);
lean_dec(x_15);
lean_dec(x_9);
lean_dec(x_2);
x_19 = lean_box(0);
x_20 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_20, 0, x_19);
lean_ctor_set(x_20, 1, x_3);
return x_20;
}
else
{
lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; 
x_21 = l_Lean_Syntax_getArg(x_15, x_8);
x_22 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_23 = lean_nat_sub(x_17, x_8);
lean_dec(x_17);
x_24 = l_Array_extract___rarg(x_22, x_14, x_23);
lean_dec(x_23);
lean_dec(x_22);
x_25 = lean_box(2);
x_26 = l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__15;
x_27 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_27, 0, x_25);
lean_ctor_set(x_27, 1, x_26);
lean_ctor_set(x_27, 2, x_24);
x_28 = l_Lean_Syntax_getArgs(x_27);
lean_dec(x_27);
x_29 = l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__modusPonens__1___lambda__1(x_9, x_4, x_21, x_28, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_29;
}
}
else
{
lean_object* x_30; lean_object* x_31; uint8_t x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; 
x_30 = l_Lean_Syntax_getArg(x_15, x_8);
lean_dec(x_15);
x_31 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_32 = 0;
x_33 = l_Lean_SourceInfo_fromRef(x_31, x_32);
x_34 = l_ML_term_u1d39_____closed__3;
lean_inc(x_33);
x_35 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_35, 0, x_33);
lean_ctor_set(x_35, 1, x_34);
x_36 = l_ML_term_u1d39_____closed__2;
x_37 = l_Lean_Syntax_node2(x_33, x_36, x_35, x_30);
x_38 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_38, 0, x_37);
lean_ctor_set(x_38, 1, x_3);
return x_38;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__modusPonens__1___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; 
x_7 = l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__modusPonens__1___lambda__1(x_1, x_2, x_3, x_4, x_5, x_6);
lean_dec(x_5);
lean_dec(x_1);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_size___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
switch (lean_obj_tag(x_2)) {
case 2:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; 
x_3 = lean_ctor_get(x_2, 0);
lean_inc(x_3);
x_4 = lean_ctor_get(x_2, 2);
lean_inc(x_4);
x_5 = lean_ctor_get(x_2, 3);
lean_inc(x_5);
lean_dec(x_2);
lean_inc(x_3);
x_6 = l_ML_Proof_size___rarg(x_3, x_4);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_3);
lean_ctor_set(x_7, 1, x_1);
x_8 = l_ML_Proof_size___rarg(x_7, x_5);
x_9 = lean_nat_add(x_6, x_8);
lean_dec(x_8);
lean_dec(x_6);
x_10 = lean_unsigned_to_nat(1u);
x_11 = lean_nat_add(x_9, x_10);
lean_dec(x_9);
return x_11;
}
case 4:
{
lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_1);
x_12 = lean_ctor_get(x_2, 0);
lean_inc(x_12);
x_13 = lean_ctor_get(x_2, 1);
lean_inc(x_13);
x_14 = lean_ctor_get(x_2, 3);
lean_inc(x_14);
lean_dec(x_2);
x_15 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_15, 0, x_12);
lean_ctor_set(x_15, 1, x_13);
x_16 = l_ML_Proof_size___rarg(x_15, x_14);
x_17 = lean_unsigned_to_nat(1u);
x_18 = lean_nat_add(x_16, x_17);
lean_dec(x_16);
return x_18;
}
case 13:
{
lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; 
lean_dec(x_1);
x_19 = lean_ctor_get(x_2, 0);
lean_inc(x_19);
x_20 = lean_ctor_get(x_2, 1);
lean_inc(x_20);
x_21 = lean_ctor_get(x_2, 3);
lean_inc(x_21);
lean_dec(x_2);
x_22 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_22, 0, x_19);
lean_ctor_set(x_22, 1, x_20);
x_23 = l_ML_Proof_size___rarg(x_22, x_21);
x_24 = lean_unsigned_to_nat(1u);
x_25 = lean_nat_add(x_23, x_24);
lean_dec(x_23);
return x_25;
}
case 14:
{
lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; 
lean_dec(x_1);
x_26 = lean_ctor_get(x_2, 0);
lean_inc(x_26);
x_27 = lean_ctor_get(x_2, 1);
lean_inc(x_27);
x_28 = lean_ctor_get(x_2, 3);
lean_inc(x_28);
lean_dec(x_2);
x_29 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_29, 0, x_26);
lean_ctor_set(x_29, 1, x_27);
x_30 = l_ML_Proof_size___rarg(x_29, x_28);
x_31 = lean_unsigned_to_nat(1u);
x_32 = lean_nat_add(x_30, x_31);
lean_dec(x_30);
return x_32;
}
case 15:
{
lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; 
lean_dec(x_1);
x_33 = lean_ctor_get(x_2, 0);
lean_inc(x_33);
x_34 = lean_ctor_get(x_2, 3);
lean_inc(x_34);
lean_dec(x_2);
x_35 = l_ML_Proof_size___rarg(x_33, x_34);
x_36 = lean_unsigned_to_nat(1u);
x_37 = lean_nat_add(x_35, x_36);
lean_dec(x_35);
return x_37;
}
case 17:
{
lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; 
lean_dec(x_1);
x_38 = lean_ctor_get(x_2, 0);
lean_inc(x_38);
x_39 = lean_ctor_get(x_2, 1);
lean_inc(x_39);
x_40 = lean_ctor_get(x_2, 2);
lean_inc(x_40);
x_41 = lean_ctor_get(x_2, 3);
lean_inc(x_41);
lean_dec(x_2);
x_42 = l_ML_Pattern_substSvar___rarg(x_38, x_40, x_39);
lean_dec(x_40);
x_43 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_43, 0, x_42);
lean_ctor_set(x_43, 1, x_39);
x_44 = l_ML_Proof_size___rarg(x_43, x_41);
x_45 = lean_unsigned_to_nat(1u);
x_46 = lean_nat_add(x_44, x_45);
lean_dec(x_44);
return x_46;
}
default: 
{
lean_object* x_47; 
lean_dec(x_2);
lean_dec(x_1);
x_47 = lean_unsigned_to_nat(1u);
return x_47;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Proof_size(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_size___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Proof_0__ML_Proof_size_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8, lean_object* x_9, lean_object* x_10, lean_object* x_11, lean_object* x_12, lean_object* x_13, lean_object* x_14, lean_object* x_15, lean_object* x_16, lean_object* x_17, lean_object* x_18, lean_object* x_19, lean_object* x_20) {
_start:
{
switch (lean_obj_tag(x_2)) {
case 0:
{
lean_object* x_21; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_21 = lean_apply_3(x_3, x_1, lean_box(0), lean_box(0));
return x_21;
}
case 1:
{
lean_object* x_22; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_22 = lean_apply_2(x_4, x_1, lean_box(0));
return x_22;
}
case 2:
{
lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
x_23 = lean_ctor_get(x_2, 0);
lean_inc(x_23);
x_24 = lean_ctor_get(x_2, 2);
lean_inc(x_24);
x_25 = lean_ctor_get(x_2, 3);
lean_inc(x_25);
lean_dec(x_2);
x_26 = lean_apply_4(x_5, x_1, x_23, x_24, x_25);
return x_26;
}
case 3:
{
lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_27 = lean_ctor_get(x_2, 0);
lean_inc(x_27);
x_28 = lean_ctor_get(x_2, 1);
lean_inc(x_28);
x_29 = lean_ctor_get(x_2, 2);
lean_inc(x_29);
lean_dec(x_2);
x_30 = lean_apply_4(x_6, x_27, x_28, x_29, lean_box(0));
return x_30;
}
case 4:
{
lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_31 = lean_ctor_get(x_2, 0);
lean_inc(x_31);
x_32 = lean_ctor_get(x_2, 1);
lean_inc(x_32);
x_33 = lean_ctor_get(x_2, 2);
lean_inc(x_33);
x_34 = lean_ctor_get(x_2, 3);
lean_inc(x_34);
lean_dec(x_2);
x_35 = lean_apply_5(x_7, x_31, x_32, x_33, lean_box(0), x_34);
return x_35;
}
case 5:
{
lean_object* x_36; lean_object* x_37; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_36 = lean_ctor_get(x_2, 0);
lean_inc(x_36);
lean_dec(x_2);
x_37 = lean_apply_1(x_8, x_36);
return x_37;
}
case 6:
{
lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_38 = lean_ctor_get(x_2, 0);
lean_inc(x_38);
x_39 = lean_ctor_get(x_2, 1);
lean_inc(x_39);
x_40 = lean_ctor_get(x_2, 2);
lean_inc(x_40);
x_41 = lean_ctor_get(x_2, 3);
lean_inc(x_41);
lean_dec(x_2);
x_42 = lean_apply_4(x_9, x_38, x_39, x_40, x_41);
return x_42;
}
case 7:
{
lean_object* x_43; lean_object* x_44; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_43 = lean_ctor_get(x_2, 0);
lean_inc(x_43);
lean_dec(x_2);
x_44 = lean_apply_1(x_10, x_43);
return x_44;
}
case 8:
{
lean_object* x_45; lean_object* x_46; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_45 = lean_ctor_get(x_2, 0);
lean_inc(x_45);
lean_dec(x_2);
x_46 = lean_apply_1(x_11, x_45);
return x_46;
}
case 9:
{
lean_object* x_47; lean_object* x_48; lean_object* x_49; lean_object* x_50; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_47 = lean_ctor_get(x_2, 0);
lean_inc(x_47);
x_48 = lean_ctor_get(x_2, 1);
lean_inc(x_48);
x_49 = lean_ctor_get(x_2, 2);
lean_inc(x_49);
lean_dec(x_2);
x_50 = lean_apply_3(x_12, x_47, x_48, x_49);
return x_50;
}
case 10:
{
lean_object* x_51; lean_object* x_52; lean_object* x_53; lean_object* x_54; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_51 = lean_ctor_get(x_2, 0);
lean_inc(x_51);
x_52 = lean_ctor_get(x_2, 1);
lean_inc(x_52);
x_53 = lean_ctor_get(x_2, 2);
lean_inc(x_53);
lean_dec(x_2);
x_54 = lean_apply_3(x_13, x_51, x_52, x_53);
return x_54;
}
case 11:
{
lean_object* x_55; lean_object* x_56; lean_object* x_57; lean_object* x_58; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_55 = lean_ctor_get(x_2, 0);
lean_inc(x_55);
x_56 = lean_ctor_get(x_2, 1);
lean_inc(x_56);
x_57 = lean_ctor_get(x_2, 2);
lean_inc(x_57);
lean_dec(x_2);
x_58 = lean_apply_4(x_14, x_55, x_56, x_57, lean_box(0));
return x_58;
}
case 12:
{
lean_object* x_59; lean_object* x_60; lean_object* x_61; lean_object* x_62; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_59 = lean_ctor_get(x_2, 0);
lean_inc(x_59);
x_60 = lean_ctor_get(x_2, 1);
lean_inc(x_60);
x_61 = lean_ctor_get(x_2, 2);
lean_inc(x_61);
lean_dec(x_2);
x_62 = lean_apply_4(x_15, x_59, x_60, x_61, lean_box(0));
return x_62;
}
case 13:
{
lean_object* x_63; lean_object* x_64; lean_object* x_65; lean_object* x_66; lean_object* x_67; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_63 = lean_ctor_get(x_2, 0);
lean_inc(x_63);
x_64 = lean_ctor_get(x_2, 1);
lean_inc(x_64);
x_65 = lean_ctor_get(x_2, 2);
lean_inc(x_65);
x_66 = lean_ctor_get(x_2, 3);
lean_inc(x_66);
lean_dec(x_2);
x_67 = lean_apply_4(x_16, x_63, x_64, x_65, x_66);
return x_67;
}
case 14:
{
lean_object* x_68; lean_object* x_69; lean_object* x_70; lean_object* x_71; lean_object* x_72; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_68 = lean_ctor_get(x_2, 0);
lean_inc(x_68);
x_69 = lean_ctor_get(x_2, 1);
lean_inc(x_69);
x_70 = lean_ctor_get(x_2, 2);
lean_inc(x_70);
x_71 = lean_ctor_get(x_2, 3);
lean_inc(x_71);
lean_dec(x_2);
x_72 = lean_apply_4(x_17, x_68, x_69, x_70, x_71);
return x_72;
}
case 15:
{
lean_object* x_73; lean_object* x_74; lean_object* x_75; lean_object* x_76; lean_object* x_77; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_73 = lean_ctor_get(x_2, 0);
lean_inc(x_73);
x_74 = lean_ctor_get(x_2, 1);
lean_inc(x_74);
x_75 = lean_ctor_get(x_2, 2);
lean_inc(x_75);
x_76 = lean_ctor_get(x_2, 3);
lean_inc(x_76);
lean_dec(x_2);
x_77 = lean_apply_5(x_18, x_73, x_74, x_75, lean_box(0), x_76);
return x_77;
}
case 16:
{
lean_object* x_78; lean_object* x_79; lean_object* x_80; 
lean_dec(x_20);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_78 = lean_ctor_get(x_2, 0);
lean_inc(x_78);
x_79 = lean_ctor_get(x_2, 1);
lean_inc(x_79);
lean_dec(x_2);
x_80 = lean_apply_4(x_19, x_78, x_79, lean_box(0), lean_box(0));
return x_80;
}
default: 
{
lean_object* x_81; lean_object* x_82; lean_object* x_83; lean_object* x_84; lean_object* x_85; 
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_81 = lean_ctor_get(x_2, 0);
lean_inc(x_81);
x_82 = lean_ctor_get(x_2, 1);
lean_inc(x_82);
x_83 = lean_ctor_get(x_2, 2);
lean_inc(x_83);
x_84 = lean_ctor_get(x_2, 3);
lean_inc(x_84);
lean_dec(x_2);
x_85 = lean_apply_5(x_20, x_81, x_82, x_83, lean_box(0), x_84);
return x_85;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Proof_0__ML_Proof_size_match__1_splitter(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = lean_alloc_closure((void*)(l___private_MatchingLogic_Proof_0__ML_Proof_size_match__1_splitter___rarg___boxed), 20, 0);
return x_4;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Proof_0__ML_Proof_size_match__1_splitter___rarg___boxed(lean_object** _args) {
lean_object* x_1 = _args[0];
lean_object* x_2 = _args[1];
lean_object* x_3 = _args[2];
lean_object* x_4 = _args[3];
lean_object* x_5 = _args[4];
lean_object* x_6 = _args[5];
lean_object* x_7 = _args[6];
lean_object* x_8 = _args[7];
lean_object* x_9 = _args[8];
lean_object* x_10 = _args[9];
lean_object* x_11 = _args[10];
lean_object* x_12 = _args[11];
lean_object* x_13 = _args[12];
lean_object* x_14 = _args[13];
lean_object* x_15 = _args[14];
lean_object* x_16 = _args[15];
lean_object* x_17 = _args[16];
lean_object* x_18 = _args[17];
lean_object* x_19 = _args[18];
lean_object* x_20 = _args[19];
_start:
{
lean_object* x_21; 
x_21 = l___private_MatchingLogic_Proof_0__ML_Proof_size_match__1_splitter___rarg(x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8, x_9, x_10, x_11, x_12, x_13, x_14, x_15, x_16, x_17, x_18, x_19, x_20);
return x_21;
}
}
LEAN_EXPORT uint8_t l_ML_Proof_usesKnasterTarski___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
switch (lean_obj_tag(x_2)) {
case 2:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; uint8_t x_6; 
x_3 = lean_ctor_get(x_2, 0);
lean_inc(x_3);
x_4 = lean_ctor_get(x_2, 2);
lean_inc(x_4);
x_5 = lean_ctor_get(x_2, 3);
lean_inc(x_5);
lean_dec(x_2);
lean_inc(x_3);
x_6 = l_ML_Proof_usesKnasterTarski___rarg(x_3, x_4);
if (x_6 == 0)
{
lean_object* x_7; 
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_3);
lean_ctor_set(x_7, 1, x_1);
x_1 = x_7;
x_2 = x_5;
goto _start;
}
else
{
uint8_t x_9; 
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_1);
x_9 = 1;
return x_9;
}
}
case 4:
{
lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; 
lean_dec(x_1);
x_10 = lean_ctor_get(x_2, 0);
lean_inc(x_10);
x_11 = lean_ctor_get(x_2, 1);
lean_inc(x_11);
x_12 = lean_ctor_get(x_2, 3);
lean_inc(x_12);
lean_dec(x_2);
x_13 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_13, 0, x_10);
lean_ctor_set(x_13, 1, x_11);
x_1 = x_13;
x_2 = x_12;
goto _start;
}
case 13:
{
lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_1);
x_15 = lean_ctor_get(x_2, 0);
lean_inc(x_15);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
x_17 = lean_ctor_get(x_2, 3);
lean_inc(x_17);
lean_dec(x_2);
x_18 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_18, 0, x_15);
lean_ctor_set(x_18, 1, x_16);
x_1 = x_18;
x_2 = x_17;
goto _start;
}
case 14:
{
lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; 
lean_dec(x_1);
x_20 = lean_ctor_get(x_2, 0);
lean_inc(x_20);
x_21 = lean_ctor_get(x_2, 1);
lean_inc(x_21);
x_22 = lean_ctor_get(x_2, 3);
lean_inc(x_22);
lean_dec(x_2);
x_23 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_23, 0, x_20);
lean_ctor_set(x_23, 1, x_21);
x_1 = x_23;
x_2 = x_22;
goto _start;
}
case 15:
{
lean_object* x_25; lean_object* x_26; 
lean_dec(x_1);
x_25 = lean_ctor_get(x_2, 0);
lean_inc(x_25);
x_26 = lean_ctor_get(x_2, 3);
lean_inc(x_26);
lean_dec(x_2);
x_1 = x_25;
x_2 = x_26;
goto _start;
}
case 17:
{
uint8_t x_28; 
lean_dec(x_2);
lean_dec(x_1);
x_28 = 1;
return x_28;
}
default: 
{
uint8_t x_29; 
lean_dec(x_2);
lean_dec(x_1);
x_29 = 0;
return x_29;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Proof_usesKnasterTarski(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_usesKnasterTarski___rarg___boxed), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_usesKnasterTarski___rarg___boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; lean_object* x_4; 
x_3 = l_ML_Proof_usesKnasterTarski___rarg(x_1, x_2);
x_4 = lean_box(x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Proof_0__ML_Proof_usesKnasterTarski_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8, lean_object* x_9, lean_object* x_10, lean_object* x_11, lean_object* x_12, lean_object* x_13, lean_object* x_14, lean_object* x_15, lean_object* x_16, lean_object* x_17, lean_object* x_18, lean_object* x_19, lean_object* x_20) {
_start:
{
switch (lean_obj_tag(x_2)) {
case 0:
{
lean_object* x_21; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_21 = lean_apply_3(x_3, x_1, lean_box(0), lean_box(0));
return x_21;
}
case 1:
{
lean_object* x_22; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_22 = lean_apply_2(x_4, x_1, lean_box(0));
return x_22;
}
case 2:
{
lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_23 = lean_ctor_get(x_2, 0);
lean_inc(x_23);
x_24 = lean_ctor_get(x_2, 2);
lean_inc(x_24);
x_25 = lean_ctor_get(x_2, 3);
lean_inc(x_25);
lean_dec(x_2);
x_26 = lean_apply_4(x_15, x_1, x_23, x_24, x_25);
return x_26;
}
case 3:
{
lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_27 = lean_ctor_get(x_2, 0);
lean_inc(x_27);
x_28 = lean_ctor_get(x_2, 1);
lean_inc(x_28);
x_29 = lean_ctor_get(x_2, 2);
lean_inc(x_29);
lean_dec(x_2);
x_30 = lean_apply_4(x_5, x_27, x_28, x_29, lean_box(0));
return x_30;
}
case 4:
{
lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_31 = lean_ctor_get(x_2, 0);
lean_inc(x_31);
x_32 = lean_ctor_get(x_2, 1);
lean_inc(x_32);
x_33 = lean_ctor_get(x_2, 2);
lean_inc(x_33);
x_34 = lean_ctor_get(x_2, 3);
lean_inc(x_34);
lean_dec(x_2);
x_35 = lean_apply_5(x_16, x_31, x_32, x_33, lean_box(0), x_34);
return x_35;
}
case 5:
{
lean_object* x_36; lean_object* x_37; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_36 = lean_ctor_get(x_2, 0);
lean_inc(x_36);
lean_dec(x_2);
x_37 = lean_apply_1(x_6, x_36);
return x_37;
}
case 6:
{
lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_38 = lean_ctor_get(x_2, 0);
lean_inc(x_38);
x_39 = lean_ctor_get(x_2, 1);
lean_inc(x_39);
x_40 = lean_ctor_get(x_2, 2);
lean_inc(x_40);
x_41 = lean_ctor_get(x_2, 3);
lean_inc(x_41);
lean_dec(x_2);
x_42 = lean_apply_4(x_7, x_38, x_39, x_40, x_41);
return x_42;
}
case 7:
{
lean_object* x_43; lean_object* x_44; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_43 = lean_ctor_get(x_2, 0);
lean_inc(x_43);
lean_dec(x_2);
x_44 = lean_apply_1(x_8, x_43);
return x_44;
}
case 8:
{
lean_object* x_45; lean_object* x_46; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_45 = lean_ctor_get(x_2, 0);
lean_inc(x_45);
lean_dec(x_2);
x_46 = lean_apply_1(x_9, x_45);
return x_46;
}
case 9:
{
lean_object* x_47; lean_object* x_48; lean_object* x_49; lean_object* x_50; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_47 = lean_ctor_get(x_2, 0);
lean_inc(x_47);
x_48 = lean_ctor_get(x_2, 1);
lean_inc(x_48);
x_49 = lean_ctor_get(x_2, 2);
lean_inc(x_49);
lean_dec(x_2);
x_50 = lean_apply_3(x_10, x_47, x_48, x_49);
return x_50;
}
case 10:
{
lean_object* x_51; lean_object* x_52; lean_object* x_53; lean_object* x_54; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_51 = lean_ctor_get(x_2, 0);
lean_inc(x_51);
x_52 = lean_ctor_get(x_2, 1);
lean_inc(x_52);
x_53 = lean_ctor_get(x_2, 2);
lean_inc(x_53);
lean_dec(x_2);
x_54 = lean_apply_3(x_11, x_51, x_52, x_53);
return x_54;
}
case 11:
{
lean_object* x_55; lean_object* x_56; lean_object* x_57; lean_object* x_58; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_55 = lean_ctor_get(x_2, 0);
lean_inc(x_55);
x_56 = lean_ctor_get(x_2, 1);
lean_inc(x_56);
x_57 = lean_ctor_get(x_2, 2);
lean_inc(x_57);
lean_dec(x_2);
x_58 = lean_apply_4(x_12, x_55, x_56, x_57, lean_box(0));
return x_58;
}
case 12:
{
lean_object* x_59; lean_object* x_60; lean_object* x_61; lean_object* x_62; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_59 = lean_ctor_get(x_2, 0);
lean_inc(x_59);
x_60 = lean_ctor_get(x_2, 1);
lean_inc(x_60);
x_61 = lean_ctor_get(x_2, 2);
lean_inc(x_61);
lean_dec(x_2);
x_62 = lean_apply_4(x_13, x_59, x_60, x_61, lean_box(0));
return x_62;
}
case 13:
{
lean_object* x_63; lean_object* x_64; lean_object* x_65; lean_object* x_66; lean_object* x_67; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_63 = lean_ctor_get(x_2, 0);
lean_inc(x_63);
x_64 = lean_ctor_get(x_2, 1);
lean_inc(x_64);
x_65 = lean_ctor_get(x_2, 2);
lean_inc(x_65);
x_66 = lean_ctor_get(x_2, 3);
lean_inc(x_66);
lean_dec(x_2);
x_67 = lean_apply_4(x_17, x_63, x_64, x_65, x_66);
return x_67;
}
case 14:
{
lean_object* x_68; lean_object* x_69; lean_object* x_70; lean_object* x_71; lean_object* x_72; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_68 = lean_ctor_get(x_2, 0);
lean_inc(x_68);
x_69 = lean_ctor_get(x_2, 1);
lean_inc(x_69);
x_70 = lean_ctor_get(x_2, 2);
lean_inc(x_70);
x_71 = lean_ctor_get(x_2, 3);
lean_inc(x_71);
lean_dec(x_2);
x_72 = lean_apply_4(x_18, x_68, x_69, x_70, x_71);
return x_72;
}
case 15:
{
lean_object* x_73; lean_object* x_74; lean_object* x_75; lean_object* x_76; lean_object* x_77; 
lean_dec(x_20);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_73 = lean_ctor_get(x_2, 0);
lean_inc(x_73);
x_74 = lean_ctor_get(x_2, 1);
lean_inc(x_74);
x_75 = lean_ctor_get(x_2, 2);
lean_inc(x_75);
x_76 = lean_ctor_get(x_2, 3);
lean_inc(x_76);
lean_dec(x_2);
x_77 = lean_apply_5(x_19, x_73, x_74, x_75, lean_box(0), x_76);
return x_77;
}
case 16:
{
lean_object* x_78; lean_object* x_79; lean_object* x_80; 
lean_dec(x_20);
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_78 = lean_ctor_get(x_2, 0);
lean_inc(x_78);
x_79 = lean_ctor_get(x_2, 1);
lean_inc(x_79);
lean_dec(x_2);
x_80 = lean_apply_4(x_14, x_78, x_79, lean_box(0), lean_box(0));
return x_80;
}
default: 
{
lean_object* x_81; lean_object* x_82; lean_object* x_83; lean_object* x_84; lean_object* x_85; 
lean_dec(x_19);
lean_dec(x_18);
lean_dec(x_17);
lean_dec(x_16);
lean_dec(x_15);
lean_dec(x_14);
lean_dec(x_13);
lean_dec(x_12);
lean_dec(x_11);
lean_dec(x_10);
lean_dec(x_9);
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_1);
x_81 = lean_ctor_get(x_2, 0);
lean_inc(x_81);
x_82 = lean_ctor_get(x_2, 1);
lean_inc(x_82);
x_83 = lean_ctor_get(x_2, 2);
lean_inc(x_83);
x_84 = lean_ctor_get(x_2, 3);
lean_inc(x_84);
lean_dec(x_2);
x_85 = lean_apply_5(x_20, x_81, x_82, x_83, lean_box(0), x_84);
return x_85;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Proof_0__ML_Proof_usesKnasterTarski_match__1_splitter(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = lean_alloc_closure((void*)(l___private_MatchingLogic_Proof_0__ML_Proof_usesKnasterTarski_match__1_splitter___rarg___boxed), 20, 0);
return x_4;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Proof_0__ML_Proof_usesKnasterTarski_match__1_splitter___rarg___boxed(lean_object** _args) {
lean_object* x_1 = _args[0];
lean_object* x_2 = _args[1];
lean_object* x_3 = _args[2];
lean_object* x_4 = _args[3];
lean_object* x_5 = _args[4];
lean_object* x_6 = _args[5];
lean_object* x_7 = _args[6];
lean_object* x_8 = _args[7];
lean_object* x_9 = _args[8];
lean_object* x_10 = _args[9];
lean_object* x_11 = _args[10];
lean_object* x_12 = _args[11];
lean_object* x_13 = _args[12];
lean_object* x_14 = _args[13];
lean_object* x_15 = _args[14];
lean_object* x_16 = _args[15];
lean_object* x_17 = _args[16];
lean_object* x_18 = _args[17];
lean_object* x_19 = _args[18];
lean_object* x_20 = _args[19];
_start:
{
lean_object* x_21; 
x_21 = l___private_MatchingLogic_Proof_0__ML_Proof_usesKnasterTarski_match__1_splitter___rarg(x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8, x_9, x_10, x_11, x_12, x_13, x_14, x_15, x_16, x_17, x_18, x_19, x_20);
return x_21;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_weaken___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
switch (lean_obj_tag(x_5)) {
case 0:
{
uint8_t x_6; 
x_6 = !lean_is_exclusive(x_5);
if (x_6 == 0)
{
lean_object* x_7; 
x_7 = lean_ctor_get(x_5, 0);
lean_dec(x_7);
lean_ctor_set(x_5, 0, x_1);
return x_5;
}
else
{
lean_object* x_8; 
lean_dec(x_5);
x_8 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_8, 0, x_1);
return x_8;
}
}
case 1:
{
uint8_t x_9; 
x_9 = !lean_is_exclusive(x_5);
if (x_9 == 0)
{
lean_object* x_10; 
x_10 = lean_ctor_get(x_5, 0);
lean_dec(x_10);
lean_ctor_set(x_5, 0, x_1);
return x_5;
}
else
{
lean_object* x_11; 
lean_dec(x_5);
x_11 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_11, 0, x_1);
return x_11;
}
}
case 2:
{
uint8_t x_12; 
x_12 = !lean_is_exclusive(x_5);
if (x_12 == 0)
{
lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; 
x_13 = lean_ctor_get(x_5, 0);
x_14 = lean_ctor_get(x_5, 2);
x_15 = lean_ctor_get(x_5, 3);
x_16 = lean_ctor_get(x_5, 1);
lean_dec(x_16);
lean_inc(x_13);
x_17 = l_ML_Proof_weaken___rarg(x_13, lean_box(0), lean_box(0), lean_box(0), x_14);
lean_inc(x_1);
lean_inc(x_13);
x_18 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_18, 0, x_13);
lean_ctor_set(x_18, 1, x_1);
x_19 = l_ML_Proof_weaken___rarg(x_18, lean_box(0), lean_box(0), lean_box(0), x_15);
lean_ctor_set(x_5, 3, x_19);
lean_ctor_set(x_5, 2, x_17);
lean_ctor_set(x_5, 1, x_1);
return x_5;
}
else
{
lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
x_20 = lean_ctor_get(x_5, 0);
x_21 = lean_ctor_get(x_5, 2);
x_22 = lean_ctor_get(x_5, 3);
lean_inc(x_22);
lean_inc(x_21);
lean_inc(x_20);
lean_dec(x_5);
lean_inc(x_20);
x_23 = l_ML_Proof_weaken___rarg(x_20, lean_box(0), lean_box(0), lean_box(0), x_21);
lean_inc(x_1);
lean_inc(x_20);
x_24 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_24, 0, x_20);
lean_ctor_set(x_24, 1, x_1);
x_25 = l_ML_Proof_weaken___rarg(x_24, lean_box(0), lean_box(0), lean_box(0), x_22);
x_26 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_26, 0, x_20);
lean_ctor_set(x_26, 1, x_1);
lean_ctor_set(x_26, 2, x_23);
lean_ctor_set(x_26, 3, x_25);
return x_26;
}
}
case 3:
{
uint8_t x_27; 
lean_dec(x_1);
x_27 = !lean_is_exclusive(x_5);
if (x_27 == 0)
{
return x_5;
}
else
{
lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; 
x_28 = lean_ctor_get(x_5, 0);
x_29 = lean_ctor_get(x_5, 1);
x_30 = lean_ctor_get(x_5, 2);
lean_inc(x_30);
lean_inc(x_29);
lean_inc(x_28);
lean_dec(x_5);
x_31 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_31, 0, x_28);
lean_ctor_set(x_31, 1, x_29);
lean_ctor_set(x_31, 2, x_30);
return x_31;
}
}
case 4:
{
uint8_t x_32; 
lean_dec(x_1);
x_32 = !lean_is_exclusive(x_5);
if (x_32 == 0)
{
lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; 
x_33 = lean_ctor_get(x_5, 0);
x_34 = lean_ctor_get(x_5, 1);
x_35 = lean_ctor_get(x_5, 3);
lean_inc(x_34);
lean_inc(x_33);
x_36 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_36, 0, x_33);
lean_ctor_set(x_36, 1, x_34);
x_37 = l_ML_Proof_weaken___rarg(x_36, lean_box(0), lean_box(0), lean_box(0), x_35);
lean_ctor_set(x_5, 3, x_37);
return x_5;
}
else
{
lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; 
x_38 = lean_ctor_get(x_5, 0);
x_39 = lean_ctor_get(x_5, 1);
x_40 = lean_ctor_get(x_5, 2);
x_41 = lean_ctor_get(x_5, 3);
lean_inc(x_41);
lean_inc(x_40);
lean_inc(x_39);
lean_inc(x_38);
lean_dec(x_5);
lean_inc(x_39);
lean_inc(x_38);
x_42 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_42, 0, x_38);
lean_ctor_set(x_42, 1, x_39);
x_43 = l_ML_Proof_weaken___rarg(x_42, lean_box(0), lean_box(0), lean_box(0), x_41);
x_44 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_44, 0, x_38);
lean_ctor_set(x_44, 1, x_39);
lean_ctor_set(x_44, 2, x_40);
lean_ctor_set(x_44, 3, x_43);
return x_44;
}
}
case 5:
{
uint8_t x_45; 
lean_dec(x_1);
x_45 = !lean_is_exclusive(x_5);
if (x_45 == 0)
{
return x_5;
}
else
{
lean_object* x_46; lean_object* x_47; 
x_46 = lean_ctor_get(x_5, 0);
lean_inc(x_46);
lean_dec(x_5);
x_47 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_47, 0, x_46);
return x_47;
}
}
case 6:
{
uint8_t x_48; 
lean_dec(x_1);
x_48 = !lean_is_exclusive(x_5);
if (x_48 == 0)
{
return x_5;
}
else
{
lean_object* x_49; lean_object* x_50; lean_object* x_51; lean_object* x_52; lean_object* x_53; 
x_49 = lean_ctor_get(x_5, 0);
x_50 = lean_ctor_get(x_5, 1);
x_51 = lean_ctor_get(x_5, 2);
x_52 = lean_ctor_get(x_5, 3);
lean_inc(x_52);
lean_inc(x_51);
lean_inc(x_50);
lean_inc(x_49);
lean_dec(x_5);
x_53 = lean_alloc_ctor(6, 4, 0);
lean_ctor_set(x_53, 0, x_49);
lean_ctor_set(x_53, 1, x_50);
lean_ctor_set(x_53, 2, x_51);
lean_ctor_set(x_53, 3, x_52);
return x_53;
}
}
case 7:
{
uint8_t x_54; 
lean_dec(x_1);
x_54 = !lean_is_exclusive(x_5);
if (x_54 == 0)
{
return x_5;
}
else
{
lean_object* x_55; lean_object* x_56; 
x_55 = lean_ctor_get(x_5, 0);
lean_inc(x_55);
lean_dec(x_5);
x_56 = lean_alloc_ctor(7, 1, 0);
lean_ctor_set(x_56, 0, x_55);
return x_56;
}
}
case 8:
{
uint8_t x_57; 
lean_dec(x_1);
x_57 = !lean_is_exclusive(x_5);
if (x_57 == 0)
{
return x_5;
}
else
{
lean_object* x_58; lean_object* x_59; 
x_58 = lean_ctor_get(x_5, 0);
lean_inc(x_58);
lean_dec(x_5);
x_59 = lean_alloc_ctor(8, 1, 0);
lean_ctor_set(x_59, 0, x_58);
return x_59;
}
}
case 9:
{
uint8_t x_60; 
lean_dec(x_1);
x_60 = !lean_is_exclusive(x_5);
if (x_60 == 0)
{
return x_5;
}
else
{
lean_object* x_61; lean_object* x_62; lean_object* x_63; lean_object* x_64; 
x_61 = lean_ctor_get(x_5, 0);
x_62 = lean_ctor_get(x_5, 1);
x_63 = lean_ctor_get(x_5, 2);
lean_inc(x_63);
lean_inc(x_62);
lean_inc(x_61);
lean_dec(x_5);
x_64 = lean_alloc_ctor(9, 3, 0);
lean_ctor_set(x_64, 0, x_61);
lean_ctor_set(x_64, 1, x_62);
lean_ctor_set(x_64, 2, x_63);
return x_64;
}
}
case 10:
{
uint8_t x_65; 
lean_dec(x_1);
x_65 = !lean_is_exclusive(x_5);
if (x_65 == 0)
{
return x_5;
}
else
{
lean_object* x_66; lean_object* x_67; lean_object* x_68; lean_object* x_69; 
x_66 = lean_ctor_get(x_5, 0);
x_67 = lean_ctor_get(x_5, 1);
x_68 = lean_ctor_get(x_5, 2);
lean_inc(x_68);
lean_inc(x_67);
lean_inc(x_66);
lean_dec(x_5);
x_69 = lean_alloc_ctor(10, 3, 0);
lean_ctor_set(x_69, 0, x_66);
lean_ctor_set(x_69, 1, x_67);
lean_ctor_set(x_69, 2, x_68);
return x_69;
}
}
case 11:
{
uint8_t x_70; 
lean_dec(x_1);
x_70 = !lean_is_exclusive(x_5);
if (x_70 == 0)
{
return x_5;
}
else
{
lean_object* x_71; lean_object* x_72; lean_object* x_73; lean_object* x_74; 
x_71 = lean_ctor_get(x_5, 0);
x_72 = lean_ctor_get(x_5, 1);
x_73 = lean_ctor_get(x_5, 2);
lean_inc(x_73);
lean_inc(x_72);
lean_inc(x_71);
lean_dec(x_5);
x_74 = lean_alloc_ctor(11, 3, 0);
lean_ctor_set(x_74, 0, x_71);
lean_ctor_set(x_74, 1, x_72);
lean_ctor_set(x_74, 2, x_73);
return x_74;
}
}
case 12:
{
uint8_t x_75; 
lean_dec(x_1);
x_75 = !lean_is_exclusive(x_5);
if (x_75 == 0)
{
return x_5;
}
else
{
lean_object* x_76; lean_object* x_77; lean_object* x_78; lean_object* x_79; 
x_76 = lean_ctor_get(x_5, 0);
x_77 = lean_ctor_get(x_5, 1);
x_78 = lean_ctor_get(x_5, 2);
lean_inc(x_78);
lean_inc(x_77);
lean_inc(x_76);
lean_dec(x_5);
x_79 = lean_alloc_ctor(12, 3, 0);
lean_ctor_set(x_79, 0, x_76);
lean_ctor_set(x_79, 1, x_77);
lean_ctor_set(x_79, 2, x_78);
return x_79;
}
}
case 13:
{
uint8_t x_80; 
lean_dec(x_1);
x_80 = !lean_is_exclusive(x_5);
if (x_80 == 0)
{
lean_object* x_81; lean_object* x_82; lean_object* x_83; lean_object* x_84; lean_object* x_85; 
x_81 = lean_ctor_get(x_5, 0);
x_82 = lean_ctor_get(x_5, 1);
x_83 = lean_ctor_get(x_5, 3);
lean_inc(x_82);
lean_inc(x_81);
x_84 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_84, 0, x_81);
lean_ctor_set(x_84, 1, x_82);
x_85 = l_ML_Proof_weaken___rarg(x_84, lean_box(0), lean_box(0), lean_box(0), x_83);
lean_ctor_set(x_5, 3, x_85);
return x_5;
}
else
{
lean_object* x_86; lean_object* x_87; lean_object* x_88; lean_object* x_89; lean_object* x_90; lean_object* x_91; lean_object* x_92; 
x_86 = lean_ctor_get(x_5, 0);
x_87 = lean_ctor_get(x_5, 1);
x_88 = lean_ctor_get(x_5, 2);
x_89 = lean_ctor_get(x_5, 3);
lean_inc(x_89);
lean_inc(x_88);
lean_inc(x_87);
lean_inc(x_86);
lean_dec(x_5);
lean_inc(x_87);
lean_inc(x_86);
x_90 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_90, 0, x_86);
lean_ctor_set(x_90, 1, x_87);
x_91 = l_ML_Proof_weaken___rarg(x_90, lean_box(0), lean_box(0), lean_box(0), x_89);
x_92 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_92, 0, x_86);
lean_ctor_set(x_92, 1, x_87);
lean_ctor_set(x_92, 2, x_88);
lean_ctor_set(x_92, 3, x_91);
return x_92;
}
}
case 14:
{
uint8_t x_93; 
lean_dec(x_1);
x_93 = !lean_is_exclusive(x_5);
if (x_93 == 0)
{
lean_object* x_94; lean_object* x_95; lean_object* x_96; lean_object* x_97; lean_object* x_98; 
x_94 = lean_ctor_get(x_5, 0);
x_95 = lean_ctor_get(x_5, 1);
x_96 = lean_ctor_get(x_5, 3);
lean_inc(x_95);
lean_inc(x_94);
x_97 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_97, 0, x_94);
lean_ctor_set(x_97, 1, x_95);
x_98 = l_ML_Proof_weaken___rarg(x_97, lean_box(0), lean_box(0), lean_box(0), x_96);
lean_ctor_set(x_5, 3, x_98);
return x_5;
}
else
{
lean_object* x_99; lean_object* x_100; lean_object* x_101; lean_object* x_102; lean_object* x_103; lean_object* x_104; lean_object* x_105; 
x_99 = lean_ctor_get(x_5, 0);
x_100 = lean_ctor_get(x_5, 1);
x_101 = lean_ctor_get(x_5, 2);
x_102 = lean_ctor_get(x_5, 3);
lean_inc(x_102);
lean_inc(x_101);
lean_inc(x_100);
lean_inc(x_99);
lean_dec(x_5);
lean_inc(x_100);
lean_inc(x_99);
x_103 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_103, 0, x_99);
lean_ctor_set(x_103, 1, x_100);
x_104 = l_ML_Proof_weaken___rarg(x_103, lean_box(0), lean_box(0), lean_box(0), x_102);
x_105 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_105, 0, x_99);
lean_ctor_set(x_105, 1, x_100);
lean_ctor_set(x_105, 2, x_101);
lean_ctor_set(x_105, 3, x_104);
return x_105;
}
}
case 15:
{
uint8_t x_106; 
lean_dec(x_1);
x_106 = !lean_is_exclusive(x_5);
if (x_106 == 0)
{
lean_object* x_107; lean_object* x_108; lean_object* x_109; 
x_107 = lean_ctor_get(x_5, 0);
x_108 = lean_ctor_get(x_5, 3);
lean_inc(x_107);
x_109 = l_ML_Proof_weaken___rarg(x_107, lean_box(0), lean_box(0), lean_box(0), x_108);
lean_ctor_set(x_5, 3, x_109);
return x_5;
}
else
{
lean_object* x_110; lean_object* x_111; lean_object* x_112; lean_object* x_113; lean_object* x_114; lean_object* x_115; 
x_110 = lean_ctor_get(x_5, 0);
x_111 = lean_ctor_get(x_5, 1);
x_112 = lean_ctor_get(x_5, 2);
x_113 = lean_ctor_get(x_5, 3);
lean_inc(x_113);
lean_inc(x_112);
lean_inc(x_111);
lean_inc(x_110);
lean_dec(x_5);
lean_inc(x_110);
x_114 = l_ML_Proof_weaken___rarg(x_110, lean_box(0), lean_box(0), lean_box(0), x_113);
x_115 = lean_alloc_ctor(15, 4, 0);
lean_ctor_set(x_115, 0, x_110);
lean_ctor_set(x_115, 1, x_111);
lean_ctor_set(x_115, 2, x_112);
lean_ctor_set(x_115, 3, x_114);
return x_115;
}
}
case 16:
{
uint8_t x_116; 
lean_dec(x_1);
x_116 = !lean_is_exclusive(x_5);
if (x_116 == 0)
{
return x_5;
}
else
{
lean_object* x_117; lean_object* x_118; lean_object* x_119; 
x_117 = lean_ctor_get(x_5, 0);
x_118 = lean_ctor_get(x_5, 1);
lean_inc(x_118);
lean_inc(x_117);
lean_dec(x_5);
x_119 = lean_alloc_ctor(16, 2, 0);
lean_ctor_set(x_119, 0, x_117);
lean_ctor_set(x_119, 1, x_118);
return x_119;
}
}
default: 
{
uint8_t x_120; 
lean_dec(x_1);
x_120 = !lean_is_exclusive(x_5);
if (x_120 == 0)
{
lean_object* x_121; lean_object* x_122; lean_object* x_123; lean_object* x_124; lean_object* x_125; lean_object* x_126; lean_object* x_127; 
x_121 = lean_ctor_get(x_5, 0);
x_122 = lean_ctor_get(x_5, 1);
x_123 = lean_ctor_get(x_5, 2);
x_124 = lean_ctor_get(x_5, 3);
lean_inc(x_121);
x_125 = l_ML_Pattern_substSvar___rarg(x_121, x_123, x_122);
lean_inc(x_122);
x_126 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_126, 0, x_125);
lean_ctor_set(x_126, 1, x_122);
x_127 = l_ML_Proof_weaken___rarg(x_126, lean_box(0), lean_box(0), lean_box(0), x_124);
lean_ctor_set(x_5, 3, x_127);
return x_5;
}
else
{
lean_object* x_128; lean_object* x_129; lean_object* x_130; lean_object* x_131; lean_object* x_132; lean_object* x_133; lean_object* x_134; lean_object* x_135; 
x_128 = lean_ctor_get(x_5, 0);
x_129 = lean_ctor_get(x_5, 1);
x_130 = lean_ctor_get(x_5, 2);
x_131 = lean_ctor_get(x_5, 3);
lean_inc(x_131);
lean_inc(x_130);
lean_inc(x_129);
lean_inc(x_128);
lean_dec(x_5);
lean_inc(x_128);
x_132 = l_ML_Pattern_substSvar___rarg(x_128, x_130, x_129);
lean_inc(x_129);
x_133 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_133, 0, x_132);
lean_ctor_set(x_133, 1, x_129);
x_134 = l_ML_Proof_weaken___rarg(x_133, lean_box(0), lean_box(0), lean_box(0), x_131);
x_135 = lean_alloc_ctor(17, 4, 0);
lean_ctor_set(x_135, 0, x_128);
lean_ctor_set(x_135, 1, x_129);
lean_ctor_set(x_135, 2, x_130);
lean_ctor_set(x_135, 3, x_134);
return x_135;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Proof_weaken(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Proof_weaken___rarg), 5, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_weakeningDisj___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; 
lean_inc(x_1);
x_3 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_3);
x_5 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_5, 0, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_weakeningDisj(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_weakeningDisj___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_weakeningConj___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; 
lean_inc(x_1);
x_3 = l_ML_Pattern_conjunction___rarg(x_1, x_2);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_3);
lean_ctor_set(x_4, 1, x_1);
x_5 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_5, 0, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_weakeningConj(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_weakeningConj___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_contractionDisj___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; 
lean_inc_n(x_1, 2);
x_2 = l_ML_Pattern_disjunction___rarg(x_1, x_1);
x_3 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
x_4 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_4, 0, x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_contractionDisj(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_contractionDisj___rarg), 1, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_contractionConj___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; 
lean_inc_n(x_1, 2);
x_2 = l_ML_Pattern_conjunction___rarg(x_1, x_1);
x_3 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
x_4 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_4, 0, x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_contractionConj(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_contractionConj___rarg), 1, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_permutationDisj___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; 
lean_inc(x_2);
lean_inc(x_1);
x_3 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_4 = l_ML_Pattern_disjunction___rarg(x_2, x_1);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_3);
lean_ctor_set(x_5, 1, x_4);
x_6 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_6, 0, x_5);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_permutationDisj(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_permutationDisj___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_permutationConj___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; 
lean_inc(x_2);
lean_inc(x_1);
x_3 = l_ML_Pattern_conjunction___rarg(x_1, x_2);
x_4 = l_ML_Pattern_conjunction___rarg(x_2, x_1);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_3);
lean_ctor_set(x_5, 1, x_4);
x_6 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_6, 0, x_5);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_permutationConj(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_permutationConj___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_exFalso___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_2 = lean_box(2);
x_3 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
x_4 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_4, 0, x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_exFalso(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_exFalso___rarg), 1, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_excluddedMiddle___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; 
lean_inc(x_1);
x_2 = l_ML_Pattern_negation___rarg(x_1);
x_3 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_4 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_4, 0, x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_excluddedMiddle(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_excluddedMiddle___rarg), 1, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_toRule___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_4);
lean_ctor_set(x_5, 3, x_3);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_toRule(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_toRule___rarg), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_toRule2___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; 
lean_inc(x_3);
lean_inc(x_2);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_2);
lean_ctor_set(x_7, 1, x_3);
x_8 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_8, 0, x_1);
lean_ctor_set(x_8, 1, x_7);
lean_ctor_set(x_8, 2, x_5);
lean_ctor_set(x_8, 3, x_4);
x_9 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_9, 0, x_2);
lean_ctor_set(x_9, 1, x_3);
lean_ctor_set(x_9, 2, x_6);
lean_ctor_set(x_9, 3, x_8);
return x_9;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_toRule2(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_toRule2___rarg), 6, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_syllogism___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; 
lean_inc(x_2);
lean_inc(x_1);
x_6 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_6, 0, x_1);
lean_ctor_set(x_6, 1, x_2);
lean_inc(x_3);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_2);
lean_ctor_set(x_7, 1, x_3);
x_8 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_8, 0, x_1);
lean_ctor_set(x_8, 1, x_3);
lean_inc(x_8);
lean_inc(x_7);
x_9 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_9, 0, x_7);
lean_ctor_set(x_9, 1, x_8);
lean_inc(x_6);
x_10 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_10, 0, x_6);
lean_ctor_set(x_10, 1, x_9);
x_11 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_11, 0, x_10);
x_12 = l_ML_Proof_toRule2___rarg(x_6, x_7, x_8, x_11, x_4, x_5);
return x_12;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_syllogism(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_syllogism___rarg), 5, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_importation___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; 
lean_inc(x_3);
lean_inc(x_2);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_2);
lean_ctor_set(x_5, 1, x_3);
lean_inc(x_1);
x_6 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_6, 0, x_1);
lean_ctor_set(x_6, 1, x_5);
x_7 = l_ML_Pattern_conjunction___rarg(x_1, x_2);
x_8 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_8, 0, x_7);
lean_ctor_set(x_8, 1, x_3);
lean_inc(x_8);
lean_inc(x_6);
x_9 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_9, 0, x_6);
lean_ctor_set(x_9, 1, x_8);
x_10 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_10, 0, x_9);
x_11 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_11, 0, x_6);
lean_ctor_set(x_11, 1, x_8);
lean_ctor_set(x_11, 2, x_4);
lean_ctor_set(x_11, 3, x_10);
return x_11;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_importation(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_importation___rarg), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_exportation___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; 
lean_inc(x_2);
lean_inc(x_1);
x_5 = l_ML_Pattern_conjunction___rarg(x_1, x_2);
lean_inc(x_3);
x_6 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_6, 0, x_5);
lean_ctor_set(x_6, 1, x_3);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_2);
lean_ctor_set(x_7, 1, x_3);
x_8 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_8, 0, x_1);
lean_ctor_set(x_8, 1, x_7);
lean_inc(x_8);
lean_inc(x_6);
x_9 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_9, 0, x_6);
lean_ctor_set(x_9, 1, x_8);
x_10 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_10, 0, x_9);
x_11 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_11, 0, x_6);
lean_ctor_set(x_11, 1, x_8);
lean_ctor_set(x_11, 2, x_4);
lean_ctor_set(x_11, 3, x_10);
return x_11;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_exportation(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_exportation___rarg), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_expansion___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; 
lean_inc(x_2);
lean_inc(x_1);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_inc(x_3);
x_6 = l_ML_Pattern_disjunction___rarg(x_3, x_1);
x_7 = l_ML_Pattern_disjunction___rarg(x_3, x_2);
x_8 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_8, 0, x_6);
lean_ctor_set(x_8, 1, x_7);
lean_inc(x_8);
lean_inc(x_5);
x_9 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_9, 0, x_5);
lean_ctor_set(x_9, 1, x_8);
x_10 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_10, 0, x_9);
x_11 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_11, 0, x_5);
lean_ctor_set(x_11, 1, x_8);
lean_ctor_set(x_11, 2, x_4);
lean_ctor_set(x_11, 3, x_10);
return x_11;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_expansion(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_expansion___rarg), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroLeft___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = l_ML_Proof_weakeningDisj___rarg(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroLeft(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_disjIntroLeft___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroRight___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; 
lean_inc(x_2);
lean_inc(x_1);
x_3 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
lean_inc(x_1);
lean_inc(x_2);
x_4 = l_ML_Pattern_disjunction___rarg(x_2, x_1);
lean_inc(x_2);
lean_inc(x_1);
x_5 = l_ML_Proof_weakeningDisj___rarg(x_1, x_2);
lean_inc(x_1);
x_6 = l_ML_Proof_permutationDisj___rarg(x_1, x_2);
x_7 = l_ML_Proof_syllogism___rarg(x_1, x_3, x_4, x_5, x_6);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroRight(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_disjIntroRight___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_disjImpl___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; 
lean_inc(x_2);
lean_inc(x_1);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_inc(x_3);
lean_inc(x_1);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_3);
x_6 = l_ML_Pattern_disjunction___rarg(x_4, x_5);
x_7 = l_ML_Pattern_disjunction___rarg(x_2, x_3);
x_8 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_8, 0, x_1);
lean_ctor_set(x_8, 1, x_7);
x_9 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_9, 0, x_6);
lean_ctor_set(x_9, 1, x_8);
x_10 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_10, 0, x_9);
return x_10;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_disjImpl(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_disjImpl___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_disjImpl2___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; 
lean_inc(x_3);
lean_inc(x_2);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_2);
lean_ctor_set(x_5, 1, x_3);
lean_inc(x_1);
x_6 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_6, 0, x_1);
lean_ctor_set(x_6, 1, x_5);
lean_inc(x_4);
lean_inc(x_2);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_2);
lean_ctor_set(x_7, 1, x_4);
lean_inc(x_1);
x_8 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_8, 0, x_1);
lean_ctor_set(x_8, 1, x_7);
x_9 = l_ML_Pattern_disjunction___rarg(x_6, x_8);
x_10 = l_ML_Pattern_disjunction___rarg(x_3, x_4);
x_11 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_11, 0, x_2);
lean_ctor_set(x_11, 1, x_10);
x_12 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_12, 0, x_1);
lean_ctor_set(x_12, 1, x_11);
x_13 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_13, 0, x_9);
lean_ctor_set(x_13, 1, x_12);
x_14 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_14, 0, x_13);
return x_14;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_disjImpl2(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_disjImpl2___rarg), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroAtHyp___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; 
lean_inc_n(x_3, 2);
lean_inc(x_1);
x_6 = l_ML_Proof_expansion___rarg(x_1, x_3, x_3, x_4);
lean_inc(x_3);
x_7 = l_ML_Proof_contractionDisj___rarg(x_3);
lean_inc(x_3);
lean_inc(x_1);
x_8 = l_ML_Proof_permutationDisj___rarg(x_1, x_3);
lean_inc(x_1);
lean_inc(x_3);
x_9 = l_ML_Pattern_disjunction___rarg(x_3, x_1);
lean_inc_n(x_3, 2);
x_10 = l_ML_Pattern_disjunction___rarg(x_3, x_3);
lean_inc(x_3);
lean_inc(x_9);
x_11 = l_ML_Proof_syllogism___rarg(x_9, x_10, x_3, x_6, x_7);
lean_inc(x_1);
lean_inc(x_3);
lean_inc(x_2);
x_12 = l_ML_Proof_expansion___rarg(x_2, x_3, x_1, x_5);
lean_inc(x_3);
lean_inc(x_1);
x_13 = l_ML_Pattern_disjunction___rarg(x_1, x_3);
lean_inc(x_3);
lean_inc(x_13);
x_14 = l_ML_Proof_syllogism___rarg(x_13, x_9, x_3, x_8, x_11);
x_15 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_16 = l_ML_Proof_syllogism___rarg(x_15, x_13, x_3, x_12, x_14);
return x_16;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroAtHyp(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_disjIntroAtHyp___rarg), 5, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_conjElimLeft___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = l_ML_Proof_weakeningConj___rarg(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_conjElimLeft(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_conjElimLeft___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_conjElimRight___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; 
lean_inc(x_2);
lean_inc(x_1);
x_3 = l_ML_Pattern_conjunction___rarg(x_1, x_2);
lean_inc(x_1);
lean_inc(x_2);
x_4 = l_ML_Pattern_conjunction___rarg(x_2, x_1);
lean_inc(x_2);
lean_inc(x_1);
x_5 = l_ML_Proof_permutationConj___rarg(x_1, x_2);
lean_inc(x_2);
x_6 = l_ML_Proof_weakeningConj___rarg(x_2, x_1);
x_7 = l_ML_Proof_syllogism___rarg(x_3, x_4, x_2, x_5, x_6);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_conjElimRight(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_conjElimRight___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_conjIntro___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; 
lean_inc(x_2);
lean_inc(x_1);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_inc(x_3);
lean_inc(x_1);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_3);
x_6 = l_ML_Pattern_conjunction___rarg(x_2, x_3);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_1);
lean_ctor_set(x_7, 1, x_6);
x_8 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_8, 0, x_5);
lean_ctor_set(x_8, 1, x_7);
x_9 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_9, 0, x_4);
lean_ctor_set(x_9, 1, x_8);
x_10 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_10, 0, x_9);
return x_10;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_conjIntro(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_conjIntro___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_implProjLeft___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; 
lean_inc(x_2);
lean_inc(x_1);
x_3 = l_ML_Proof_weakeningConj___rarg(x_1, x_2);
lean_inc(x_1);
x_4 = l_ML_Proof_exportation___rarg(x_1, x_2, x_1, x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_implProjLeft(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_implProjLeft___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_implProjRight___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; 
lean_inc(x_2);
lean_inc(x_1);
x_3 = l_ML_Proof_conjElimRight___rarg(x_1, x_2);
lean_inc(x_2);
x_4 = l_ML_Proof_exportation___rarg(x_1, x_2, x_2, x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_implProjRight(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_implProjRight___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_implSelf___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
lean_inc_n(x_1, 2);
x_2 = l_ML_Pattern_conjunction___rarg(x_1, x_1);
lean_inc(x_1);
x_3 = l_ML_Proof_contractionConj___rarg(x_1);
lean_inc_n(x_1, 2);
x_4 = l_ML_Proof_weakeningConj___rarg(x_1, x_1);
lean_inc(x_1);
x_5 = l_ML_Proof_syllogism___rarg(x_1, x_2, x_1, x_3, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_implSelf(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_implSelf___rarg), 1, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_extraPremise___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; 
lean_inc(x_1);
lean_inc(x_2);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_2);
lean_ctor_set(x_4, 1, x_1);
lean_inc(x_1);
x_5 = l_ML_Proof_implProjLeft___rarg(x_1, x_2);
x_6 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_6, 0, x_1);
lean_ctor_set(x_6, 1, x_4);
lean_ctor_set(x_6, 2, x_3);
lean_ctor_set(x_6, 3, x_5);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_extraPremise(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_extraPremise___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_conjIntroRule___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; 
lean_inc(x_3);
lean_inc(x_2);
x_6 = l_ML_Pattern_conjunction___rarg(x_2, x_3);
lean_inc(x_6);
x_7 = l_ML_Proof_implSelf___rarg(x_6);
lean_inc(x_6);
lean_inc(x_3);
lean_inc(x_2);
x_8 = l_ML_Proof_exportation___rarg(x_2, x_3, x_6, x_7);
lean_inc(x_6);
lean_inc(x_3);
x_9 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_9, 0, x_3);
lean_ctor_set(x_9, 1, x_6);
lean_inc(x_1);
x_10 = l_ML_Proof_syllogism___rarg(x_1, x_2, x_9, x_4, x_8);
lean_inc(x_1);
lean_inc(x_3);
x_11 = l_ML_Proof_permutationConj___rarg(x_3, x_1);
lean_inc(x_6);
lean_inc(x_3);
lean_inc(x_1);
x_12 = l_ML_Proof_importation___rarg(x_1, x_3, x_6, x_10);
lean_inc(x_1);
lean_inc(x_3);
x_13 = l_ML_Pattern_conjunction___rarg(x_3, x_1);
lean_inc(x_3);
lean_inc(x_1);
x_14 = l_ML_Pattern_conjunction___rarg(x_1, x_3);
lean_inc(x_6);
x_15 = l_ML_Proof_syllogism___rarg(x_13, x_14, x_6, x_11, x_12);
lean_inc(x_6);
lean_inc(x_1);
lean_inc(x_3);
x_16 = l_ML_Proof_exportation___rarg(x_3, x_1, x_6, x_15);
lean_inc(x_6);
lean_inc(x_1);
x_17 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_17, 0, x_1);
lean_ctor_set(x_17, 1, x_6);
lean_inc(x_1);
x_18 = l_ML_Proof_syllogism___rarg(x_1, x_3, x_17, x_5, x_16);
lean_inc(x_6);
lean_inc_n(x_1, 2);
x_19 = l_ML_Proof_importation___rarg(x_1, x_1, x_6, x_18);
lean_inc_n(x_1, 2);
x_20 = l_ML_Pattern_conjunction___rarg(x_1, x_1);
lean_inc(x_1);
x_21 = l_ML_Proof_contractionConj___rarg(x_1);
x_22 = l_ML_Proof_syllogism___rarg(x_1, x_20, x_6, x_21, x_19);
return x_22;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_conjIntroRule(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_conjIntroRule___rarg), 5, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_conjIntroHypConcLeft___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; 
lean_inc(x_3);
lean_inc(x_1);
x_5 = l_ML_Proof_conjElimRight___rarg(x_1, x_3);
lean_inc(x_3);
lean_inc(x_1);
x_6 = l_ML_Pattern_conjunction___rarg(x_1, x_3);
lean_inc(x_3);
lean_inc(x_1);
x_7 = l_ML_Proof_weakeningConj___rarg(x_1, x_3);
lean_inc(x_2);
lean_inc(x_6);
x_8 = l_ML_Proof_syllogism___rarg(x_6, x_1, x_2, x_7, x_4);
x_9 = l_ML_Proof_conjIntroRule___rarg(x_6, x_2, x_3, x_8, x_5);
return x_9;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_conjIntroHypConcLeft(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_conjIntroHypConcLeft___rarg), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_modusPonensExtraHyp___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; 
lean_inc(x_2);
lean_inc(x_1);
x_6 = l_ML_Pattern_conjunction___rarg(x_1, x_2);
lean_inc(x_1);
x_7 = l_ML_Proof_implSelf___rarg(x_1);
lean_inc(x_2);
lean_inc_n(x_1, 2);
x_8 = l_ML_Proof_conjIntroRule___rarg(x_1, x_1, x_2, x_7, x_4);
lean_inc(x_3);
lean_inc(x_1);
x_9 = l_ML_Proof_importation___rarg(x_1, x_2, x_3, x_5);
x_10 = l_ML_Proof_syllogism___rarg(x_1, x_6, x_3, x_8, x_9);
return x_10;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_modusPonensExtraHyp(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_modusPonensExtraHyp___rarg), 5, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_modusPonensThm___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; 
lean_inc(x_3);
lean_inc(x_2);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_2);
lean_ctor_set(x_4, 1, x_3);
lean_inc(x_4);
lean_inc(x_1);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_4);
lean_inc(x_2);
lean_inc(x_1);
x_6 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_6, 0, x_1);
lean_ctor_set(x_6, 1, x_2);
lean_inc(x_6);
lean_inc(x_5);
x_7 = l_ML_Pattern_conjunction___rarg(x_5, x_6);
lean_inc(x_1);
lean_inc(x_7);
x_8 = l_ML_Pattern_conjunction___rarg(x_7, x_1);
lean_inc(x_1);
lean_inc(x_7);
x_9 = l_ML_Proof_weakeningConj___rarg(x_7, x_1);
lean_inc(x_6);
lean_inc(x_5);
x_10 = l_ML_Proof_conjElimRight___rarg(x_5, x_6);
lean_inc(x_1);
lean_inc(x_7);
x_11 = l_ML_Proof_conjElimRight___rarg(x_7, x_1);
lean_inc(x_9);
lean_inc(x_6);
lean_inc(x_7);
lean_inc(x_8);
x_12 = l_ML_Proof_syllogism___rarg(x_8, x_7, x_6, x_9, x_10);
lean_inc(x_11);
lean_inc(x_2);
lean_inc(x_1);
lean_inc(x_8);
x_13 = l_ML_Proof_modusPonensExtraHyp___rarg(x_8, x_1, x_2, x_11, x_12);
lean_inc(x_5);
x_14 = l_ML_Proof_weakeningConj___rarg(x_5, x_6);
lean_inc(x_8);
x_15 = l_ML_Proof_syllogism___rarg(x_8, x_7, x_5, x_9, x_14);
lean_inc(x_8);
x_16 = l_ML_Proof_modusPonensExtraHyp___rarg(x_8, x_1, x_4, x_11, x_15);
x_17 = l_ML_Proof_modusPonensExtraHyp___rarg(x_8, x_2, x_3, x_13, x_16);
return x_17;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_modusPonensThm(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_modusPonensThm___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_implDistribLeft___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; 
lean_inc(x_3);
lean_inc(x_2);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_2);
lean_ctor_set(x_4, 1, x_3);
lean_inc(x_1);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_4);
lean_inc(x_2);
lean_inc(x_1);
x_6 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_6, 0, x_1);
lean_ctor_set(x_6, 1, x_2);
lean_inc(x_3);
lean_inc(x_1);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_1);
lean_ctor_set(x_7, 1, x_3);
lean_inc(x_6);
lean_inc(x_5);
x_8 = l_ML_Pattern_conjunction___rarg(x_5, x_6);
lean_inc(x_3);
lean_inc(x_1);
x_9 = l_ML_Proof_modusPonensThm___rarg(x_1, x_2, x_3);
x_10 = l_ML_Proof_exportation___rarg(x_8, x_1, x_3, x_9);
x_11 = l_ML_Proof_exportation___rarg(x_5, x_6, x_7, x_10);
return x_11;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_implDistribLeft(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_implDistribLeft___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_implDistribRight___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; 
lean_inc(x_2);
lean_inc(x_1);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_inc(x_3);
lean_inc(x_1);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_3);
x_6 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_6, 0, x_4);
lean_ctor_set(x_6, 1, x_5);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_2);
lean_ctor_set(x_7, 1, x_3);
x_8 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_8, 0, x_1);
lean_ctor_set(x_8, 1, x_7);
x_9 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_9, 0, x_6);
lean_ctor_set(x_9, 1, x_8);
x_10 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_10, 0, x_9);
return x_10;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_implDistribRight(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_implDistribRight___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_syllogismExtraHyp___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; 
lean_inc(x_3);
lean_inc(x_2);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_2);
lean_ctor_set(x_7, 1, x_3);
lean_inc(x_1);
x_8 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_8, 0, x_1);
lean_ctor_set(x_8, 1, x_7);
lean_inc(x_2);
lean_inc(x_1);
x_9 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_9, 0, x_1);
lean_ctor_set(x_9, 1, x_2);
lean_inc(x_3);
lean_inc(x_1);
x_10 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_10, 0, x_1);
lean_ctor_set(x_10, 1, x_3);
lean_inc(x_10);
lean_inc(x_9);
x_11 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_11, 0, x_9);
lean_ctor_set(x_11, 1, x_10);
lean_inc(x_3);
lean_inc(x_2);
lean_inc(x_1);
x_12 = l_ML_Proof_implDistribLeft___rarg(x_1, x_2, x_3);
x_13 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_13, 0, x_8);
lean_ctor_set(x_13, 1, x_11);
lean_ctor_set(x_13, 2, x_5);
lean_ctor_set(x_13, 3, x_12);
lean_inc(x_4);
lean_inc(x_3);
x_14 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_14, 0, x_3);
lean_ctor_set(x_14, 1, x_4);
lean_inc(x_1);
x_15 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_15, 0, x_1);
lean_ctor_set(x_15, 1, x_14);
lean_inc(x_4);
lean_inc(x_1);
x_16 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_16, 0, x_1);
lean_ctor_set(x_16, 1, x_4);
lean_inc(x_16);
lean_inc(x_10);
x_17 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_17, 0, x_10);
lean_ctor_set(x_17, 1, x_16);
lean_inc(x_4);
lean_inc(x_1);
x_18 = l_ML_Proof_implDistribLeft___rarg(x_1, x_3, x_4);
x_19 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_19, 0, x_15);
lean_ctor_set(x_19, 1, x_17);
lean_ctor_set(x_19, 2, x_6);
lean_ctor_set(x_19, 3, x_18);
lean_inc(x_16);
lean_inc(x_9);
x_20 = l_ML_Proof_syllogism___rarg(x_9, x_10, x_16, x_13, x_19);
x_21 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_21, 0, x_9);
lean_ctor_set(x_21, 1, x_16);
lean_inc(x_4);
lean_inc(x_2);
x_22 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_22, 0, x_2);
lean_ctor_set(x_22, 1, x_4);
lean_inc(x_1);
x_23 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_23, 0, x_1);
lean_ctor_set(x_23, 1, x_22);
x_24 = l_ML_Proof_implDistribRight___rarg(x_1, x_2, x_4);
x_25 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_25, 0, x_21);
lean_ctor_set(x_25, 1, x_23);
lean_ctor_set(x_25, 2, x_20);
lean_ctor_set(x_25, 3, x_24);
return x_25;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_syllogismExtraHyp(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_syllogismExtraHyp___rarg), 6, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_permuteHyps___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; 
lean_inc(x_3);
lean_inc(x_2);
lean_inc(x_1);
x_5 = l_ML_Proof_importation___rarg(x_1, x_2, x_3, x_4);
lean_inc(x_1);
lean_inc(x_2);
x_6 = l_ML_Pattern_conjunction___rarg(x_2, x_1);
lean_inc(x_2);
lean_inc(x_1);
x_7 = l_ML_Pattern_conjunction___rarg(x_1, x_2);
lean_inc(x_1);
lean_inc(x_2);
x_8 = l_ML_Proof_permutationConj___rarg(x_2, x_1);
lean_inc(x_3);
x_9 = l_ML_Proof_syllogism___rarg(x_6, x_7, x_3, x_8, x_5);
x_10 = l_ML_Proof_exportation___rarg(x_2, x_1, x_3, x_9);
return x_10;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_permuteHyps(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_permuteHyps___rarg), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroAtHypThm___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; 
lean_inc(x_3);
lean_inc(x_1);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_3);
lean_inc(x_3);
lean_inc(x_2);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_2);
lean_ctor_set(x_5, 1, x_3);
lean_inc(x_5);
lean_inc(x_4);
x_6 = l_ML_Proof_weakeningConj___rarg(x_4, x_5);
lean_inc(x_5);
lean_inc(x_4);
x_7 = l_ML_Pattern_conjunction___rarg(x_4, x_5);
lean_inc(x_3);
lean_inc(x_1);
lean_inc(x_7);
x_8 = l_ML_Proof_permuteHyps___rarg(x_7, x_1, x_3, x_6);
x_9 = l_ML_Proof_conjElimRight___rarg(x_4, x_5);
lean_inc(x_3);
lean_inc(x_2);
lean_inc(x_7);
x_10 = l_ML_Proof_permuteHyps___rarg(x_7, x_2, x_3, x_9);
lean_inc(x_3);
lean_inc(x_7);
x_11 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_11, 0, x_7);
lean_ctor_set(x_11, 1, x_3);
lean_inc(x_2);
lean_inc(x_1);
x_12 = l_ML_Proof_disjIntroAtHyp___rarg(x_1, x_2, x_11, x_8, x_10);
x_13 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_14 = l_ML_Proof_permuteHyps___rarg(x_13, x_7, x_3, x_12);
return x_14;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_disjIntroAtHypThm(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_disjIntroAtHypThm___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_conjIntroAtConclThm___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; 
lean_inc(x_2);
lean_inc(x_1);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_inc(x_3);
lean_inc(x_1);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_3);
lean_inc(x_5);
lean_inc(x_4);
x_6 = l_ML_Pattern_conjunction___rarg(x_4, x_5);
lean_inc(x_1);
lean_inc(x_6);
x_7 = l_ML_Proof_conjElimRight___rarg(x_6, x_1);
lean_inc(x_1);
lean_inc(x_6);
x_8 = l_ML_Pattern_conjunction___rarg(x_6, x_1);
lean_inc(x_1);
lean_inc(x_6);
x_9 = l_ML_Proof_weakeningConj___rarg(x_6, x_1);
lean_inc(x_5);
lean_inc(x_4);
x_10 = l_ML_Proof_weakeningConj___rarg(x_4, x_5);
lean_inc(x_9);
lean_inc(x_4);
lean_inc(x_6);
lean_inc(x_8);
x_11 = l_ML_Proof_syllogism___rarg(x_8, x_6, x_4, x_9, x_10);
lean_inc(x_7);
lean_inc(x_2);
lean_inc(x_1);
lean_inc(x_8);
x_12 = l_ML_Proof_modusPonensExtraHyp___rarg(x_8, x_1, x_2, x_7, x_11);
lean_inc(x_5);
lean_inc(x_4);
x_13 = l_ML_Proof_conjElimRight___rarg(x_4, x_5);
lean_inc(x_5);
lean_inc(x_6);
lean_inc(x_8);
x_14 = l_ML_Proof_syllogism___rarg(x_8, x_6, x_5, x_9, x_13);
lean_inc(x_3);
lean_inc(x_1);
lean_inc(x_8);
x_15 = l_ML_Proof_modusPonensExtraHyp___rarg(x_8, x_1, x_3, x_7, x_14);
lean_inc(x_3);
lean_inc(x_2);
x_16 = l_ML_Proof_conjIntroRule___rarg(x_8, x_2, x_3, x_12, x_15);
x_17 = l_ML_Pattern_conjunction___rarg(x_2, x_3);
lean_inc(x_17);
lean_inc(x_1);
x_18 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_18, 0, x_1);
lean_ctor_set(x_18, 1, x_17);
x_19 = l_ML_Proof_exportation___rarg(x_6, x_1, x_17, x_16);
x_20 = l_ML_Proof_exportation___rarg(x_4, x_5, x_18, x_19);
return x_20;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_conjIntroAtConclThm(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_conjIntroAtConclThm___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_negImplIntro___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; 
lean_inc(x_2);
lean_inc(x_1);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
x_5 = l_ML_Pattern_negation___rarg(x_2);
x_6 = l_ML_Pattern_negation___rarg(x_1);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_5);
lean_ctor_set(x_7, 1, x_6);
lean_inc(x_7);
lean_inc(x_4);
x_8 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_8, 0, x_4);
lean_ctor_set(x_8, 1, x_7);
x_9 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_9, 0, x_8);
x_10 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_10, 0, x_4);
lean_ctor_set(x_10, 1, x_7);
lean_ctor_set(x_10, 2, x_3);
lean_ctor_set(x_10, 3, x_9);
return x_10;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_negImplIntro(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_negImplIntro___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_negConjAsImpl___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; 
lean_inc(x_2);
lean_inc(x_1);
x_3 = l_ML_Pattern_conjunction___rarg(x_1, x_2);
x_4 = l_ML_Pattern_negation___rarg(x_3);
x_5 = l_ML_Pattern_negation___rarg(x_2);
x_6 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_6, 0, x_1);
lean_ctor_set(x_6, 1, x_5);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_4);
lean_ctor_set(x_7, 1, x_6);
x_8 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_8, 0, x_7);
return x_8;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_negConjAsImpl(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_negConjAsImpl___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_doubleNegIntro___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
lean_inc(x_1);
x_2 = l_ML_Pattern_negation___rarg(x_1);
x_3 = l_ML_Pattern_negation___rarg(x_2);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_3);
x_5 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_5, 0, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_doubleNegIntro(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_doubleNegIntro___rarg), 1, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_doubleNegElim___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
lean_inc(x_1);
x_2 = l_ML_Pattern_negation___rarg(x_1);
x_3 = l_ML_Pattern_negation___rarg(x_2);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_3);
lean_ctor_set(x_4, 1, x_1);
x_5 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_5, 0, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_doubleNegElim(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_doubleNegElim___rarg), 1, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_equivSelf___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; 
lean_inc(x_1);
x_2 = l_ML_Pattern_equivalence___rarg(x_1, x_1);
x_3 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_3, 0, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_equivSelf(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_equivSelf___rarg), 1, 0);
return x_3;
}
}
static lean_object* _init_l_ML_Proof_topImplSelfImplSelf___rarg___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_Pattern_top(lean_box(0));
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_topImplSelfImplSelf___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_2 = l_ML_Proof_topImplSelfImplSelf___rarg___closed__1;
lean_inc(x_1);
x_3 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_3);
lean_ctor_set(x_4, 1, x_1);
x_5 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_5, 0, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_topImplSelfImplSelf(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_topImplSelfImplSelf___rarg), 1, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_selfImplTopImplSelf___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_2 = l_ML_Proof_topImplSelfImplSelf___rarg___closed__1;
lean_inc(x_1);
x_3 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_3);
x_5 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_5, 0, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_selfImplTopImplSelf(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_selfImplTopImplSelf___rarg), 1, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_implExistSelf___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
lean_inc(x_2);
x_3 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
lean_ctor_set(x_3, 2, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_implExistSelf(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_implExistSelf___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_existSelfImpl___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; 
lean_inc(x_1);
x_4 = l_ML_Proof_implSelf___rarg(x_1);
lean_inc(x_1);
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_1);
lean_ctor_set(x_5, 2, x_2);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_existSelfImpl(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_existSelfImpl___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_pushExistInConj___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; 
lean_inc_n(x_3, 2);
lean_inc(x_1);
x_5 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_3);
lean_ctor_set(x_5, 2, x_3);
lean_inc(x_1);
lean_inc(x_3);
x_6 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_6, 0, x_3);
lean_ctor_set(x_6, 1, x_1);
lean_inc(x_2);
lean_inc(x_6);
lean_inc(x_1);
x_7 = l_ML_Proof_conjIntroHypConcLeft___rarg(x_1, x_6, x_2, x_5);
lean_inc(x_2);
x_8 = l_ML_Pattern_conjunction___rarg(x_1, x_2);
x_9 = l_ML_Pattern_conjunction___rarg(x_6, x_2);
x_10 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_10, 0, x_8);
lean_ctor_set(x_10, 1, x_9);
lean_ctor_set(x_10, 2, x_3);
lean_ctor_set(x_10, 3, x_7);
return x_10;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_pushExistInConj(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_pushExistInConj___rarg), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_univQuan___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; 
lean_inc(x_3);
x_5 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_5, 0, x_3);
lean_inc(x_1);
x_6 = l_ML_Pattern_substEvar___rarg(x_1, x_2, x_5);
lean_dec(x_5);
lean_inc(x_6);
x_7 = l_ML_Pattern_negation___rarg(x_6);
lean_inc(x_1);
x_8 = l_ML_Pattern_negation___rarg(x_1);
lean_inc(x_8);
lean_inc(x_2);
x_9 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_9, 0, x_2);
lean_ctor_set(x_9, 1, x_8);
lean_inc(x_2);
x_10 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_10, 0, x_8);
lean_ctor_set(x_10, 1, x_2);
lean_ctor_set(x_10, 2, x_3);
lean_inc(x_7);
x_11 = l_ML_Proof_negImplIntro___rarg(x_7, x_9, x_10);
x_12 = l_ML_Pattern_universal___rarg(x_2, x_1);
x_13 = l_ML_Pattern_negation___rarg(x_7);
lean_inc(x_6);
x_14 = l_ML_Proof_doubleNegElim___rarg(x_6);
x_15 = l_ML_Proof_syllogism___rarg(x_12, x_13, x_6, x_11, x_14);
return x_15;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_univQuan(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_univQuan___rarg), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_univGen___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; 
lean_inc(x_2);
lean_inc(x_1);
x_6 = l_ML_Proof_negImplIntro___rarg(x_1, x_2, x_5);
lean_inc(x_2);
x_7 = l_ML_Pattern_negation___rarg(x_2);
lean_inc(x_1);
x_8 = l_ML_Pattern_negation___rarg(x_1);
lean_inc(x_3);
lean_inc(x_8);
lean_inc(x_7);
x_9 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_9, 0, x_7);
lean_ctor_set(x_9, 1, x_8);
lean_ctor_set(x_9, 2, x_3);
lean_ctor_set(x_9, 3, x_6);
lean_inc(x_3);
x_10 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_10, 0, x_3);
lean_ctor_set(x_10, 1, x_7);
lean_inc(x_8);
x_11 = l_ML_Proof_negImplIntro___rarg(x_10, x_8, x_9);
x_12 = l_ML_Pattern_negation___rarg(x_8);
x_13 = l_ML_Pattern_universal___rarg(x_3, x_2);
lean_inc(x_1);
x_14 = l_ML_Proof_doubleNegIntro___rarg(x_1);
x_15 = l_ML_Proof_syllogism___rarg(x_1, x_12, x_13, x_14, x_11);
return x_15;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_univGen(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_univGen___rarg), 5, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_univGeneralization___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; 
x_4 = l_ML_Proof_topImplSelfImplSelf___rarg___closed__1;
lean_inc(x_1);
x_5 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_5, 0, x_4);
lean_ctor_set(x_5, 1, x_1);
lean_inc(x_1);
x_6 = l_ML_Proof_selfImplTopImplSelf___rarg(x_1);
lean_inc(x_1);
x_7 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_7, 0, x_1);
lean_ctor_set(x_7, 1, x_5);
lean_ctor_set(x_7, 2, x_3);
lean_ctor_set(x_7, 3, x_6);
lean_inc(x_2);
lean_inc(x_1);
x_8 = l_ML_Proof_univGen___rarg(x_4, x_1, x_2, lean_box(0), x_7);
x_9 = l_ML_Pattern_universal___rarg(x_2, x_1);
lean_inc(x_9);
x_10 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_10, 0, x_4);
lean_ctor_set(x_10, 1, x_9);
lean_inc(x_9);
x_11 = l_ML_Proof_topImplSelfImplSelf___rarg(x_9);
x_12 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_9);
lean_ctor_set(x_12, 2, x_8);
lean_ctor_set(x_12, 3, x_11);
return x_12;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_univGeneralization(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_univGeneralization___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_pushConjInExist___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; 
lean_inc(x_2);
lean_inc(x_1);
x_5 = l_ML_Pattern_conjunction___rarg(x_1, x_2);
lean_inc_n(x_3, 2);
lean_inc(x_5);
x_6 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_6, 0, x_5);
lean_ctor_set(x_6, 1, x_3);
lean_ctor_set(x_6, 2, x_3);
lean_inc(x_3);
x_7 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_7, 0, x_3);
lean_ctor_set(x_7, 1, x_5);
lean_inc(x_7);
lean_inc(x_2);
lean_inc(x_1);
x_8 = l_ML_Proof_exportation___rarg(x_1, x_2, x_7, x_6);
lean_inc(x_7);
lean_inc(x_2);
x_9 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_9, 0, x_2);
lean_ctor_set(x_9, 1, x_7);
lean_inc(x_3);
lean_inc(x_1);
x_10 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_10, 0, x_1);
lean_ctor_set(x_10, 1, x_9);
lean_ctor_set(x_10, 2, x_3);
lean_ctor_set(x_10, 3, x_8);
x_11 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_11, 0, x_3);
lean_ctor_set(x_11, 1, x_1);
x_12 = l_ML_Proof_importation___rarg(x_11, x_2, x_7, x_10);
return x_12;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_pushConjInExist(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_pushConjInExist___rarg), 4, 0);
return x_3;
}
}
static lean_object* _init_l_ML_Proof_propagationBottom___rarg___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(2);
x_2 = lean_box(0);
x_3 = l_ML_AppContext_insert___rarg(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Proof_propagationBottom___rarg___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Proof_propagationBottom___rarg___closed__1;
x_2 = l_ML_Proof_implSelf___rarg(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottom___rarg(lean_object* x_1) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 0:
{
lean_object* x_2; 
x_2 = l_ML_Proof_propagationBottom___rarg___closed__2;
return x_2;
}
case 1:
{
uint8_t x_3; 
x_3 = !lean_is_exclusive(x_1);
if (x_3 == 0)
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; 
x_4 = lean_ctor_get(x_1, 0);
x_5 = lean_ctor_get(x_1, 1);
lean_inc(x_5);
x_6 = l_ML_Proof_propagationBottom___rarg(x_5);
x_7 = lean_box(2);
x_8 = l_ML_AppContext_insert___rarg(x_7, x_5);
lean_inc(x_4);
x_9 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_9, 0, x_8);
lean_ctor_set(x_9, 1, x_7);
lean_ctor_set(x_9, 2, x_4);
lean_ctor_set(x_9, 3, x_6);
lean_inc(x_4);
x_10 = lean_alloc_ctor(7, 1, 0);
lean_ctor_set(x_10, 0, x_4);
lean_inc(x_4);
x_11 = l_ML_AppContext_insert___rarg(x_7, x_1);
lean_dec(x_1);
x_12 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_12, 0, x_4);
lean_ctor_set(x_12, 1, x_7);
x_13 = l_ML_Proof_syllogism___rarg(x_11, x_12, x_7, x_9, x_10);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; 
x_14 = lean_ctor_get(x_1, 0);
x_15 = lean_ctor_get(x_1, 1);
lean_inc(x_15);
lean_inc(x_14);
lean_dec(x_1);
lean_inc(x_15);
x_16 = l_ML_Proof_propagationBottom___rarg(x_15);
x_17 = lean_box(2);
x_18 = l_ML_AppContext_insert___rarg(x_17, x_15);
lean_inc(x_14);
x_19 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_19, 0, x_18);
lean_ctor_set(x_19, 1, x_17);
lean_ctor_set(x_19, 2, x_14);
lean_ctor_set(x_19, 3, x_16);
lean_inc(x_14);
x_20 = lean_alloc_ctor(7, 1, 0);
lean_ctor_set(x_20, 0, x_14);
lean_inc(x_14);
x_21 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_21, 0, x_14);
lean_ctor_set(x_21, 1, x_15);
x_22 = l_ML_AppContext_insert___rarg(x_17, x_21);
lean_dec(x_21);
x_23 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_23, 0, x_14);
lean_ctor_set(x_23, 1, x_17);
x_24 = l_ML_Proof_syllogism___rarg(x_22, x_23, x_17, x_19, x_20);
return x_24;
}
}
default: 
{
uint8_t x_25; 
x_25 = !lean_is_exclusive(x_1);
if (x_25 == 0)
{
lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; 
x_26 = lean_ctor_get(x_1, 0);
x_27 = lean_ctor_get(x_1, 1);
lean_inc(x_27);
x_28 = l_ML_Proof_propagationBottom___rarg(x_27);
x_29 = lean_box(2);
x_30 = l_ML_AppContext_insert___rarg(x_29, x_27);
lean_inc(x_26);
x_31 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_31, 0, x_30);
lean_ctor_set(x_31, 1, x_29);
lean_ctor_set(x_31, 2, x_26);
lean_ctor_set(x_31, 3, x_28);
lean_inc(x_26);
x_32 = lean_alloc_ctor(8, 1, 0);
lean_ctor_set(x_32, 0, x_26);
lean_inc(x_26);
x_33 = l_ML_AppContext_insert___rarg(x_29, x_1);
lean_dec(x_1);
x_34 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_34, 0, x_29);
lean_ctor_set(x_34, 1, x_26);
x_35 = l_ML_Proof_syllogism___rarg(x_33, x_34, x_29, x_31, x_32);
return x_35;
}
else
{
lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; 
x_36 = lean_ctor_get(x_1, 0);
x_37 = lean_ctor_get(x_1, 1);
lean_inc(x_37);
lean_inc(x_36);
lean_dec(x_1);
lean_inc(x_37);
x_38 = l_ML_Proof_propagationBottom___rarg(x_37);
x_39 = lean_box(2);
x_40 = l_ML_AppContext_insert___rarg(x_39, x_37);
lean_inc(x_36);
x_41 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_41, 0, x_40);
lean_ctor_set(x_41, 1, x_39);
lean_ctor_set(x_41, 2, x_36);
lean_ctor_set(x_41, 3, x_38);
lean_inc(x_36);
x_42 = lean_alloc_ctor(8, 1, 0);
lean_ctor_set(x_42, 0, x_36);
lean_inc(x_36);
x_43 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_43, 0, x_36);
lean_ctor_set(x_43, 1, x_37);
x_44 = l_ML_AppContext_insert___rarg(x_39, x_43);
lean_dec(x_43);
x_45 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_45, 0, x_39);
lean_ctor_set(x_45, 1, x_36);
x_46 = l_ML_Proof_syllogism___rarg(x_44, x_45, x_39, x_41, x_42);
return x_46;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottom(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_propagationBottom___rarg), 1, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottomR___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_2 = lean_box(2);
x_3 = l_ML_AppContext_insert___rarg(x_2, x_1);
x_4 = l_ML_Proof_exFalso___rarg(x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottomR(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_propagationBottomR___rarg___boxed), 1, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottomR___rarg___boxed(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = l_ML_Proof_propagationBottomR___rarg(x_1);
lean_dec(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationDisj___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
switch (lean_obj_tag(x_3)) {
case 0:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; 
x_4 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_5 = lean_box(0);
x_6 = l_ML_AppContext_insert___rarg(x_4, x_5);
lean_dec(x_4);
x_7 = l_ML_Proof_implSelf___rarg(x_6);
return x_7;
}
case 1:
{
uint8_t x_8; 
x_8 = !lean_is_exclusive(x_3);
if (x_8 == 0)
{
lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; 
x_9 = lean_ctor_get(x_3, 0);
x_10 = lean_ctor_get(x_3, 1);
lean_inc(x_10);
lean_inc(x_2);
lean_inc(x_1);
x_11 = l_ML_Proof_propagationDisj___rarg(x_1, x_2, x_10);
lean_inc(x_2);
lean_inc(x_1);
x_12 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_13 = l_ML_AppContext_insert___rarg(x_12, x_10);
x_14 = l_ML_AppContext_insert___rarg(x_1, x_10);
x_15 = l_ML_AppContext_insert___rarg(x_2, x_10);
lean_inc(x_15);
lean_inc(x_14);
x_16 = l_ML_Pattern_disjunction___rarg(x_14, x_15);
lean_inc(x_9);
lean_inc(x_16);
x_17 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_17, 0, x_13);
lean_ctor_set(x_17, 1, x_16);
lean_ctor_set(x_17, 2, x_9);
lean_ctor_set(x_17, 3, x_11);
lean_inc(x_9);
x_18 = lean_alloc_ctor(9, 3, 0);
lean_ctor_set(x_18, 0, x_14);
lean_ctor_set(x_18, 1, x_15);
lean_ctor_set(x_18, 2, x_9);
lean_inc(x_9);
x_19 = l_ML_AppContext_insert___rarg(x_12, x_3);
lean_dec(x_12);
x_20 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_20, 0, x_9);
lean_ctor_set(x_20, 1, x_16);
x_21 = l_ML_AppContext_insert___rarg(x_1, x_3);
lean_dec(x_1);
x_22 = l_ML_AppContext_insert___rarg(x_2, x_3);
lean_dec(x_3);
lean_dec(x_2);
x_23 = l_ML_Pattern_disjunction___rarg(x_21, x_22);
x_24 = l_ML_Proof_syllogism___rarg(x_19, x_20, x_23, x_17, x_18);
return x_24;
}
else
{
lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; 
x_25 = lean_ctor_get(x_3, 0);
x_26 = lean_ctor_get(x_3, 1);
lean_inc(x_26);
lean_inc(x_25);
lean_dec(x_3);
lean_inc(x_26);
lean_inc(x_2);
lean_inc(x_1);
x_27 = l_ML_Proof_propagationDisj___rarg(x_1, x_2, x_26);
lean_inc(x_2);
lean_inc(x_1);
x_28 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_29 = l_ML_AppContext_insert___rarg(x_28, x_26);
x_30 = l_ML_AppContext_insert___rarg(x_1, x_26);
x_31 = l_ML_AppContext_insert___rarg(x_2, x_26);
lean_inc(x_31);
lean_inc(x_30);
x_32 = l_ML_Pattern_disjunction___rarg(x_30, x_31);
lean_inc(x_25);
lean_inc(x_32);
x_33 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_33, 0, x_29);
lean_ctor_set(x_33, 1, x_32);
lean_ctor_set(x_33, 2, x_25);
lean_ctor_set(x_33, 3, x_27);
lean_inc(x_25);
x_34 = lean_alloc_ctor(9, 3, 0);
lean_ctor_set(x_34, 0, x_30);
lean_ctor_set(x_34, 1, x_31);
lean_ctor_set(x_34, 2, x_25);
lean_inc(x_25);
x_35 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_35, 0, x_25);
lean_ctor_set(x_35, 1, x_26);
x_36 = l_ML_AppContext_insert___rarg(x_28, x_35);
lean_dec(x_28);
x_37 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_37, 0, x_25);
lean_ctor_set(x_37, 1, x_32);
x_38 = l_ML_AppContext_insert___rarg(x_1, x_35);
lean_dec(x_1);
x_39 = l_ML_AppContext_insert___rarg(x_2, x_35);
lean_dec(x_35);
lean_dec(x_2);
x_40 = l_ML_Pattern_disjunction___rarg(x_38, x_39);
x_41 = l_ML_Proof_syllogism___rarg(x_36, x_37, x_40, x_33, x_34);
return x_41;
}
}
default: 
{
uint8_t x_42; 
x_42 = !lean_is_exclusive(x_3);
if (x_42 == 0)
{
lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; lean_object* x_50; lean_object* x_51; lean_object* x_52; lean_object* x_53; lean_object* x_54; lean_object* x_55; lean_object* x_56; lean_object* x_57; lean_object* x_58; 
x_43 = lean_ctor_get(x_3, 0);
x_44 = lean_ctor_get(x_3, 1);
lean_inc(x_44);
lean_inc(x_2);
lean_inc(x_1);
x_45 = l_ML_Proof_propagationDisj___rarg(x_1, x_2, x_44);
lean_inc(x_2);
lean_inc(x_1);
x_46 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_47 = l_ML_AppContext_insert___rarg(x_46, x_44);
x_48 = l_ML_AppContext_insert___rarg(x_1, x_44);
x_49 = l_ML_AppContext_insert___rarg(x_2, x_44);
lean_inc(x_49);
lean_inc(x_48);
x_50 = l_ML_Pattern_disjunction___rarg(x_48, x_49);
lean_inc(x_43);
lean_inc(x_50);
x_51 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_51, 0, x_47);
lean_ctor_set(x_51, 1, x_50);
lean_ctor_set(x_51, 2, x_43);
lean_ctor_set(x_51, 3, x_45);
lean_inc(x_43);
x_52 = lean_alloc_ctor(10, 3, 0);
lean_ctor_set(x_52, 0, x_48);
lean_ctor_set(x_52, 1, x_49);
lean_ctor_set(x_52, 2, x_43);
lean_inc(x_43);
x_53 = l_ML_AppContext_insert___rarg(x_46, x_3);
lean_dec(x_46);
x_54 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_54, 0, x_50);
lean_ctor_set(x_54, 1, x_43);
x_55 = l_ML_AppContext_insert___rarg(x_1, x_3);
lean_dec(x_1);
x_56 = l_ML_AppContext_insert___rarg(x_2, x_3);
lean_dec(x_3);
lean_dec(x_2);
x_57 = l_ML_Pattern_disjunction___rarg(x_55, x_56);
x_58 = l_ML_Proof_syllogism___rarg(x_53, x_54, x_57, x_51, x_52);
return x_58;
}
else
{
lean_object* x_59; lean_object* x_60; lean_object* x_61; lean_object* x_62; lean_object* x_63; lean_object* x_64; lean_object* x_65; lean_object* x_66; lean_object* x_67; lean_object* x_68; lean_object* x_69; lean_object* x_70; lean_object* x_71; lean_object* x_72; lean_object* x_73; lean_object* x_74; lean_object* x_75; 
x_59 = lean_ctor_get(x_3, 0);
x_60 = lean_ctor_get(x_3, 1);
lean_inc(x_60);
lean_inc(x_59);
lean_dec(x_3);
lean_inc(x_60);
lean_inc(x_2);
lean_inc(x_1);
x_61 = l_ML_Proof_propagationDisj___rarg(x_1, x_2, x_60);
lean_inc(x_2);
lean_inc(x_1);
x_62 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_63 = l_ML_AppContext_insert___rarg(x_62, x_60);
x_64 = l_ML_AppContext_insert___rarg(x_1, x_60);
x_65 = l_ML_AppContext_insert___rarg(x_2, x_60);
lean_inc(x_65);
lean_inc(x_64);
x_66 = l_ML_Pattern_disjunction___rarg(x_64, x_65);
lean_inc(x_59);
lean_inc(x_66);
x_67 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_67, 0, x_63);
lean_ctor_set(x_67, 1, x_66);
lean_ctor_set(x_67, 2, x_59);
lean_ctor_set(x_67, 3, x_61);
lean_inc(x_59);
x_68 = lean_alloc_ctor(10, 3, 0);
lean_ctor_set(x_68, 0, x_64);
lean_ctor_set(x_68, 1, x_65);
lean_ctor_set(x_68, 2, x_59);
lean_inc(x_59);
x_69 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_69, 0, x_59);
lean_ctor_set(x_69, 1, x_60);
x_70 = l_ML_AppContext_insert___rarg(x_62, x_69);
lean_dec(x_62);
x_71 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_71, 0, x_66);
lean_ctor_set(x_71, 1, x_59);
x_72 = l_ML_AppContext_insert___rarg(x_1, x_69);
lean_dec(x_1);
x_73 = l_ML_AppContext_insert___rarg(x_2, x_69);
lean_dec(x_69);
lean_dec(x_2);
x_74 = l_ML_Pattern_disjunction___rarg(x_72, x_73);
x_75 = l_ML_Proof_syllogism___rarg(x_70, x_71, x_74, x_67, x_68);
return x_75;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationDisj(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_propagationDisj___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagtionDisjR___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
switch (lean_obj_tag(x_3)) {
case 0:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; 
x_4 = lean_box(0);
x_5 = l_ML_AppContext_insert___rarg(x_1, x_4);
lean_dec(x_1);
x_6 = l_ML_AppContext_insert___rarg(x_2, x_4);
lean_dec(x_2);
x_7 = l_ML_Pattern_disjunction___rarg(x_5, x_6);
x_8 = l_ML_Proof_implSelf___rarg(x_7);
return x_8;
}
case 1:
{
uint8_t x_9; 
x_9 = !lean_is_exclusive(x_3);
if (x_9 == 0)
{
lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; 
x_10 = lean_ctor_get(x_3, 0);
x_11 = lean_ctor_get(x_3, 1);
lean_inc(x_11);
lean_inc(x_2);
lean_inc(x_1);
x_12 = l_ML_Proof_propagtionDisjR___rarg(x_1, x_2, x_11);
x_13 = l_ML_AppContext_insert___rarg(x_1, x_11);
x_14 = l_ML_AppContext_insert___rarg(x_2, x_11);
lean_inc(x_14);
lean_inc(x_13);
x_15 = l_ML_Pattern_disjunction___rarg(x_13, x_14);
lean_inc(x_2);
lean_inc(x_1);
x_16 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_17 = l_ML_AppContext_insert___rarg(x_16, x_11);
lean_inc(x_14);
lean_inc(x_13);
x_18 = l_ML_Proof_weakeningDisj___rarg(x_13, x_14);
lean_inc(x_12);
lean_inc(x_17);
lean_inc(x_15);
lean_inc(x_13);
x_19 = l_ML_Proof_syllogism___rarg(x_13, x_15, x_17, x_18, x_12);
lean_inc(x_10);
lean_inc(x_17);
lean_inc(x_13);
x_20 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_20, 0, x_13);
lean_ctor_set(x_20, 1, x_17);
lean_ctor_set(x_20, 2, x_10);
lean_ctor_set(x_20, 3, x_19);
lean_inc(x_14);
x_21 = l_ML_Proof_disjIntroRight___rarg(x_14, x_13);
lean_inc(x_17);
lean_inc(x_14);
x_22 = l_ML_Proof_syllogism___rarg(x_14, x_15, x_17, x_21, x_12);
lean_inc(x_10);
x_23 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_23, 0, x_14);
lean_ctor_set(x_23, 1, x_17);
lean_ctor_set(x_23, 2, x_10);
lean_ctor_set(x_23, 3, x_22);
x_24 = l_ML_AppContext_insert___rarg(x_1, x_3);
lean_dec(x_1);
x_25 = l_ML_AppContext_insert___rarg(x_2, x_3);
lean_dec(x_2);
x_26 = l_ML_AppContext_insert___rarg(x_16, x_3);
lean_dec(x_3);
lean_dec(x_16);
x_27 = l_ML_Proof_disjIntroAtHyp___rarg(x_24, x_25, x_26, x_20, x_23);
return x_27;
}
else
{
lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; 
x_28 = lean_ctor_get(x_3, 0);
x_29 = lean_ctor_get(x_3, 1);
lean_inc(x_29);
lean_inc(x_28);
lean_dec(x_3);
lean_inc(x_29);
lean_inc(x_2);
lean_inc(x_1);
x_30 = l_ML_Proof_propagtionDisjR___rarg(x_1, x_2, x_29);
x_31 = l_ML_AppContext_insert___rarg(x_1, x_29);
x_32 = l_ML_AppContext_insert___rarg(x_2, x_29);
lean_inc(x_32);
lean_inc(x_31);
x_33 = l_ML_Pattern_disjunction___rarg(x_31, x_32);
lean_inc(x_2);
lean_inc(x_1);
x_34 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_35 = l_ML_AppContext_insert___rarg(x_34, x_29);
lean_inc(x_32);
lean_inc(x_31);
x_36 = l_ML_Proof_weakeningDisj___rarg(x_31, x_32);
lean_inc(x_30);
lean_inc(x_35);
lean_inc(x_33);
lean_inc(x_31);
x_37 = l_ML_Proof_syllogism___rarg(x_31, x_33, x_35, x_36, x_30);
lean_inc(x_28);
lean_inc(x_35);
lean_inc(x_31);
x_38 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_38, 0, x_31);
lean_ctor_set(x_38, 1, x_35);
lean_ctor_set(x_38, 2, x_28);
lean_ctor_set(x_38, 3, x_37);
lean_inc(x_32);
x_39 = l_ML_Proof_disjIntroRight___rarg(x_32, x_31);
lean_inc(x_35);
lean_inc(x_32);
x_40 = l_ML_Proof_syllogism___rarg(x_32, x_33, x_35, x_39, x_30);
lean_inc(x_28);
x_41 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_41, 0, x_32);
lean_ctor_set(x_41, 1, x_35);
lean_ctor_set(x_41, 2, x_28);
lean_ctor_set(x_41, 3, x_40);
x_42 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_42, 0, x_28);
lean_ctor_set(x_42, 1, x_29);
x_43 = l_ML_AppContext_insert___rarg(x_1, x_42);
lean_dec(x_1);
x_44 = l_ML_AppContext_insert___rarg(x_2, x_42);
lean_dec(x_2);
x_45 = l_ML_AppContext_insert___rarg(x_34, x_42);
lean_dec(x_42);
lean_dec(x_34);
x_46 = l_ML_Proof_disjIntroAtHyp___rarg(x_43, x_44, x_45, x_38, x_41);
return x_46;
}
}
default: 
{
uint8_t x_47; 
x_47 = !lean_is_exclusive(x_3);
if (x_47 == 0)
{
lean_object* x_48; lean_object* x_49; lean_object* x_50; lean_object* x_51; lean_object* x_52; lean_object* x_53; lean_object* x_54; lean_object* x_55; lean_object* x_56; lean_object* x_57; lean_object* x_58; lean_object* x_59; lean_object* x_60; lean_object* x_61; lean_object* x_62; lean_object* x_63; lean_object* x_64; lean_object* x_65; 
x_48 = lean_ctor_get(x_3, 0);
x_49 = lean_ctor_get(x_3, 1);
lean_inc(x_49);
lean_inc(x_2);
lean_inc(x_1);
x_50 = l_ML_Proof_propagtionDisjR___rarg(x_1, x_2, x_49);
x_51 = l_ML_AppContext_insert___rarg(x_1, x_49);
x_52 = l_ML_AppContext_insert___rarg(x_2, x_49);
lean_inc(x_52);
lean_inc(x_51);
x_53 = l_ML_Pattern_disjunction___rarg(x_51, x_52);
lean_inc(x_2);
lean_inc(x_1);
x_54 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_55 = l_ML_AppContext_insert___rarg(x_54, x_49);
lean_inc(x_52);
lean_inc(x_51);
x_56 = l_ML_Proof_weakeningDisj___rarg(x_51, x_52);
lean_inc(x_50);
lean_inc(x_55);
lean_inc(x_53);
lean_inc(x_51);
x_57 = l_ML_Proof_syllogism___rarg(x_51, x_53, x_55, x_56, x_50);
lean_inc(x_48);
lean_inc(x_55);
lean_inc(x_51);
x_58 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_58, 0, x_51);
lean_ctor_set(x_58, 1, x_55);
lean_ctor_set(x_58, 2, x_48);
lean_ctor_set(x_58, 3, x_57);
lean_inc(x_52);
x_59 = l_ML_Proof_disjIntroRight___rarg(x_52, x_51);
lean_inc(x_55);
lean_inc(x_52);
x_60 = l_ML_Proof_syllogism___rarg(x_52, x_53, x_55, x_59, x_50);
lean_inc(x_48);
x_61 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_61, 0, x_52);
lean_ctor_set(x_61, 1, x_55);
lean_ctor_set(x_61, 2, x_48);
lean_ctor_set(x_61, 3, x_60);
x_62 = l_ML_AppContext_insert___rarg(x_1, x_3);
lean_dec(x_1);
x_63 = l_ML_AppContext_insert___rarg(x_2, x_3);
lean_dec(x_2);
x_64 = l_ML_AppContext_insert___rarg(x_54, x_3);
lean_dec(x_3);
lean_dec(x_54);
x_65 = l_ML_Proof_disjIntroAtHyp___rarg(x_62, x_63, x_64, x_58, x_61);
return x_65;
}
else
{
lean_object* x_66; lean_object* x_67; lean_object* x_68; lean_object* x_69; lean_object* x_70; lean_object* x_71; lean_object* x_72; lean_object* x_73; lean_object* x_74; lean_object* x_75; lean_object* x_76; lean_object* x_77; lean_object* x_78; lean_object* x_79; lean_object* x_80; lean_object* x_81; lean_object* x_82; lean_object* x_83; lean_object* x_84; 
x_66 = lean_ctor_get(x_3, 0);
x_67 = lean_ctor_get(x_3, 1);
lean_inc(x_67);
lean_inc(x_66);
lean_dec(x_3);
lean_inc(x_67);
lean_inc(x_2);
lean_inc(x_1);
x_68 = l_ML_Proof_propagtionDisjR___rarg(x_1, x_2, x_67);
x_69 = l_ML_AppContext_insert___rarg(x_1, x_67);
x_70 = l_ML_AppContext_insert___rarg(x_2, x_67);
lean_inc(x_70);
lean_inc(x_69);
x_71 = l_ML_Pattern_disjunction___rarg(x_69, x_70);
lean_inc(x_2);
lean_inc(x_1);
x_72 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
x_73 = l_ML_AppContext_insert___rarg(x_72, x_67);
lean_inc(x_70);
lean_inc(x_69);
x_74 = l_ML_Proof_weakeningDisj___rarg(x_69, x_70);
lean_inc(x_68);
lean_inc(x_73);
lean_inc(x_71);
lean_inc(x_69);
x_75 = l_ML_Proof_syllogism___rarg(x_69, x_71, x_73, x_74, x_68);
lean_inc(x_66);
lean_inc(x_73);
lean_inc(x_69);
x_76 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_76, 0, x_69);
lean_ctor_set(x_76, 1, x_73);
lean_ctor_set(x_76, 2, x_66);
lean_ctor_set(x_76, 3, x_75);
lean_inc(x_70);
x_77 = l_ML_Proof_disjIntroRight___rarg(x_70, x_69);
lean_inc(x_73);
lean_inc(x_70);
x_78 = l_ML_Proof_syllogism___rarg(x_70, x_71, x_73, x_77, x_68);
lean_inc(x_66);
x_79 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_79, 0, x_70);
lean_ctor_set(x_79, 1, x_73);
lean_ctor_set(x_79, 2, x_66);
lean_ctor_set(x_79, 3, x_78);
x_80 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_80, 0, x_66);
lean_ctor_set(x_80, 1, x_67);
x_81 = l_ML_AppContext_insert___rarg(x_1, x_80);
lean_dec(x_1);
x_82 = l_ML_AppContext_insert___rarg(x_2, x_80);
lean_dec(x_2);
x_83 = l_ML_AppContext_insert___rarg(x_72, x_80);
lean_dec(x_80);
lean_dec(x_72);
x_84 = l_ML_Proof_disjIntroAtHyp___rarg(x_81, x_82, x_83, x_76, x_79);
return x_84;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagtionDisjR(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_propagtionDisjR___rarg), 3, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationExist___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
switch (lean_obj_tag(x_3)) {
case 0:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; 
x_5 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_5, 0, x_2);
lean_ctor_set(x_5, 1, x_1);
x_6 = lean_box(0);
x_7 = l_ML_AppContext_insert___rarg(x_5, x_6);
lean_dec(x_5);
x_8 = l_ML_Proof_implSelf___rarg(x_7);
return x_8;
}
case 1:
{
lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; 
x_9 = lean_ctor_get(x_3, 0);
x_10 = lean_ctor_get(x_3, 1);
lean_inc(x_2);
lean_inc(x_1);
x_11 = l_ML_Proof_propagationExist___rarg(x_1, x_2, x_10, lean_box(0));
lean_inc(x_1);
lean_inc(x_2);
x_12 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_12, 0, x_2);
lean_ctor_set(x_12, 1, x_1);
x_13 = l_ML_AppContext_insert___rarg(x_12, x_10);
lean_dec(x_12);
x_14 = l_ML_AppContext_insert___rarg(x_1, x_10);
lean_dec(x_1);
lean_inc(x_14);
lean_inc(x_2);
x_15 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_15, 0, x_2);
lean_ctor_set(x_15, 1, x_14);
lean_inc(x_9);
lean_inc(x_15);
lean_inc(x_13);
x_16 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_16, 0, x_13);
lean_ctor_set(x_16, 1, x_15);
lean_ctor_set(x_16, 2, x_9);
lean_ctor_set(x_16, 3, x_11);
lean_inc(x_9);
lean_inc(x_2);
lean_inc(x_14);
x_17 = lean_alloc_ctor(11, 3, 0);
lean_ctor_set(x_17, 0, x_14);
lean_ctor_set(x_17, 1, x_2);
lean_ctor_set(x_17, 2, x_9);
lean_inc(x_9);
x_18 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_18, 0, x_9);
lean_ctor_set(x_18, 1, x_13);
lean_inc(x_9);
x_19 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_19, 0, x_9);
lean_ctor_set(x_19, 1, x_15);
lean_inc(x_9);
x_20 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_20, 0, x_9);
lean_ctor_set(x_20, 1, x_14);
x_21 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_21, 0, x_2);
lean_ctor_set(x_21, 1, x_20);
x_22 = l_ML_Proof_syllogism___rarg(x_18, x_19, x_21, x_16, x_17);
return x_22;
}
default: 
{
lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; 
x_23 = lean_ctor_get(x_3, 0);
x_24 = lean_ctor_get(x_3, 1);
lean_inc(x_2);
lean_inc(x_1);
x_25 = l_ML_Proof_propagationExist___rarg(x_1, x_2, x_24, lean_box(0));
lean_inc(x_1);
lean_inc(x_2);
x_26 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_26, 0, x_2);
lean_ctor_set(x_26, 1, x_1);
x_27 = l_ML_AppContext_insert___rarg(x_26, x_24);
lean_dec(x_26);
x_28 = l_ML_AppContext_insert___rarg(x_1, x_24);
lean_dec(x_1);
lean_inc(x_28);
lean_inc(x_2);
x_29 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_29, 0, x_2);
lean_ctor_set(x_29, 1, x_28);
lean_inc(x_23);
lean_inc(x_29);
lean_inc(x_27);
x_30 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_30, 0, x_27);
lean_ctor_set(x_30, 1, x_29);
lean_ctor_set(x_30, 2, x_23);
lean_ctor_set(x_30, 3, x_25);
lean_inc(x_23);
lean_inc(x_2);
lean_inc(x_28);
x_31 = lean_alloc_ctor(12, 3, 0);
lean_ctor_set(x_31, 0, x_28);
lean_ctor_set(x_31, 1, x_2);
lean_ctor_set(x_31, 2, x_23);
lean_inc(x_23);
x_32 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_32, 0, x_27);
lean_ctor_set(x_32, 1, x_23);
lean_inc(x_23);
x_33 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_33, 0, x_29);
lean_ctor_set(x_33, 1, x_23);
lean_inc(x_23);
x_34 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_34, 0, x_28);
lean_ctor_set(x_34, 1, x_23);
x_35 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_35, 0, x_2);
lean_ctor_set(x_35, 1, x_34);
x_36 = l_ML_Proof_syllogism___rarg(x_32, x_33, x_35, x_30, x_31);
return x_36;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationExist(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_propagationExist___rarg___boxed), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationExist___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = l_ML_Proof_propagationExist___rarg(x_1, x_2, x_3, x_4);
lean_dec(x_3);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationExistR___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
switch (lean_obj_tag(x_3)) {
case 0:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; 
x_5 = lean_box(0);
x_6 = l_ML_AppContext_insert___rarg(x_1, x_5);
lean_dec(x_1);
x_7 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_7, 0, x_2);
lean_ctor_set(x_7, 1, x_6);
x_8 = l_ML_Proof_implSelf___rarg(x_7);
return x_8;
}
case 1:
{
uint8_t x_9; 
x_9 = !lean_is_exclusive(x_3);
if (x_9 == 0)
{
lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; 
x_10 = lean_ctor_get(x_3, 0);
x_11 = lean_ctor_get(x_3, 1);
lean_inc(x_11);
lean_inc(x_2);
lean_inc(x_1);
x_12 = l_ML_Proof_propagationExistR___rarg(x_1, x_2, x_11, lean_box(0));
x_13 = l_ML_AppContext_insert___rarg(x_1, x_11);
lean_inc_n(x_2, 2);
lean_inc(x_13);
x_14 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_14, 0, x_13);
lean_ctor_set(x_14, 1, x_2);
lean_ctor_set(x_14, 2, x_2);
lean_inc(x_2);
x_15 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_15, 0, x_2);
lean_inc(x_13);
x_16 = l_ML_Pattern_substEvar___rarg(x_13, x_2, x_15);
lean_dec(x_15);
lean_inc(x_13);
lean_inc(x_2);
x_17 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_17, 0, x_2);
lean_ctor_set(x_17, 1, x_13);
lean_inc(x_1);
lean_inc(x_2);
x_18 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_18, 0, x_2);
lean_ctor_set(x_18, 1, x_1);
x_19 = l_ML_AppContext_insert___rarg(x_18, x_11);
lean_inc(x_19);
x_20 = l_ML_Proof_syllogism___rarg(x_16, x_17, x_19, x_14, x_12);
lean_inc(x_10);
x_21 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_21, 0, x_13);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_10);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML_AppContext_insert___rarg(x_1, x_3);
lean_dec(x_1);
x_23 = l_ML_AppContext_insert___rarg(x_18, x_3);
lean_dec(x_3);
lean_dec(x_18);
x_24 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_24, 0, x_22);
lean_ctor_set(x_24, 1, x_23);
lean_ctor_set(x_24, 2, x_2);
lean_ctor_set(x_24, 3, x_21);
return x_24;
}
else
{
lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; 
x_25 = lean_ctor_get(x_3, 0);
x_26 = lean_ctor_get(x_3, 1);
lean_inc(x_26);
lean_inc(x_25);
lean_dec(x_3);
lean_inc(x_26);
lean_inc(x_2);
lean_inc(x_1);
x_27 = l_ML_Proof_propagationExistR___rarg(x_1, x_2, x_26, lean_box(0));
x_28 = l_ML_AppContext_insert___rarg(x_1, x_26);
lean_inc_n(x_2, 2);
lean_inc(x_28);
x_29 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_29, 0, x_28);
lean_ctor_set(x_29, 1, x_2);
lean_ctor_set(x_29, 2, x_2);
lean_inc(x_2);
x_30 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_30, 0, x_2);
lean_inc(x_28);
x_31 = l_ML_Pattern_substEvar___rarg(x_28, x_2, x_30);
lean_dec(x_30);
lean_inc(x_28);
lean_inc(x_2);
x_32 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_32, 0, x_2);
lean_ctor_set(x_32, 1, x_28);
lean_inc(x_1);
lean_inc(x_2);
x_33 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_33, 0, x_2);
lean_ctor_set(x_33, 1, x_1);
x_34 = l_ML_AppContext_insert___rarg(x_33, x_26);
lean_inc(x_34);
x_35 = l_ML_Proof_syllogism___rarg(x_31, x_32, x_34, x_29, x_27);
lean_inc(x_25);
x_36 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_36, 0, x_28);
lean_ctor_set(x_36, 1, x_34);
lean_ctor_set(x_36, 2, x_25);
lean_ctor_set(x_36, 3, x_35);
x_37 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_37, 0, x_25);
lean_ctor_set(x_37, 1, x_26);
x_38 = l_ML_AppContext_insert___rarg(x_1, x_37);
lean_dec(x_1);
x_39 = l_ML_AppContext_insert___rarg(x_33, x_37);
lean_dec(x_37);
lean_dec(x_33);
x_40 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_40, 0, x_38);
lean_ctor_set(x_40, 1, x_39);
lean_ctor_set(x_40, 2, x_2);
lean_ctor_set(x_40, 3, x_36);
return x_40;
}
}
default: 
{
uint8_t x_41; 
x_41 = !lean_is_exclusive(x_3);
if (x_41 == 0)
{
lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; lean_object* x_50; lean_object* x_51; lean_object* x_52; lean_object* x_53; lean_object* x_54; lean_object* x_55; lean_object* x_56; 
x_42 = lean_ctor_get(x_3, 0);
x_43 = lean_ctor_get(x_3, 1);
lean_inc(x_43);
lean_inc(x_2);
lean_inc(x_1);
x_44 = l_ML_Proof_propagationExistR___rarg(x_1, x_2, x_43, lean_box(0));
x_45 = l_ML_AppContext_insert___rarg(x_1, x_43);
lean_inc_n(x_2, 2);
lean_inc(x_45);
x_46 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_46, 0, x_45);
lean_ctor_set(x_46, 1, x_2);
lean_ctor_set(x_46, 2, x_2);
lean_inc(x_2);
x_47 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_47, 0, x_2);
lean_inc(x_45);
x_48 = l_ML_Pattern_substEvar___rarg(x_45, x_2, x_47);
lean_dec(x_47);
lean_inc(x_45);
lean_inc(x_2);
x_49 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_49, 0, x_2);
lean_ctor_set(x_49, 1, x_45);
lean_inc(x_1);
lean_inc(x_2);
x_50 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_50, 0, x_2);
lean_ctor_set(x_50, 1, x_1);
x_51 = l_ML_AppContext_insert___rarg(x_50, x_43);
lean_inc(x_51);
x_52 = l_ML_Proof_syllogism___rarg(x_48, x_49, x_51, x_46, x_44);
lean_inc(x_42);
x_53 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_53, 0, x_45);
lean_ctor_set(x_53, 1, x_51);
lean_ctor_set(x_53, 2, x_42);
lean_ctor_set(x_53, 3, x_52);
x_54 = l_ML_AppContext_insert___rarg(x_1, x_3);
lean_dec(x_1);
x_55 = l_ML_AppContext_insert___rarg(x_50, x_3);
lean_dec(x_3);
lean_dec(x_50);
x_56 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_56, 0, x_54);
lean_ctor_set(x_56, 1, x_55);
lean_ctor_set(x_56, 2, x_2);
lean_ctor_set(x_56, 3, x_53);
return x_56;
}
else
{
lean_object* x_57; lean_object* x_58; lean_object* x_59; lean_object* x_60; lean_object* x_61; lean_object* x_62; lean_object* x_63; lean_object* x_64; lean_object* x_65; lean_object* x_66; lean_object* x_67; lean_object* x_68; lean_object* x_69; lean_object* x_70; lean_object* x_71; lean_object* x_72; 
x_57 = lean_ctor_get(x_3, 0);
x_58 = lean_ctor_get(x_3, 1);
lean_inc(x_58);
lean_inc(x_57);
lean_dec(x_3);
lean_inc(x_58);
lean_inc(x_2);
lean_inc(x_1);
x_59 = l_ML_Proof_propagationExistR___rarg(x_1, x_2, x_58, lean_box(0));
x_60 = l_ML_AppContext_insert___rarg(x_1, x_58);
lean_inc_n(x_2, 2);
lean_inc(x_60);
x_61 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_61, 0, x_60);
lean_ctor_set(x_61, 1, x_2);
lean_ctor_set(x_61, 2, x_2);
lean_inc(x_2);
x_62 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_62, 0, x_2);
lean_inc(x_60);
x_63 = l_ML_Pattern_substEvar___rarg(x_60, x_2, x_62);
lean_dec(x_62);
lean_inc(x_60);
lean_inc(x_2);
x_64 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_64, 0, x_2);
lean_ctor_set(x_64, 1, x_60);
lean_inc(x_1);
lean_inc(x_2);
x_65 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_65, 0, x_2);
lean_ctor_set(x_65, 1, x_1);
x_66 = l_ML_AppContext_insert___rarg(x_65, x_58);
lean_inc(x_66);
x_67 = l_ML_Proof_syllogism___rarg(x_63, x_64, x_66, x_61, x_59);
lean_inc(x_57);
x_68 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_68, 0, x_60);
lean_ctor_set(x_68, 1, x_66);
lean_ctor_set(x_68, 2, x_57);
lean_ctor_set(x_68, 3, x_67);
x_69 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_69, 0, x_57);
lean_ctor_set(x_69, 1, x_58);
x_70 = l_ML_AppContext_insert___rarg(x_1, x_69);
lean_dec(x_1);
x_71 = l_ML_AppContext_insert___rarg(x_65, x_69);
lean_dec(x_69);
lean_dec(x_65);
x_72 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_72, 0, x_70);
lean_ctor_set(x_72, 1, x_71);
lean_ctor_set(x_72, 2, x_2);
lean_ctor_set(x_72, 3, x_68);
return x_72;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationExistR(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_propagationExistR___rarg), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_framing___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
switch (lean_obj_tag(x_3)) {
case 0:
{
lean_inc(x_4);
return x_4;
}
case 1:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; 
x_5 = lean_ctor_get(x_3, 0);
x_6 = lean_ctor_get(x_3, 1);
x_7 = l_ML_Proof_framing___rarg(x_1, x_2, x_6, x_4);
x_8 = l_ML_AppContext_insert___rarg(x_1, x_6);
x_9 = l_ML_AppContext_insert___rarg(x_2, x_6);
lean_inc(x_5);
x_10 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_10, 0, x_8);
lean_ctor_set(x_10, 1, x_9);
lean_ctor_set(x_10, 2, x_5);
lean_ctor_set(x_10, 3, x_7);
return x_10;
}
default: 
{
lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; 
x_11 = lean_ctor_get(x_3, 0);
x_12 = lean_ctor_get(x_3, 1);
x_13 = l_ML_Proof_framing___rarg(x_1, x_2, x_12, x_4);
x_14 = l_ML_AppContext_insert___rarg(x_1, x_12);
x_15 = l_ML_AppContext_insert___rarg(x_2, x_12);
lean_inc(x_11);
x_16 = lean_alloc_ctor(13, 4, 0);
lean_ctor_set(x_16, 0, x_14);
lean_ctor_set(x_16, 1, x_15);
lean_ctor_set(x_16, 2, x_11);
lean_ctor_set(x_16, 3, x_13);
return x_16;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Proof_framing(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_framing___rarg___boxed), 4, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_framing___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = l_ML_Proof_framing___rarg(x_1, x_2, x_3, x_4);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
lean_dec(x_1);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_doubleNegCtx___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; 
lean_inc(x_1);
x_4 = l_ML_Pattern_negation___rarg(x_1);
x_5 = lean_box(2);
lean_inc(x_4);
x_6 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_6, 0, x_4);
lean_ctor_set(x_6, 1, x_5);
lean_inc(x_1);
x_7 = l_ML_Proof_doubleNegIntro___rarg(x_1);
x_8 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_8, 0, x_1);
lean_ctor_set(x_8, 1, x_6);
lean_ctor_set(x_8, 2, x_3);
lean_ctor_set(x_8, 3, x_7);
x_9 = l_ML_Proof_framing___rarg(x_4, x_5, x_2, x_8);
lean_dec(x_8);
lean_inc(x_2);
x_10 = l_ML_Proof_propagationBottom___rarg(x_2);
x_11 = l_ML_AppContext_insert___rarg(x_4, x_2);
lean_dec(x_4);
x_12 = l_ML_AppContext_insert___rarg(x_5, x_2);
lean_dec(x_2);
x_13 = l_ML_Proof_syllogism___rarg(x_11, x_12, x_5, x_9, x_10);
return x_13;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_doubleNegCtx(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_doubleNegCtx___rarg), 3, 0);
return x_3;
}
}
static lean_object* _init_l_ML_Proof_propagationBottom_x27___rarg___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_unsigned_to_nat(0u);
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Proof_propagationBottom_x27___rarg___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(2);
x_2 = l_ML_Proof_propagationBottom_x27___rarg___closed__1;
x_3 = l_ML_Pattern_conjunction___rarg(x_2, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_Proof_propagationBottom_x27___rarg___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Proof_propagationBottom_x27___rarg___closed__2;
x_2 = l_ML_Proof_exFalso___rarg(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Proof_propagationBottom_x27___rarg___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_box(2);
x_2 = l_ML_Pattern_negation___rarg(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Proof_propagationBottom_x27___rarg___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_Proof_propagationBottom_x27___rarg___closed__1;
x_2 = l_ML_Proof_propagationBottom_x27___rarg___closed__4;
x_3 = l_ML_Pattern_conjunction___rarg(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Proof_propagationBottom_x27___rarg___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Proof_propagationBottom_x27___rarg___closed__5;
x_2 = l_ML_Proof_exFalso___rarg(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottom_x27___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; 
x_2 = lean_box(2);
x_3 = l_ML_Proof_propagationBottom_x27___rarg___closed__2;
x_4 = l_ML_Proof_propagationBottom_x27___rarg___closed__3;
x_5 = l_ML_Proof_framing___rarg(x_2, x_3, x_1, x_4);
x_6 = l_ML_Proof_propagationBottom_x27___rarg___closed__5;
x_7 = l_ML_Proof_propagationBottom_x27___rarg___closed__6;
x_8 = l_ML_Proof_framing___rarg(x_2, x_6, x_1, x_7);
x_9 = l_ML_AppContext_insert___rarg(x_2, x_1);
x_10 = l_ML_AppContext_insert___rarg(x_3, x_1);
x_11 = l_ML_AppContext_insert___rarg(x_6, x_1);
lean_inc(x_11);
lean_inc(x_10);
lean_inc(x_9);
x_12 = l_ML_Proof_conjIntroRule___rarg(x_9, x_10, x_11, x_5, x_8);
x_13 = lean_unsigned_to_nat(0u);
lean_inc(x_1);
x_14 = lean_alloc_ctor(6, 4, 0);
lean_ctor_set(x_14, 0, x_1);
lean_ctor_set(x_14, 1, x_1);
lean_ctor_set(x_14, 2, x_13);
lean_ctor_set(x_14, 3, x_2);
x_15 = l_ML_Pattern_conjunction___rarg(x_10, x_11);
x_16 = l_ML_Proof_syllogism___rarg(x_9, x_15, x_2, x_12, x_14);
return x_16;
}
}
LEAN_EXPORT lean_object* l_ML_Proof_propagationBottom_x27(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_Proof_propagationBottom_x27___rarg), 1, 0);
return x_3;
}
}
lean_object* initialize_Init(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Pattern(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Substitution(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Positivity(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_AppContext(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Premises(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Tautology(uint8_t builtin, lean_object*);
static bool _G_initialized = false;
LEAN_EXPORT lean_object* initialize_MatchingLogic_Proof(uint8_t builtin, lean_object* w) {
lean_object * res;
if (_G_initialized) return lean_io_result_mk_ok(lean_box(0));
_G_initialized = true;
res = initialize_Init(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Pattern(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Substitution(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Positivity(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_AppContext(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Premises(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Tautology(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
l_ML_term___u22a2_____closed__1 = _init_l_ML_term___u22a2_____closed__1();
lean_mark_persistent(l_ML_term___u22a2_____closed__1);
l_ML_term___u22a2_____closed__2 = _init_l_ML_term___u22a2_____closed__2();
lean_mark_persistent(l_ML_term___u22a2_____closed__2);
l_ML_term___u22a2_____closed__3 = _init_l_ML_term___u22a2_____closed__3();
lean_mark_persistent(l_ML_term___u22a2_____closed__3);
l_ML_term___u22a2_____closed__4 = _init_l_ML_term___u22a2_____closed__4();
lean_mark_persistent(l_ML_term___u22a2_____closed__4);
l_ML_term___u22a2_____closed__5 = _init_l_ML_term___u22a2_____closed__5();
lean_mark_persistent(l_ML_term___u22a2_____closed__5);
l_ML_term___u22a2_____closed__6 = _init_l_ML_term___u22a2_____closed__6();
lean_mark_persistent(l_ML_term___u22a2_____closed__6);
l_ML_term___u22a2_____closed__7 = _init_l_ML_term___u22a2_____closed__7();
lean_mark_persistent(l_ML_term___u22a2_____closed__7);
l_ML_term___u22a2_____closed__8 = _init_l_ML_term___u22a2_____closed__8();
lean_mark_persistent(l_ML_term___u22a2_____closed__8);
l_ML_term___u22a2_____closed__9 = _init_l_ML_term___u22a2_____closed__9();
lean_mark_persistent(l_ML_term___u22a2_____closed__9);
l_ML_term___u22a2_____closed__10 = _init_l_ML_term___u22a2_____closed__10();
lean_mark_persistent(l_ML_term___u22a2_____closed__10);
l_ML_term___u22a2_____closed__11 = _init_l_ML_term___u22a2_____closed__11();
lean_mark_persistent(l_ML_term___u22a2_____closed__11);
l_ML_term___u22a2_____closed__12 = _init_l_ML_term___u22a2_____closed__12();
lean_mark_persistent(l_ML_term___u22a2_____closed__12);
l_ML_term___u22a2__ = _init_l_ML_term___u22a2__();
lean_mark_persistent(l_ML_term___u22a2__);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__1 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__1);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__2 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__2);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__3 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__3);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__4 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__4);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__5 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__5);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__6 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__6);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__7 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__7);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__8 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__8);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__9 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__9();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__9);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__10 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__10();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__10);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__11 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__11();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__11);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__12 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__12();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__12);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__13 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__13();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__13);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__14 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__14();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__14);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__15 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__15();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term___u22a2____1___closed__15);
l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2___closed__1 = _init_l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___lambda__2___closed__1);
l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__1 = _init_l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__1);
l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__2 = _init_l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__2);
l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__3 = _init_l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______unexpand__ML__Proof__1___closed__3);
l_ML_term_u1d39_____closed__1 = _init_l_ML_term_u1d39_____closed__1();
lean_mark_persistent(l_ML_term_u1d39_____closed__1);
l_ML_term_u1d39_____closed__2 = _init_l_ML_term_u1d39_____closed__2();
lean_mark_persistent(l_ML_term_u1d39_____closed__2);
l_ML_term_u1d39_____closed__3 = _init_l_ML_term_u1d39_____closed__3();
lean_mark_persistent(l_ML_term_u1d39_____closed__3);
l_ML_term_u1d39_____closed__4 = _init_l_ML_term_u1d39_____closed__4();
lean_mark_persistent(l_ML_term_u1d39_____closed__4);
l_ML_term_u1d39_____closed__5 = _init_l_ML_term_u1d39_____closed__5();
lean_mark_persistent(l_ML_term_u1d39_____closed__5);
l_ML_term_u1d39_____closed__6 = _init_l_ML_term_u1d39_____closed__6();
lean_mark_persistent(l_ML_term_u1d39_____closed__6);
l_ML_term_u1d39_____closed__7 = _init_l_ML_term_u1d39_____closed__7();
lean_mark_persistent(l_ML_term_u1d39_____closed__7);
l_ML_term_u1d39__ = _init_l_ML_term_u1d39__();
lean_mark_persistent(l_ML_term_u1d39__);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__1 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__1);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__2 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__2);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__3 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__3);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__4 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__4);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__5 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__5);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__6 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__6);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__7 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__7);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__8 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__8);
l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__9 = _init_l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__9();
lean_mark_persistent(l_ML___aux__MatchingLogic__Proof______macroRules__ML__term_u1d39____1___closed__9);
l_ML_Proof_topImplSelfImplSelf___rarg___closed__1 = _init_l_ML_Proof_topImplSelfImplSelf___rarg___closed__1();
lean_mark_persistent(l_ML_Proof_topImplSelfImplSelf___rarg___closed__1);
l_ML_Proof_propagationBottom___rarg___closed__1 = _init_l_ML_Proof_propagationBottom___rarg___closed__1();
lean_mark_persistent(l_ML_Proof_propagationBottom___rarg___closed__1);
l_ML_Proof_propagationBottom___rarg___closed__2 = _init_l_ML_Proof_propagationBottom___rarg___closed__2();
lean_mark_persistent(l_ML_Proof_propagationBottom___rarg___closed__2);
l_ML_Proof_propagationBottom_x27___rarg___closed__1 = _init_l_ML_Proof_propagationBottom_x27___rarg___closed__1();
lean_mark_persistent(l_ML_Proof_propagationBottom_x27___rarg___closed__1);
l_ML_Proof_propagationBottom_x27___rarg___closed__2 = _init_l_ML_Proof_propagationBottom_x27___rarg___closed__2();
lean_mark_persistent(l_ML_Proof_propagationBottom_x27___rarg___closed__2);
l_ML_Proof_propagationBottom_x27___rarg___closed__3 = _init_l_ML_Proof_propagationBottom_x27___rarg___closed__3();
lean_mark_persistent(l_ML_Proof_propagationBottom_x27___rarg___closed__3);
l_ML_Proof_propagationBottom_x27___rarg___closed__4 = _init_l_ML_Proof_propagationBottom_x27___rarg___closed__4();
lean_mark_persistent(l_ML_Proof_propagationBottom_x27___rarg___closed__4);
l_ML_Proof_propagationBottom_x27___rarg___closed__5 = _init_l_ML_Proof_propagationBottom_x27___rarg___closed__5();
lean_mark_persistent(l_ML_Proof_propagationBottom_x27___rarg___closed__5);
l_ML_Proof_propagationBottom_x27___rarg___closed__6 = _init_l_ML_Proof_propagationBottom_x27___rarg___closed__6();
lean_mark_persistent(l_ML_Proof_propagationBottom_x27___rarg___closed__6);
return lean_io_result_mk_ok(lean_box(0));
}
#ifdef __cplusplus
}
#endif
