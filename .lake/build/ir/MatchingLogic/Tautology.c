// Lean compiler output
// Module: MatchingLogic.Tautology
// Imports: Init MatchingLogic.Pattern
#include <lean/lean.h>
#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wunused-label"
#elif defined(__GNUC__) && !defined(__CLANG__)
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-label"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#endif
#ifdef __cplusplus
extern "C" {
#endif
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__55;
lean_object* lean_mk_empty_array_with_capacity(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__22;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__7;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__53;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__23;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__37;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__2;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__1;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__27;
static lean_object* l_ML_tacticUnfold__tautology_x21___closed__5;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__8;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__45;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__10;
lean_object* lean_array_push(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__47;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__50;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__15;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__25;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__3;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__20;
static lean_object* l_ML_tacticUnfold__tautology_x21___closed__4;
lean_object* l_Lean_Syntax_node5(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
uint8_t l_Lean_Syntax_isOfKind(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__48;
static lean_object* l_ML_tacticUnfold__tautology___closed__2;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__32;
static lean_object* l_ML_tacticUnfold__tautology_x21___closed__3;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__3;
lean_object* l_Lean_Name_mkStr3(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__21;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__28;
lean_object* l_Lean_SourceInfo_fromRef(lean_object*, uint8_t);
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__5;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__11;
static lean_object* l_ML_tacticUnfold__tautology___closed__6;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__49;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__8;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__38;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__4;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__30;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__42;
lean_object* l_Lean_Syntax_node3(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_tacticUnfold__tautology___closed__4;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__43;
static lean_object* l_ML_tacticUnfold__tautology___closed__3;
static lean_object* l_ML_tacticUnfold__tautology_x21___closed__2;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__44;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__2;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__14;
lean_object* l_Lean_addMacroScope(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__7;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__31;
lean_object* l_Lean_Name_str___override(lean_object*, lean_object*);
lean_object* l_Lean_Syntax_node2(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__54;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__17;
LEAN_EXPORT lean_object* l_ML_tacticUnfold__tautology;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__39;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__18;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__19;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__11;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__4;
static lean_object* l_ML_tacticUnfold__tautology_x21___closed__1;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__10;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__16;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__12;
lean_object* l_Lean_Syntax_node4(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__26;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__33;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__29;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__41;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__9;
lean_object* l_Lean_Name_mkStr2(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__24;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__5;
LEAN_EXPORT lean_object* l_ML_tacticUnfold__tautology_x21;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__12;
lean_object* l_Lean_Syntax_node1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__36;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__14;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__1;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_tacticUnfold__tautology___closed__5;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__35;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__6;
lean_object* l_Lean_Name_mkStr4(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__13;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__6;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__13;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__56;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__46;
static lean_object* l_ML_tacticUnfold__tautology___closed__1;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__52;
lean_object* l_String_toSubstring_x27(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__9;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__40;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__51;
static lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__34;
static lean_object* _init_l_ML_tacticUnfold__tautology___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML", 2);
return x_1;
}
}
static lean_object* _init_l_ML_tacticUnfold__tautology___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticUnfold_tautology", 22);
return x_1;
}
}
static lean_object* _init_l_ML_tacticUnfold__tautology___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_tacticUnfold__tautology___closed__1;
x_2 = l_ML_tacticUnfold__tautology___closed__2;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_tacticUnfold__tautology___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("unfold_tautology", 16);
return x_1;
}
}
static lean_object* _init_l_ML_tacticUnfold__tautology___closed__5() {
_start:
{
lean_object* x_1; uint8_t x_2; lean_object* x_3; 
x_1 = l_ML_tacticUnfold__tautology___closed__4;
x_2 = 0;
x_3 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set_uint8(x_3, sizeof(void*)*1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_tacticUnfold__tautology___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_tacticUnfold__tautology___closed__3;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = l_ML_tacticUnfold__tautology___closed__5;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_tacticUnfold__tautology() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_tacticUnfold__tautology___closed__6;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Mathlib", 7);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Tactic", 6);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticSimp_rw___", 16);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__1;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__2;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__3;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("simp_rw", 7);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("null", 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__6;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_unsigned_to_nat(0u);
x_2 = lean_mk_empty_array_with_capacity(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__9() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Lean", 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__10() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Parser", 6);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__11() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("rwRuleSeq", 9);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__12() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__9;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__10;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__2;
x_4 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__11;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__13() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("[", 1);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__14() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("rwRule", 6);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__9;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__10;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__2;
x_4 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__14;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__16() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML.Morphism.morphism_conj", 25);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__17() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__16;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__18() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Morphism", 8);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__19() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("morphism_conj", 13);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__20() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_tacticUnfold__tautology___closed__1;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__18;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__19;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__21() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__20;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__22() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__21;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__23() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(",", 1);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__24() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML.Morphism.morphism_disj", 25);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__25() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__24;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__26() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("morphism_disj", 13);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__27() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_tacticUnfold__tautology___closed__1;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__18;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__26;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__28() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__27;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__29() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__28;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__30() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML.Morphism.morphism_impl", 25);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__31() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__30;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__32() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("morphism_impl", 13);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__33() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_tacticUnfold__tautology___closed__1;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__18;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__32;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__34() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__33;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__35() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__34;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__36() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML.Morphism.morphism_neg", 24);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__37() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__36;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__38() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("morphism_neg", 12);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__39() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_tacticUnfold__tautology___closed__1;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__18;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__38;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__40() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__39;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__41() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__40;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__42() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML.Morphism.morphism_equiv", 26);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__43() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__42;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__44() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("morphism_equiv", 14);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__45() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_tacticUnfold__tautology___closed__1;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__18;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__44;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__46() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__45;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__47() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__46;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__48() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Morphism.morphism_true", 22);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__49() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__48;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__50() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("morphism_true", 13);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__51() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__18;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__50;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__52() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_tacticUnfold__tautology___closed__1;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__18;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__50;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__53() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__52;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__54() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__53;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__55() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_unsigned_to_nat(11u);
x_2 = lean_mk_empty_array_with_capacity(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__56() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("]", 1);
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_tacticUnfold__tautology___closed__3;
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; lean_object* x_50; lean_object* x_51; lean_object* x_52; lean_object* x_53; lean_object* x_54; lean_object* x_55; lean_object* x_56; lean_object* x_57; lean_object* x_58; lean_object* x_59; lean_object* x_60; lean_object* x_61; lean_object* x_62; lean_object* x_63; lean_object* x_64; lean_object* x_65; lean_object* x_66; lean_object* x_67; lean_object* x_68; lean_object* x_69; lean_object* x_70; lean_object* x_71; lean_object* x_72; lean_object* x_73; lean_object* x_74; lean_object* x_75; lean_object* x_76; lean_object* x_77; lean_object* x_78; 
x_8 = lean_ctor_get(x_2, 5);
lean_inc(x_8);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = lean_ctor_get(x_2, 2);
lean_inc(x_11);
x_12 = lean_ctor_get(x_2, 1);
lean_inc(x_12);
lean_dec(x_2);
x_13 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__5;
lean_inc(x_10);
x_14 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_13);
x_15 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__7;
x_16 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__8;
lean_inc(x_10);
x_17 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_17, 0, x_10);
lean_ctor_set(x_17, 1, x_15);
lean_ctor_set(x_17, 2, x_16);
x_18 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__13;
lean_inc(x_10);
x_19 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_19, 0, x_10);
lean_ctor_set(x_19, 1, x_18);
x_20 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__20;
lean_inc(x_11);
lean_inc(x_12);
x_21 = l_Lean_addMacroScope(x_12, x_20, x_11);
x_22 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__17;
x_23 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__22;
lean_inc(x_10);
x_24 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_24, 0, x_10);
lean_ctor_set(x_24, 1, x_22);
lean_ctor_set(x_24, 2, x_21);
lean_ctor_set(x_24, 3, x_23);
x_25 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__15;
lean_inc(x_17);
lean_inc(x_10);
x_26 = l_Lean_Syntax_node2(x_10, x_25, x_17, x_24);
x_27 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__23;
lean_inc(x_10);
x_28 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_28, 0, x_10);
lean_ctor_set(x_28, 1, x_27);
x_29 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__27;
lean_inc(x_11);
lean_inc(x_12);
x_30 = l_Lean_addMacroScope(x_12, x_29, x_11);
x_31 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__25;
x_32 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__29;
lean_inc(x_10);
x_33 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_33, 0, x_10);
lean_ctor_set(x_33, 1, x_31);
lean_ctor_set(x_33, 2, x_30);
lean_ctor_set(x_33, 3, x_32);
lean_inc(x_17);
lean_inc(x_10);
x_34 = l_Lean_Syntax_node2(x_10, x_25, x_17, x_33);
x_35 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__33;
lean_inc(x_11);
lean_inc(x_12);
x_36 = l_Lean_addMacroScope(x_12, x_35, x_11);
x_37 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__31;
x_38 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__35;
lean_inc(x_10);
x_39 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_39, 0, x_10);
lean_ctor_set(x_39, 1, x_37);
lean_ctor_set(x_39, 2, x_36);
lean_ctor_set(x_39, 3, x_38);
lean_inc(x_17);
lean_inc(x_10);
x_40 = l_Lean_Syntax_node2(x_10, x_25, x_17, x_39);
x_41 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__39;
lean_inc(x_11);
lean_inc(x_12);
x_42 = l_Lean_addMacroScope(x_12, x_41, x_11);
x_43 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__37;
x_44 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__41;
lean_inc(x_10);
x_45 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_45, 0, x_10);
lean_ctor_set(x_45, 1, x_43);
lean_ctor_set(x_45, 2, x_42);
lean_ctor_set(x_45, 3, x_44);
lean_inc(x_17);
lean_inc(x_10);
x_46 = l_Lean_Syntax_node2(x_10, x_25, x_17, x_45);
x_47 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__45;
lean_inc(x_11);
lean_inc(x_12);
x_48 = l_Lean_addMacroScope(x_12, x_47, x_11);
x_49 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__43;
x_50 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__47;
lean_inc(x_10);
x_51 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_51, 0, x_10);
lean_ctor_set(x_51, 1, x_49);
lean_ctor_set(x_51, 2, x_48);
lean_ctor_set(x_51, 3, x_50);
lean_inc(x_17);
lean_inc(x_10);
x_52 = l_Lean_Syntax_node2(x_10, x_25, x_17, x_51);
x_53 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__51;
x_54 = l_Lean_addMacroScope(x_12, x_53, x_11);
x_55 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__49;
x_56 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__54;
lean_inc(x_10);
x_57 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_57, 0, x_10);
lean_ctor_set(x_57, 1, x_55);
lean_ctor_set(x_57, 2, x_54);
lean_ctor_set(x_57, 3, x_56);
lean_inc(x_17);
lean_inc(x_10);
x_58 = l_Lean_Syntax_node2(x_10, x_25, x_17, x_57);
x_59 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__55;
x_60 = lean_array_push(x_59, x_26);
lean_inc(x_28);
x_61 = lean_array_push(x_60, x_28);
x_62 = lean_array_push(x_61, x_34);
lean_inc(x_28);
x_63 = lean_array_push(x_62, x_28);
x_64 = lean_array_push(x_63, x_40);
lean_inc(x_28);
x_65 = lean_array_push(x_64, x_28);
x_66 = lean_array_push(x_65, x_46);
lean_inc(x_28);
x_67 = lean_array_push(x_66, x_28);
x_68 = lean_array_push(x_67, x_52);
x_69 = lean_array_push(x_68, x_28);
x_70 = lean_array_push(x_69, x_58);
lean_inc(x_10);
x_71 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_71, 0, x_10);
lean_ctor_set(x_71, 1, x_15);
lean_ctor_set(x_71, 2, x_70);
x_72 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__56;
lean_inc(x_10);
x_73 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_73, 0, x_10);
lean_ctor_set(x_73, 1, x_72);
x_74 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__12;
lean_inc(x_10);
x_75 = l_Lean_Syntax_node3(x_10, x_74, x_19, x_71, x_73);
x_76 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__4;
lean_inc(x_17);
x_77 = l_Lean_Syntax_node4(x_10, x_76, x_14, x_17, x_75, x_17);
x_78 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_78, 0, x_77);
lean_ctor_set(x_78, 1, x_3);
return x_78;
}
}
}
static lean_object* _init_l_ML_tacticUnfold__tautology_x21___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticUnfold_tautology!", 23);
return x_1;
}
}
static lean_object* _init_l_ML_tacticUnfold__tautology_x21___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_tacticUnfold__tautology___closed__1;
x_2 = l_ML_tacticUnfold__tautology_x21___closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_tacticUnfold__tautology_x21___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("unfold_tautology!", 17);
return x_1;
}
}
static lean_object* _init_l_ML_tacticUnfold__tautology_x21___closed__4() {
_start:
{
lean_object* x_1; uint8_t x_2; lean_object* x_3; 
x_1 = l_ML_tacticUnfold__tautology_x21___closed__3;
x_2 = 0;
x_3 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set_uint8(x_3, sizeof(void*)*1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_tacticUnfold__tautology_x21___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_tacticUnfold__tautology_x21___closed__2;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = l_ML_tacticUnfold__tautology_x21___closed__4;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_tacticUnfold__tautology_x21() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_tacticUnfold__tautology_x21___closed__5;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticTry_", 10);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__9;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__10;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__2;
x_4 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__1;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("try", 3);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticSeq", 9);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__9;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__10;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__2;
x_4 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__4;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticSeq1Indented", 18);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__9;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__10;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__2;
x_4 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__6;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__8() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("intros", 6);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__9;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__10;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__2;
x_4 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__8;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__10() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Term", 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__11() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("hole", 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__12() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__9;
x_2 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__10;
x_3 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__10;
x_4 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__11;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__13() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("_", 1);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__14() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(";", 1);
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_tacticUnfold__tautology_x21___closed__2;
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; 
x_8 = lean_ctor_get(x_2, 5);
lean_inc(x_8);
lean_dec(x_2);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__3;
lean_inc(x_10);
x_12 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_11);
x_13 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__8;
lean_inc(x_10);
x_14 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_13);
x_15 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__13;
lean_inc(x_10);
x_16 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_16, 0, x_10);
lean_ctor_set(x_16, 1, x_15);
x_17 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__12;
lean_inc(x_10);
x_18 = l_Lean_Syntax_node1(x_10, x_17, x_16);
x_19 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__7;
lean_inc_n(x_18, 4);
lean_inc(x_10);
x_20 = l_Lean_Syntax_node5(x_10, x_19, x_18, x_18, x_18, x_18, x_18);
x_21 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__9;
lean_inc(x_10);
x_22 = l_Lean_Syntax_node2(x_10, x_21, x_14, x_20);
x_23 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__14;
lean_inc(x_10);
x_24 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_24, 0, x_10);
lean_ctor_set(x_24, 1, x_23);
x_25 = l_ML_tacticUnfold__tautology___closed__4;
lean_inc(x_10);
x_26 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_26, 0, x_10);
lean_ctor_set(x_26, 1, x_25);
x_27 = l_ML_tacticUnfold__tautology___closed__3;
lean_inc(x_10);
x_28 = l_Lean_Syntax_node1(x_10, x_27, x_26);
lean_inc(x_10);
x_29 = l_Lean_Syntax_node3(x_10, x_19, x_22, x_24, x_28);
x_30 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__7;
lean_inc(x_10);
x_31 = l_Lean_Syntax_node1(x_10, x_30, x_29);
x_32 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__5;
lean_inc(x_10);
x_33 = l_Lean_Syntax_node1(x_10, x_32, x_31);
x_34 = l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__2;
x_35 = l_Lean_Syntax_node2(x_10, x_34, x_12, x_33);
x_36 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_36, 0, x_35);
lean_ctor_set(x_36, 1, x_3);
return x_36;
}
}
}
lean_object* initialize_Init(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Pattern(uint8_t builtin, lean_object*);
static bool _G_initialized = false;
LEAN_EXPORT lean_object* initialize_MatchingLogic_Tautology(uint8_t builtin, lean_object* w) {
lean_object * res;
if (_G_initialized) return lean_io_result_mk_ok(lean_box(0));
_G_initialized = true;
res = initialize_Init(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Pattern(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
l_ML_tacticUnfold__tautology___closed__1 = _init_l_ML_tacticUnfold__tautology___closed__1();
lean_mark_persistent(l_ML_tacticUnfold__tautology___closed__1);
l_ML_tacticUnfold__tautology___closed__2 = _init_l_ML_tacticUnfold__tautology___closed__2();
lean_mark_persistent(l_ML_tacticUnfold__tautology___closed__2);
l_ML_tacticUnfold__tautology___closed__3 = _init_l_ML_tacticUnfold__tautology___closed__3();
lean_mark_persistent(l_ML_tacticUnfold__tautology___closed__3);
l_ML_tacticUnfold__tautology___closed__4 = _init_l_ML_tacticUnfold__tautology___closed__4();
lean_mark_persistent(l_ML_tacticUnfold__tautology___closed__4);
l_ML_tacticUnfold__tautology___closed__5 = _init_l_ML_tacticUnfold__tautology___closed__5();
lean_mark_persistent(l_ML_tacticUnfold__tautology___closed__5);
l_ML_tacticUnfold__tautology___closed__6 = _init_l_ML_tacticUnfold__tautology___closed__6();
lean_mark_persistent(l_ML_tacticUnfold__tautology___closed__6);
l_ML_tacticUnfold__tautology = _init_l_ML_tacticUnfold__tautology();
lean_mark_persistent(l_ML_tacticUnfold__tautology);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__1 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__1);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__2 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__2);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__3 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__3);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__4 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__4);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__5 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__5);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__6 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__6);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__7 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__7);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__8 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__8);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__9 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__9();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__9);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__10 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__10();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__10);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__11 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__11();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__11);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__12 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__12();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__12);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__13 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__13();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__13);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__14 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__14();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__14);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__15 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__15();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__15);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__16 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__16();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__16);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__17 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__17();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__17);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__18 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__18();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__18);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__19 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__19();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__19);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__20 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__20();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__20);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__21 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__21();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__21);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__22 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__22();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__22);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__23 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__23();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__23);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__24 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__24();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__24);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__25 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__25();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__25);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__26 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__26();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__26);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__27 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__27();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__27);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__28 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__28();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__28);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__29 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__29();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__29);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__30 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__30();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__30);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__31 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__31();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__31);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__32 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__32();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__32);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__33 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__33();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__33);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__34 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__34();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__34);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__35 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__35();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__35);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__36 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__36();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__36);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__37 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__37();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__37);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__38 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__38();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__38);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__39 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__39();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__39);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__40 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__40();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__40);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__41 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__41();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__41);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__42 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__42();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__42);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__43 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__43();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__43);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__44 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__44();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__44);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__45 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__45();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__45);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__46 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__46();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__46);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__47 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__47();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__47);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__48 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__48();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__48);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__49 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__49();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__49);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__50 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__50();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__50);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__51 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__51();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__51);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__52 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__52();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__52);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__53 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__53();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__53);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__54 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__54();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__54);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__55 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__55();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__55);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__56 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__56();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology__1___closed__56);
l_ML_tacticUnfold__tautology_x21___closed__1 = _init_l_ML_tacticUnfold__tautology_x21___closed__1();
lean_mark_persistent(l_ML_tacticUnfold__tautology_x21___closed__1);
l_ML_tacticUnfold__tautology_x21___closed__2 = _init_l_ML_tacticUnfold__tautology_x21___closed__2();
lean_mark_persistent(l_ML_tacticUnfold__tautology_x21___closed__2);
l_ML_tacticUnfold__tautology_x21___closed__3 = _init_l_ML_tacticUnfold__tautology_x21___closed__3();
lean_mark_persistent(l_ML_tacticUnfold__tautology_x21___closed__3);
l_ML_tacticUnfold__tautology_x21___closed__4 = _init_l_ML_tacticUnfold__tautology_x21___closed__4();
lean_mark_persistent(l_ML_tacticUnfold__tautology_x21___closed__4);
l_ML_tacticUnfold__tautology_x21___closed__5 = _init_l_ML_tacticUnfold__tautology_x21___closed__5();
lean_mark_persistent(l_ML_tacticUnfold__tautology_x21___closed__5);
l_ML_tacticUnfold__tautology_x21 = _init_l_ML_tacticUnfold__tautology_x21();
lean_mark_persistent(l_ML_tacticUnfold__tautology_x21);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__1 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__1);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__2 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__2);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__3 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__3);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__4 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__4);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__5 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__5);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__6 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__6);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__7 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__7);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__8 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__8);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__9 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__9();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__9);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__10 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__10();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__10);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__11 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__11();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__11);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__12 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__12();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__12);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__13 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__13();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__13);
l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__14 = _init_l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__14();
lean_mark_persistent(l_ML___aux__MatchingLogic__Tautology______macroRules__ML__tacticUnfold__tautology_x21__1___closed__14);
return lean_io_result_mk_ok(lean_box(0));
}
#ifdef __cplusplus
}
#endif
