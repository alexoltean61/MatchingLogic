// Lean compiler output
// Module: MatchingLogic.Substitution
// Imports: Init MatchingLogic.Pattern
#include <lean/lean.h>
#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wunused-label"
#elif defined(__GNUC__) && !defined(__CLANG__)
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-label"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#endif
#ifdef __cplusplus
extern "C" {
#endif
static lean_object* l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__7;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__4;
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__3;
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__2___lambda__1___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6;
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__2___lambda__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__7;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_evars_match__1_splitter(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substSvar_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__1___lambda__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* lean_mk_empty_array_with_capacity(lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__16;
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0_u02e2___x5d__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__8;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__3;
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__11;
LEAN_EXPORT lean_object* l_ML_Pattern_term___x5b___u21d0_u1d49___x5d;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__1;
lean_object* l_Lean_Syntax_getArgs(lean_object*);
lean_object* l_Lean_replaceRef(lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__12;
LEAN_EXPORT lean_object* l_ML_Pattern_renameEvar_x27(lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__2___lambda__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_substEvar___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__9;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substSvar_match__1_splitter___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__2(lean_object*, lean_object*, lean_object*);
uint8_t l_Lean_Syntax_isOfKind(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_substitutableForSvarIn___rarg___boxed(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__14;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__12;
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__2(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__10;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__1;
lean_object* l_Lean_Syntax_getNumArgs(lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__4;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__10;
lean_object* l_Lean_Name_mkStr3(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_substEvar___rarg___boxed(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__6;
uint8_t l_instDecidableNot___rarg(uint8_t);
static lean_object* l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__1;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__6;
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__14;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__7;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__3;
static uint8_t l_ML_Pattern_substitutableForSvarIn___rarg___closed__1;
static lean_object* l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__6;
lean_object* l_Lean_SourceInfo_fromRef(lean_object*, uint8_t);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__2;
lean_object* l_Lean_Syntax_node6(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__8;
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__2;
uint8_t l_ML_Pattern_isFreeSvar___rarg(lean_object*, lean_object*);
static uint8_t l_ML_Pattern_substitutableForSvarIn___rarg___closed__2;
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__4;
static lean_object* l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__9;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__13;
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__2;
static lean_object* l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__4;
LEAN_EXPORT lean_object* l_ML_Pattern_renameEvar_x27___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__15;
LEAN_EXPORT lean_object* l_ML_Pattern_renameEvar(lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__8;
LEAN_EXPORT lean_object* l_ML_Pattern_renameEvar_x27___rarg___boxed(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__3;
lean_object* l_Lean_Syntax_node3(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__2___lambda__1___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__4;
LEAN_EXPORT lean_object* l_ML_Pattern_term___x5b___u21d0_u02e2___x5d;
lean_object* l_Lean_addMacroScope(lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Name_str___override(lean_object*, lean_object*);
lean_object* l_Lean_Syntax_node2(lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Syntax_getArg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_substSvar___rarg___boxed(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__3;
uint8_t l_Lean_Syntax_matchesNull(lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__18;
static lean_object* l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__8;
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__2;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substitutableForEvarIn_match__1_splitter(lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__1;
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__5;
static lean_object* l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__5;
LEAN_EXPORT lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u02e2;
lean_object* l_Array_append___rarg(lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__6;
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__19;
static lean_object* l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__2;
lean_object* l_Array_extract___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_substSvar___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_substitutableForSvarIn(lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7;
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__5;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2___closed__1;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_isFreeSvar_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__1;
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0_u1d49___x5d__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__6;
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__17;
LEAN_EXPORT lean_object* l_ML_Pattern_substSvar(lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__8;
uint8_t lean_nat_dec_eq(lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__1;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__5;
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__9;
static lean_object* l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__3;
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT uint8_t l_ML_Pattern_substitutableForSvarIn___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__4;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_renameEvar_x27_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substitutableForEvarIn_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_substEvar(lean_object*);
lean_object* lean_nat_sub(lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__5;
static lean_object* l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__1;
static lean_object* l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__2;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_isFreeSvar_match__1_splitter(lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__11;
uint8_t l_ML_Pattern_isFreeEvar___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substSvar_match__1_splitter(lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__13;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_renameEvar_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substEvar_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_renameEvar_match__1_splitter(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_renameEvar___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_renameEvar_x27_match__1_splitter(lean_object*, lean_object*);
lean_object* l_Lean_Name_mkStr4(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_evars_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__10;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substitutableForSvarIn_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
uint8_t lean_nat_dec_le(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__2;
static lean_object* l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__20;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substEvar_match__1_splitter(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_renameEvar___rarg(lean_object*, lean_object*, lean_object*, uint8_t);
lean_object* l_String_toSubstring_x27(lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__1___lambda__1___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__3;
static lean_object* l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__7;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substEvar_match__1_splitter___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__1___boxed(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substitutableForSvarIn_match__1_splitter(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Pattern_substEvar___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 0:
{
uint8_t x_4; 
x_4 = !lean_is_exclusive(x_1);
if (x_4 == 0)
{
lean_object* x_5; uint8_t x_6; 
x_5 = lean_ctor_get(x_1, 0);
x_6 = lean_nat_dec_eq(x_2, x_5);
if (x_6 == 0)
{
return x_1;
}
else
{
lean_free_object(x_1);
lean_dec(x_5);
lean_inc(x_3);
return x_3;
}
}
else
{
lean_object* x_7; uint8_t x_8; 
x_7 = lean_ctor_get(x_1, 0);
lean_inc(x_7);
lean_dec(x_1);
x_8 = lean_nat_dec_eq(x_2, x_7);
if (x_8 == 0)
{
lean_object* x_9; 
x_9 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_9, 0, x_7);
return x_9;
}
else
{
lean_dec(x_7);
lean_inc(x_3);
return x_3;
}
}
}
case 2:
{
lean_object* x_10; 
x_10 = lean_box(2);
return x_10;
}
case 4:
{
uint8_t x_11; 
x_11 = !lean_is_exclusive(x_1);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; 
x_12 = lean_ctor_get(x_1, 0);
x_13 = lean_ctor_get(x_1, 1);
x_14 = l_ML_Pattern_substEvar___rarg(x_12, x_2, x_3);
x_15 = l_ML_Pattern_substEvar___rarg(x_13, x_2, x_3);
lean_ctor_set(x_1, 1, x_15);
lean_ctor_set(x_1, 0, x_14);
return x_1;
}
else
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; 
x_16 = lean_ctor_get(x_1, 0);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_inc(x_16);
lean_dec(x_1);
x_18 = l_ML_Pattern_substEvar___rarg(x_16, x_2, x_3);
x_19 = l_ML_Pattern_substEvar___rarg(x_17, x_2, x_3);
x_20 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_20, 0, x_18);
lean_ctor_set(x_20, 1, x_19);
return x_20;
}
}
case 5:
{
uint8_t x_21; 
x_21 = !lean_is_exclusive(x_1);
if (x_21 == 0)
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; 
x_22 = lean_ctor_get(x_1, 0);
x_23 = lean_ctor_get(x_1, 1);
x_24 = l_ML_Pattern_substEvar___rarg(x_22, x_2, x_3);
x_25 = l_ML_Pattern_substEvar___rarg(x_23, x_2, x_3);
lean_ctor_set(x_1, 1, x_25);
lean_ctor_set(x_1, 0, x_24);
return x_1;
}
else
{
lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; 
x_26 = lean_ctor_get(x_1, 0);
x_27 = lean_ctor_get(x_1, 1);
lean_inc(x_27);
lean_inc(x_26);
lean_dec(x_1);
x_28 = l_ML_Pattern_substEvar___rarg(x_26, x_2, x_3);
x_29 = l_ML_Pattern_substEvar___rarg(x_27, x_2, x_3);
x_30 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_30, 0, x_28);
lean_ctor_set(x_30, 1, x_29);
return x_30;
}
}
case 6:
{
uint8_t x_31; 
x_31 = !lean_is_exclusive(x_1);
if (x_31 == 0)
{
lean_object* x_32; lean_object* x_33; uint8_t x_34; 
x_32 = lean_ctor_get(x_1, 0);
x_33 = lean_ctor_get(x_1, 1);
x_34 = lean_nat_dec_eq(x_2, x_32);
if (x_34 == 0)
{
lean_object* x_35; 
x_35 = l_ML_Pattern_substEvar___rarg(x_33, x_2, x_3);
lean_ctor_set(x_1, 1, x_35);
return x_1;
}
else
{
return x_1;
}
}
else
{
lean_object* x_36; lean_object* x_37; uint8_t x_38; 
x_36 = lean_ctor_get(x_1, 0);
x_37 = lean_ctor_get(x_1, 1);
lean_inc(x_37);
lean_inc(x_36);
lean_dec(x_1);
x_38 = lean_nat_dec_eq(x_2, x_36);
if (x_38 == 0)
{
lean_object* x_39; lean_object* x_40; 
x_39 = l_ML_Pattern_substEvar___rarg(x_37, x_2, x_3);
x_40 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_40, 0, x_36);
lean_ctor_set(x_40, 1, x_39);
return x_40;
}
else
{
lean_object* x_41; 
x_41 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_41, 0, x_36);
lean_ctor_set(x_41, 1, x_37);
return x_41;
}
}
}
case 7:
{
uint8_t x_42; 
x_42 = !lean_is_exclusive(x_1);
if (x_42 == 0)
{
lean_object* x_43; lean_object* x_44; 
x_43 = lean_ctor_get(x_1, 1);
x_44 = l_ML_Pattern_substEvar___rarg(x_43, x_2, x_3);
lean_ctor_set(x_1, 1, x_44);
return x_1;
}
else
{
lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; 
x_45 = lean_ctor_get(x_1, 0);
x_46 = lean_ctor_get(x_1, 1);
lean_inc(x_46);
lean_inc(x_45);
lean_dec(x_1);
x_47 = l_ML_Pattern_substEvar___rarg(x_46, x_2, x_3);
x_48 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_48, 0, x_45);
lean_ctor_set(x_48, 1, x_47);
return x_48;
}
}
default: 
{
return x_1;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_substEvar(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Pattern_substEvar___rarg___boxed), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_substEvar___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML_Pattern_substEvar___rarg(x_1, x_2, x_3);
lean_dec(x_3);
lean_dec(x_2);
return x_4;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substEvar_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8, lean_object* x_9) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 0:
{
lean_object* x_10; lean_object* x_11; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_10 = lean_ctor_get(x_1, 0);
lean_inc(x_10);
lean_dec(x_1);
x_11 = lean_apply_1(x_2, x_10);
return x_11;
}
case 1:
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_12 = lean_ctor_get(x_1, 0);
lean_inc(x_12);
lean_dec(x_1);
x_13 = lean_apply_1(x_3, x_12);
return x_13;
}
case 2:
{
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
lean_inc(x_9);
return x_9;
}
case 3:
{
lean_object* x_14; lean_object* x_15; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_14 = lean_ctor_get(x_1, 0);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_1(x_8, x_14);
return x_15;
}
case 4:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_6, x_16, x_17);
return x_18;
}
case 5:
{
lean_object* x_19; lean_object* x_20; lean_object* x_21; 
lean_dec(x_8);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_ctor_get(x_1, 0);
lean_inc(x_19);
x_20 = lean_ctor_get(x_1, 1);
lean_inc(x_20);
lean_dec(x_1);
x_21 = lean_apply_2(x_7, x_19, x_20);
return x_21;
}
case 6:
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_22 = lean_ctor_get(x_1, 0);
lean_inc(x_22);
x_23 = lean_ctor_get(x_1, 1);
lean_inc(x_23);
lean_dec(x_1);
x_24 = lean_apply_2(x_4, x_22, x_23);
return x_24;
}
default: 
{
lean_object* x_25; lean_object* x_26; lean_object* x_27; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_25 = lean_ctor_get(x_1, 0);
lean_inc(x_25);
x_26 = lean_ctor_get(x_1, 1);
lean_inc(x_26);
lean_dec(x_1);
x_27 = lean_apply_2(x_5, x_25, x_26);
return x_27;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substEvar_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Substitution_0__ML_Pattern_substEvar_match__1_splitter___rarg___boxed), 9, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substEvar_match__1_splitter___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8, lean_object* x_9) {
_start:
{
lean_object* x_10; 
x_10 = l___private_MatchingLogic_Substitution_0__ML_Pattern_substEvar_match__1_splitter___rarg(x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8, x_9);
lean_dec(x_9);
return x_10;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML", 2);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Pattern", 7);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_[_⇐_]ᵉ", 15);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__1;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__2;
x_3 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__3;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("andthen", 7);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__5;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("[", 1);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__9() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term", 4);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__9;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__10;
x_2 = lean_unsigned_to_nat(0u);
x_3 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__12() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__8;
x_3 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__11;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__13() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(" ⇐ ", 5);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__14() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__13;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__12;
x_3 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__14;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__16() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__15;
x_3 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__11;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__17() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("]ᵉ", 4);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__18() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__17;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__19() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__16;
x_3 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__18;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__20() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__4;
x_2 = lean_unsigned_to_nat(1022u);
x_3 = lean_unsigned_to_nat(0u);
x_4 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__19;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__20;
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Lean", 4);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Parser", 6);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Term", 4);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("app", 3);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__1;
x_2 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__3;
x_4 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__4;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("substEvar", 9);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__6;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__6;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__1;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__6;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__9;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__9;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__12() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__11;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__10;
x_2 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__12;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__14() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("null", 4);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__14;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__4;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; uint8_t x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
x_12 = lean_unsigned_to_nat(4u);
x_13 = l_Lean_Syntax_getArg(x_1, x_12);
lean_dec(x_1);
x_14 = lean_ctor_get(x_2, 5);
lean_inc(x_14);
x_15 = 0;
x_16 = l_Lean_SourceInfo_fromRef(x_14, x_15);
x_17 = lean_ctor_get(x_2, 2);
lean_inc(x_17);
x_18 = lean_ctor_get(x_2, 1);
lean_inc(x_18);
lean_dec(x_2);
x_19 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__8;
x_20 = l_Lean_addMacroScope(x_18, x_19, x_17);
x_21 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__7;
x_22 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__13;
lean_inc(x_16);
x_23 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_23, 0, x_16);
lean_ctor_set(x_23, 1, x_21);
lean_ctor_set(x_23, 2, x_20);
lean_ctor_set(x_23, 3, x_22);
x_24 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
lean_inc(x_16);
x_25 = l_Lean_Syntax_node3(x_16, x_24, x_9, x_11, x_13);
x_26 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5;
x_27 = l_Lean_Syntax_node2(x_16, x_26, x_23, x_25);
x_28 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_28, 0, x_27);
lean_ctor_set(x_28, 1, x_3);
return x_28;
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; 
x_4 = lean_box(0);
x_5 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_5, 0, x_4);
lean_ctor_set(x_5, 1, x_3);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_unsigned_to_nat(0u);
x_2 = lean_mk_empty_array_with_capacity(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8) {
_start:
{
lean_object* x_9; uint8_t x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; 
x_9 = l_Lean_replaceRef(x_1, x_7);
x_10 = 0;
x_11 = l_Lean_SourceInfo_fromRef(x_9, x_10);
x_12 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7;
lean_inc(x_11);
x_13 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_13, 0, x_11);
lean_ctor_set(x_13, 1, x_12);
x_14 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__13;
lean_inc(x_11);
x_15 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_15, 0, x_11);
lean_ctor_set(x_15, 1, x_14);
x_16 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__17;
lean_inc(x_11);
x_17 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_17, 0, x_11);
lean_ctor_set(x_17, 1, x_16);
x_18 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__4;
lean_inc(x_11);
x_19 = l_Lean_Syntax_node6(x_11, x_18, x_3, x_13, x_4, x_15, x_5, x_17);
x_20 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2___closed__1;
x_21 = l_Array_append___rarg(x_20, x_6);
x_22 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
lean_inc(x_11);
x_23 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_23, 0, x_11);
lean_ctor_set(x_23, 1, x_22);
lean_ctor_set(x_23, 2, x_21);
x_24 = l_Lean_Syntax_node2(x_11, x_2, x_19, x_23);
x_25 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_25, 0, x_24);
lean_ctor_set(x_25, 1, x_8);
return x_25;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ident", 5);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_alloc_closure((void*)(l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__1___boxed), 3, 0);
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; uint8_t x_18; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__3;
x_17 = lean_unsigned_to_nat(3u);
lean_inc(x_15);
x_18 = l_Lean_Syntax_matchesNull(x_15, x_17);
if (x_18 == 0)
{
lean_object* x_19; uint8_t x_20; 
x_19 = l_Lean_Syntax_getNumArgs(x_15);
x_20 = lean_nat_dec_le(x_17, x_19);
if (x_20 == 0)
{
lean_object* x_21; lean_object* x_22; 
lean_dec(x_19);
lean_dec(x_15);
lean_dec(x_9);
x_21 = lean_box(0);
x_22 = lean_apply_3(x_16, x_21, x_2, x_3);
return x_22;
}
else
{
lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; 
x_23 = l_Lean_Syntax_getArg(x_15, x_8);
x_24 = l_Lean_Syntax_getArg(x_15, x_14);
x_25 = lean_unsigned_to_nat(2u);
x_26 = l_Lean_Syntax_getArg(x_15, x_25);
x_27 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_28 = lean_nat_sub(x_19, x_8);
lean_dec(x_19);
x_29 = l_Array_extract___rarg(x_27, x_17, x_28);
lean_dec(x_28);
lean_dec(x_27);
x_30 = lean_box(2);
x_31 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
x_32 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_32, 0, x_30);
lean_ctor_set(x_32, 1, x_31);
lean_ctor_set(x_32, 2, x_29);
x_33 = l_Lean_Syntax_getArgs(x_32);
lean_dec(x_32);
x_34 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2(x_9, x_4, x_23, x_24, x_26, x_33, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_34;
}
}
else
{
lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; uint8_t x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; lean_object* x_50; 
x_35 = l_Lean_Syntax_getArg(x_15, x_8);
x_36 = l_Lean_Syntax_getArg(x_15, x_14);
x_37 = lean_unsigned_to_nat(2u);
x_38 = l_Lean_Syntax_getArg(x_15, x_37);
lean_dec(x_15);
x_39 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_40 = 0;
x_41 = l_Lean_SourceInfo_fromRef(x_39, x_40);
x_42 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7;
lean_inc(x_41);
x_43 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_43, 0, x_41);
lean_ctor_set(x_43, 1, x_42);
x_44 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__13;
lean_inc(x_41);
x_45 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_45, 0, x_41);
lean_ctor_set(x_45, 1, x_44);
x_46 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__17;
lean_inc(x_41);
x_47 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_47, 0, x_41);
lean_ctor_set(x_47, 1, x_46);
x_48 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__4;
x_49 = l_Lean_Syntax_node6(x_41, x_48, x_35, x_43, x_36, x_45, x_38, x_47);
x_50 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_50, 0, x_49);
lean_ctor_set(x_50, 1, x_3);
return x_50;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__1(x_1, x_2, x_3);
lean_dec(x_2);
lean_dec(x_1);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8) {
_start:
{
lean_object* x_9; 
x_9 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2(x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8);
lean_dec(x_7);
lean_dec(x_1);
return x_9;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_[_⇐ᵉ_]", 15);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__1;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__2;
x_3 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__1;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(" ⇐ᵉ ", 8);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__12;
x_3 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__4;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6;
x_2 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__5;
x_3 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__11;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__7() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("]", 1);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__7;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6;
x_2 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__6;
x_3 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__8;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__2;
x_2 = lean_unsigned_to_nat(1022u);
x_3 = lean_unsigned_to_nat(0u);
x_4 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__9;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__10;
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0_u1d49___x5d__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; uint8_t x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
x_12 = lean_unsigned_to_nat(4u);
x_13 = l_Lean_Syntax_getArg(x_1, x_12);
lean_dec(x_1);
x_14 = lean_ctor_get(x_2, 5);
lean_inc(x_14);
x_15 = 0;
x_16 = l_Lean_SourceInfo_fromRef(x_14, x_15);
x_17 = lean_ctor_get(x_2, 2);
lean_inc(x_17);
x_18 = lean_ctor_get(x_2, 1);
lean_inc(x_18);
lean_dec(x_2);
x_19 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__8;
x_20 = l_Lean_addMacroScope(x_18, x_19, x_17);
x_21 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__7;
x_22 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__13;
lean_inc(x_16);
x_23 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_23, 0, x_16);
lean_ctor_set(x_23, 1, x_21);
lean_ctor_set(x_23, 2, x_20);
lean_ctor_set(x_23, 3, x_22);
x_24 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
lean_inc(x_16);
x_25 = l_Lean_Syntax_node3(x_16, x_24, x_9, x_11, x_13);
x_26 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5;
x_27 = l_Lean_Syntax_node2(x_16, x_26, x_23, x_25);
x_28 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_28, 0, x_27);
lean_ctor_set(x_28, 1, x_3);
return x_28;
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__2___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8) {
_start:
{
lean_object* x_9; uint8_t x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; 
x_9 = l_Lean_replaceRef(x_1, x_7);
x_10 = 0;
x_11 = l_Lean_SourceInfo_fromRef(x_9, x_10);
x_12 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7;
lean_inc(x_11);
x_13 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_13, 0, x_11);
lean_ctor_set(x_13, 1, x_12);
x_14 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__3;
lean_inc(x_11);
x_15 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_15, 0, x_11);
lean_ctor_set(x_15, 1, x_14);
x_16 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__7;
lean_inc(x_11);
x_17 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_17, 0, x_11);
lean_ctor_set(x_17, 1, x_16);
x_18 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__2;
lean_inc(x_11);
x_19 = l_Lean_Syntax_node6(x_11, x_18, x_3, x_13, x_4, x_15, x_5, x_17);
x_20 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2___closed__1;
x_21 = l_Array_append___rarg(x_20, x_6);
x_22 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
lean_inc(x_11);
x_23 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_23, 0, x_11);
lean_ctor_set(x_23, 1, x_22);
lean_ctor_set(x_23, 2, x_21);
x_24 = l_Lean_Syntax_node2(x_11, x_2, x_19, x_23);
x_25 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_25, 0, x_24);
lean_ctor_set(x_25, 1, x_8);
return x_25;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__2(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = lean_unsigned_to_nat(3u);
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; uint8_t x_19; 
x_18 = l_Lean_Syntax_getNumArgs(x_15);
x_19 = lean_nat_dec_le(x_16, x_18);
if (x_19 == 0)
{
lean_object* x_20; lean_object* x_21; 
lean_dec(x_18);
lean_dec(x_15);
lean_dec(x_9);
lean_dec(x_2);
x_20 = lean_box(0);
x_21 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_21, 0, x_20);
lean_ctor_set(x_21, 1, x_3);
return x_21;
}
else
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; 
x_22 = l_Lean_Syntax_getArg(x_15, x_8);
x_23 = l_Lean_Syntax_getArg(x_15, x_14);
x_24 = lean_unsigned_to_nat(2u);
x_25 = l_Lean_Syntax_getArg(x_15, x_24);
x_26 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_27 = lean_nat_sub(x_18, x_8);
lean_dec(x_18);
x_28 = l_Array_extract___rarg(x_26, x_16, x_27);
lean_dec(x_27);
lean_dec(x_26);
x_29 = lean_box(2);
x_30 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
x_31 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_31, 0, x_29);
lean_ctor_set(x_31, 1, x_30);
lean_ctor_set(x_31, 2, x_28);
x_32 = l_Lean_Syntax_getArgs(x_31);
lean_dec(x_31);
x_33 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__2___lambda__1(x_9, x_4, x_22, x_23, x_25, x_32, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_33;
}
}
else
{
lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; uint8_t x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; 
x_34 = l_Lean_Syntax_getArg(x_15, x_8);
x_35 = l_Lean_Syntax_getArg(x_15, x_14);
x_36 = lean_unsigned_to_nat(2u);
x_37 = l_Lean_Syntax_getArg(x_15, x_36);
lean_dec(x_15);
x_38 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_39 = 0;
x_40 = l_Lean_SourceInfo_fromRef(x_38, x_39);
x_41 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7;
lean_inc(x_40);
x_42 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_42, 0, x_40);
lean_ctor_set(x_42, 1, x_41);
x_43 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__3;
lean_inc(x_40);
x_44 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_44, 0, x_40);
lean_ctor_set(x_44, 1, x_43);
x_45 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__7;
lean_inc(x_40);
x_46 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_46, 0, x_40);
lean_ctor_set(x_46, 1, x_45);
x_47 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__2;
x_48 = l_Lean_Syntax_node6(x_40, x_47, x_34, x_42, x_35, x_44, x_37, x_46);
x_49 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_49, 0, x_48);
lean_ctor_set(x_49, 1, x_3);
return x_49;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__2___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8) {
_start:
{
lean_object* x_9; 
x_9 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__2___lambda__1(x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8);
lean_dec(x_7);
lean_dec(x_1);
return x_9;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substitutableForEvarIn_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 4:
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_7 = lean_ctor_get(x_1, 0);
lean_inc(x_7);
x_8 = lean_ctor_get(x_1, 1);
lean_inc(x_8);
lean_dec(x_1);
x_9 = lean_apply_2(x_4, x_7, x_8);
return x_9;
}
case 5:
{
lean_object* x_10; lean_object* x_11; lean_object* x_12; 
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_10 = lean_ctor_get(x_1, 0);
lean_inc(x_10);
x_11 = lean_ctor_get(x_1, 1);
lean_inc(x_11);
lean_dec(x_1);
x_12 = lean_apply_2(x_5, x_10, x_11);
return x_12;
}
case 6:
{
lean_object* x_13; lean_object* x_14; lean_object* x_15; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_13 = lean_ctor_get(x_1, 0);
lean_inc(x_13);
x_14 = lean_ctor_get(x_1, 1);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_2(x_2, x_13, x_14);
return x_15;
}
case 7:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_3, x_16, x_17);
return x_18;
}
default: 
{
lean_object* x_19; 
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_apply_5(x_6, x_1, lean_box(0), lean_box(0), lean_box(0), lean_box(0));
return x_19;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substitutableForEvarIn_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Substitution_0__ML_Pattern_substitutableForEvarIn_match__1_splitter___rarg), 6, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_evars_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 0:
{
lean_object* x_9; lean_object* x_10; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_9 = lean_ctor_get(x_1, 0);
lean_inc(x_9);
lean_dec(x_1);
x_10 = lean_apply_1(x_2, x_9);
return x_10;
}
case 1:
{
lean_object* x_11; lean_object* x_12; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_11 = lean_ctor_get(x_1, 0);
lean_inc(x_11);
lean_dec(x_1);
x_12 = lean_apply_1(x_3, x_11);
return x_12;
}
case 4:
{
lean_object* x_13; lean_object* x_14; lean_object* x_15; 
lean_dec(x_8);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_13 = lean_ctor_get(x_1, 0);
lean_inc(x_13);
x_14 = lean_ctor_get(x_1, 1);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_2(x_7, x_13, x_14);
return x_15;
}
case 5:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_6, x_16, x_17);
return x_18;
}
case 6:
{
lean_object* x_19; lean_object* x_20; lean_object* x_21; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_ctor_get(x_1, 0);
lean_inc(x_19);
x_20 = lean_ctor_get(x_1, 1);
lean_inc(x_20);
lean_dec(x_1);
x_21 = lean_apply_2(x_4, x_19, x_20);
return x_21;
}
case 7:
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_22 = lean_ctor_get(x_1, 0);
lean_inc(x_22);
x_23 = lean_ctor_get(x_1, 1);
lean_inc(x_23);
lean_dec(x_1);
x_24 = lean_apply_2(x_5, x_22, x_23);
return x_24;
}
default: 
{
lean_object* x_25; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_25 = lean_apply_7(x_8, x_1, lean_box(0), lean_box(0), lean_box(0), lean_box(0), lean_box(0), lean_box(0));
return x_25;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_evars_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Substitution_0__ML_Pattern_evars_match__1_splitter___rarg), 8, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_renameEvar___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, uint8_t x_4) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 0:
{
if (x_4 == 0)
{
uint8_t x_5; 
lean_dec(x_3);
x_5 = !lean_is_exclusive(x_1);
if (x_5 == 0)
{
return x_1;
}
else
{
lean_object* x_6; lean_object* x_7; 
x_6 = lean_ctor_get(x_1, 0);
lean_inc(x_6);
lean_dec(x_1);
x_7 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_7, 0, x_6);
return x_7;
}
}
else
{
uint8_t x_8; 
x_8 = !lean_is_exclusive(x_1);
if (x_8 == 0)
{
lean_object* x_9; uint8_t x_10; 
x_9 = lean_ctor_get(x_1, 0);
x_10 = lean_nat_dec_eq(x_2, x_9);
if (x_10 == 0)
{
lean_dec(x_3);
return x_1;
}
else
{
lean_dec(x_9);
lean_ctor_set(x_1, 0, x_3);
return x_1;
}
}
else
{
lean_object* x_11; uint8_t x_12; 
x_11 = lean_ctor_get(x_1, 0);
lean_inc(x_11);
lean_dec(x_1);
x_12 = lean_nat_dec_eq(x_2, x_11);
if (x_12 == 0)
{
lean_object* x_13; 
lean_dec(x_3);
x_13 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_13, 0, x_11);
return x_13;
}
else
{
lean_object* x_14; 
lean_dec(x_11);
x_14 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_14, 0, x_3);
return x_14;
}
}
}
}
case 4:
{
uint8_t x_15; 
x_15 = !lean_is_exclusive(x_1);
if (x_15 == 0)
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; 
x_16 = lean_ctor_get(x_1, 0);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_3);
x_18 = l_ML_Pattern_renameEvar___rarg(x_16, x_2, x_3, x_4);
x_19 = l_ML_Pattern_renameEvar___rarg(x_17, x_2, x_3, x_4);
lean_ctor_set(x_1, 1, x_19);
lean_ctor_set(x_1, 0, x_18);
return x_1;
}
else
{
lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; 
x_20 = lean_ctor_get(x_1, 0);
x_21 = lean_ctor_get(x_1, 1);
lean_inc(x_21);
lean_inc(x_20);
lean_dec(x_1);
lean_inc(x_3);
x_22 = l_ML_Pattern_renameEvar___rarg(x_20, x_2, x_3, x_4);
x_23 = l_ML_Pattern_renameEvar___rarg(x_21, x_2, x_3, x_4);
x_24 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_24, 0, x_22);
lean_ctor_set(x_24, 1, x_23);
return x_24;
}
}
case 5:
{
uint8_t x_25; 
x_25 = !lean_is_exclusive(x_1);
if (x_25 == 0)
{
lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; 
x_26 = lean_ctor_get(x_1, 0);
x_27 = lean_ctor_get(x_1, 1);
lean_inc(x_3);
x_28 = l_ML_Pattern_renameEvar___rarg(x_26, x_2, x_3, x_4);
x_29 = l_ML_Pattern_renameEvar___rarg(x_27, x_2, x_3, x_4);
lean_ctor_set(x_1, 1, x_29);
lean_ctor_set(x_1, 0, x_28);
return x_1;
}
else
{
lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; 
x_30 = lean_ctor_get(x_1, 0);
x_31 = lean_ctor_get(x_1, 1);
lean_inc(x_31);
lean_inc(x_30);
lean_dec(x_1);
lean_inc(x_3);
x_32 = l_ML_Pattern_renameEvar___rarg(x_30, x_2, x_3, x_4);
x_33 = l_ML_Pattern_renameEvar___rarg(x_31, x_2, x_3, x_4);
x_34 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_34, 0, x_32);
lean_ctor_set(x_34, 1, x_33);
return x_34;
}
}
case 6:
{
uint8_t x_35; 
x_35 = !lean_is_exclusive(x_1);
if (x_35 == 0)
{
lean_object* x_36; lean_object* x_37; uint8_t x_38; 
x_36 = lean_ctor_get(x_1, 0);
x_37 = lean_ctor_get(x_1, 1);
x_38 = lean_nat_dec_eq(x_2, x_36);
if (x_38 == 0)
{
lean_object* x_39; 
x_39 = l_ML_Pattern_renameEvar___rarg(x_37, x_2, x_3, x_4);
lean_ctor_set(x_1, 1, x_39);
return x_1;
}
else
{
uint8_t x_40; lean_object* x_41; 
lean_dec(x_36);
x_40 = 1;
lean_inc(x_3);
x_41 = l_ML_Pattern_renameEvar___rarg(x_37, x_2, x_3, x_40);
lean_ctor_set(x_1, 1, x_41);
lean_ctor_set(x_1, 0, x_3);
return x_1;
}
}
else
{
lean_object* x_42; lean_object* x_43; uint8_t x_44; 
x_42 = lean_ctor_get(x_1, 0);
x_43 = lean_ctor_get(x_1, 1);
lean_inc(x_43);
lean_inc(x_42);
lean_dec(x_1);
x_44 = lean_nat_dec_eq(x_2, x_42);
if (x_44 == 0)
{
lean_object* x_45; lean_object* x_46; 
x_45 = l_ML_Pattern_renameEvar___rarg(x_43, x_2, x_3, x_4);
x_46 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_46, 0, x_42);
lean_ctor_set(x_46, 1, x_45);
return x_46;
}
else
{
uint8_t x_47; lean_object* x_48; lean_object* x_49; 
lean_dec(x_42);
x_47 = 1;
lean_inc(x_3);
x_48 = l_ML_Pattern_renameEvar___rarg(x_43, x_2, x_3, x_47);
x_49 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_49, 0, x_3);
lean_ctor_set(x_49, 1, x_48);
return x_49;
}
}
}
case 7:
{
uint8_t x_50; 
x_50 = !lean_is_exclusive(x_1);
if (x_50 == 0)
{
lean_object* x_51; lean_object* x_52; 
x_51 = lean_ctor_get(x_1, 1);
x_52 = l_ML_Pattern_renameEvar___rarg(x_51, x_2, x_3, x_4);
lean_ctor_set(x_1, 1, x_52);
return x_1;
}
else
{
lean_object* x_53; lean_object* x_54; lean_object* x_55; lean_object* x_56; 
x_53 = lean_ctor_get(x_1, 0);
x_54 = lean_ctor_get(x_1, 1);
lean_inc(x_54);
lean_inc(x_53);
lean_dec(x_1);
x_55 = l_ML_Pattern_renameEvar___rarg(x_54, x_2, x_3, x_4);
x_56 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_56, 0, x_53);
lean_ctor_set(x_56, 1, x_55);
return x_56;
}
}
default: 
{
lean_dec(x_3);
return x_1;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_renameEvar(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Pattern_renameEvar___rarg___boxed), 4, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_renameEvar___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
uint8_t x_5; lean_object* x_6; 
x_5 = lean_unbox(x_4);
lean_dec(x_4);
x_6 = l_ML_Pattern_renameEvar___rarg(x_1, x_2, x_3, x_5);
lean_dec(x_2);
return x_6;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_renameEvar_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 0:
{
lean_object* x_8; lean_object* x_9; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_8 = lean_ctor_get(x_1, 0);
lean_inc(x_8);
lean_dec(x_1);
x_9 = lean_apply_1(x_2, x_8);
return x_9;
}
case 4:
{
lean_object* x_10; lean_object* x_11; lean_object* x_12; 
lean_dec(x_7);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_10 = lean_ctor_get(x_1, 0);
lean_inc(x_10);
x_11 = lean_ctor_get(x_1, 1);
lean_inc(x_11);
lean_dec(x_1);
x_12 = lean_apply_2(x_6, x_10, x_11);
return x_12;
}
case 5:
{
lean_object* x_13; lean_object* x_14; lean_object* x_15; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_13 = lean_ctor_get(x_1, 0);
lean_inc(x_13);
x_14 = lean_ctor_get(x_1, 1);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_2(x_5, x_13, x_14);
return x_15;
}
case 6:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_3, x_16, x_17);
return x_18;
}
case 7:
{
lean_object* x_19; lean_object* x_20; lean_object* x_21; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_ctor_get(x_1, 0);
lean_inc(x_19);
x_20 = lean_ctor_get(x_1, 1);
lean_inc(x_20);
lean_dec(x_1);
x_21 = lean_apply_2(x_4, x_19, x_20);
return x_21;
}
default: 
{
lean_object* x_22; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_22 = lean_apply_6(x_7, x_1, lean_box(0), lean_box(0), lean_box(0), lean_box(0), lean_box(0));
return x_22;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_renameEvar_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Substitution_0__ML_Pattern_renameEvar_match__1_splitter___rarg), 7, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_renameEvar_x27___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 4:
{
uint8_t x_4; 
x_4 = !lean_is_exclusive(x_1);
if (x_4 == 0)
{
lean_object* x_5; lean_object* x_6; uint8_t x_7; lean_object* x_8; lean_object* x_9; 
x_5 = lean_ctor_get(x_1, 0);
x_6 = lean_ctor_get(x_1, 1);
x_7 = 0;
lean_inc(x_3);
x_8 = l_ML_Pattern_renameEvar___rarg(x_5, x_2, x_3, x_7);
x_9 = l_ML_Pattern_renameEvar___rarg(x_6, x_2, x_3, x_7);
lean_ctor_set(x_1, 1, x_9);
lean_ctor_set(x_1, 0, x_8);
return x_1;
}
else
{
lean_object* x_10; lean_object* x_11; uint8_t x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; 
x_10 = lean_ctor_get(x_1, 0);
x_11 = lean_ctor_get(x_1, 1);
lean_inc(x_11);
lean_inc(x_10);
lean_dec(x_1);
x_12 = 0;
lean_inc(x_3);
x_13 = l_ML_Pattern_renameEvar___rarg(x_10, x_2, x_3, x_12);
x_14 = l_ML_Pattern_renameEvar___rarg(x_11, x_2, x_3, x_12);
x_15 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_15, 0, x_13);
lean_ctor_set(x_15, 1, x_14);
return x_15;
}
}
case 5:
{
uint8_t x_16; 
x_16 = !lean_is_exclusive(x_1);
if (x_16 == 0)
{
lean_object* x_17; lean_object* x_18; uint8_t x_19; lean_object* x_20; lean_object* x_21; 
x_17 = lean_ctor_get(x_1, 0);
x_18 = lean_ctor_get(x_1, 1);
x_19 = 0;
lean_inc(x_3);
x_20 = l_ML_Pattern_renameEvar___rarg(x_17, x_2, x_3, x_19);
x_21 = l_ML_Pattern_renameEvar___rarg(x_18, x_2, x_3, x_19);
lean_ctor_set(x_1, 1, x_21);
lean_ctor_set(x_1, 0, x_20);
return x_1;
}
else
{
lean_object* x_22; lean_object* x_23; uint8_t x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; 
x_22 = lean_ctor_get(x_1, 0);
x_23 = lean_ctor_get(x_1, 1);
lean_inc(x_23);
lean_inc(x_22);
lean_dec(x_1);
x_24 = 0;
lean_inc(x_3);
x_25 = l_ML_Pattern_renameEvar___rarg(x_22, x_2, x_3, x_24);
x_26 = l_ML_Pattern_renameEvar___rarg(x_23, x_2, x_3, x_24);
x_27 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_27, 0, x_25);
lean_ctor_set(x_27, 1, x_26);
return x_27;
}
}
case 6:
{
uint8_t x_28; 
x_28 = !lean_is_exclusive(x_1);
if (x_28 == 0)
{
lean_object* x_29; lean_object* x_30; uint8_t x_31; 
x_29 = lean_ctor_get(x_1, 0);
x_30 = lean_ctor_get(x_1, 1);
x_31 = lean_nat_dec_eq(x_2, x_29);
if (x_31 == 0)
{
lean_object* x_32; 
x_32 = l_ML_Pattern_renameEvar_x27___rarg(x_30, x_2, x_3);
lean_ctor_set(x_1, 1, x_32);
return x_1;
}
else
{
lean_object* x_33; lean_object* x_34; lean_object* x_35; 
lean_dec(x_29);
lean_inc(x_3);
x_33 = l_ML_Pattern_renameEvar_x27___rarg(x_30, x_2, x_3);
lean_inc(x_3);
x_34 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_34, 0, x_3);
x_35 = l_ML_Pattern_substEvar___rarg(x_33, x_2, x_34);
lean_dec(x_34);
lean_ctor_set(x_1, 1, x_35);
lean_ctor_set(x_1, 0, x_3);
return x_1;
}
}
else
{
lean_object* x_36; lean_object* x_37; uint8_t x_38; 
x_36 = lean_ctor_get(x_1, 0);
x_37 = lean_ctor_get(x_1, 1);
lean_inc(x_37);
lean_inc(x_36);
lean_dec(x_1);
x_38 = lean_nat_dec_eq(x_2, x_36);
if (x_38 == 0)
{
lean_object* x_39; lean_object* x_40; 
x_39 = l_ML_Pattern_renameEvar_x27___rarg(x_37, x_2, x_3);
x_40 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_40, 0, x_36);
lean_ctor_set(x_40, 1, x_39);
return x_40;
}
else
{
lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; 
lean_dec(x_36);
lean_inc(x_3);
x_41 = l_ML_Pattern_renameEvar_x27___rarg(x_37, x_2, x_3);
lean_inc(x_3);
x_42 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_42, 0, x_3);
x_43 = l_ML_Pattern_substEvar___rarg(x_41, x_2, x_42);
lean_dec(x_42);
x_44 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_44, 0, x_3);
lean_ctor_set(x_44, 1, x_43);
return x_44;
}
}
}
case 7:
{
uint8_t x_45; 
x_45 = !lean_is_exclusive(x_1);
if (x_45 == 0)
{
lean_object* x_46; uint8_t x_47; lean_object* x_48; 
x_46 = lean_ctor_get(x_1, 1);
x_47 = 0;
x_48 = l_ML_Pattern_renameEvar___rarg(x_46, x_2, x_3, x_47);
lean_ctor_set(x_1, 1, x_48);
return x_1;
}
else
{
lean_object* x_49; lean_object* x_50; uint8_t x_51; lean_object* x_52; lean_object* x_53; 
x_49 = lean_ctor_get(x_1, 0);
x_50 = lean_ctor_get(x_1, 1);
lean_inc(x_50);
lean_inc(x_49);
lean_dec(x_1);
x_51 = 0;
x_52 = l_ML_Pattern_renameEvar___rarg(x_50, x_2, x_3, x_51);
x_53 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_53, 0, x_49);
lean_ctor_set(x_53, 1, x_52);
return x_53;
}
}
default: 
{
lean_dec(x_3);
return x_1;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_renameEvar_x27(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Pattern_renameEvar_x27___rarg___boxed), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_renameEvar_x27___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML_Pattern_renameEvar_x27___rarg(x_1, x_2, x_3);
lean_dec(x_2);
return x_4;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_renameEvar_x27_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 4:
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; 
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_7 = lean_ctor_get(x_1, 0);
lean_inc(x_7);
x_8 = lean_ctor_get(x_1, 1);
lean_inc(x_8);
lean_dec(x_1);
x_9 = lean_apply_2(x_5, x_7, x_8);
return x_9;
}
case 5:
{
lean_object* x_10; lean_object* x_11; lean_object* x_12; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_10 = lean_ctor_get(x_1, 0);
lean_inc(x_10);
x_11 = lean_ctor_get(x_1, 1);
lean_inc(x_11);
lean_dec(x_1);
x_12 = lean_apply_2(x_4, x_10, x_11);
return x_12;
}
case 6:
{
lean_object* x_13; lean_object* x_14; lean_object* x_15; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_13 = lean_ctor_get(x_1, 0);
lean_inc(x_13);
x_14 = lean_ctor_get(x_1, 1);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_2(x_2, x_13, x_14);
return x_15;
}
case 7:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_3, x_16, x_17);
return x_18;
}
default: 
{
lean_object* x_19; 
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_apply_5(x_6, x_1, lean_box(0), lean_box(0), lean_box(0), lean_box(0));
return x_19;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_renameEvar_x27_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Substitution_0__ML_Pattern_renameEvar_x27_match__1_splitter___rarg), 6, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_substSvar___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 1:
{
uint8_t x_4; 
x_4 = !lean_is_exclusive(x_1);
if (x_4 == 0)
{
lean_object* x_5; uint8_t x_6; 
x_5 = lean_ctor_get(x_1, 0);
x_6 = lean_nat_dec_eq(x_2, x_5);
if (x_6 == 0)
{
return x_1;
}
else
{
lean_free_object(x_1);
lean_dec(x_5);
lean_inc(x_3);
return x_3;
}
}
else
{
lean_object* x_7; uint8_t x_8; 
x_7 = lean_ctor_get(x_1, 0);
lean_inc(x_7);
lean_dec(x_1);
x_8 = lean_nat_dec_eq(x_2, x_7);
if (x_8 == 0)
{
lean_object* x_9; 
x_9 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_9, 0, x_7);
return x_9;
}
else
{
lean_dec(x_7);
lean_inc(x_3);
return x_3;
}
}
}
case 2:
{
lean_object* x_10; 
x_10 = lean_box(2);
return x_10;
}
case 4:
{
uint8_t x_11; 
x_11 = !lean_is_exclusive(x_1);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; 
x_12 = lean_ctor_get(x_1, 0);
x_13 = lean_ctor_get(x_1, 1);
x_14 = l_ML_Pattern_substSvar___rarg(x_12, x_2, x_3);
x_15 = l_ML_Pattern_substSvar___rarg(x_13, x_2, x_3);
lean_ctor_set(x_1, 1, x_15);
lean_ctor_set(x_1, 0, x_14);
return x_1;
}
else
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; 
x_16 = lean_ctor_get(x_1, 0);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_inc(x_16);
lean_dec(x_1);
x_18 = l_ML_Pattern_substSvar___rarg(x_16, x_2, x_3);
x_19 = l_ML_Pattern_substSvar___rarg(x_17, x_2, x_3);
x_20 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_20, 0, x_18);
lean_ctor_set(x_20, 1, x_19);
return x_20;
}
}
case 5:
{
uint8_t x_21; 
x_21 = !lean_is_exclusive(x_1);
if (x_21 == 0)
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; 
x_22 = lean_ctor_get(x_1, 0);
x_23 = lean_ctor_get(x_1, 1);
x_24 = l_ML_Pattern_substSvar___rarg(x_22, x_2, x_3);
x_25 = l_ML_Pattern_substSvar___rarg(x_23, x_2, x_3);
lean_ctor_set(x_1, 1, x_25);
lean_ctor_set(x_1, 0, x_24);
return x_1;
}
else
{
lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; 
x_26 = lean_ctor_get(x_1, 0);
x_27 = lean_ctor_get(x_1, 1);
lean_inc(x_27);
lean_inc(x_26);
lean_dec(x_1);
x_28 = l_ML_Pattern_substSvar___rarg(x_26, x_2, x_3);
x_29 = l_ML_Pattern_substSvar___rarg(x_27, x_2, x_3);
x_30 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_30, 0, x_28);
lean_ctor_set(x_30, 1, x_29);
return x_30;
}
}
case 6:
{
uint8_t x_31; 
x_31 = !lean_is_exclusive(x_1);
if (x_31 == 0)
{
lean_object* x_32; lean_object* x_33; 
x_32 = lean_ctor_get(x_1, 1);
x_33 = l_ML_Pattern_substSvar___rarg(x_32, x_2, x_3);
lean_ctor_set(x_1, 1, x_33);
return x_1;
}
else
{
lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; 
x_34 = lean_ctor_get(x_1, 0);
x_35 = lean_ctor_get(x_1, 1);
lean_inc(x_35);
lean_inc(x_34);
lean_dec(x_1);
x_36 = l_ML_Pattern_substSvar___rarg(x_35, x_2, x_3);
x_37 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_37, 0, x_34);
lean_ctor_set(x_37, 1, x_36);
return x_37;
}
}
case 7:
{
uint8_t x_38; 
x_38 = !lean_is_exclusive(x_1);
if (x_38 == 0)
{
lean_object* x_39; lean_object* x_40; uint8_t x_41; 
x_39 = lean_ctor_get(x_1, 0);
x_40 = lean_ctor_get(x_1, 1);
x_41 = lean_nat_dec_eq(x_2, x_39);
if (x_41 == 0)
{
lean_object* x_42; 
x_42 = l_ML_Pattern_substSvar___rarg(x_40, x_2, x_3);
lean_ctor_set(x_1, 1, x_42);
return x_1;
}
else
{
return x_1;
}
}
else
{
lean_object* x_43; lean_object* x_44; uint8_t x_45; 
x_43 = lean_ctor_get(x_1, 0);
x_44 = lean_ctor_get(x_1, 1);
lean_inc(x_44);
lean_inc(x_43);
lean_dec(x_1);
x_45 = lean_nat_dec_eq(x_2, x_43);
if (x_45 == 0)
{
lean_object* x_46; lean_object* x_47; 
x_46 = l_ML_Pattern_substSvar___rarg(x_44, x_2, x_3);
x_47 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_47, 0, x_43);
lean_ctor_set(x_47, 1, x_46);
return x_47;
}
else
{
lean_object* x_48; 
x_48 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_48, 0, x_43);
lean_ctor_set(x_48, 1, x_44);
return x_48;
}
}
}
default: 
{
return x_1;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_substSvar(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Pattern_substSvar___rarg___boxed), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_substSvar___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML_Pattern_substSvar___rarg(x_1, x_2, x_3);
lean_dec(x_3);
lean_dec(x_2);
return x_4;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substSvar_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8, lean_object* x_9) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 0:
{
lean_object* x_10; lean_object* x_11; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_10 = lean_ctor_get(x_1, 0);
lean_inc(x_10);
lean_dec(x_1);
x_11 = lean_apply_1(x_3, x_10);
return x_11;
}
case 1:
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_12 = lean_ctor_get(x_1, 0);
lean_inc(x_12);
lean_dec(x_1);
x_13 = lean_apply_1(x_2, x_12);
return x_13;
}
case 2:
{
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
lean_inc(x_9);
return x_9;
}
case 3:
{
lean_object* x_14; lean_object* x_15; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_14 = lean_ctor_get(x_1, 0);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_1(x_8, x_14);
return x_15;
}
case 4:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_6, x_16, x_17);
return x_18;
}
case 5:
{
lean_object* x_19; lean_object* x_20; lean_object* x_21; 
lean_dec(x_8);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_ctor_get(x_1, 0);
lean_inc(x_19);
x_20 = lean_ctor_get(x_1, 1);
lean_inc(x_20);
lean_dec(x_1);
x_21 = lean_apply_2(x_7, x_19, x_20);
return x_21;
}
case 6:
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_22 = lean_ctor_get(x_1, 0);
lean_inc(x_22);
x_23 = lean_ctor_get(x_1, 1);
lean_inc(x_23);
lean_dec(x_1);
x_24 = lean_apply_2(x_5, x_22, x_23);
return x_24;
}
default: 
{
lean_object* x_25; lean_object* x_26; lean_object* x_27; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_25 = lean_ctor_get(x_1, 0);
lean_inc(x_25);
x_26 = lean_ctor_get(x_1, 1);
lean_inc(x_26);
lean_dec(x_1);
x_27 = lean_apply_2(x_4, x_25, x_26);
return x_27;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substSvar_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Substitution_0__ML_Pattern_substSvar_match__1_splitter___rarg___boxed), 9, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substSvar_match__1_splitter___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8, lean_object* x_9) {
_start:
{
lean_object* x_10; 
x_10 = l___private_MatchingLogic_Substitution_0__ML_Pattern_substSvar_match__1_splitter___rarg(x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8, x_9);
lean_dec(x_9);
return x_10;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_[_⇐_]ˢ", 14);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__1;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__2;
x_3 = l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__1;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("]ˢ", 3);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__16;
x_3 = l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__4;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__2;
x_2 = lean_unsigned_to_nat(1022u);
x_3 = lean_unsigned_to_nat(0u);
x_4 = l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__5;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__6;
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("substSvar", 9);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__1;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__2;
x_3 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__1;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__4;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__4;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__6;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__5;
x_2 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; uint8_t x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
x_12 = lean_unsigned_to_nat(4u);
x_13 = l_Lean_Syntax_getArg(x_1, x_12);
lean_dec(x_1);
x_14 = lean_ctor_get(x_2, 5);
lean_inc(x_14);
x_15 = 0;
x_16 = l_Lean_SourceInfo_fromRef(x_14, x_15);
x_17 = lean_ctor_get(x_2, 2);
lean_inc(x_17);
x_18 = lean_ctor_get(x_2, 1);
lean_inc(x_18);
lean_dec(x_2);
x_19 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__3;
x_20 = l_Lean_addMacroScope(x_18, x_19, x_17);
x_21 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__2;
x_22 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__8;
lean_inc(x_16);
x_23 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_23, 0, x_16);
lean_ctor_set(x_23, 1, x_21);
lean_ctor_set(x_23, 2, x_20);
lean_ctor_set(x_23, 3, x_22);
x_24 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
lean_inc(x_16);
x_25 = l_Lean_Syntax_node3(x_16, x_24, x_9, x_11, x_13);
x_26 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5;
x_27 = l_Lean_Syntax_node2(x_16, x_26, x_23, x_25);
x_28 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_28, 0, x_27);
lean_ctor_set(x_28, 1, x_3);
return x_28;
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__1___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8) {
_start:
{
lean_object* x_9; uint8_t x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; 
x_9 = l_Lean_replaceRef(x_1, x_7);
x_10 = 0;
x_11 = l_Lean_SourceInfo_fromRef(x_9, x_10);
x_12 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7;
lean_inc(x_11);
x_13 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_13, 0, x_11);
lean_ctor_set(x_13, 1, x_12);
x_14 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__13;
lean_inc(x_11);
x_15 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_15, 0, x_11);
lean_ctor_set(x_15, 1, x_14);
x_16 = l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__3;
lean_inc(x_11);
x_17 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_17, 0, x_11);
lean_ctor_set(x_17, 1, x_16);
x_18 = l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__2;
lean_inc(x_11);
x_19 = l_Lean_Syntax_node6(x_11, x_18, x_3, x_13, x_4, x_15, x_5, x_17);
x_20 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2___closed__1;
x_21 = l_Array_append___rarg(x_20, x_6);
x_22 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
lean_inc(x_11);
x_23 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_23, 0, x_11);
lean_ctor_set(x_23, 1, x_22);
lean_ctor_set(x_23, 2, x_21);
x_24 = l_Lean_Syntax_node2(x_11, x_2, x_19, x_23);
x_25 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_25, 0, x_24);
lean_ctor_set(x_25, 1, x_8);
return x_25;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = lean_unsigned_to_nat(3u);
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; uint8_t x_19; 
x_18 = l_Lean_Syntax_getNumArgs(x_15);
x_19 = lean_nat_dec_le(x_16, x_18);
if (x_19 == 0)
{
lean_object* x_20; lean_object* x_21; 
lean_dec(x_18);
lean_dec(x_15);
lean_dec(x_9);
lean_dec(x_2);
x_20 = lean_box(0);
x_21 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_21, 0, x_20);
lean_ctor_set(x_21, 1, x_3);
return x_21;
}
else
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; 
x_22 = l_Lean_Syntax_getArg(x_15, x_8);
x_23 = l_Lean_Syntax_getArg(x_15, x_14);
x_24 = lean_unsigned_to_nat(2u);
x_25 = l_Lean_Syntax_getArg(x_15, x_24);
x_26 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_27 = lean_nat_sub(x_18, x_8);
lean_dec(x_18);
x_28 = l_Array_extract___rarg(x_26, x_16, x_27);
lean_dec(x_27);
lean_dec(x_26);
x_29 = lean_box(2);
x_30 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
x_31 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_31, 0, x_29);
lean_ctor_set(x_31, 1, x_30);
lean_ctor_set(x_31, 2, x_28);
x_32 = l_Lean_Syntax_getArgs(x_31);
lean_dec(x_31);
x_33 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__1___lambda__1(x_9, x_4, x_22, x_23, x_25, x_32, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_33;
}
}
else
{
lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; uint8_t x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; 
x_34 = l_Lean_Syntax_getArg(x_15, x_8);
x_35 = l_Lean_Syntax_getArg(x_15, x_14);
x_36 = lean_unsigned_to_nat(2u);
x_37 = l_Lean_Syntax_getArg(x_15, x_36);
lean_dec(x_15);
x_38 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_39 = 0;
x_40 = l_Lean_SourceInfo_fromRef(x_38, x_39);
x_41 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7;
lean_inc(x_40);
x_42 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_42, 0, x_40);
lean_ctor_set(x_42, 1, x_41);
x_43 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__13;
lean_inc(x_40);
x_44 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_44, 0, x_40);
lean_ctor_set(x_44, 1, x_43);
x_45 = l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__3;
lean_inc(x_40);
x_46 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_46, 0, x_40);
lean_ctor_set(x_46, 1, x_45);
x_47 = l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__2;
x_48 = l_Lean_Syntax_node6(x_40, x_47, x_34, x_42, x_35, x_44, x_37, x_46);
x_49 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_49, 0, x_48);
lean_ctor_set(x_49, 1, x_3);
return x_49;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__1___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8) {
_start:
{
lean_object* x_9; 
x_9 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__1___lambda__1(x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8);
lean_dec(x_7);
lean_dec(x_1);
return x_9;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_[_⇐ˢ_]", 14);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__1;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__2;
x_3 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__1;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(" ⇐ˢ ", 7);
return x_1;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6;
x_2 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__12;
x_3 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__4;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6;
x_2 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__5;
x_3 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__11;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6;
x_2 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__6;
x_3 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__8;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__2;
x_2 = lean_unsigned_to_nat(1022u);
x_3 = lean_unsigned_to_nat(0u);
x_4 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__7;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__8;
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0_u02e2___x5d__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; uint8_t x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
x_12 = lean_unsigned_to_nat(4u);
x_13 = l_Lean_Syntax_getArg(x_1, x_12);
lean_dec(x_1);
x_14 = lean_ctor_get(x_2, 5);
lean_inc(x_14);
x_15 = 0;
x_16 = l_Lean_SourceInfo_fromRef(x_14, x_15);
x_17 = lean_ctor_get(x_2, 2);
lean_inc(x_17);
x_18 = lean_ctor_get(x_2, 1);
lean_inc(x_18);
lean_dec(x_2);
x_19 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__3;
x_20 = l_Lean_addMacroScope(x_18, x_19, x_17);
x_21 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__2;
x_22 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__8;
lean_inc(x_16);
x_23 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_23, 0, x_16);
lean_ctor_set(x_23, 1, x_21);
lean_ctor_set(x_23, 2, x_20);
lean_ctor_set(x_23, 3, x_22);
x_24 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
lean_inc(x_16);
x_25 = l_Lean_Syntax_node3(x_16, x_24, x_9, x_11, x_13);
x_26 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5;
x_27 = l_Lean_Syntax_node2(x_16, x_26, x_23, x_25);
x_28 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_28, 0, x_27);
lean_ctor_set(x_28, 1, x_3);
return x_28;
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__2___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8) {
_start:
{
lean_object* x_9; uint8_t x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; 
x_9 = l_Lean_replaceRef(x_1, x_7);
x_10 = 0;
x_11 = l_Lean_SourceInfo_fromRef(x_9, x_10);
x_12 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7;
lean_inc(x_11);
x_13 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_13, 0, x_11);
lean_ctor_set(x_13, 1, x_12);
x_14 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__3;
lean_inc(x_11);
x_15 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_15, 0, x_11);
lean_ctor_set(x_15, 1, x_14);
x_16 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__7;
lean_inc(x_11);
x_17 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_17, 0, x_11);
lean_ctor_set(x_17, 1, x_16);
x_18 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__2;
lean_inc(x_11);
x_19 = l_Lean_Syntax_node6(x_11, x_18, x_3, x_13, x_4, x_15, x_5, x_17);
x_20 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2___closed__1;
x_21 = l_Array_append___rarg(x_20, x_6);
x_22 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
lean_inc(x_11);
x_23 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_23, 0, x_11);
lean_ctor_set(x_23, 1, x_22);
lean_ctor_set(x_23, 2, x_21);
x_24 = l_Lean_Syntax_node2(x_11, x_2, x_19, x_23);
x_25 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_25, 0, x_24);
lean_ctor_set(x_25, 1, x_8);
return x_25;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__2(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = lean_unsigned_to_nat(3u);
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; uint8_t x_19; 
x_18 = l_Lean_Syntax_getNumArgs(x_15);
x_19 = lean_nat_dec_le(x_16, x_18);
if (x_19 == 0)
{
lean_object* x_20; lean_object* x_21; 
lean_dec(x_18);
lean_dec(x_15);
lean_dec(x_9);
lean_dec(x_2);
x_20 = lean_box(0);
x_21 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_21, 0, x_20);
lean_ctor_set(x_21, 1, x_3);
return x_21;
}
else
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; 
x_22 = l_Lean_Syntax_getArg(x_15, x_8);
x_23 = l_Lean_Syntax_getArg(x_15, x_14);
x_24 = lean_unsigned_to_nat(2u);
x_25 = l_Lean_Syntax_getArg(x_15, x_24);
x_26 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_27 = lean_nat_sub(x_18, x_8);
lean_dec(x_18);
x_28 = l_Array_extract___rarg(x_26, x_16, x_27);
lean_dec(x_27);
lean_dec(x_26);
x_29 = lean_box(2);
x_30 = l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15;
x_31 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_31, 0, x_29);
lean_ctor_set(x_31, 1, x_30);
lean_ctor_set(x_31, 2, x_28);
x_32 = l_Lean_Syntax_getArgs(x_31);
lean_dec(x_31);
x_33 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__2___lambda__1(x_9, x_4, x_22, x_23, x_25, x_32, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_33;
}
}
else
{
lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; uint8_t x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; 
x_34 = l_Lean_Syntax_getArg(x_15, x_8);
x_35 = l_Lean_Syntax_getArg(x_15, x_14);
x_36 = lean_unsigned_to_nat(2u);
x_37 = l_Lean_Syntax_getArg(x_15, x_36);
lean_dec(x_15);
x_38 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_39 = 0;
x_40 = l_Lean_SourceInfo_fromRef(x_38, x_39);
x_41 = l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7;
lean_inc(x_40);
x_42 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_42, 0, x_40);
lean_ctor_set(x_42, 1, x_41);
x_43 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__3;
lean_inc(x_40);
x_44 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_44, 0, x_40);
lean_ctor_set(x_44, 1, x_43);
x_45 = l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__7;
lean_inc(x_40);
x_46 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_46, 0, x_40);
lean_ctor_set(x_46, 1, x_45);
x_47 = l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__2;
x_48 = l_Lean_Syntax_node6(x_40, x_47, x_34, x_42, x_35, x_44, x_37, x_46);
x_49 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_49, 0, x_48);
lean_ctor_set(x_49, 1, x_3);
return x_49;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__2___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8) {
_start:
{
lean_object* x_9; 
x_9 = l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substSvar__2___lambda__1(x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8);
lean_dec(x_7);
lean_dec(x_1);
return x_9;
}
}
static uint8_t _init_l_ML_Pattern_substitutableForSvarIn___rarg___closed__1() {
_start:
{
uint8_t x_1; uint8_t x_2; 
x_1 = 0;
x_2 = l_instDecidableNot___rarg(x_1);
return x_2;
}
}
static uint8_t _init_l_ML_Pattern_substitutableForSvarIn___rarg___closed__2() {
_start:
{
uint8_t x_1; uint8_t x_2; 
x_1 = 1;
x_2 = l_instDecidableNot___rarg(x_1);
return x_2;
}
}
LEAN_EXPORT uint8_t l_ML_Pattern_substitutableForSvarIn___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
switch (lean_obj_tag(x_3)) {
case 4:
{
lean_object* x_4; lean_object* x_5; uint8_t x_6; 
x_4 = lean_ctor_get(x_3, 0);
lean_inc(x_4);
x_5 = lean_ctor_get(x_3, 1);
lean_inc(x_5);
lean_dec(x_3);
x_6 = l_ML_Pattern_substitutableForSvarIn___rarg(x_1, x_2, x_4);
if (x_6 == 0)
{
uint8_t x_7; 
lean_dec(x_5);
x_7 = 0;
return x_7;
}
else
{
x_3 = x_5;
goto _start;
}
}
case 5:
{
lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_9 = lean_ctor_get(x_3, 0);
lean_inc(x_9);
x_10 = lean_ctor_get(x_3, 1);
lean_inc(x_10);
lean_dec(x_3);
x_11 = l_ML_Pattern_substitutableForSvarIn___rarg(x_1, x_2, x_9);
if (x_11 == 0)
{
uint8_t x_12; 
lean_dec(x_10);
x_12 = 0;
return x_12;
}
else
{
x_3 = x_10;
goto _start;
}
}
case 6:
{
uint8_t x_14; 
x_14 = !lean_is_exclusive(x_3);
if (x_14 == 0)
{
lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_15 = lean_ctor_get(x_3, 0);
x_16 = lean_ctor_get(x_3, 1);
lean_inc(x_16);
lean_inc(x_15);
x_17 = l_ML_Pattern_isFreeSvar___rarg(x_3, x_1);
lean_dec(x_3);
if (x_17 == 0)
{
uint8_t x_18; 
lean_dec(x_16);
lean_dec(x_15);
x_18 = 1;
return x_18;
}
else
{
uint8_t x_19; 
x_19 = l_ML_Pattern_isFreeEvar___rarg(x_2, x_15);
lean_dec(x_15);
if (x_19 == 0)
{
uint8_t x_20; 
x_20 = l_ML_Pattern_substitutableForSvarIn___rarg___closed__1;
if (x_20 == 0)
{
uint8_t x_21; 
lean_dec(x_16);
x_21 = 0;
return x_21;
}
else
{
x_3 = x_16;
goto _start;
}
}
else
{
uint8_t x_23; 
x_23 = l_ML_Pattern_substitutableForSvarIn___rarg___closed__2;
if (x_23 == 0)
{
uint8_t x_24; 
lean_dec(x_16);
x_24 = 0;
return x_24;
}
else
{
x_3 = x_16;
goto _start;
}
}
}
}
else
{
lean_object* x_26; lean_object* x_27; lean_object* x_28; uint8_t x_29; 
x_26 = lean_ctor_get(x_3, 0);
x_27 = lean_ctor_get(x_3, 1);
lean_inc(x_27);
lean_inc(x_26);
lean_dec(x_3);
lean_inc(x_27);
lean_inc(x_26);
x_28 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_28, 0, x_26);
lean_ctor_set(x_28, 1, x_27);
x_29 = l_ML_Pattern_isFreeSvar___rarg(x_28, x_1);
lean_dec(x_28);
if (x_29 == 0)
{
uint8_t x_30; 
lean_dec(x_27);
lean_dec(x_26);
x_30 = 1;
return x_30;
}
else
{
uint8_t x_31; 
x_31 = l_ML_Pattern_isFreeEvar___rarg(x_2, x_26);
lean_dec(x_26);
if (x_31 == 0)
{
uint8_t x_32; 
x_32 = l_ML_Pattern_substitutableForSvarIn___rarg___closed__1;
if (x_32 == 0)
{
uint8_t x_33; 
lean_dec(x_27);
x_33 = 0;
return x_33;
}
else
{
x_3 = x_27;
goto _start;
}
}
else
{
uint8_t x_35; 
x_35 = l_ML_Pattern_substitutableForSvarIn___rarg___closed__2;
if (x_35 == 0)
{
uint8_t x_36; 
lean_dec(x_27);
x_36 = 0;
return x_36;
}
else
{
x_3 = x_27;
goto _start;
}
}
}
}
}
case 7:
{
uint8_t x_38; 
x_38 = !lean_is_exclusive(x_3);
if (x_38 == 0)
{
lean_object* x_39; lean_object* x_40; uint8_t x_41; 
x_39 = lean_ctor_get(x_3, 0);
x_40 = lean_ctor_get(x_3, 1);
lean_inc(x_40);
lean_inc(x_39);
x_41 = l_ML_Pattern_isFreeSvar___rarg(x_3, x_1);
lean_dec(x_3);
if (x_41 == 0)
{
uint8_t x_42; 
lean_dec(x_40);
lean_dec(x_39);
x_42 = 1;
return x_42;
}
else
{
uint8_t x_43; 
x_43 = l_ML_Pattern_isFreeSvar___rarg(x_2, x_39);
lean_dec(x_39);
if (x_43 == 0)
{
uint8_t x_44; 
x_44 = l_ML_Pattern_substitutableForSvarIn___rarg___closed__1;
if (x_44 == 0)
{
uint8_t x_45; 
lean_dec(x_40);
x_45 = 0;
return x_45;
}
else
{
x_3 = x_40;
goto _start;
}
}
else
{
uint8_t x_47; 
x_47 = l_ML_Pattern_substitutableForSvarIn___rarg___closed__2;
if (x_47 == 0)
{
uint8_t x_48; 
lean_dec(x_40);
x_48 = 0;
return x_48;
}
else
{
x_3 = x_40;
goto _start;
}
}
}
}
else
{
lean_object* x_50; lean_object* x_51; lean_object* x_52; uint8_t x_53; 
x_50 = lean_ctor_get(x_3, 0);
x_51 = lean_ctor_get(x_3, 1);
lean_inc(x_51);
lean_inc(x_50);
lean_dec(x_3);
lean_inc(x_51);
lean_inc(x_50);
x_52 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_52, 0, x_50);
lean_ctor_set(x_52, 1, x_51);
x_53 = l_ML_Pattern_isFreeSvar___rarg(x_52, x_1);
lean_dec(x_52);
if (x_53 == 0)
{
uint8_t x_54; 
lean_dec(x_51);
lean_dec(x_50);
x_54 = 1;
return x_54;
}
else
{
uint8_t x_55; 
x_55 = l_ML_Pattern_isFreeSvar___rarg(x_2, x_50);
lean_dec(x_50);
if (x_55 == 0)
{
uint8_t x_56; 
x_56 = l_ML_Pattern_substitutableForSvarIn___rarg___closed__1;
if (x_56 == 0)
{
uint8_t x_57; 
lean_dec(x_51);
x_57 = 0;
return x_57;
}
else
{
x_3 = x_51;
goto _start;
}
}
else
{
uint8_t x_59; 
x_59 = l_ML_Pattern_substitutableForSvarIn___rarg___closed__2;
if (x_59 == 0)
{
uint8_t x_60; 
lean_dec(x_51);
x_60 = 0;
return x_60;
}
else
{
x_3 = x_51;
goto _start;
}
}
}
}
}
default: 
{
uint8_t x_62; 
lean_dec(x_3);
x_62 = 1;
return x_62;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_substitutableForSvarIn(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_Pattern_substitutableForSvarIn___rarg___boxed), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Pattern_substitutableForSvarIn___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
uint8_t x_4; lean_object* x_5; 
x_4 = l_ML_Pattern_substitutableForSvarIn___rarg(x_1, x_2, x_3);
lean_dec(x_2);
lean_dec(x_1);
x_5 = lean_box(x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substitutableForSvarIn_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 4:
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_7 = lean_ctor_get(x_1, 0);
lean_inc(x_7);
x_8 = lean_ctor_get(x_1, 1);
lean_inc(x_8);
lean_dec(x_1);
x_9 = lean_apply_2(x_4, x_7, x_8);
return x_9;
}
case 5:
{
lean_object* x_10; lean_object* x_11; lean_object* x_12; 
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_10 = lean_ctor_get(x_1, 0);
lean_inc(x_10);
x_11 = lean_ctor_get(x_1, 1);
lean_inc(x_11);
lean_dec(x_1);
x_12 = lean_apply_2(x_5, x_10, x_11);
return x_12;
}
case 6:
{
lean_object* x_13; lean_object* x_14; lean_object* x_15; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_13 = lean_ctor_get(x_1, 0);
lean_inc(x_13);
x_14 = lean_ctor_get(x_1, 1);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_2(x_3, x_13, x_14);
return x_15;
}
case 7:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_2, x_16, x_17);
return x_18;
}
default: 
{
lean_object* x_19; 
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_apply_5(x_6, x_1, lean_box(0), lean_box(0), lean_box(0), lean_box(0));
return x_19;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_substitutableForSvarIn_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Substitution_0__ML_Pattern_substitutableForSvarIn_match__1_splitter___rarg), 6, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_isFreeSvar_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7, lean_object* x_8) {
_start:
{
switch (lean_obj_tag(x_1)) {
case 0:
{
lean_object* x_9; lean_object* x_10; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
x_9 = lean_ctor_get(x_1, 0);
lean_inc(x_9);
lean_dec(x_1);
x_10 = lean_apply_1(x_3, x_9);
return x_10;
}
case 1:
{
lean_object* x_11; lean_object* x_12; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
x_11 = lean_ctor_get(x_1, 0);
lean_inc(x_11);
lean_dec(x_1);
x_12 = lean_apply_1(x_2, x_11);
return x_12;
}
case 4:
{
lean_object* x_13; lean_object* x_14; lean_object* x_15; 
lean_dec(x_8);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_13 = lean_ctor_get(x_1, 0);
lean_inc(x_13);
x_14 = lean_ctor_get(x_1, 1);
lean_inc(x_14);
lean_dec(x_1);
x_15 = lean_apply_2(x_7, x_13, x_14);
return x_15;
}
case 5:
{
lean_object* x_16; lean_object* x_17; lean_object* x_18; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_16 = lean_ctor_get(x_1, 0);
lean_inc(x_16);
x_17 = lean_ctor_get(x_1, 1);
lean_inc(x_17);
lean_dec(x_1);
x_18 = lean_apply_2(x_6, x_16, x_17);
return x_18;
}
case 6:
{
lean_object* x_19; lean_object* x_20; lean_object* x_21; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_19 = lean_ctor_get(x_1, 0);
lean_inc(x_19);
x_20 = lean_ctor_get(x_1, 1);
lean_inc(x_20);
lean_dec(x_1);
x_21 = lean_apply_2(x_5, x_19, x_20);
return x_21;
}
case 7:
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; 
lean_dec(x_8);
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_3);
lean_dec(x_2);
x_22 = lean_ctor_get(x_1, 0);
lean_inc(x_22);
x_23 = lean_ctor_get(x_1, 1);
lean_inc(x_23);
lean_dec(x_1);
x_24 = lean_apply_2(x_4, x_22, x_23);
return x_24;
}
default: 
{
lean_object* x_25; 
lean_dec(x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
x_25 = lean_apply_7(x_8, x_1, lean_box(0), lean_box(0), lean_box(0), lean_box(0), lean_box(0), lean_box(0));
return x_25;
}
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Substitution_0__ML_Pattern_isFreeSvar_match__1_splitter(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l___private_MatchingLogic_Substitution_0__ML_Pattern_isFreeSvar_match__1_splitter___rarg), 8, 0);
return x_3;
}
}
lean_object* initialize_Init(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Pattern(uint8_t builtin, lean_object*);
static bool _G_initialized = false;
LEAN_EXPORT lean_object* initialize_MatchingLogic_Substitution(uint8_t builtin, lean_object* w) {
lean_object * res;
if (_G_initialized) return lean_io_result_mk_ok(lean_box(0));
_G_initialized = true;
res = initialize_Init(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Pattern(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__1 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__1();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__1);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__2 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__2();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__2);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__3 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__3();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__3);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__4 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__4();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__4);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__5 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__5();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__5);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__6);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__7);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__8 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__8();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__8);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__9 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__9();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__9);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__10 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__10();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__10);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__11 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__11();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__11);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__12 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__12();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__12);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__13 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__13();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__13);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__14 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__14();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__14);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__15 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__15();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__15);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__16 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__16();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__16);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__17 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__17();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__17);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__18 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__18();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__18);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__19 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__19();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__19);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__20 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__20();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49___closed__20);
l_ML_Pattern_term___x5b___u21d0___x5d_u1d49 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u1d49();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u1d49);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__1 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__1();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__1);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__2 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__2();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__2);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__3 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__3();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__3);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__4 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__4();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__4);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__5);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__6 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__6();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__6);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__7 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__7();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__7);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__8 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__8();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__8);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__9 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__9();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__9);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__10 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__10();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__10);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__11 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__11();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__11);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__12 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__12();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__12);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__13 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__13();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__13);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__14 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__14();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__14);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u1d49__1___closed__15);
l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2___closed__1 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2___closed__1();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___lambda__2___closed__1);
l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__1 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__1();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__1);
l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__2 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__2();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__2);
l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__3 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__3();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______unexpand__ML__Pattern__substEvar__1___closed__3);
l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__1 = _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__1();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__1);
l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__2 = _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__2();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__2);
l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__3 = _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__3();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__3);
l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__4 = _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__4();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__4);
l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__5 = _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__5();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__5);
l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__6 = _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__6();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__6);
l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__7 = _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__7();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__7);
l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__8 = _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__8();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__8);
l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__9 = _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__9();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__9);
l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__10 = _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__10();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u1d49___x5d___closed__10);
l_ML_Pattern_term___x5b___u21d0_u1d49___x5d = _init_l_ML_Pattern_term___x5b___u21d0_u1d49___x5d();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u1d49___x5d);
l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__1 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__1();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__1);
l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__2 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__2();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__2);
l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__3 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__3();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__3);
l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__4 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__4();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__4);
l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__5 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__5();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__5);
l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__6 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__6();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u02e2___closed__6);
l_ML_Pattern_term___x5b___u21d0___x5d_u02e2 = _init_l_ML_Pattern_term___x5b___u21d0___x5d_u02e2();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0___x5d_u02e2);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__1 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__1();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__1);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__2 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__2();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__2);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__3 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__3();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__3);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__4 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__4();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__4);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__5 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__5();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__5);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__6 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__6();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__6);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__7 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__7();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__7);
l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__8 = _init_l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__8();
lean_mark_persistent(l_ML_Pattern___aux__MatchingLogic__Substitution______macroRules__ML__Pattern__term___x5b___u21d0___x5d_u02e2__1___closed__8);
l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__1 = _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__1();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__1);
l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__2 = _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__2();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__2);
l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__3 = _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__3();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__3);
l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__4 = _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__4();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__4);
l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__5 = _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__5();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__5);
l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__6 = _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__6();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__6);
l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__7 = _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__7();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__7);
l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__8 = _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__8();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u02e2___x5d___closed__8);
l_ML_Pattern_term___x5b___u21d0_u02e2___x5d = _init_l_ML_Pattern_term___x5b___u21d0_u02e2___x5d();
lean_mark_persistent(l_ML_Pattern_term___x5b___u21d0_u02e2___x5d);
l_ML_Pattern_substitutableForSvarIn___rarg___closed__1 = _init_l_ML_Pattern_substitutableForSvarIn___rarg___closed__1();
l_ML_Pattern_substitutableForSvarIn___rarg___closed__2 = _init_l_ML_Pattern_substitutableForSvarIn___rarg___closed__2();
return lean_io_result_mk_ok(lean_box(0));
}
#ifdef __cplusplus
}
#endif
