// Lean compiler output
// Module: MatchingLogic.Definedness
// Imports: Init MatchingLogic.Pattern MatchingLogic.Proof
#include <lean/lean.h>
#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wunused-label"
#elif defined(__GNUC__) && !defined(__CLANG__)
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-label"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#endif
#ifdef __cplusplus
extern "C" {
#endif
static lean_object* l_ML_term_u2308___u2309___closed__14;
LEAN_EXPORT lean_object* l_ML_equalSelf___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1___lambda__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__3;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__6;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
static lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__3;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__5;
lean_object* lean_mk_empty_array_with_capacity(lean_object*);
LEAN_EXPORT lean_object* l_ML_ctxImplDefined___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__1;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__2;
uint8_t l_Lean_Syntax_matchesIdent(lean_object*, lean_object*);
lean_object* l___private_Lean_Expr_0__Lean_Expr_getAppNumArgsAux(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__17;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1;
static lean_object* l_ML_term_u230a___u230b___closed__7;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__2;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1;
static lean_object* l_ML_term_u2308___u2309___closed__1;
static lean_object* l_ML_delabDefined___closed__3;
lean_object* l_Lean_PrettyPrinter_Delaborator_SubExpr_getExpr___at_Lean_PrettyPrinter_Delaborator_getExprKind___spec__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__23;
lean_object* l_ML_Proof_conjIntroRule___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u230a___u230b___closed__4;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28;
static lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___closed__6;
static lean_object* l_ML_term___u2286_u2286_____closed__4;
static lean_object* l_ML_term_u2308___u2309___closed__15;
LEAN_EXPORT lean_object* l_ML_member___rarg(lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Proof_negConjAsImpl___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__2;
static lean_object* l_ML_term___u2286_u2286_____closed__6;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__3;
lean_object* l_ML_Proof_weakeningDisj___rarg(lean_object*, lean_object*);
lean_object* l_ML_AppContext_evars___rarg(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__3;
lean_object* l_ML_Proof_doubleNegCtx___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__12;
static lean_object* l_ML_term___u2208_u2208_____closed__1;
static lean_object* l_ML_term_u2308___u2309___closed__10;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__6;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4;
lean_object* l_Lean_Syntax_getArgs(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__8;
lean_object* l_Lean_replaceRef(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__2;
LEAN_EXPORT lean_object* l_ML_definedness_x27___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT uint8_t l_ML_isApplicationOfDefinedSymbolToPattern(lean_object*);
static lean_object* l_ML_term___u2261_____closed__6;
lean_object* l_ML_Proof_extraPremise___rarg(lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Proof_univQuan___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__8;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__3;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
LEAN_EXPORT lean_object* l_ML_definedness(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__7;
static lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2___closed__1;
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion___boxed(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u230a___u230b___closed__2;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__6;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__19;
uint8_t l_Lean_Syntax_isOfKind(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__1___boxed(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u2286_u2286_____closed__1;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__2;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1(lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Proof_equivSelf___rarg(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3;
LEAN_EXPORT lean_object* l_ML_instHasDefinedDefinedness;
static lean_object* l_ML_term___u2261_____closed__1;
lean_object* l_ML_Pattern_negation___rarg(lean_object*);
static lean_object* l_ML_delabDefined___closed__6;
lean_object* l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppArg___at_Lean_PrettyPrinter_Delaborator_delabLetFun___spec__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Syntax_getNumArgs(lean_object*);
static lean_object* l_ML_term_u230a___u230b___closed__3;
static lean_object* l_ML_delabDefined___closed__4;
static lean_object* l_ML_delabDefined___closed__2;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_definedness_x27(lean_object*);
LEAN_EXPORT lean_object* l_ML_equalSelf(lean_object*);
lean_object* l_Lean_Name_mkStr3(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1___lambda__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_List_appendTR___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_equivImplEqual___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__31;
lean_object* l_ML_Proof_implSelf___rarg(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15;
lean_object* l_ML_Proof_syllogism___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_delabDefined___closed__7;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__7;
lean_object* l_ML_Pattern_universal___rarg(lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__12;
LEAN_EXPORT lean_object* l_ML_ctxImplDefinedAux1___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1___lambda__1___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_term___u2286_u2286__;
static lean_object* l_ML_term_u2308___u2309___closed__9;
lean_object* l_Lean_SourceInfo_fromRef(lean_object*, uint8_t);
static lean_object* l_ML_delabDefined___closed__1;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27;
LEAN_EXPORT lean_object* l_ML_implDefined___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32;
lean_object* l_ML_Proof_propagationExist___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_auxTauto(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1___lambda__1___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___closed__3;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__10;
static lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___closed__4;
static lean_object* l_ML_term_u2308___u2309___closed__8;
static lean_object* l_ML_term_u230a___u230b___closed__1;
static lean_object* l_ML_term_u2308___u2309___closed__6;
LEAN_EXPORT lean_object* l_ML_ctxImplDefined___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u2208_u2208_____closed__2;
LEAN_EXPORT lean_object* l_ML_auxTauto___rarg(lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__13;
lean_object* l_ML_Pattern_equivalence___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1___lambda__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion___rarg___boxed(lean_object*);
LEAN_EXPORT lean_object* l_ML_term_u2308___u2309;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16;
lean_object* l_ML_Proof_framing___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__11;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1;
lean_object* l_Lean_Syntax_node3(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_PrettyPrinter_Delaborator_delab(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_ML_AppContext_insert___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_equivImplEqual(lean_object*);
lean_object* l_Lean_addMacroScope(lean_object*, lean_object*, lean_object*);
lean_object* l_ML_EVar_getFresh(lean_object*);
LEAN_EXPORT lean_object* l_ML_totalImpl___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u230a___u230b___closed__5;
lean_object* l_Lean_Name_str___override(lean_object*, lean_object*);
lean_object* l_ML_Pattern_evars___rarg(lean_object*);
lean_object* l_Lean_Syntax_node2(lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Syntax_getArg(lean_object*, lean_object*);
uint8_t l_Lean_Syntax_matchesNull(lean_object*, lean_object*);
static lean_object* l_ML_term___u2261_____closed__5;
LEAN_EXPORT lean_object* l_ML_memberIntro___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_included___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_memberIntro(lean_object*);
lean_object* l_ML_Proof_doubleNegElim___rarg(lean_object*);
static lean_object* l_ML_term___u2286_u2286_____closed__2;
static lean_object* l_ML_term___u2261_____closed__4;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__8;
static lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__24;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14;
LEAN_EXPORT lean_object* l_ML_definedness___rarg(lean_object*, lean_object*);
lean_object* l_Array_append___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_implDefined___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u2208_u2208_____closed__6;
LEAN_EXPORT lean_object* l_ML_equal(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__9;
LEAN_EXPORT lean_object* l_ML_equal___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4;
static lean_object* l_ML_term_u2308___u2309___closed__7;
lean_object* l_Array_extract___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__6;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4;
LEAN_EXPORT lean_object* l_ML_AppContext_CtxDefined___rarg(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__5;
LEAN_EXPORT lean_object* l_ML_term___u2208_u2208__;
LEAN_EXPORT lean_object* l_ML_ctxImplDefinedAux1___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u2286_u2286_____closed__5;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5;
LEAN_EXPORT lean_object* l_ML_ctxImplDefinedAux1(lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__4;
LEAN_EXPORT lean_object* l_ML_definedness_x27___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Definedness_toCtorIdx___boxed(lean_object*);
uint8_t lean_nat_dec_eq(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_term___u2261__;
lean_object* l_ML_Proof_univGeneralization___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1___lambda__1___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_total(lean_object*);
lean_object* l_Lean_PrettyPrinter_Delaborator_failure___rarg(lean_object*);
static lean_object* l_ML_term_u230a___u230b___closed__6;
lean_object* l_Lean_Name_mkStr2(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_delabDefined(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_member(lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__3;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__5;
lean_object* l_Lean_Syntax_node1(lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Pattern_conjunction___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_implDefined(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26;
LEAN_EXPORT lean_object* l_ML_AppContext_CtxDefined(lean_object*);
lean_object* l_ML_Proof_propagtionDisjR___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__7;
lean_object* lean_nat_sub(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__21;
LEAN_EXPORT lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___boxed(lean_object*);
lean_object* l_ML_Proof_conjIntro___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__30;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__29;
LEAN_EXPORT lean_object* l_ML_included(lean_object*);
LEAN_EXPORT lean_object* l_ML_Definedness_toCtorIdx(lean_object*);
static lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___closed__1;
static lean_object* l_ML_term_u230a___u230b___closed__9;
static lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___closed__5;
lean_object* l_ML_Proof_negImplIntro___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__5;
lean_object* l_ML_Proof_pushConjInExist___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_ctxImplDefined(lean_object*);
lean_object* l_ML_Pattern_disjunction___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__13;
lean_object* l_Lean_Name_mkStr4(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u2208_u2208_____closed__5;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1;
static lean_object* l_ML_term___u2261_____closed__7;
static lean_object* l_ML_term___u2261_____closed__3;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__8;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__7;
static lean_object* l_ML_term___u2208_u2208_____closed__3;
uint8_t lean_nat_dec_le(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__5;
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion___rarg(lean_object*);
static lean_object* l_ML_term___u2261_____closed__2;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__18;
LEAN_EXPORT lean_object* l_ML_term_u230a___u230b;
static lean_object* l_ML_term___u2208_u2208_____closed__4;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__8;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__7;
static lean_object* l_ML_delabDefined___closed__5;
static lean_object* l_ML_term___u2286_u2286_____closed__3;
lean_object* l_String_toSubstring_x27(lean_object*);
lean_object* l_ML_Proof_propagationDisj___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_totalImpl(lean_object*);
LEAN_EXPORT lean_object* l_ML_totalImpl___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_memberIntro___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u230a___u230b___closed__8;
static lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___closed__2;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_total___rarg(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__20;
lean_object* l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppFn___at_Lean_PrettyPrinter_Delaborator_delabLetFun___spec__2(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__2;
LEAN_EXPORT lean_object* l_ML_Definedness_toCtorIdx(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_unsigned_to_nat(0u);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Definedness_toCtorIdx___boxed(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = l_ML_Definedness_toCtorIdx(x_1);
lean_dec(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion___rarg(lean_object* x_1) {
_start:
{
lean_inc(x_1);
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = lean_alloc_closure((void*)(l_ML_Definedness_noConfusion___rarg___boxed), 1, 0);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion___rarg___boxed(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = l_ML_Definedness_noConfusion___rarg(x_1);
lean_dec(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = l_ML_Definedness_noConfusion(x_1, x_2, x_3, x_4);
lean_dec(x_3);
lean_dec(x_2);
return x_5;
}
}
static lean_object* _init_l_ML_instHasDefinedDefinedness() {
_start:
{
lean_object* x_1; 
x_1 = lean_box(0);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ML", 2);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term⌈_⌉", 11);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML_term_u2308___u2309___closed__2;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("andthen", 7);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_term_u2308___u2309___closed__4;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("⌈", 3);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term_u2308___u2309___closed__6;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__8() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term", 4);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_term_u2308___u2309___closed__8;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__9;
x_2 = lean_unsigned_to_nat(0u);
x_3 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term_u2308___u2309___closed__7;
x_3 = l_ML_term_u2308___u2309___closed__10;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__12() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("⌉", 3);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term_u2308___u2309___closed__12;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__14() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term_u2308___u2309___closed__11;
x_3 = l_ML_term_u2308___u2309___closed__13;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__3;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = l_ML_term_u2308___u2309___closed__14;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u2308___u2309() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_term_u2308___u2309___closed__15;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Pattern", 7);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_⬝_", 9);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1;
x_3 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__2;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Lean", 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Parser", 6);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Term", 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__7() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("paren", 5);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5;
x_3 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6;
x_4 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__7;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__9() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("(", 1);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__10() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("app", 3);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5;
x_3 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6;
x_4 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__10;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__12() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Pattern.symbol", 14);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__12;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("symbol", 6);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1;
x_3 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__17() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__18() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__19() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__18;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__20() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__17;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__19;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__21() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("null", 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__21;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__23() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("HasDefined.defined", 18);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__24() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__23;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("HasDefined", 10);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("defined", 7);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25;
x_3 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__29() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__30() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__29;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__31() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(")", 1);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("⬝", 3);
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_term_u2308___u2309___closed__3;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; 
x_8 = lean_unsigned_to_nat(1u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
lean_dec(x_1);
x_10 = lean_ctor_get(x_2, 5);
lean_inc(x_10);
x_11 = 0;
x_12 = l_Lean_SourceInfo_fromRef(x_10, x_11);
x_13 = lean_ctor_get(x_2, 2);
lean_inc(x_13);
x_14 = lean_ctor_get(x_2, 1);
lean_inc(x_14);
lean_dec(x_2);
x_15 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__9;
lean_inc(x_12);
x_16 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_16, 0, x_12);
lean_ctor_set(x_16, 1, x_15);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15;
lean_inc(x_13);
lean_inc(x_14);
x_18 = l_Lean_addMacroScope(x_14, x_17, x_13);
x_19 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__13;
x_20 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__20;
lean_inc(x_12);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_12);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27;
x_23 = l_Lean_addMacroScope(x_14, x_22, x_13);
x_24 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__24;
x_25 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__30;
lean_inc(x_12);
x_26 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_26, 0, x_12);
lean_ctor_set(x_26, 1, x_24);
lean_ctor_set(x_26, 2, x_23);
lean_ctor_set(x_26, 3, x_25);
x_27 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_12);
x_28 = l_Lean_Syntax_node1(x_12, x_27, x_26);
x_29 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
lean_inc(x_12);
x_30 = l_Lean_Syntax_node2(x_12, x_29, x_21, x_28);
x_31 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__31;
lean_inc(x_12);
x_32 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_32, 0, x_12);
lean_ctor_set(x_32, 1, x_31);
x_33 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__8;
lean_inc(x_12);
x_34 = l_Lean_Syntax_node3(x_12, x_33, x_16, x_30, x_32);
x_35 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32;
lean_inc(x_12);
x_36 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_36, 0, x_12);
lean_ctor_set(x_36, 1, x_35);
x_37 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3;
x_38 = l_Lean_Syntax_node3(x_12, x_37, x_34, x_36, x_9);
x_39 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_39, 0, x_38);
lean_ctor_set(x_39, 1, x_3);
return x_39;
}
}
}
LEAN_EXPORT lean_object* l_ML_definedness___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; 
x_3 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_3, 0, x_1);
lean_inc(x_2);
x_4 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_4, 0, x_2);
x_5 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_5, 0, x_3);
lean_ctor_set(x_5, 1, x_4);
x_6 = l_ML_Pattern_universal___rarg(x_2, x_5);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_definedness(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_definedness___rarg), 2, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_total___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; 
x_3 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_3, 0, x_1);
x_4 = l_ML_Pattern_negation___rarg(x_2);
x_5 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_5, 0, x_3);
lean_ctor_set(x_5, 1, x_4);
x_6 = l_ML_Pattern_negation___rarg(x_5);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_total(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_total___rarg), 2, 0);
return x_2;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term⌊_⌋", 11);
return x_1;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML_term_u230a___u230b___closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("⌊", 3);
return x_1;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term_u230a___u230b___closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term_u230a___u230b___closed__4;
x_3 = l_ML_term_u2308___u2309___closed__10;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("⌋", 3);
return x_1;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term_u230a___u230b___closed__6;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term_u230a___u230b___closed__5;
x_3 = l_ML_term_u230a___u230b___closed__7;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u230a___u230b___closed__2;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = l_ML_term_u230a___u230b___closed__8;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u230a___u230b() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_term_u230a___u230b___closed__9;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("total", 5);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__6;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__5;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_term_u230a___u230b___closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; 
x_8 = lean_unsigned_to_nat(1u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
lean_dec(x_1);
x_10 = lean_ctor_get(x_2, 5);
lean_inc(x_10);
x_11 = 0;
x_12 = l_Lean_SourceInfo_fromRef(x_10, x_11);
x_13 = lean_ctor_get(x_2, 2);
lean_inc(x_13);
x_14 = lean_ctor_get(x_2, 1);
lean_inc(x_14);
lean_dec(x_2);
x_15 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__3;
x_16 = l_Lean_addMacroScope(x_14, x_15, x_13);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__2;
x_18 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__8;
lean_inc(x_12);
x_19 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_19, 0, x_12);
lean_ctor_set(x_19, 1, x_17);
lean_ctor_set(x_19, 2, x_16);
lean_ctor_set(x_19, 3, x_18);
x_20 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_12);
x_21 = l_Lean_Syntax_node1(x_12, x_20, x_9);
x_22 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
x_23 = l_Lean_Syntax_node2(x_12, x_22, x_19, x_21);
x_24 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_24, 0, x_23);
lean_ctor_set(x_24, 1, x_3);
return x_24;
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; 
x_4 = lean_box(0);
x_5 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_5, 0, x_4);
lean_ctor_set(x_5, 1, x_3);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_unsigned_to_nat(0u);
x_2 = lean_mk_empty_array_with_capacity(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; uint8_t x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; 
x_7 = l_Lean_replaceRef(x_1, x_5);
x_8 = 0;
x_9 = l_Lean_SourceInfo_fromRef(x_7, x_8);
x_10 = l_ML_term_u230a___u230b___closed__3;
lean_inc(x_9);
x_11 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_11, 0, x_9);
lean_ctor_set(x_11, 1, x_10);
x_12 = l_ML_term_u230a___u230b___closed__6;
lean_inc(x_9);
x_13 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_13, 0, x_9);
lean_ctor_set(x_13, 1, x_12);
x_14 = l_ML_term_u230a___u230b___closed__2;
lean_inc(x_9);
x_15 = l_Lean_Syntax_node3(x_9, x_14, x_11, x_3, x_13);
x_16 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2___closed__1;
x_17 = l_Array_append___rarg(x_16, x_4);
x_18 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_9);
x_19 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_19, 0, x_9);
lean_ctor_set(x_19, 1, x_18);
lean_ctor_set(x_19, 2, x_17);
x_20 = l_Lean_Syntax_node2(x_9, x_2, x_15, x_19);
x_21 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_21, 0, x_20);
lean_ctor_set(x_21, 1, x_6);
return x_21;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ident", 5);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_alloc_closure((void*)(l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__1___boxed), 3, 0);
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__3;
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_14);
if (x_17 == 0)
{
lean_object* x_18; uint8_t x_19; 
x_18 = l_Lean_Syntax_getNumArgs(x_15);
x_19 = lean_nat_dec_le(x_14, x_18);
if (x_19 == 0)
{
lean_object* x_20; lean_object* x_21; 
lean_dec(x_18);
lean_dec(x_15);
lean_dec(x_9);
x_20 = lean_box(0);
x_21 = lean_apply_3(x_16, x_20, x_2, x_3);
return x_21;
}
else
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; 
x_22 = l_Lean_Syntax_getArg(x_15, x_8);
x_23 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_24 = lean_nat_sub(x_18, x_8);
lean_dec(x_18);
x_25 = l_Array_extract___rarg(x_23, x_14, x_24);
lean_dec(x_24);
lean_dec(x_23);
x_26 = lean_box(2);
x_27 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
x_28 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_28, 0, x_26);
lean_ctor_set(x_28, 1, x_27);
lean_ctor_set(x_28, 2, x_25);
x_29 = l_Lean_Syntax_getArgs(x_28);
lean_dec(x_28);
x_30 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2(x_9, x_4, x_22, x_29, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_30;
}
}
else
{
lean_object* x_31; lean_object* x_32; uint8_t x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; 
x_31 = l_Lean_Syntax_getArg(x_15, x_8);
lean_dec(x_15);
x_32 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_33 = 0;
x_34 = l_Lean_SourceInfo_fromRef(x_32, x_33);
x_35 = l_ML_term_u230a___u230b___closed__3;
lean_inc(x_34);
x_36 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_36, 0, x_34);
lean_ctor_set(x_36, 1, x_35);
x_37 = l_ML_term_u230a___u230b___closed__6;
lean_inc(x_34);
x_38 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_38, 0, x_34);
lean_ctor_set(x_38, 1, x_37);
x_39 = l_ML_term_u230a___u230b___closed__2;
x_40 = l_Lean_Syntax_node3(x_34, x_39, x_36, x_31, x_38);
x_41 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_41, 0, x_40);
lean_ctor_set(x_41, 1, x_3);
return x_41;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__1(x_1, x_2, x_3);
lean_dec(x_2);
lean_dec(x_1);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; 
x_7 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2(x_1, x_2, x_3, x_4, x_5, x_6);
lean_dec(x_5);
lean_dec(x_1);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_equal___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; 
x_4 = l_ML_Pattern_equivalence___rarg(x_2, x_3);
x_5 = l_ML_total___rarg(x_1, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_equal(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_equal___rarg), 3, 0);
return x_2;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_≡_", 9);
return x_1;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML_term___u2261_____closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(" ≡ ", 5);
return x_1;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term___u2261_____closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__9;
x_2 = lean_unsigned_to_nat(51u);
x_3 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term___u2261_____closed__4;
x_3 = l_ML_term___u2261_____closed__5;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_term___u2261_____closed__2;
x_2 = lean_unsigned_to_nat(50u);
x_3 = lean_unsigned_to_nat(51u);
x_4 = l_ML_term___u2261_____closed__6;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_term___u2261__() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_term___u2261_____closed__7;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("equal", 5);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__6;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__5;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_term___u2261_____closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; uint8_t x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
lean_dec(x_1);
x_12 = lean_ctor_get(x_2, 5);
lean_inc(x_12);
x_13 = 0;
x_14 = l_Lean_SourceInfo_fromRef(x_12, x_13);
x_15 = lean_ctor_get(x_2, 2);
lean_inc(x_15);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
lean_dec(x_2);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__3;
x_18 = l_Lean_addMacroScope(x_16, x_17, x_15);
x_19 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__2;
x_20 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__8;
lean_inc(x_14);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_14);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_14);
x_23 = l_Lean_Syntax_node2(x_14, x_22, x_9, x_11);
x_24 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
x_25 = l_Lean_Syntax_node2(x_14, x_24, x_21, x_23);
x_26 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_26, 0, x_25);
lean_ctor_set(x_26, 1, x_3);
return x_26;
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; 
x_8 = l_Lean_replaceRef(x_1, x_6);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = l_ML_term___u2261_____closed__3;
lean_inc(x_10);
x_12 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_11);
x_13 = l_ML_term___u2261_____closed__2;
lean_inc(x_10);
x_14 = l_Lean_Syntax_node3(x_10, x_13, x_3, x_12, x_4);
x_15 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2___closed__1;
x_16 = l_Array_append___rarg(x_15, x_5);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_10);
x_18 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_18, 0, x_10);
lean_ctor_set(x_18, 1, x_17);
lean_ctor_set(x_18, 2, x_16);
x_19 = l_Lean_Syntax_node2(x_10, x_2, x_14, x_18);
x_20 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_20, 0, x_19);
lean_ctor_set(x_20, 1, x_7);
return x_20;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = lean_unsigned_to_nat(2u);
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; uint8_t x_19; 
x_18 = l_Lean_Syntax_getNumArgs(x_15);
x_19 = lean_nat_dec_le(x_16, x_18);
if (x_19 == 0)
{
lean_object* x_20; lean_object* x_21; 
lean_dec(x_18);
lean_dec(x_15);
lean_dec(x_9);
lean_dec(x_2);
x_20 = lean_box(0);
x_21 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_21, 0, x_20);
lean_ctor_set(x_21, 1, x_3);
return x_21;
}
else
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; 
x_22 = l_Lean_Syntax_getArg(x_15, x_8);
x_23 = l_Lean_Syntax_getArg(x_15, x_14);
x_24 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_25 = lean_nat_sub(x_18, x_8);
lean_dec(x_18);
x_26 = l_Array_extract___rarg(x_24, x_16, x_25);
lean_dec(x_25);
lean_dec(x_24);
x_27 = lean_box(2);
x_28 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
x_29 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_29, 0, x_27);
lean_ctor_set(x_29, 1, x_28);
lean_ctor_set(x_29, 2, x_26);
x_30 = l_Lean_Syntax_getArgs(x_29);
lean_dec(x_29);
x_31 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1___lambda__1(x_9, x_4, x_22, x_23, x_30, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_31;
}
}
else
{
lean_object* x_32; lean_object* x_33; lean_object* x_34; uint8_t x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; 
x_32 = l_Lean_Syntax_getArg(x_15, x_8);
x_33 = l_Lean_Syntax_getArg(x_15, x_14);
lean_dec(x_15);
x_34 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_35 = 0;
x_36 = l_Lean_SourceInfo_fromRef(x_34, x_35);
x_37 = l_ML_term___u2261_____closed__3;
lean_inc(x_36);
x_38 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_38, 0, x_36);
lean_ctor_set(x_38, 1, x_37);
x_39 = l_ML_term___u2261_____closed__2;
x_40 = l_Lean_Syntax_node3(x_36, x_39, x_32, x_38, x_33);
x_41 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_41, 0, x_40);
lean_ctor_set(x_41, 1, x_3);
return x_41;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; 
x_8 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1___lambda__1(x_1, x_2, x_3, x_4, x_5, x_6, x_7);
lean_dec(x_6);
lean_dec(x_1);
return x_8;
}
}
LEAN_EXPORT lean_object* l_ML_member___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; 
x_4 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_4, 0, x_1);
x_5 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_5, 0, x_2);
x_6 = l_ML_Pattern_conjunction___rarg(x_5, x_3);
x_7 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_7, 0, x_4);
lean_ctor_set(x_7, 1, x_6);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_member(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_member___rarg), 3, 0);
return x_2;
}
}
static lean_object* _init_l_ML_term___u2208_u2208_____closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_∈∈_", 12);
return x_1;
}
}
static lean_object* _init_l_ML_term___u2208_u2208_____closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML_term___u2208_u2208_____closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term___u2208_u2208_____closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(" ∈∈ ", 8);
return x_1;
}
}
static lean_object* _init_l_ML_term___u2208_u2208_____closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term___u2208_u2208_____closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term___u2208_u2208_____closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term___u2208_u2208_____closed__4;
x_3 = l_ML_term___u2261_____closed__5;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term___u2208_u2208_____closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_term___u2208_u2208_____closed__2;
x_2 = lean_unsigned_to_nat(50u);
x_3 = lean_unsigned_to_nat(51u);
x_4 = l_ML_term___u2208_u2208_____closed__5;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_term___u2208_u2208__() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_term___u2208_u2208_____closed__6;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("member", 6);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__6;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__5;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_term___u2208_u2208_____closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; uint8_t x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
lean_dec(x_1);
x_12 = lean_ctor_get(x_2, 5);
lean_inc(x_12);
x_13 = 0;
x_14 = l_Lean_SourceInfo_fromRef(x_12, x_13);
x_15 = lean_ctor_get(x_2, 2);
lean_inc(x_15);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
lean_dec(x_2);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__3;
x_18 = l_Lean_addMacroScope(x_16, x_17, x_15);
x_19 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__2;
x_20 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__8;
lean_inc(x_14);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_14);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_14);
x_23 = l_Lean_Syntax_node2(x_14, x_22, x_9, x_11);
x_24 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
x_25 = l_Lean_Syntax_node2(x_14, x_24, x_21, x_23);
x_26 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_26, 0, x_25);
lean_ctor_set(x_26, 1, x_3);
return x_26;
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; 
x_8 = l_Lean_replaceRef(x_1, x_6);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = l_ML_term___u2208_u2208_____closed__3;
lean_inc(x_10);
x_12 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_11);
x_13 = l_ML_term___u2208_u2208_____closed__2;
lean_inc(x_10);
x_14 = l_Lean_Syntax_node3(x_10, x_13, x_3, x_12, x_4);
x_15 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2___closed__1;
x_16 = l_Array_append___rarg(x_15, x_5);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_10);
x_18 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_18, 0, x_10);
lean_ctor_set(x_18, 1, x_17);
lean_ctor_set(x_18, 2, x_16);
x_19 = l_Lean_Syntax_node2(x_10, x_2, x_14, x_18);
x_20 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_20, 0, x_19);
lean_ctor_set(x_20, 1, x_7);
return x_20;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = lean_unsigned_to_nat(2u);
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; uint8_t x_19; 
x_18 = l_Lean_Syntax_getNumArgs(x_15);
x_19 = lean_nat_dec_le(x_16, x_18);
if (x_19 == 0)
{
lean_object* x_20; lean_object* x_21; 
lean_dec(x_18);
lean_dec(x_15);
lean_dec(x_9);
lean_dec(x_2);
x_20 = lean_box(0);
x_21 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_21, 0, x_20);
lean_ctor_set(x_21, 1, x_3);
return x_21;
}
else
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; 
x_22 = l_Lean_Syntax_getArg(x_15, x_8);
x_23 = l_Lean_Syntax_getArg(x_15, x_14);
x_24 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_25 = lean_nat_sub(x_18, x_8);
lean_dec(x_18);
x_26 = l_Array_extract___rarg(x_24, x_16, x_25);
lean_dec(x_25);
lean_dec(x_24);
x_27 = lean_box(2);
x_28 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
x_29 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_29, 0, x_27);
lean_ctor_set(x_29, 1, x_28);
lean_ctor_set(x_29, 2, x_26);
x_30 = l_Lean_Syntax_getArgs(x_29);
lean_dec(x_29);
x_31 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1___lambda__1(x_9, x_4, x_22, x_23, x_30, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_31;
}
}
else
{
lean_object* x_32; lean_object* x_33; lean_object* x_34; uint8_t x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; 
x_32 = l_Lean_Syntax_getArg(x_15, x_8);
x_33 = l_Lean_Syntax_getArg(x_15, x_14);
lean_dec(x_15);
x_34 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_35 = 0;
x_36 = l_Lean_SourceInfo_fromRef(x_34, x_35);
x_37 = l_ML_term___u2208_u2208_____closed__3;
lean_inc(x_36);
x_38 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_38, 0, x_36);
lean_ctor_set(x_38, 1, x_37);
x_39 = l_ML_term___u2208_u2208_____closed__2;
x_40 = l_Lean_Syntax_node3(x_36, x_39, x_32, x_38, x_33);
x_41 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_41, 0, x_40);
lean_ctor_set(x_41, 1, x_3);
return x_41;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; 
x_8 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1___lambda__1(x_1, x_2, x_3, x_4, x_5, x_6, x_7);
lean_dec(x_6);
lean_dec(x_1);
return x_8;
}
}
LEAN_EXPORT lean_object* l_ML_included___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; 
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_2);
lean_ctor_set(x_4, 1, x_3);
x_5 = l_ML_total___rarg(x_1, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_included(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_included___rarg), 3, 0);
return x_2;
}
}
static lean_object* _init_l_ML_term___u2286_u2286_____closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("term_⊆⊆_", 12);
return x_1;
}
}
static lean_object* _init_l_ML_term___u2286_u2286_____closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML_term___u2286_u2286_____closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term___u2286_u2286_____closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(" ⊆⊆ ", 8);
return x_1;
}
}
static lean_object* _init_l_ML_term___u2286_u2286_____closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term___u2286_u2286_____closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term___u2286_u2286_____closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term___u2286_u2286_____closed__4;
x_3 = l_ML_term___u2261_____closed__5;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term___u2286_u2286_____closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_term___u2286_u2286_____closed__2;
x_2 = lean_unsigned_to_nat(50u);
x_3 = lean_unsigned_to_nat(51u);
x_4 = l_ML_term___u2286_u2286_____closed__5;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_term___u2286_u2286__() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_term___u2286_u2286_____closed__6;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("included", 8);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__6;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__5;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_term___u2286_u2286_____closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; uint8_t x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
lean_dec(x_1);
x_12 = lean_ctor_get(x_2, 5);
lean_inc(x_12);
x_13 = 0;
x_14 = l_Lean_SourceInfo_fromRef(x_12, x_13);
x_15 = lean_ctor_get(x_2, 2);
lean_inc(x_15);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
lean_dec(x_2);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__3;
x_18 = l_Lean_addMacroScope(x_16, x_17, x_15);
x_19 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__2;
x_20 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__8;
lean_inc(x_14);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_14);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_14);
x_23 = l_Lean_Syntax_node2(x_14, x_22, x_9, x_11);
x_24 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
x_25 = l_Lean_Syntax_node2(x_14, x_24, x_21, x_23);
x_26 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_26, 0, x_25);
lean_ctor_set(x_26, 1, x_3);
return x_26;
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1___lambda__1(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; 
x_8 = l_Lean_replaceRef(x_1, x_6);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = l_ML_term___u2286_u2286_____closed__3;
lean_inc(x_10);
x_12 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_11);
x_13 = l_ML_term___u2286_u2286_____closed__2;
lean_inc(x_10);
x_14 = l_Lean_Syntax_node3(x_10, x_13, x_3, x_12, x_4);
x_15 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2___closed__1;
x_16 = l_Array_append___rarg(x_15, x_5);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_10);
x_18 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_18, 0, x_10);
lean_ctor_set(x_18, 1, x_17);
lean_ctor_set(x_18, 2, x_16);
x_19 = l_Lean_Syntax_node2(x_10, x_2, x_14, x_18);
x_20 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_20, 0, x_19);
lean_ctor_set(x_20, 1, x_7);
return x_20;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_2);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = lean_unsigned_to_nat(2u);
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; uint8_t x_19; 
x_18 = l_Lean_Syntax_getNumArgs(x_15);
x_19 = lean_nat_dec_le(x_16, x_18);
if (x_19 == 0)
{
lean_object* x_20; lean_object* x_21; 
lean_dec(x_18);
lean_dec(x_15);
lean_dec(x_9);
lean_dec(x_2);
x_20 = lean_box(0);
x_21 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_21, 0, x_20);
lean_ctor_set(x_21, 1, x_3);
return x_21;
}
else
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; 
x_22 = l_Lean_Syntax_getArg(x_15, x_8);
x_23 = l_Lean_Syntax_getArg(x_15, x_14);
x_24 = l_Lean_Syntax_getArgs(x_15);
lean_dec(x_15);
x_25 = lean_nat_sub(x_18, x_8);
lean_dec(x_18);
x_26 = l_Array_extract___rarg(x_24, x_16, x_25);
lean_dec(x_25);
lean_dec(x_24);
x_27 = lean_box(2);
x_28 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
x_29 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_29, 0, x_27);
lean_ctor_set(x_29, 1, x_28);
lean_ctor_set(x_29, 2, x_26);
x_30 = l_Lean_Syntax_getArgs(x_29);
lean_dec(x_29);
x_31 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1___lambda__1(x_9, x_4, x_22, x_23, x_30, x_2, x_3);
lean_dec(x_2);
lean_dec(x_9);
return x_31;
}
}
else
{
lean_object* x_32; lean_object* x_33; lean_object* x_34; uint8_t x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; 
x_32 = l_Lean_Syntax_getArg(x_15, x_8);
x_33 = l_Lean_Syntax_getArg(x_15, x_14);
lean_dec(x_15);
x_34 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_2);
lean_dec(x_9);
x_35 = 0;
x_36 = l_Lean_SourceInfo_fromRef(x_34, x_35);
x_37 = l_ML_term___u2286_u2286_____closed__3;
lean_inc(x_36);
x_38 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_38, 0, x_36);
lean_ctor_set(x_38, 1, x_37);
x_39 = l_ML_term___u2286_u2286_____closed__2;
x_40 = l_Lean_Syntax_node3(x_36, x_39, x_32, x_38, x_33);
x_41 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_41, 0, x_40);
lean_ctor_set(x_41, 1, x_3);
return x_41;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1___lambda__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; 
x_8 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1___lambda__1(x_1, x_2, x_3, x_4, x_5, x_6, x_7);
lean_dec(x_6);
lean_dec(x_1);
return x_8;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_CtxDefined___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_2 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_2, 0, x_1);
x_3 = lean_box(0);
x_4 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_4, 0, x_2);
lean_ctor_set(x_4, 1, x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_CtxDefined(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_AppContext_CtxDefined___rarg), 1, 0);
return x_2;
}
}
static lean_object* _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Mdefined", 8);
return x_1;
}
}
static lean_object* _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_isApplicationOfDefinedSymbolToPattern___closed__3;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("bdefined", 8);
return x_1;
}
}
static lean_object* _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_isApplicationOfDefinedSymbolToPattern___closed__5;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT uint8_t l_ML_isApplicationOfDefinedSymbolToPattern(lean_object* x_1) {
_start:
{
lean_object* x_2; uint8_t x_3; 
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
lean_inc(x_1);
x_3 = l_Lean_Syntax_isOfKind(x_1, x_2);
if (x_3 == 0)
{
uint8_t x_4; 
lean_dec(x_1);
x_4 = 0;
return x_4;
}
else
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; uint8_t x_8; 
x_5 = lean_unsigned_to_nat(0u);
x_6 = l_Lean_Syntax_getArg(x_1, x_5);
x_7 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16;
x_8 = l_Lean_Syntax_matchesIdent(x_6, x_7);
if (x_8 == 0)
{
lean_object* x_9; uint8_t x_10; 
x_9 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15;
x_10 = l_Lean_Syntax_matchesIdent(x_6, x_9);
if (x_10 == 0)
{
lean_object* x_11; uint8_t x_12; 
x_11 = l_ML_isApplicationOfDefinedSymbolToPattern___closed__1;
x_12 = l_Lean_Syntax_matchesIdent(x_6, x_11);
lean_dec(x_6);
if (x_12 == 0)
{
uint8_t x_13; 
lean_dec(x_1);
x_13 = 0;
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; uint8_t x_16; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
lean_inc(x_15);
x_16 = l_Lean_Syntax_matchesNull(x_15, x_14);
if (x_16 == 0)
{
uint8_t x_17; 
lean_dec(x_15);
x_17 = 0;
return x_17;
}
else
{
lean_object* x_18; lean_object* x_19; uint8_t x_20; 
x_18 = l_Lean_Syntax_getArg(x_15, x_5);
lean_dec(x_15);
x_19 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28;
x_20 = l_Lean_Syntax_matchesIdent(x_18, x_19);
if (x_20 == 0)
{
lean_object* x_21; uint8_t x_22; 
x_21 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27;
x_22 = l_Lean_Syntax_matchesIdent(x_18, x_21);
if (x_22 == 0)
{
lean_object* x_23; uint8_t x_24; 
x_23 = l_ML_isApplicationOfDefinedSymbolToPattern___closed__2;
x_24 = l_Lean_Syntax_matchesIdent(x_18, x_23);
lean_dec(x_18);
return x_24;
}
else
{
uint8_t x_25; 
lean_dec(x_18);
x_25 = 1;
return x_25;
}
}
else
{
uint8_t x_26; 
lean_dec(x_18);
x_26 = 1;
return x_26;
}
}
}
}
else
{
lean_object* x_27; lean_object* x_28; uint8_t x_29; 
lean_dec(x_6);
x_27 = lean_unsigned_to_nat(1u);
x_28 = l_Lean_Syntax_getArg(x_1, x_27);
lean_dec(x_1);
lean_inc(x_28);
x_29 = l_Lean_Syntax_matchesNull(x_28, x_27);
if (x_29 == 0)
{
uint8_t x_30; 
lean_dec(x_28);
x_30 = 0;
return x_30;
}
else
{
lean_object* x_31; lean_object* x_32; uint8_t x_33; 
x_31 = l_Lean_Syntax_getArg(x_28, x_5);
lean_dec(x_28);
x_32 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28;
x_33 = l_Lean_Syntax_matchesIdent(x_31, x_32);
if (x_33 == 0)
{
lean_object* x_34; uint8_t x_35; 
x_34 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27;
x_35 = l_Lean_Syntax_matchesIdent(x_31, x_34);
if (x_35 == 0)
{
lean_object* x_36; uint8_t x_37; 
x_36 = l_ML_isApplicationOfDefinedSymbolToPattern___closed__4;
x_37 = l_Lean_Syntax_matchesIdent(x_31, x_36);
lean_dec(x_31);
return x_37;
}
else
{
uint8_t x_38; 
lean_dec(x_31);
x_38 = 1;
return x_38;
}
}
else
{
uint8_t x_39; 
lean_dec(x_31);
x_39 = 1;
return x_39;
}
}
}
}
else
{
lean_object* x_40; lean_object* x_41; uint8_t x_42; 
lean_dec(x_6);
x_40 = lean_unsigned_to_nat(1u);
x_41 = l_Lean_Syntax_getArg(x_1, x_40);
lean_dec(x_1);
lean_inc(x_41);
x_42 = l_Lean_Syntax_matchesNull(x_41, x_40);
if (x_42 == 0)
{
uint8_t x_43; 
lean_dec(x_41);
x_43 = 0;
return x_43;
}
else
{
lean_object* x_44; lean_object* x_45; uint8_t x_46; 
x_44 = l_Lean_Syntax_getArg(x_41, x_5);
lean_dec(x_41);
x_45 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27;
x_46 = l_Lean_Syntax_matchesIdent(x_44, x_45);
if (x_46 == 0)
{
lean_object* x_47; uint8_t x_48; 
x_47 = l_ML_isApplicationOfDefinedSymbolToPattern___closed__6;
x_48 = l_Lean_Syntax_matchesIdent(x_44, x_47);
lean_dec(x_44);
return x_48;
}
else
{
uint8_t x_49; 
lean_dec(x_44);
x_49 = 1;
return x_49;
}
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___boxed(lean_object* x_1) {
_start:
{
uint8_t x_2; lean_object* x_3; 
x_2 = l_ML_isApplicationOfDefinedSymbolToPattern(x_1);
x_3 = lean_box(x_2);
return x_3;
}
}
static lean_object* _init_l_ML_delabDefined___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_alloc_closure((void*)(l_Lean_PrettyPrinter_Delaborator_delab), 7, 0);
return x_1;
}
}
static lean_object* _init_l_ML_delabDefined___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_delabDefined___closed__1;
x_2 = lean_alloc_closure((void*)(l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppArg___at_Lean_PrettyPrinter_Delaborator_delabLetFun___spec__1), 8, 1);
lean_closure_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_delabDefined___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_delabDefined___closed__2;
x_2 = lean_alloc_closure((void*)(l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppFn___at_Lean_PrettyPrinter_Delaborator_delabLetFun___spec__2), 8, 1);
lean_closure_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_delabDefined___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("choice", 6);
return x_1;
}
}
static lean_object* _init_l_ML_delabDefined___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_delabDefined___closed__4;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_delabDefined___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Int", 3);
return x_1;
}
}
static lean_object* _init_l_ML_delabDefined___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_delabDefined___closed__6;
x_2 = l_ML_term_u2308___u2309___closed__2;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_delabDefined(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; uint8_t x_14; lean_object* x_15; 
x_8 = l_Lean_PrettyPrinter_Delaborator_SubExpr_getExpr___at_Lean_PrettyPrinter_Delaborator_getExprKind___spec__1(x_1, x_2, x_3, x_4, x_5, x_6, x_7);
x_9 = lean_ctor_get(x_8, 0);
lean_inc(x_9);
x_10 = lean_ctor_get(x_8, 1);
lean_inc(x_10);
lean_dec(x_8);
x_11 = lean_unsigned_to_nat(0u);
x_12 = l___private_Lean_Expr_0__Lean_Expr_getAppNumArgsAux(x_9, x_11);
lean_dec(x_9);
x_13 = lean_unsigned_to_nat(3u);
x_14 = lean_nat_dec_eq(x_12, x_13);
lean_dec(x_12);
if (x_14 == 0)
{
lean_object* x_86; uint8_t x_87; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
lean_dec(x_1);
x_86 = l_Lean_PrettyPrinter_Delaborator_failure___rarg(x_10);
x_87 = !lean_is_exclusive(x_86);
if (x_87 == 0)
{
return x_86;
}
else
{
lean_object* x_88; lean_object* x_89; lean_object* x_90; 
x_88 = lean_ctor_get(x_86, 0);
x_89 = lean_ctor_get(x_86, 1);
lean_inc(x_89);
lean_inc(x_88);
lean_dec(x_86);
x_90 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_90, 0, x_88);
lean_ctor_set(x_90, 1, x_89);
return x_90;
}
}
else
{
x_15 = x_10;
goto block_85;
}
block_85:
{
lean_object* x_16; lean_object* x_17; 
x_16 = l_ML_delabDefined___closed__3;
lean_inc(x_6);
lean_inc(x_5);
lean_inc(x_4);
lean_inc(x_3);
lean_inc(x_2);
lean_inc(x_1);
x_17 = l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppFn___at_Lean_PrettyPrinter_Delaborator_delabLetFun___spec__2(x_16, x_1, x_2, x_3, x_4, x_5, x_6, x_15);
if (lean_obj_tag(x_17) == 0)
{
lean_object* x_18; lean_object* x_19; lean_object* x_20; 
x_18 = lean_ctor_get(x_17, 1);
lean_inc(x_18);
lean_dec(x_17);
x_19 = l_ML_delabDefined___closed__2;
lean_inc(x_6);
lean_inc(x_5);
lean_inc(x_4);
lean_inc(x_3);
lean_inc(x_2);
lean_inc(x_1);
x_20 = l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppFn___at_Lean_PrettyPrinter_Delaborator_delabLetFun___spec__2(x_19, x_1, x_2, x_3, x_4, x_5, x_6, x_18);
if (lean_obj_tag(x_20) == 0)
{
lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; 
x_21 = lean_ctor_get(x_20, 0);
lean_inc(x_21);
x_22 = lean_ctor_get(x_20, 1);
lean_inc(x_22);
lean_dec(x_20);
x_23 = l_ML_delabDefined___closed__1;
lean_inc(x_5);
x_24 = l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppArg___at_Lean_PrettyPrinter_Delaborator_delabLetFun___spec__1(x_23, x_1, x_2, x_3, x_4, x_5, x_6, x_22);
if (lean_obj_tag(x_24) == 0)
{
uint8_t x_25; 
x_25 = !lean_is_exclusive(x_24);
if (x_25 == 0)
{
lean_object* x_26; uint8_t x_27; 
x_26 = lean_ctor_get(x_24, 0);
lean_inc(x_21);
x_27 = l_ML_isApplicationOfDefinedSymbolToPattern(x_21);
if (x_27 == 0)
{
lean_object* x_28; uint8_t x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; 
x_28 = lean_ctor_get(x_5, 5);
lean_inc(x_28);
lean_dec(x_5);
x_29 = 0;
x_30 = l_Lean_SourceInfo_fromRef(x_28, x_29);
x_31 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32;
lean_inc(x_30);
x_32 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_32, 0, x_30);
lean_ctor_set(x_32, 1, x_31);
x_33 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3;
x_34 = l_Lean_Syntax_node3(x_30, x_33, x_21, x_32, x_26);
lean_ctor_set(x_24, 0, x_34);
return x_24;
}
else
{
lean_object* x_35; uint8_t x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; 
lean_dec(x_21);
x_35 = lean_ctor_get(x_5, 5);
lean_inc(x_35);
lean_dec(x_5);
x_36 = 0;
x_37 = l_Lean_SourceInfo_fromRef(x_35, x_36);
x_38 = l_ML_term_u2308___u2309___closed__6;
lean_inc(x_37);
x_39 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_39, 0, x_37);
lean_ctor_set(x_39, 1, x_38);
x_40 = l_ML_term_u2308___u2309___closed__12;
lean_inc(x_37);
x_41 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_41, 0, x_37);
lean_ctor_set(x_41, 1, x_40);
x_42 = l_ML_term_u2308___u2309___closed__3;
lean_inc(x_41);
lean_inc(x_26);
lean_inc(x_39);
lean_inc(x_37);
x_43 = l_Lean_Syntax_node3(x_37, x_42, x_39, x_26, x_41);
x_44 = l_ML_delabDefined___closed__7;
lean_inc(x_37);
x_45 = l_Lean_Syntax_node3(x_37, x_44, x_39, x_26, x_41);
x_46 = l_ML_delabDefined___closed__5;
x_47 = l_Lean_Syntax_node2(x_37, x_46, x_43, x_45);
lean_ctor_set(x_24, 0, x_47);
return x_24;
}
}
else
{
lean_object* x_48; lean_object* x_49; uint8_t x_50; 
x_48 = lean_ctor_get(x_24, 0);
x_49 = lean_ctor_get(x_24, 1);
lean_inc(x_49);
lean_inc(x_48);
lean_dec(x_24);
lean_inc(x_21);
x_50 = l_ML_isApplicationOfDefinedSymbolToPattern(x_21);
if (x_50 == 0)
{
lean_object* x_51; uint8_t x_52; lean_object* x_53; lean_object* x_54; lean_object* x_55; lean_object* x_56; lean_object* x_57; lean_object* x_58; 
x_51 = lean_ctor_get(x_5, 5);
lean_inc(x_51);
lean_dec(x_5);
x_52 = 0;
x_53 = l_Lean_SourceInfo_fromRef(x_51, x_52);
x_54 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32;
lean_inc(x_53);
x_55 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_55, 0, x_53);
lean_ctor_set(x_55, 1, x_54);
x_56 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3;
x_57 = l_Lean_Syntax_node3(x_53, x_56, x_21, x_55, x_48);
x_58 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_58, 0, x_57);
lean_ctor_set(x_58, 1, x_49);
return x_58;
}
else
{
lean_object* x_59; uint8_t x_60; lean_object* x_61; lean_object* x_62; lean_object* x_63; lean_object* x_64; lean_object* x_65; lean_object* x_66; lean_object* x_67; lean_object* x_68; lean_object* x_69; lean_object* x_70; lean_object* x_71; lean_object* x_72; 
lean_dec(x_21);
x_59 = lean_ctor_get(x_5, 5);
lean_inc(x_59);
lean_dec(x_5);
x_60 = 0;
x_61 = l_Lean_SourceInfo_fromRef(x_59, x_60);
x_62 = l_ML_term_u2308___u2309___closed__6;
lean_inc(x_61);
x_63 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_63, 0, x_61);
lean_ctor_set(x_63, 1, x_62);
x_64 = l_ML_term_u2308___u2309___closed__12;
lean_inc(x_61);
x_65 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_65, 0, x_61);
lean_ctor_set(x_65, 1, x_64);
x_66 = l_ML_term_u2308___u2309___closed__3;
lean_inc(x_65);
lean_inc(x_48);
lean_inc(x_63);
lean_inc(x_61);
x_67 = l_Lean_Syntax_node3(x_61, x_66, x_63, x_48, x_65);
x_68 = l_ML_delabDefined___closed__7;
lean_inc(x_61);
x_69 = l_Lean_Syntax_node3(x_61, x_68, x_63, x_48, x_65);
x_70 = l_ML_delabDefined___closed__5;
x_71 = l_Lean_Syntax_node2(x_61, x_70, x_67, x_69);
x_72 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_72, 0, x_71);
lean_ctor_set(x_72, 1, x_49);
return x_72;
}
}
}
else
{
uint8_t x_73; 
lean_dec(x_21);
lean_dec(x_5);
x_73 = !lean_is_exclusive(x_24);
if (x_73 == 0)
{
return x_24;
}
else
{
lean_object* x_74; lean_object* x_75; lean_object* x_76; 
x_74 = lean_ctor_get(x_24, 0);
x_75 = lean_ctor_get(x_24, 1);
lean_inc(x_75);
lean_inc(x_74);
lean_dec(x_24);
x_76 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_76, 0, x_74);
lean_ctor_set(x_76, 1, x_75);
return x_76;
}
}
}
else
{
uint8_t x_77; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
lean_dec(x_1);
x_77 = !lean_is_exclusive(x_20);
if (x_77 == 0)
{
return x_20;
}
else
{
lean_object* x_78; lean_object* x_79; lean_object* x_80; 
x_78 = lean_ctor_get(x_20, 0);
x_79 = lean_ctor_get(x_20, 1);
lean_inc(x_79);
lean_inc(x_78);
lean_dec(x_20);
x_80 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_80, 0, x_78);
lean_ctor_set(x_80, 1, x_79);
return x_80;
}
}
}
else
{
uint8_t x_81; 
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
lean_dec(x_1);
x_81 = !lean_is_exclusive(x_17);
if (x_81 == 0)
{
return x_17;
}
else
{
lean_object* x_82; lean_object* x_83; lean_object* x_84; 
x_82 = lean_ctor_get(x_17, 0);
x_83 = lean_ctor_get(x_17, 1);
lean_inc(x_83);
lean_inc(x_82);
lean_dec(x_17);
x_84 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_84, 0, x_82);
lean_ctor_set(x_84, 1, x_83);
return x_84;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_equivImplEqual___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; 
x_6 = l_ML_Pattern_equivalence___rarg(x_3, x_4);
x_7 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_7, 0, x_1);
x_8 = lean_box(0);
x_9 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_9, 0, x_7);
lean_ctor_set(x_9, 1, x_8);
x_10 = l_ML_Proof_doubleNegCtx___rarg(x_6, x_9, x_5);
return x_10;
}
}
LEAN_EXPORT lean_object* l_ML_equivImplEqual(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_equivImplEqual___rarg), 5, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_equalSelf___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; 
lean_inc(x_3);
x_4 = l_ML_Proof_equivSelf___rarg(x_3);
lean_inc(x_3);
x_5 = l_ML_equivImplEqual___rarg(x_1, lean_box(0), x_3, x_3, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_equalSelf(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_equalSelf___rarg), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_definedness_x27___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; 
x_5 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_5, 0, x_1);
lean_inc(x_4);
x_6 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_6, 0, x_4);
x_7 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_7, 0, x_5);
lean_ctor_set(x_7, 1, x_6);
lean_inc(x_7);
lean_inc(x_4);
x_8 = l_ML_Pattern_universal___rarg(x_4, x_7);
lean_inc(x_8);
x_9 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_9, 0, x_8);
lean_inc(x_4);
lean_inc(x_7);
x_10 = l_ML_Proof_univQuan___rarg(x_7, x_4, x_4, lean_box(0));
x_11 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_11, 0, x_8);
lean_ctor_set(x_11, 1, x_7);
lean_ctor_set(x_11, 2, x_9);
lean_ctor_set(x_11, 3, x_10);
return x_11;
}
}
LEAN_EXPORT lean_object* l_ML_definedness_x27(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_definedness_x27___rarg___boxed), 4, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_definedness_x27___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = l_ML_definedness_x27___rarg(x_1, x_2, x_3, x_4);
lean_dec(x_3);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_memberIntro___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; 
lean_inc(x_5);
x_7 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_7, 0, x_5);
lean_inc(x_7);
lean_inc(x_3);
x_8 = l_ML_Proof_extraPremise___rarg(x_3, x_7, x_6);
lean_inc(x_7);
x_9 = l_ML_Proof_implSelf___rarg(x_7);
lean_inc(x_3);
lean_inc(x_7);
x_10 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_10, 0, x_7);
lean_ctor_set(x_10, 1, x_3);
lean_inc(x_3);
lean_inc(x_7);
x_11 = l_ML_Pattern_conjunction___rarg(x_7, x_3);
lean_inc(x_11);
lean_inc(x_7);
x_12 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_12, 0, x_7);
lean_ctor_set(x_12, 1, x_11);
lean_inc_n(x_7, 2);
x_13 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_13, 0, x_7);
lean_ctor_set(x_13, 1, x_7);
lean_inc(x_12);
lean_inc(x_10);
x_14 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_12);
lean_inc(x_3);
lean_inc_n(x_7, 2);
x_15 = l_ML_Proof_conjIntro___rarg(x_7, x_7, x_3);
x_16 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_16, 0, x_13);
lean_ctor_set(x_16, 1, x_14);
lean_ctor_set(x_16, 2, x_9);
lean_ctor_set(x_16, 3, x_15);
x_17 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_17, 0, x_10);
lean_ctor_set(x_17, 1, x_12);
lean_ctor_set(x_17, 2, x_8);
lean_ctor_set(x_17, 3, x_16);
lean_inc(x_7);
x_18 = l_ML_Pattern_negation___rarg(x_7);
lean_inc(x_3);
x_19 = l_ML_Pattern_negation___rarg(x_3);
x_20 = l_ML_Pattern_disjunction___rarg(x_18, x_19);
x_21 = l_ML_Pattern_negation___rarg(x_20);
lean_inc(x_1);
x_22 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_22, 0, x_1);
x_23 = lean_box(0);
lean_inc(x_22);
x_24 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_24, 0, x_22);
lean_ctor_set(x_24, 1, x_23);
x_25 = l_ML_Proof_framing___rarg(x_7, x_21, x_24, x_17);
lean_dec(x_17);
lean_dec(x_24);
lean_dec(x_21);
lean_inc(x_5);
lean_inc(x_1);
x_26 = l_ML_definedness_x27___rarg(x_1, lean_box(0), x_4, x_5);
lean_inc(x_22);
x_27 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_27, 0, x_22);
lean_ctor_set(x_27, 1, x_7);
x_28 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_28, 0, x_22);
lean_ctor_set(x_28, 1, x_11);
x_29 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_29, 0, x_27);
lean_ctor_set(x_29, 1, x_28);
lean_ctor_set(x_29, 2, x_26);
lean_ctor_set(x_29, 3, x_25);
lean_inc(x_5);
x_30 = l_ML_member___rarg(x_1, x_5, x_3);
x_31 = l_ML_Proof_univGeneralization___rarg(x_30, x_5, x_29);
return x_31;
}
}
LEAN_EXPORT lean_object* l_ML_memberIntro(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_memberIntro___rarg___boxed), 6, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_memberIntro___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; 
x_7 = l_ML_memberIntro___rarg(x_1, x_2, x_3, x_4, x_5, x_6);
lean_dec(x_4);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_auxTauto___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; 
lean_inc(x_2);
lean_inc(x_1);
x_3 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
lean_inc(x_2);
x_4 = l_ML_Pattern_negation___rarg(x_2);
x_5 = l_ML_Pattern_conjunction___rarg(x_1, x_4);
x_6 = l_ML_Pattern_disjunction___rarg(x_5, x_2);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_3);
lean_ctor_set(x_7, 1, x_6);
x_8 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_8, 0, x_7);
return x_8;
}
}
LEAN_EXPORT lean_object* l_ML_auxTauto(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_auxTauto___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_ctxImplDefinedAux1___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; 
lean_inc(x_6);
lean_inc(x_1);
x_7 = l_ML_definedness_x27___rarg(x_1, lean_box(0), x_5, x_6);
lean_inc(x_1);
x_8 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_8, 0, x_1);
lean_inc(x_6);
x_9 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_9, 0, x_6);
lean_inc(x_9);
lean_inc(x_8);
x_10 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_10, 0, x_8);
lean_ctor_set(x_10, 1, x_9);
lean_inc(x_3);
lean_inc(x_8);
x_11 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_11, 0, x_8);
lean_ctor_set(x_11, 1, x_3);
lean_inc(x_11);
lean_inc(x_10);
x_12 = l_ML_Pattern_disjunction___rarg(x_10, x_11);
lean_inc(x_11);
lean_inc(x_10);
x_13 = l_ML_Proof_weakeningDisj___rarg(x_10, x_11);
x_14 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_12);
lean_ctor_set(x_14, 2, x_7);
lean_ctor_set(x_14, 3, x_13);
x_15 = l_ML_AppContext_CtxDefined___rarg(x_1);
x_16 = l_ML_AppContext_insert___rarg(x_9, x_15);
x_17 = l_ML_AppContext_insert___rarg(x_3, x_15);
x_18 = l_ML_Pattern_disjunction___rarg(x_16, x_17);
lean_inc(x_3);
lean_inc(x_9);
x_19 = l_ML_Pattern_disjunction___rarg(x_9, x_3);
lean_inc(x_19);
lean_inc(x_8);
x_20 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_20, 0, x_8);
lean_ctor_set(x_20, 1, x_19);
lean_inc(x_15);
lean_inc(x_3);
lean_inc(x_9);
x_21 = l_ML_Proof_propagtionDisjR___rarg(x_9, x_3, x_15);
lean_inc(x_20);
x_22 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_22, 0, x_18);
lean_ctor_set(x_22, 1, x_20);
lean_ctor_set(x_22, 2, x_14);
lean_ctor_set(x_22, 3, x_21);
lean_inc(x_3);
lean_inc(x_9);
x_23 = l_ML_auxTauto___rarg(x_9, x_3);
lean_inc(x_3);
x_24 = l_ML_Pattern_negation___rarg(x_3);
lean_inc(x_24);
lean_inc(x_9);
x_25 = l_ML_Pattern_conjunction___rarg(x_9, x_24);
lean_inc(x_3);
lean_inc(x_25);
x_26 = l_ML_Pattern_disjunction___rarg(x_25, x_3);
lean_inc(x_8);
lean_inc(x_26);
x_27 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_27, 0, x_19);
lean_ctor_set(x_27, 1, x_26);
lean_ctor_set(x_27, 2, x_8);
lean_ctor_set(x_27, 3, x_23);
lean_inc(x_8);
x_28 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_28, 0, x_8);
lean_ctor_set(x_28, 1, x_26);
x_29 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_29, 0, x_20);
lean_ctor_set(x_29, 1, x_28);
lean_ctor_set(x_29, 2, x_22);
lean_ctor_set(x_29, 3, x_27);
lean_inc(x_9);
x_30 = l_ML_Pattern_negation___rarg(x_9);
x_31 = l_ML_Pattern_negation___rarg(x_24);
x_32 = l_ML_Pattern_disjunction___rarg(x_30, x_31);
x_33 = l_ML_Pattern_negation___rarg(x_32);
lean_inc(x_3);
lean_inc(x_33);
x_34 = l_ML_Pattern_disjunction___rarg(x_33, x_3);
x_35 = l_ML_AppContext_insert___rarg(x_34, x_15);
lean_dec(x_34);
x_36 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_36, 0, x_8);
lean_ctor_set(x_36, 1, x_25);
lean_inc(x_11);
lean_inc(x_36);
x_37 = l_ML_Pattern_disjunction___rarg(x_36, x_11);
lean_inc(x_15);
lean_inc(x_3);
x_38 = l_ML_Proof_propagationDisj___rarg(x_33, x_3, x_15);
x_39 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_39, 0, x_35);
lean_ctor_set(x_39, 1, x_37);
lean_ctor_set(x_39, 2, x_29);
lean_ctor_set(x_39, 3, x_38);
lean_inc(x_3);
lean_inc(x_4);
x_40 = lean_alloc_ctor(6, 4, 0);
lean_ctor_set(x_40, 0, x_4);
lean_ctor_set(x_40, 1, x_15);
lean_ctor_set(x_40, 2, x_6);
lean_ctor_set(x_40, 3, x_3);
x_41 = l_ML_Pattern_conjunction___rarg(x_9, x_3);
x_42 = l_ML_AppContext_insert___rarg(x_41, x_4);
lean_dec(x_4);
lean_dec(x_41);
lean_inc(x_36);
lean_inc(x_42);
x_43 = l_ML_Pattern_conjunction___rarg(x_42, x_36);
x_44 = l_ML_Pattern_negation___rarg(x_43);
lean_inc(x_36);
x_45 = l_ML_Pattern_negation___rarg(x_36);
lean_inc(x_45);
lean_inc(x_42);
x_46 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_46, 0, x_42);
lean_ctor_set(x_46, 1, x_45);
lean_inc(x_42);
x_47 = l_ML_Proof_negConjAsImpl___rarg(x_42, x_36);
x_48 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_48, 0, x_44);
lean_ctor_set(x_48, 1, x_46);
lean_ctor_set(x_48, 2, x_40);
lean_ctor_set(x_48, 3, x_47);
x_49 = l_ML_Proof_syllogism___rarg(x_42, x_45, x_11, x_48, x_39);
return x_49;
}
}
LEAN_EXPORT lean_object* l_ML_ctxImplDefinedAux1(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_ctxImplDefinedAux1___rarg___boxed), 6, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_ctxImplDefinedAux1___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; 
x_7 = l_ML_ctxImplDefinedAux1___rarg(x_1, x_2, x_3, x_4, x_5, x_6);
lean_dec(x_5);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_ctxImplDefined___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; 
x_6 = l_ML_Pattern_evars___rarg(x_3);
x_7 = l_ML_AppContext_evars___rarg(x_4);
x_8 = l_List_appendTR___rarg(x_6, x_7);
x_9 = l_ML_EVar_getFresh(x_8);
lean_inc(x_9);
lean_inc(x_4);
lean_inc(x_3);
lean_inc(x_1);
x_10 = l_ML_ctxImplDefinedAux1___rarg(x_1, lean_box(0), x_3, x_4, x_5, x_9);
lean_inc(x_9);
x_11 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_11, 0, x_9);
lean_inc(x_3);
lean_inc(x_11);
x_12 = l_ML_Pattern_conjunction___rarg(x_11, x_3);
x_13 = l_ML_AppContext_insert___rarg(x_12, x_4);
x_14 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_14, 0, x_1);
lean_inc(x_3);
x_15 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_15, 0, x_14);
lean_ctor_set(x_15, 1, x_3);
lean_inc(x_9);
lean_inc(x_15);
lean_inc(x_13);
x_16 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_16, 0, x_13);
lean_ctor_set(x_16, 1, x_15);
lean_ctor_set(x_16, 2, x_9);
lean_ctor_set(x_16, 3, x_10);
lean_inc(x_3);
x_17 = l_ML_Proof_implSelf___rarg(x_3);
lean_inc(x_9);
x_18 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_18, 0, x_9);
lean_inc(x_11);
lean_inc(x_9);
x_19 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_19, 0, x_9);
lean_ctor_set(x_19, 1, x_11);
lean_inc(x_3);
lean_inc(x_19);
x_20 = l_ML_Proof_extraPremise___rarg(x_19, x_3, x_18);
lean_inc(x_19);
lean_inc_n(x_3, 2);
x_21 = l_ML_Proof_conjIntroRule___rarg(x_3, x_19, x_3, x_20, x_17);
lean_inc(x_3);
x_22 = l_ML_Pattern_conjunction___rarg(x_19, x_3);
lean_inc(x_12);
lean_inc(x_9);
x_23 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_23, 0, x_9);
lean_ctor_set(x_23, 1, x_12);
lean_inc(x_9);
lean_inc(x_3);
x_24 = l_ML_Proof_pushConjInExist___rarg(x_11, x_3, x_9, lean_box(0));
lean_inc(x_23);
lean_inc(x_3);
x_25 = l_ML_Proof_syllogism___rarg(x_3, x_22, x_23, x_21, x_24);
x_26 = l_ML_Proof_framing___rarg(x_3, x_23, x_4, x_25);
lean_dec(x_25);
lean_inc(x_9);
x_27 = l_ML_Proof_propagationExist___rarg(x_12, x_9, x_4, lean_box(0));
x_28 = l_ML_AppContext_insert___rarg(x_23, x_4);
lean_dec(x_23);
x_29 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_29, 0, x_9);
lean_ctor_set(x_29, 1, x_13);
lean_inc(x_15);
lean_inc(x_28);
x_30 = l_ML_Proof_syllogism___rarg(x_28, x_29, x_15, x_27, x_16);
x_31 = l_ML_AppContext_insert___rarg(x_3, x_4);
lean_dec(x_4);
lean_dec(x_3);
x_32 = l_ML_Proof_syllogism___rarg(x_31, x_28, x_15, x_26, x_30);
return x_32;
}
}
LEAN_EXPORT lean_object* l_ML_ctxImplDefined(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_ctxImplDefined___rarg___boxed), 5, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_ctxImplDefined___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; 
x_6 = l_ML_ctxImplDefined___rarg(x_1, x_2, x_3, x_4, x_5);
lean_dec(x_5);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_implDefined___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; 
x_5 = lean_box(0);
x_6 = l_ML_ctxImplDefined___rarg(x_1, lean_box(0), x_3, x_5, x_4);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_implDefined(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_implDefined___rarg___boxed), 4, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_implDefined___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = l_ML_implDefined___rarg(x_1, x_2, x_3, x_4);
lean_dec(x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_totalImpl___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; 
lean_inc(x_3);
x_5 = l_ML_Pattern_negation___rarg(x_3);
lean_inc(x_5);
lean_inc(x_1);
x_6 = l_ML_implDefined___rarg(x_1, lean_box(0), x_5, x_4);
lean_inc(x_1);
x_7 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_7, 0, x_1);
lean_inc(x_5);
x_8 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_8, 0, x_7);
lean_ctor_set(x_8, 1, x_5);
lean_inc(x_5);
x_9 = l_ML_Proof_negImplIntro___rarg(x_5, x_8, x_6);
lean_inc(x_3);
x_10 = l_ML_total___rarg(x_1, x_3);
x_11 = l_ML_Pattern_negation___rarg(x_5);
lean_inc(x_3);
x_12 = l_ML_Proof_doubleNegElim___rarg(x_3);
x_13 = l_ML_Proof_syllogism___rarg(x_10, x_11, x_3, x_9, x_12);
return x_13;
}
}
LEAN_EXPORT lean_object* l_ML_totalImpl(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_totalImpl___rarg___boxed), 4, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_totalImpl___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = l_ML_totalImpl___rarg(x_1, x_2, x_3, x_4);
lean_dec(x_4);
return x_5;
}
}
lean_object* initialize_Init(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Pattern(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Proof(uint8_t builtin, lean_object*);
static bool _G_initialized = false;
LEAN_EXPORT lean_object* initialize_MatchingLogic_Definedness(uint8_t builtin, lean_object* w) {
lean_object * res;
if (_G_initialized) return lean_io_result_mk_ok(lean_box(0));
_G_initialized = true;
res = initialize_Init(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Pattern(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Proof(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
l_ML_instHasDefinedDefinedness = _init_l_ML_instHasDefinedDefinedness();
lean_mark_persistent(l_ML_instHasDefinedDefinedness);
l_ML_term_u2308___u2309___closed__1 = _init_l_ML_term_u2308___u2309___closed__1();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__1);
l_ML_term_u2308___u2309___closed__2 = _init_l_ML_term_u2308___u2309___closed__2();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__2);
l_ML_term_u2308___u2309___closed__3 = _init_l_ML_term_u2308___u2309___closed__3();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__3);
l_ML_term_u2308___u2309___closed__4 = _init_l_ML_term_u2308___u2309___closed__4();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__4);
l_ML_term_u2308___u2309___closed__5 = _init_l_ML_term_u2308___u2309___closed__5();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__5);
l_ML_term_u2308___u2309___closed__6 = _init_l_ML_term_u2308___u2309___closed__6();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__6);
l_ML_term_u2308___u2309___closed__7 = _init_l_ML_term_u2308___u2309___closed__7();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__7);
l_ML_term_u2308___u2309___closed__8 = _init_l_ML_term_u2308___u2309___closed__8();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__8);
l_ML_term_u2308___u2309___closed__9 = _init_l_ML_term_u2308___u2309___closed__9();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__9);
l_ML_term_u2308___u2309___closed__10 = _init_l_ML_term_u2308___u2309___closed__10();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__10);
l_ML_term_u2308___u2309___closed__11 = _init_l_ML_term_u2308___u2309___closed__11();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__11);
l_ML_term_u2308___u2309___closed__12 = _init_l_ML_term_u2308___u2309___closed__12();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__12);
l_ML_term_u2308___u2309___closed__13 = _init_l_ML_term_u2308___u2309___closed__13();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__13);
l_ML_term_u2308___u2309___closed__14 = _init_l_ML_term_u2308___u2309___closed__14();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__14);
l_ML_term_u2308___u2309___closed__15 = _init_l_ML_term_u2308___u2309___closed__15();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__15);
l_ML_term_u2308___u2309 = _init_l_ML_term_u2308___u2309();
lean_mark_persistent(l_ML_term_u2308___u2309);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__2 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__2);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__7 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__7);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__8 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__8);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__9 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__9();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__9);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__10 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__10();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__10);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__12 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__12();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__12);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__13 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__13();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__13);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__17 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__17();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__17);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__18 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__18();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__18);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__19 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__19();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__19);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__20 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__20();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__20);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__21 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__21();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__21);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__23 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__23();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__23);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__24 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__24();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__24);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__29 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__29();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__29);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__30 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__30();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__30);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__31 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__31();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__31);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32);
l_ML_term_u230a___u230b___closed__1 = _init_l_ML_term_u230a___u230b___closed__1();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__1);
l_ML_term_u230a___u230b___closed__2 = _init_l_ML_term_u230a___u230b___closed__2();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__2);
l_ML_term_u230a___u230b___closed__3 = _init_l_ML_term_u230a___u230b___closed__3();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__3);
l_ML_term_u230a___u230b___closed__4 = _init_l_ML_term_u230a___u230b___closed__4();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__4);
l_ML_term_u230a___u230b___closed__5 = _init_l_ML_term_u230a___u230b___closed__5();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__5);
l_ML_term_u230a___u230b___closed__6 = _init_l_ML_term_u230a___u230b___closed__6();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__6);
l_ML_term_u230a___u230b___closed__7 = _init_l_ML_term_u230a___u230b___closed__7();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__7);
l_ML_term_u230a___u230b___closed__8 = _init_l_ML_term_u230a___u230b___closed__8();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__8);
l_ML_term_u230a___u230b___closed__9 = _init_l_ML_term_u230a___u230b___closed__9();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__9);
l_ML_term_u230a___u230b = _init_l_ML_term_u230a___u230b();
lean_mark_persistent(l_ML_term_u230a___u230b);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__2 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__2);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__3 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__3);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__5 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__5);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__6 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__6);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__7 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__7);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__8 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__8);
l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___lambda__2___closed__1);
l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__1);
l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2 = _init_l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2);
l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__3 = _init_l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__3);
l_ML_term___u2261_____closed__1 = _init_l_ML_term___u2261_____closed__1();
lean_mark_persistent(l_ML_term___u2261_____closed__1);
l_ML_term___u2261_____closed__2 = _init_l_ML_term___u2261_____closed__2();
lean_mark_persistent(l_ML_term___u2261_____closed__2);
l_ML_term___u2261_____closed__3 = _init_l_ML_term___u2261_____closed__3();
lean_mark_persistent(l_ML_term___u2261_____closed__3);
l_ML_term___u2261_____closed__4 = _init_l_ML_term___u2261_____closed__4();
lean_mark_persistent(l_ML_term___u2261_____closed__4);
l_ML_term___u2261_____closed__5 = _init_l_ML_term___u2261_____closed__5();
lean_mark_persistent(l_ML_term___u2261_____closed__5);
l_ML_term___u2261_____closed__6 = _init_l_ML_term___u2261_____closed__6();
lean_mark_persistent(l_ML_term___u2261_____closed__6);
l_ML_term___u2261_____closed__7 = _init_l_ML_term___u2261_____closed__7();
lean_mark_persistent(l_ML_term___u2261_____closed__7);
l_ML_term___u2261__ = _init_l_ML_term___u2261__();
lean_mark_persistent(l_ML_term___u2261__);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__2 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__2);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__3 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__3);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__5 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__5);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__6 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__6);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__7 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__7);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__8 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__8);
l_ML_term___u2208_u2208_____closed__1 = _init_l_ML_term___u2208_u2208_____closed__1();
lean_mark_persistent(l_ML_term___u2208_u2208_____closed__1);
l_ML_term___u2208_u2208_____closed__2 = _init_l_ML_term___u2208_u2208_____closed__2();
lean_mark_persistent(l_ML_term___u2208_u2208_____closed__2);
l_ML_term___u2208_u2208_____closed__3 = _init_l_ML_term___u2208_u2208_____closed__3();
lean_mark_persistent(l_ML_term___u2208_u2208_____closed__3);
l_ML_term___u2208_u2208_____closed__4 = _init_l_ML_term___u2208_u2208_____closed__4();
lean_mark_persistent(l_ML_term___u2208_u2208_____closed__4);
l_ML_term___u2208_u2208_____closed__5 = _init_l_ML_term___u2208_u2208_____closed__5();
lean_mark_persistent(l_ML_term___u2208_u2208_____closed__5);
l_ML_term___u2208_u2208_____closed__6 = _init_l_ML_term___u2208_u2208_____closed__6();
lean_mark_persistent(l_ML_term___u2208_u2208_____closed__6);
l_ML_term___u2208_u2208__ = _init_l_ML_term___u2208_u2208__();
lean_mark_persistent(l_ML_term___u2208_u2208__);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__2 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__2);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__3 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__3);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__5 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__5);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__6 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__6);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__7 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__7);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__8 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__8);
l_ML_term___u2286_u2286_____closed__1 = _init_l_ML_term___u2286_u2286_____closed__1();
lean_mark_persistent(l_ML_term___u2286_u2286_____closed__1);
l_ML_term___u2286_u2286_____closed__2 = _init_l_ML_term___u2286_u2286_____closed__2();
lean_mark_persistent(l_ML_term___u2286_u2286_____closed__2);
l_ML_term___u2286_u2286_____closed__3 = _init_l_ML_term___u2286_u2286_____closed__3();
lean_mark_persistent(l_ML_term___u2286_u2286_____closed__3);
l_ML_term___u2286_u2286_____closed__4 = _init_l_ML_term___u2286_u2286_____closed__4();
lean_mark_persistent(l_ML_term___u2286_u2286_____closed__4);
l_ML_term___u2286_u2286_____closed__5 = _init_l_ML_term___u2286_u2286_____closed__5();
lean_mark_persistent(l_ML_term___u2286_u2286_____closed__5);
l_ML_term___u2286_u2286_____closed__6 = _init_l_ML_term___u2286_u2286_____closed__6();
lean_mark_persistent(l_ML_term___u2286_u2286_____closed__6);
l_ML_term___u2286_u2286__ = _init_l_ML_term___u2286_u2286__();
lean_mark_persistent(l_ML_term___u2286_u2286__);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__2 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__2);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__3 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__3);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__5 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__5);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__6 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__6);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__7 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__7);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__8 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__8);
l_ML_isApplicationOfDefinedSymbolToPattern___closed__1 = _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__1();
lean_mark_persistent(l_ML_isApplicationOfDefinedSymbolToPattern___closed__1);
l_ML_isApplicationOfDefinedSymbolToPattern___closed__2 = _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__2();
lean_mark_persistent(l_ML_isApplicationOfDefinedSymbolToPattern___closed__2);
l_ML_isApplicationOfDefinedSymbolToPattern___closed__3 = _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__3();
lean_mark_persistent(l_ML_isApplicationOfDefinedSymbolToPattern___closed__3);
l_ML_isApplicationOfDefinedSymbolToPattern___closed__4 = _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__4();
lean_mark_persistent(l_ML_isApplicationOfDefinedSymbolToPattern___closed__4);
l_ML_isApplicationOfDefinedSymbolToPattern___closed__5 = _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__5();
lean_mark_persistent(l_ML_isApplicationOfDefinedSymbolToPattern___closed__5);
l_ML_isApplicationOfDefinedSymbolToPattern___closed__6 = _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__6();
lean_mark_persistent(l_ML_isApplicationOfDefinedSymbolToPattern___closed__6);
l_ML_delabDefined___closed__1 = _init_l_ML_delabDefined___closed__1();
lean_mark_persistent(l_ML_delabDefined___closed__1);
l_ML_delabDefined___closed__2 = _init_l_ML_delabDefined___closed__2();
lean_mark_persistent(l_ML_delabDefined___closed__2);
l_ML_delabDefined___closed__3 = _init_l_ML_delabDefined___closed__3();
lean_mark_persistent(l_ML_delabDefined___closed__3);
l_ML_delabDefined___closed__4 = _init_l_ML_delabDefined___closed__4();
lean_mark_persistent(l_ML_delabDefined___closed__4);
l_ML_delabDefined___closed__5 = _init_l_ML_delabDefined___closed__5();
lean_mark_persistent(l_ML_delabDefined___closed__5);
l_ML_delabDefined___closed__6 = _init_l_ML_delabDefined___closed__6();
lean_mark_persistent(l_ML_delabDefined___closed__6);
l_ML_delabDefined___closed__7 = _init_l_ML_delabDefined___closed__7();
lean_mark_persistent(l_ML_delabDefined___closed__7);
return lean_io_result_mk_ok(lean_box(0));
}
#ifdef __cplusplus
}
#endif
