// Lean compiler output
// Module: MatchingLogic.Util
// Imports: Init Mathlib
#include <lean/lean.h>
#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wunused-label"
#elif defined(__GNUC__) && !defined(__CLANG__)
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-label"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#endif
#ifdef __cplusplus
extern "C" {
#endif
LEAN_EXPORT lean_object* l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1(lean_object*, lean_object*, lean_object*);
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__20;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__25;
lean_object* lean_mk_empty_array_with_capacity(lean_object*);
static lean_object* l_tacticLs___closed__4;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__10;
static lean_object* l_precArrow__precedence___closed__2;
static lean_object* l_commandAssert_____closed__6;
static lean_object* l_commandAssert_____closed__7;
static lean_object* l_tacticLs___closed__1;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__24;
LEAN_EXPORT lean_object* l_shiftSequence(lean_object*);
static lean_object* l_commandAssert_____closed__3;
static lean_object* l_commandAssert_____closed__1;
static lean_object* l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__3;
static lean_object* l_commandAssert_____closed__10;
static lean_object* l_commandAssert_____closed__5;
uint8_t l_Lean_Syntax_isOfKind(lean_object*, lean_object*);
static lean_object* l_commandAssert_____closed__4;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__18;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__22;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1;
LEAN_EXPORT lean_object* l_shiftSequence___rarg___boxed(lean_object*, lean_object*, lean_object*);
static lean_object* l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__2;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__9;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__28;
lean_object* l_Lean_SourceInfo_fromRef(lean_object*, uint8_t);
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__12;
lean_object* l_Lean_Syntax_node6(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__8;
static lean_object* l_precArrow__precedence___closed__4;
static lean_object* l_precArrow__precedence___closed__5;
static lean_object* l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__3;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__15;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__19;
lean_object* l_Lean_Syntax_node3(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_tacticLs___closed__5;
static lean_object* l_precArrow__precedence___closed__3;
LEAN_EXPORT lean_object* l_shiftSequence___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__11;
static lean_object* l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__5;
static lean_object* l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__2;
lean_object* l_Lean_Name_str___override(lean_object*, lean_object*);
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__13;
LEAN_EXPORT lean_object* l_precArrow__precedence;
lean_object* l_Lean_Syntax_node2(lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Syntax_getArg(lean_object*, lean_object*);
static lean_object* l_commandAssert_____closed__9;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__3;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__29;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__17;
static lean_object* l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__1;
LEAN_EXPORT lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1(lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Syntax_node4(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__6;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__27;
static lean_object* l_precArrow__precedence___closed__1;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__7;
static lean_object* l_tacticLs___closed__2;
LEAN_EXPORT lean_object* l___aux__MatchingLogic__Util______macroRules__tacticLs__1(lean_object*, lean_object*, lean_object*);
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__30;
uint8_t lean_nat_dec_eq(lean_object*, lean_object*);
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__14;
LEAN_EXPORT lean_object* l_commandAssert__;
LEAN_EXPORT lean_object* l_tacticLs;
lean_object* l_Lean_Syntax_node1(lean_object*, lean_object*, lean_object*);
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__4;
lean_object* lean_nat_sub(lean_object*, lean_object*);
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__26;
static lean_object* l_tacticLs___closed__3;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__21;
static lean_object* l_commandAssert_____closed__11;
static lean_object* l_commandAssert_____closed__8;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__5;
static lean_object* l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__4;
lean_object* l_Lean_Name_mkStr4(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_Prod_fold(lean_object*);
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__16;
LEAN_EXPORT lean_object* l_Prod_fold___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_commandAssert_____closed__2;
static lean_object* l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__1;
static lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__23;
static lean_object* _init_l_commandAssert_____closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("commandAssert_", 14);
return x_1;
}
}
static lean_object* _init_l_commandAssert_____closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_commandAssert_____closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_commandAssert_____closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("andthen", 7);
return x_1;
}
}
static lean_object* _init_l_commandAssert_____closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_commandAssert_____closed__3;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_commandAssert_____closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("assert", 6);
return x_1;
}
}
static lean_object* _init_l_commandAssert_____closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_commandAssert_____closed__5;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_commandAssert_____closed__7() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("optDeclSig", 10);
return x_1;
}
}
static lean_object* _init_l_commandAssert_____closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_commandAssert_____closed__7;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_commandAssert_____closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_commandAssert_____closed__8;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_commandAssert_____closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_commandAssert_____closed__4;
x_2 = l_commandAssert_____closed__6;
x_3 = l_commandAssert_____closed__9;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_commandAssert_____closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_commandAssert_____closed__2;
x_2 = lean_unsigned_to_nat(1022u);
x_3 = l_commandAssert_____closed__10;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_commandAssert__() {
_start:
{
lean_object* x_1; 
x_1 = l_commandAssert_____closed__11;
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Lean", 4);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Parser", 6);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Command", 7);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("declaration", 11);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1;
x_2 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2;
x_3 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__3;
x_4 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__4;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("declModifiers", 13);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1;
x_2 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2;
x_3 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__3;
x_4 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__6;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__8() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("null", 4);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__8;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_unsigned_to_nat(0u);
x_2 = lean_mk_empty_array_with_capacity(x_1);
return x_2;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__11() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("example", 7);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__12() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1;
x_2 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2;
x_3 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__3;
x_4 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__11;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__13() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("declValSimple", 13);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__14() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1;
x_2 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2;
x_3 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__3;
x_4 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__13;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__15() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes(":=", 2);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__16() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Term", 4);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__17() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("byTactic", 8);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__18() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1;
x_2 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2;
x_3 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__16;
x_4 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__17;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__19() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("by", 2);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__20() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Tactic", 6);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__21() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticSeq", 9);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__22() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1;
x_2 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2;
x_3 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__20;
x_4 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__21;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__23() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticSeq1Indented", 18);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__24() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1;
x_2 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2;
x_3 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__20;
x_4 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__23;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__25() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticRfl", 9);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__26() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1;
x_2 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2;
x_3 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__20;
x_4 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__25;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__27() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("rfl", 3);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__28() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Termination", 11);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__29() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("suffix", 6);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__30() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1;
x_2 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2;
x_3 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__28;
x_4 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__29;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l___aux__MatchingLogic__Util______macroRules__commandAssert____1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_commandAssert_____closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; 
x_8 = lean_unsigned_to_nat(1u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
lean_dec(x_1);
x_10 = lean_ctor_get(x_2, 5);
lean_inc(x_10);
lean_dec(x_2);
x_11 = 0;
x_12 = l_Lean_SourceInfo_fromRef(x_10, x_11);
x_13 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__9;
x_14 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__10;
lean_inc(x_12);
x_15 = lean_alloc_ctor(1, 3, 0);
lean_ctor_set(x_15, 0, x_12);
lean_ctor_set(x_15, 1, x_13);
lean_ctor_set(x_15, 2, x_14);
x_16 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__7;
lean_inc_n(x_15, 6);
lean_inc(x_12);
x_17 = l_Lean_Syntax_node6(x_12, x_16, x_15, x_15, x_15, x_15, x_15, x_15);
x_18 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__11;
lean_inc(x_12);
x_19 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_19, 0, x_12);
lean_ctor_set(x_19, 1, x_18);
x_20 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__15;
lean_inc(x_12);
x_21 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_21, 0, x_12);
lean_ctor_set(x_21, 1, x_20);
x_22 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__19;
lean_inc(x_12);
x_23 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_23, 0, x_12);
lean_ctor_set(x_23, 1, x_22);
x_24 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__27;
lean_inc(x_12);
x_25 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_25, 0, x_12);
lean_ctor_set(x_25, 1, x_24);
x_26 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__26;
lean_inc(x_12);
x_27 = l_Lean_Syntax_node1(x_12, x_26, x_25);
lean_inc(x_12);
x_28 = l_Lean_Syntax_node1(x_12, x_13, x_27);
x_29 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__24;
lean_inc(x_12);
x_30 = l_Lean_Syntax_node1(x_12, x_29, x_28);
x_31 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__22;
lean_inc(x_12);
x_32 = l_Lean_Syntax_node1(x_12, x_31, x_30);
x_33 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__18;
lean_inc(x_12);
x_34 = l_Lean_Syntax_node2(x_12, x_33, x_23, x_32);
x_35 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__30;
lean_inc_n(x_15, 2);
lean_inc(x_12);
x_36 = l_Lean_Syntax_node2(x_12, x_35, x_15, x_15);
x_37 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__14;
lean_inc(x_12);
x_38 = l_Lean_Syntax_node4(x_12, x_37, x_21, x_34, x_36, x_15);
x_39 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__12;
lean_inc(x_12);
x_40 = l_Lean_Syntax_node3(x_12, x_39, x_19, x_9, x_38);
x_41 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__5;
x_42 = l_Lean_Syntax_node2(x_12, x_41, x_17, x_40);
x_43 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_43, 0, x_42);
lean_ctor_set(x_43, 1, x_3);
return x_43;
}
}
}
static lean_object* _init_l_tacticLs___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticLs", 8);
return x_1;
}
}
static lean_object* _init_l_tacticLs___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_tacticLs___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_tacticLs___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("ls", 2);
return x_1;
}
}
static lean_object* _init_l_tacticLs___closed__4() {
_start:
{
lean_object* x_1; uint8_t x_2; lean_object* x_3; 
x_1 = l_tacticLs___closed__3;
x_2 = 0;
x_3 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set_uint8(x_3, sizeof(void*)*1, x_2);
return x_3;
}
}
static lean_object* _init_l_tacticLs___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_tacticLs___closed__2;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = l_tacticLs___closed__4;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_tacticLs() {
_start:
{
lean_object* x_1; 
x_1 = l_tacticLs___closed__5;
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("Mathlib", 7);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("LibrarySearch", 13);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("tacticLibrary_search", 20);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__1;
x_2 = l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__20;
x_3 = l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__2;
x_4 = l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__3;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("library_search", 14);
return x_1;
}
}
LEAN_EXPORT lean_object* l___aux__MatchingLogic__Util______macroRules__tacticLs__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_tacticLs___closed__2;
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; 
x_8 = lean_ctor_get(x_2, 5);
lean_inc(x_8);
lean_dec(x_2);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__5;
lean_inc(x_10);
x_12 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_11);
x_13 = l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__4;
x_14 = l_Lean_Syntax_node1(x_10, x_13, x_12);
x_15 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_15, 0, x_14);
lean_ctor_set(x_15, 1, x_3);
return x_15;
}
}
}
LEAN_EXPORT lean_object* l_Prod_fold___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; uint8_t x_6; 
x_4 = lean_ctor_get(x_2, 0);
lean_inc(x_4);
x_5 = lean_ctor_get(x_2, 1);
lean_inc(x_5);
lean_dec(x_2);
x_6 = !lean_is_exclusive(x_3);
if (x_6 == 0)
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; 
x_7 = lean_ctor_get(x_3, 0);
x_8 = lean_ctor_get(x_3, 1);
lean_inc(x_1);
x_9 = lean_apply_2(x_1, x_4, x_7);
x_10 = lean_apply_2(x_1, x_5, x_8);
lean_ctor_set(x_3, 1, x_10);
lean_ctor_set(x_3, 0, x_9);
return x_3;
}
else
{
lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; 
x_11 = lean_ctor_get(x_3, 0);
x_12 = lean_ctor_get(x_3, 1);
lean_inc(x_12);
lean_inc(x_11);
lean_dec(x_3);
lean_inc(x_1);
x_13 = lean_apply_2(x_1, x_4, x_11);
x_14 = lean_apply_2(x_1, x_5, x_12);
x_15 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_15, 0, x_13);
lean_ctor_set(x_15, 1, x_14);
return x_15;
}
}
}
LEAN_EXPORT lean_object* l_Prod_fold(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_Prod_fold___rarg), 3, 0);
return x_2;
}
}
static lean_object* _init_l_precArrow__precedence___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("precArrow_precedence", 20);
return x_1;
}
}
static lean_object* _init_l_precArrow__precedence___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_precArrow__precedence___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_precArrow__precedence___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("arrow_precedence", 16);
return x_1;
}
}
static lean_object* _init_l_precArrow__precedence___closed__4() {
_start:
{
lean_object* x_1; uint8_t x_2; lean_object* x_3; 
x_1 = l_precArrow__precedence___closed__3;
x_2 = 0;
x_3 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set_uint8(x_3, sizeof(void*)*1, x_2);
return x_3;
}
}
static lean_object* _init_l_precArrow__precedence___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_precArrow__precedence___closed__2;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = l_precArrow__precedence___closed__4;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_precArrow__precedence() {
_start:
{
lean_object* x_1; 
x_1 = l_precArrow__precedence___closed__5;
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("num", 3);
return x_1;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_from_bytes("24", 2);
return x_1;
}
}
LEAN_EXPORT lean_object* l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_precArrow__precedence___closed__2;
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; uint8_t x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; 
x_8 = lean_ctor_get(x_2, 5);
lean_inc(x_8);
lean_dec(x_2);
x_9 = 0;
x_10 = l_Lean_SourceInfo_fromRef(x_8, x_9);
x_11 = l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__3;
lean_inc(x_10);
x_12 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_12, 0, x_10);
lean_ctor_set(x_12, 1, x_11);
x_13 = l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__2;
x_14 = l_Lean_Syntax_node1(x_10, x_13, x_12);
x_15 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_15, 0, x_14);
lean_ctor_set(x_15, 1, x_3);
return x_15;
}
}
}
LEAN_EXPORT lean_object* l_shiftSequence___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = lean_unsigned_to_nat(0u);
x_5 = lean_nat_dec_eq(x_3, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; 
x_6 = lean_unsigned_to_nat(1u);
x_7 = lean_nat_sub(x_3, x_6);
x_8 = lean_apply_1(x_2, x_7);
return x_8;
}
else
{
lean_dec(x_2);
lean_inc(x_1);
return x_1;
}
}
}
LEAN_EXPORT lean_object* l_shiftSequence(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_shiftSequence___rarg___boxed), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_shiftSequence___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_shiftSequence___rarg(x_1, x_2, x_3);
lean_dec(x_3);
lean_dec(x_1);
return x_4;
}
}
lean_object* initialize_Init(uint8_t builtin, lean_object*);
lean_object* initialize_Mathlib(uint8_t builtin, lean_object*);
static bool _G_initialized = false;
LEAN_EXPORT lean_object* initialize_MatchingLogic_Util(uint8_t builtin, lean_object* w) {
lean_object * res;
if (_G_initialized) return lean_io_result_mk_ok(lean_box(0));
_G_initialized = true;
res = initialize_Init(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_Mathlib(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
l_commandAssert_____closed__1 = _init_l_commandAssert_____closed__1();
lean_mark_persistent(l_commandAssert_____closed__1);
l_commandAssert_____closed__2 = _init_l_commandAssert_____closed__2();
lean_mark_persistent(l_commandAssert_____closed__2);
l_commandAssert_____closed__3 = _init_l_commandAssert_____closed__3();
lean_mark_persistent(l_commandAssert_____closed__3);
l_commandAssert_____closed__4 = _init_l_commandAssert_____closed__4();
lean_mark_persistent(l_commandAssert_____closed__4);
l_commandAssert_____closed__5 = _init_l_commandAssert_____closed__5();
lean_mark_persistent(l_commandAssert_____closed__5);
l_commandAssert_____closed__6 = _init_l_commandAssert_____closed__6();
lean_mark_persistent(l_commandAssert_____closed__6);
l_commandAssert_____closed__7 = _init_l_commandAssert_____closed__7();
lean_mark_persistent(l_commandAssert_____closed__7);
l_commandAssert_____closed__8 = _init_l_commandAssert_____closed__8();
lean_mark_persistent(l_commandAssert_____closed__8);
l_commandAssert_____closed__9 = _init_l_commandAssert_____closed__9();
lean_mark_persistent(l_commandAssert_____closed__9);
l_commandAssert_____closed__10 = _init_l_commandAssert_____closed__10();
lean_mark_persistent(l_commandAssert_____closed__10);
l_commandAssert_____closed__11 = _init_l_commandAssert_____closed__11();
lean_mark_persistent(l_commandAssert_____closed__11);
l_commandAssert__ = _init_l_commandAssert__();
lean_mark_persistent(l_commandAssert__);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__1);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__2);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__3 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__3();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__3);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__4 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__4();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__4);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__5 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__5();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__5);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__6 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__6();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__6);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__7 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__7();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__7);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__8 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__8();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__8);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__9 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__9();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__9);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__10 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__10();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__10);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__11 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__11();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__11);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__12 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__12();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__12);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__13 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__13();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__13);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__14 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__14();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__14);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__15 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__15();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__15);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__16 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__16();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__16);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__17 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__17();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__17);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__18 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__18();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__18);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__19 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__19();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__19);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__20 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__20();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__20);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__21 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__21();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__21);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__22 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__22();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__22);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__23 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__23();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__23);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__24 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__24();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__24);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__25 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__25();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__25);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__26 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__26();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__26);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__27 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__27();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__27);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__28 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__28();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__28);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__29 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__29();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__29);
l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__30 = _init_l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__30();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__commandAssert____1___closed__30);
l_tacticLs___closed__1 = _init_l_tacticLs___closed__1();
lean_mark_persistent(l_tacticLs___closed__1);
l_tacticLs___closed__2 = _init_l_tacticLs___closed__2();
lean_mark_persistent(l_tacticLs___closed__2);
l_tacticLs___closed__3 = _init_l_tacticLs___closed__3();
lean_mark_persistent(l_tacticLs___closed__3);
l_tacticLs___closed__4 = _init_l_tacticLs___closed__4();
lean_mark_persistent(l_tacticLs___closed__4);
l_tacticLs___closed__5 = _init_l_tacticLs___closed__5();
lean_mark_persistent(l_tacticLs___closed__5);
l_tacticLs = _init_l_tacticLs();
lean_mark_persistent(l_tacticLs);
l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__1 = _init_l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__1();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__1);
l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__2 = _init_l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__2();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__2);
l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__3 = _init_l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__3();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__3);
l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__4 = _init_l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__4();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__4);
l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__5 = _init_l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__5();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__tacticLs__1___closed__5);
l_precArrow__precedence___closed__1 = _init_l_precArrow__precedence___closed__1();
lean_mark_persistent(l_precArrow__precedence___closed__1);
l_precArrow__precedence___closed__2 = _init_l_precArrow__precedence___closed__2();
lean_mark_persistent(l_precArrow__precedence___closed__2);
l_precArrow__precedence___closed__3 = _init_l_precArrow__precedence___closed__3();
lean_mark_persistent(l_precArrow__precedence___closed__3);
l_precArrow__precedence___closed__4 = _init_l_precArrow__precedence___closed__4();
lean_mark_persistent(l_precArrow__precedence___closed__4);
l_precArrow__precedence___closed__5 = _init_l_precArrow__precedence___closed__5();
lean_mark_persistent(l_precArrow__precedence___closed__5);
l_precArrow__precedence = _init_l_precArrow__precedence();
lean_mark_persistent(l_precArrow__precedence);
l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__1 = _init_l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__1();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__1);
l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__2 = _init_l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__2();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__2);
l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__3 = _init_l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__3();
lean_mark_persistent(l___aux__MatchingLogic__Util______macroRules__precArrow__precedence__1___closed__3);
return lean_io_result_mk_ok(lean_box(0));
}
#ifdef __cplusplus
}
#endif
